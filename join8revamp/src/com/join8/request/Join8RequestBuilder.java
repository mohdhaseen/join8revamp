package com.join8.request;

import java.util.HashMap;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.join8.HomeScreen;
import com.join8.model.OrderData;
import com.join8.model.RequestAddShop;
import com.join8.model.RequestRegisteration;
import com.join8.model.RequestResetPassword;
import com.join8.model.ShopImageData;
import com.join8.utils.ConstantData;

public class Join8RequestBuilder implements PARAMS {

	public static String XML_STRING = "";

	public static final String METHOD_LOGIN = "J3400GetTokenV1";
	public static final String METHOD_REGISTRATION = "J2000RegisterUserV3";
	public static final String METHOD_SIGNIN = "J2000UserLoginNewV1";
	public static final String METHOD_ISEMAIL = "J3100IsEmailAlreadyExistV1";
	public static final String METHOD_ISPHONE = "IsPhoneAlreadyExist";
	public static final String METHOD_GETCITY = "J3000GetCityListV1";
	public static final String METHOD_SEARCH = "J5000SearchMenuItemsV3";
	public static final String METHOD_CUISIN = "GetAllCuisineTypes";
	public static final String METHOD_DISHTYPES = "GetAllDishTypes";
	public static final String METHOD_PROMOTIONS = "J8200GetAllPromotionsV2";
	public static final String METHOD_FAVORITEPROMOTIONS = "J8000GetWatchListPromotionV2";
	public static final String METHOD_RESTAURANT_MENU = "J9100GetMenuByRestaurantIDV2";
	public static final String METHOD_RESTAURANT_MENU_ITEMS = "J10100GetMenuItemByMenuIDV3";
	public static final String METHOD_RESTAURANT_SERVICE_HOURS = "J11000GetCompanyHoursV2";
	public static final String METHOD_HOLIDAY_LIST = "J11100GetCompanyHolidayListV1";
	public static final String METHOD_SINGOUT = "J2100SignOutV1";
	public static final String METHOD_IS_RESTAURANT_EXIST_IN_WATCH = "J3500IsRestaurantWatchListAlreadyExistsV1";
	public static final String METHOD_ADD_RESTAURANT_TO_WATCH = "J12000AddToWatchListV1";
	public static final String METHOD_REMOVE_RESTAURANT_FROM_WATCH = "J12200DeleteFromWatchListV1";
	public static final String METHOD_GET_PROFILES_OF_USER = "J4200GetProfilesOfUserV1";
	public static final String METHOD_USER_ORDERLIST = "J6100GetOrdersOfUserV2";
	public static final String METHOD_CREATE_UPDATE_DRAFT_ORDER = "J6000CreateUpdateDraftOrderV2";
	public static final String METHOD_GET_PARTICULAR_ORDER = "J6200GetParticularOrderV3";
	public static final String METHOD_DELETE_ORDER = "DeleteDraftOrder";
	public static final String METHOD_STAFF_ORDERLIST = "J6300GetOrderListForOwnerV1";
	public static final String METHOD_CHANGE_ORDER_STATUS = "J6400ChangeOrderStatusV1";
	public static final String METHOD_USER_BOOKINGLIST = "GetBookingsOfUser";
	public static final String METHOD_USER_BOOKING_CANCEL = "J7400CancelBookingV1";
	public static final String METHOD_STAFF_BOOKINGLIST = "J7500GetBookingListForOwnerV1";
	public static final String METHOD_STAFF_BOOKING_DETAILS = "J7200GetParticularBookingOfUserV2";
	public static final String METHOD_ACCEPT_REJECT_BOOKING = "J7600ChangeBookingStatusV1";
	public static final String METHOD_GET_PARTICULAR_BOOKING_OF_USER = "J7200GetParticularBookingOfUserV2";
	public static final String METHOD_ADD_BOOK_TABLE = "J7000AddBookTableV1";
	public static final String METHOD_UPDATE_BOOK_TABLE = "J7100UpdateBookingV1";
	public static final String METHOD_MY_FAVORITES_LIST = "J12100GetRestaurantWatchListV2";
	public static final String METHOD_SEND_FEEDBACK = "J12000ContactUsV1";
	public static final String METHOD_DELETE_USER_PROFILE = "J4400DeleteUserProfileV1";
	public static final String METHOD_ADD_USER_PROFILE = "J2000RegisterUserV1";
	public static final String METHOD_UPDATE_USER_PROFILE = "J4100UpdateProfileV1";
	public static final String METHOD_IS_USER_PROFILE_EXIST = "J3200IsProfileNameExistsV1";
	// new
	public static final String METHOD_GET_TABLE_TYPE = "J7600getAllBookingByCompanyIdV1";
	public static final String METHOD_GET_TIME_SLOT = "J7700getTimeSlotByTableId";
	public static final String METHOD_BOOK_TABLE = "J7800addBookTableWithFreeAndPreTimeSlot";
	public static final String METHOD_GET_FOOD_OPTION = "J10100getFoodOptionByMenuItemId";
	public static final String METHOD_GET_PAYMENT_DETAILS_OF_CITY = "J3000GetPaymentDetailsOfCityV1";

	public static final String METHOD_GET_CUISINE_BY_CITY = "J5100GetCuisineByCityV3";
	public static final String METHOD_GET_REST_BY_CUISINE = "J5200GetRestaurantsByCuisineV2";
	public static final String METHOD_GET_AREA = "J8300GetAreaDetailsV1";
	public static final String METHOD_GET_VERIFICATION_CODE = "J2200GetVerificationCode";

	// Login Preferences
	public static final String Facebook = "3";
	public static final String Google = "4";
	public static final String Email = "1";
	public static final String Phone = "6";

	public static HashMap<String, String> getLoginRequest(String userid, String password) {

		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_USERNAME, userid);
		parameters.put(TAG_PASSWORD, password);
		return parameters;
	}

	public static HashMap<String, String> getRegistrationRequest(String token, String password, String langauge, String firstname, String lastname, String emailid, String phoneNo, String countrycode, String province, String phoneCode, String verificationCode) {

		JSONObject reg_request = new JSONObject();
		try {
			reg_request.put(TAG_RECORDSTATUS, "99");
			reg_request.put(TAG_LOGINPASSWORD, password);
			reg_request.put(TAG_PROVINCE, province);
			reg_request.put(TAG_LANGUAGE, langauge);
			reg_request.put(TAG_PROFILE, "true");
			reg_request.put(TAG_CITY, countrycode);
			reg_request.put(TAG_FIRSTNAME, firstname);
			reg_request.put(TAG_LASTNAME, lastname);
			reg_request.put(TAG_MOBILENO, phoneCode + "|" + phoneNo);
			reg_request.put(TAG_LOGINPREFS, Phone);
			reg_request.put(TAG_EMAIL, phoneCode + "-" + phoneNo);
			reg_request.put(TAG_ACTIVATION_CODE, verificationCode);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();
		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getVerificationCodeRequest(String token, String phoneCode, String phoneNo, String countrycode, String langauge) {

		JSONObject reg_request = new JSONObject();
		try {
			reg_request.put(TAG_MOBILENO, phoneCode + "|" + phoneNo);
			reg_request.put(TAG_CITY, countrycode);
			reg_request.put(TAG_USER_PREF_LANG, langauge);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();
		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getSigninRequest(String token, String email, String password, String loginpref, String accessToken, String city) {

		JSONObject reg_request = new JSONObject();
		try {
			if (loginpref.equals(Facebook) || loginpref.equals(Google)) {
				reg_request.put(TAG_ACCESSTOKEN, accessToken);
			}
			reg_request.put(TAG_LOGINPASSWORD, password);
			reg_request.put(TAG_LOGINWITH, "10");
			reg_request.put(TAG_LOGINTYPE, loginpref);
			reg_request.put(TAG_CITY, city);
			reg_request.put(TAG_TOKEN, ConstantData.RegistrationID);
			reg_request.put(TAG_TYPE, 20);

			if (loginpref.equals(Phone)) {
				reg_request.put(TAG_EMAIL, accessToken + "-" + email);
			} else {
				reg_request.put(TAG_EMAIL, email);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> isEmailExist(String token, String email) {
		Base64 b = new Base64();
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_EMAIL, new String(b.encode(email.toString().getBytes())));
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);

		return parameters;
	}

	public static HashMap<String, String> isPhoneExist(String token, String phone, String countrycode) {
		Base64 b = new Base64();
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_CITY, new String(b.encode(countrycode.getBytes())));
		parameters.put("Mobile", new String(b.encode(phone.toString().getBytes())));
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("mobile no", phone.toString());
		return parameters;
	}

	public static HashMap<String, String> getCityList(String token) {
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		return parameters;
	}

	public static HashMap<String, String> getSearchRequest(String token, String CityId, String CompanyName, String Count, String CuisineType, String ItemName, String NoOfDays, int StartIndex, int areaId) {

		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_CITYID, CityId);
			reg_request.put(TAG_COMPANYNAME, CompanyName);
			reg_request.put(TAG_COUNT, Count);
			reg_request.put(TAG_CUSINTYPE, CuisineType);
			reg_request.put(TAG_ITEMNAME, ItemName);
			reg_request.put(TAG_NOOFDAYS, NoOfDays);
			reg_request.put(TAG_STARTINDEX, StartIndex);
			reg_request.put(TAG_AREA_ID, areaId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getPromotionsRequest(String token, String CityId, String CompanyName, String Count, String CuisineType, String NoOfDays, String StartIndex, String partitionKey) {

		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_CITYID, CityId);
			reg_request.put(TAG_COMPANYNAME, CompanyName);
			reg_request.put(TAG_COUNT, Count);
			reg_request.put(TAG_CUSINTYPE, CuisineType);
			reg_request.put(TAG_SEARCHTEXT, "0");
			reg_request.put(TAG_NOOFDAYS, NoOfDays);
			reg_request.put(TAG_STARTINDEX, StartIndex);

			reg_request.put(TAG_PARTITION_KEY, partitionKey);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getRestaurantMenuRequest(String token, String companyId) {

		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_COMPANY_ID, companyId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getRestaurantMenuItemsRequest(String token, String menuId) {

		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_MENU_ID, menuId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getSignoutRequest(String token, String email) {

		JSONObject reg_request = new JSONObject();
		try {
			reg_request.put(TAG_EMAIL, email);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getMyFavoriteListRequest(String token, String userId) {

		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_USER_ID, userId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> isRestaurantExistInWatchRequest(String token, String companyId, String userId) {

		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_COMPANY_ID, companyId);
			reg_request.put(TAG_USER_ID, userId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> addRestaurantToWatchRequest(String token, String companyId, String userId, String watchlistId) {

		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_COMPANY_ID, companyId);
			reg_request.put(TAG_USER_ID, userId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> removeRestaurantFromWatchRequest(String token, String watchlistId) {

		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_WATCHLIST_ID, watchlistId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getRestaurantServiceHoursRequest(String token, String companyId) {

		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_COMPANY_ID, companyId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getProfileOfUser(String token, String partitionKey) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_PARTITION_KEY, partitionKey);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getOrderListRequest(String token, String partitionKey, String searchText, int startIndex, int count, String startDate, String endDate) {

		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_PARTITION_KEY, partitionKey);
			reg_request.put(TAG_SEARCHTEXT, searchText);
			reg_request.put(TAG_STARTINDEX, startIndex);
			reg_request.put(TAG_COUNT, count);
			reg_request.put(TAG_START_DATE, startDate);
			reg_request.put(TAG_END_DATE, endDate);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> createUpdateDraftOrder(String token, String UserId, int OrderStatus, String FirstName, String LastName, int OrderId, String UserProfile, String OrderDeadline, int CompanyId, boolean IsOrderPickUpByUser, boolean IsOrderShared,
			boolean DeletePreviousDetails, String MenuItemData, String listEmails, String MobileNumber, OrderData orderData, String Remarks) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_USER_ID, UserId);
			reg_request.put(TAG_ORDER_STATUS, OrderStatus);
			reg_request.put(TAG_FIRSTNAME, FirstName);
			reg_request.put(TAG_LASTNAME, LastName);
			reg_request.put(TAG_ORDER_ID, OrderId);
			reg_request.put(TAG_USER_PROFILE, UserProfile);
			reg_request.put(TAG_ORDER_DEADLINE, OrderDeadline);
			reg_request.put(TAG_COMPANY_ID, CompanyId);
			reg_request.put(TAG_IS_ORDER_PICKUPBYUSER, IsOrderPickUpByUser);
			reg_request.put(TAG_IS_ORDER_SHARED, IsOrderShared);
			reg_request.put(TAG_DELETE_PREVIOUS_DETAILS, DeletePreviousDetails);
			reg_request.put(TAG_MENUDATA, MenuItemData);
			reg_request.put(TAG_LISTEMAILS, listEmails);
			reg_request.put(TAG_MOBILENO, MobileNumber);
			reg_request.put(TAG_REMARKS, Remarks);
			if (orderData == null) {
				reg_request.put(TAG_IS_ORDER_CENCELLED, false);
				reg_request.put(TAG_IS_ORDER_DELIVERED, false);
				reg_request.put(TAG_IS_TEMPLATE, false);
			} else {
				reg_request.put(TAG_IS_ORDER_CENCELLED, orderData.isIsOrderCancelled());
				reg_request.put(TAG_IS_ORDER_DELIVERED, orderData.isIsOrderDelivered());
				reg_request.put(TAG_IS_TEMPLATE, orderData.isIsTemplate());
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> placeOrderRequest(String token, String UserId, int OrderStatus, String FirstName, String LastName, String OrderId, String UserProfile, String OrderDeadline, int CompanyId, boolean IsOrderPickUpByUser, boolean IsOrderShared,
			boolean DeletePreviousDetails, JSONArray MenuData, String listEmails, String MobileNumber, OrderData orderData, String Remarks, String Currency, String HandleByUserId, int OrderPayOption, String OrderDatetime, String LastUpdatedTime, String OrderEstimatedPickUpTime,
			double TotalAmount, int LastInsertedValue, String OrderName, String DeliverAddress) {

		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_MOBILENO, MobileNumber);
			reg_request.put(TAG_DELETE_PREVIOUS_DETAILS, DeletePreviousDetails);
			reg_request.put(TAG_MENUDATA, MenuData);
			reg_request.put(TAG_USER_ID, UserId);
			reg_request.put(TAG_COMPANY_ID, CompanyId);
			reg_request.put(TAG_USER_PROFILE, UserProfile);
			reg_request.put(TAG_FIRSTNAME, FirstName);
			reg_request.put(TAG_CURRENCY, Currency);
			reg_request.put(TAG_HANDLE_BY_USER_ID, HandleByUserId);
			reg_request.put(TAG_ORDER_PAY_OPTION, OrderPayOption);
			reg_request.put(TAG_ORDER_DATETIME, OrderDatetime);
			reg_request.put(TAG_UPDATED_TIME, LastUpdatedTime);
			reg_request.put(TAG_ORDER_ID, OrderId);
			reg_request.put(TAG_LASTNAME, LastName);
			reg_request.put(TAG_LISTEMAILS, listEmails);
			reg_request.put(TAG_ORDER_SHARED_WITH, "");
			reg_request.put(TAG_ORDER_DEADLINE, OrderDeadline);
			reg_request.put(TAG_ORDER_PICKUP_TIME, OrderEstimatedPickUpTime);
			reg_request.put(TAG_TOTAL_AMOUNT, TotalAmount);
			reg_request.put(TAG_IS_ORDER_PICKUPBYUSER, IsOrderPickUpByUser);
			reg_request.put(TAG_LAST_INSERTED_VALUE, LastInsertedValue);
			reg_request.put(TAG_IS_ORDER_SHARED, IsOrderShared);
			reg_request.put(TAG_ORDER_STATUS, OrderStatus);
			reg_request.put(TAG_ORDER_NAME, OrderName);
			reg_request.put(TAG_REMARKS, Remarks);
			reg_request.put(TAG_DELIVER_ADDRESS, DeliverAddress);

			if (orderData == null) {
				reg_request.put(TAG_IS_ORDER_CENCELLED, false);
				reg_request.put(TAG_IS_ORDER_DELIVERED, false);
				reg_request.put(TAG_IS_TEMPLATE, false);
			} else {
				reg_request.put(TAG_IS_ORDER_CENCELLED, orderData.isIsOrderCancelled());
				reg_request.put(TAG_IS_ORDER_DELIVERED, orderData.isIsOrderDelivered());
				reg_request.put(TAG_IS_TEMPLATE, orderData.isIsTemplate());
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getParticularOrder(String token, String orderId) {
		JSONObject reg_request = new JSONObject();
		try {
			reg_request.put(TAG_ORDER_ID, orderId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getDeleteOrder(String token, String orderId) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_ORDER_ID, orderId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> changeOrderStatus(String token, String orderId, int OrderStatus) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_ORDER_ID, orderId);
			reg_request.put(TAG_ORDER_STATUS, OrderStatus);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> cancelBooking(String token, String tableBookingId) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_TABLEBOOKING_ID, tableBookingId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getHolidayList(String token, String companyId) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_COMPANY_ID, companyId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getBookingDetails(String token, String tableBookingId) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_TABLEBOOKING_ID, tableBookingId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> acceptRejectBooking(String token, int status, String tableBookingId, int bookingStatus) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_TABLEBOOKING_ID, tableBookingId);
			reg_request.put(TAG_BOOKING_STATUS, bookingStatus);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_STATUS, String.valueOf(status));
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getParticularBookingOfUser(String token, String TableBookingId) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_TABLEBOOKING_ID, TableBookingId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> addBookTable(String token, String bookingId, int status, String UserId, String FirstName, String LastName, String UserProfile, String DateToBook, String CompanyId, String MobileNumber, String Remarks, int Seats, int templetId,
			int bookingType, double deposit, String bookingTimeNow, String companyName, String companyNameEn) {
		JSONObject reg_request = new JSONObject();
		try {
			reg_request.put(TAG_TABLEBOOKING_ID, bookingId);
			reg_request.put(TAG_COMPANY_ID, CompanyId);
			reg_request.put(TAG_FIRSTNAME, FirstName);
			reg_request.put(TAG_LASTNAME, LastName);
			reg_request.put(TAG_MOBILENO, MobileNumber);
			reg_request.put(TAG_SEATS, Seats);
			reg_request.put(TAG_USER_ID, UserId);
			reg_request.put(TAG_USER_PROFILE, UserProfile);
			reg_request.put(TAG_DATETOBOOK, DateToBook);
			reg_request.put(TAG_BOOKING_STATUS, status);
			reg_request.put(TAG_REMARKS, Remarks);
			if (templetId > 0) {
				reg_request.put(TAG_BOOKING_TYPE_ID, templetId);
			} else {
				reg_request.put(TAG_BOOKING_TYPE_ID, "");
			}
			reg_request.put(TAG_BOOKING_TYPE, bookingType);
			reg_request.put(TAG_DEPOSIT, deposit);
			reg_request.put(TAG_BOOKING_DATETIME, bookingTimeNow);
			reg_request.put(TAG_COMPANY_NAME, companyName);
			reg_request.put(TAG_COMPANY_NAME_EN, companyNameEn);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> sendFeedbackRequest(String token, String name, String email, String phoneNumber, String subject) {

		JSONObject reg_request = new JSONObject();
		try {
			reg_request.put(TAG_NAME, name);
			reg_request.put(TAG_EMAIL, email);
			reg_request.put(TAG_PHONE_NUMBER, phoneNumber);
			reg_request.put(TAG_SUBJECT, subject);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> deleteUserProfileRequest(String token, String profileName) {

		HashMap<String, String> parameters = new HashMap<String, String>();
		Base64 b = new Base64();
		parameters.put(TAG_PROFILE_NAME, new String(b.encode(profileName.getBytes())));
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("profileName", profileName);
		return parameters;
	}

	public static HashMap<String, String> addProfile(String token, String fname, String lname, String profileName, String email, String loginPassword, String language, String mobileNo, String bussinessPhone, String companyName, String jobTitle, String address1, String address2,
			String remarks, boolean isDefaultProfile, String province, String rowKey, String loginPreference, String gender, int recordStatus, String photo, int city, String partitionKey) {

		JSONObject reg_request = new JSONObject();
		try {
			reg_request.put(TAG_FIRSTNAME, fname);
			reg_request.put(TAG_LASTNAME, lname);
			reg_request.put(TAG_PROFILE_NAME, profileName);
			reg_request.put(TAG_EMAIL, email);
			reg_request.put(TAG_LOGINPASSWORD, loginPassword);
			reg_request.put(TAG_LANGUAGE, language);
			reg_request.put(TAG_MOBILENO, mobileNo);
			reg_request.put(TAG_BUSSINESS_PHONE, bussinessPhone);
			reg_request.put(TAG_COMPANYNAME, companyName);
			reg_request.put(TAG_JOB_TITLE, jobTitle);
			reg_request.put(TAG_ADDRESS1, address1);
			reg_request.put(TAG_ADDRESS2, address2);
			reg_request.put(TAG_REMARKS, remarks);
			reg_request.put(TAG_PROFILE, isDefaultProfile);
			reg_request.put(TAG_PROVINCE, province);
			// reg_request.put(TAG_ROWKEY, rowKey);
			reg_request.put(TAG_LOGIN_PREFERENCE, loginPreference);
			reg_request.put(TAG_GENDER, gender);
			reg_request.put(TAG_RECORDSTATUS, recordStatus);
			// reg_request.put(TAG_PHOTO, photo);
			reg_request.put(TAG_CITY, city);
			reg_request.put(TAG_PARTITION_KEY, partitionKey);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> editProfile(String token, String fname, String lname, String profileName, String email, String loginPassword, String language, String mobileNo, String bussinessPhone, String companyName, String jobTitle, String address1, String address2,
			String remarks, boolean isDefaultProfile, String province, String rowKey, String loginPreference, String gender, int recordStatus, String photo, int city, String partitionKey) {

		JSONObject reg_request = new JSONObject();
		try {
			reg_request.put(TAG_FIRSTNAME, fname);
			reg_request.put(TAG_LASTNAME, lname);
			reg_request.put(TAG_PROFILE_NAME, profileName);
			reg_request.put(TAG_EMAIL, email);
			reg_request.put(TAG_LOGINPASSWORD, loginPassword);
			reg_request.put(TAG_LANGUAGE, language);
			reg_request.put(TAG_MOBILENO, mobileNo);
			reg_request.put(TAG_BUSSINESS_PHONE, bussinessPhone);
			reg_request.put(TAG_COMPANYNAME, companyName);
			reg_request.put(TAG_JOB_TITLE, jobTitle);
			reg_request.put(TAG_ADDRESS1, address1);
			reg_request.put(TAG_ADDRESS2, address2);
			reg_request.put(TAG_REMARKS, remarks);
			reg_request.put(TAG_PROFILE, isDefaultProfile);
			reg_request.put(TAG_PROVINCE, province);
			reg_request.put(TAG_ROWKEY, rowKey);
			reg_request.put(TAG_LOGIN_PREFERENCE, loginPreference);
			reg_request.put(TAG_GENDER, gender);
			reg_request.put(TAG_RECORDSTATUS, recordStatus);
			reg_request.put(TAG_PHOTO, photo);
			reg_request.put(TAG_CITY, city);
			reg_request.put(TAG_PARTITION_KEY, partitionKey);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();
		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_DATA, jsonRequest);
		parameters.put(TAG_TOKEN, token);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> isProfileExits(String token, String partitionKey, String profileName) {

		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);

		Base64 b = new Base64();
		String b64Str = new String(b.encode(partitionKey.getBytes()));
		parameters.put(TAG_PARTITION_KEY, b64Str);

		b64Str = new String(b.encode(profileName.getBytes()));
		parameters.put(TAG_PROFILE_NAME, b64Str);

		Log.e("Token", token);
		Log.e("partitionKey", partitionKey);
		Log.e("profileName", profileName);
		return parameters;
	}

	public static HashMap<String, String> getTableType(String token, String companyId) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_COMPANY_ID, companyId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getTimeSlot(String token, String companyId, String bookingTableName, String bookingDate) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_COMPANY_ID, companyId);
			reg_request.put(TAG_BOOKING_NAME_EN, bookingTableName);
			reg_request.put(TAG_BOOKING_DATE, bookingDate);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getFoodOptionRequest(String token, int menuItemId) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_MENU_ITEM_ID, menuItemId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getCurrencyCodeRequest(String token, String companyId) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_COMPANY_ID, companyId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getAllCusinesRequest(String token, String cityCode, int areaId) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_CITYID, cityCode);
			reg_request.put(TAG_AREA_ID, new JSONArray().put(areaId));

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);

		return parameters;
	}

	public static HashMap<String, String> getRestByCusineRequest(String token, String cityCode, int cuisineTypeId, int areaId) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_CITYID, cityCode);
			reg_request.put(TAG_TYPE_ID, cuisineTypeId);
			reg_request.put(TAG_AREA_ID, areaId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);
		return parameters;
	}

	public static HashMap<String, String> getRegionRequest(String token, String cityCode) {
		JSONObject reg_request = new JSONObject();
		try {

			reg_request.put(TAG_CITYID, cityCode);
			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_SIMPLECHINSESE)) {
				reg_request.put(TAG_CULTURE, "SC");
			} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_TRADITIONALCHINESE)) {
				reg_request.put(TAG_CULTURE, "TC");
			} else {
				reg_request.put(TAG_CULTURE, "EN");
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Base64 b = new Base64();

		String jsonRequest = new String(b.encode(reg_request.toString().getBytes()));
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put(TAG_TOKEN, token);
		parameters.put(TAG_DATA, jsonRequest);
		Log.e("Token", token);
		Log.e("Json Request", reg_request.toString());
		Log.e("Request", jsonRequest);

		return parameters;
	}

	public static String getAddShopRequest(RequestAddShop requestAddShop) {
		Gson gson = new Gson();
		return gson.toJson(requestAddShop, RequestAddShop.class);
	}

	public static String getUploadPhotoRequest(ShopImageData requestAddShop) {
		Gson gson = new Gson();
		return "[" + gson.toJson(requestAddShop, ShopImageData.class) + "]";
	}

	public static String getResetPwdRequest(RequestResetPassword req) {
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		return gson.toJson(req, RequestResetPassword.class);
	}

	public static String getRegisterRequest(RequestRegisteration req) {
		return new Gson().toJson(req, RequestRegisteration.class);
	}
}
