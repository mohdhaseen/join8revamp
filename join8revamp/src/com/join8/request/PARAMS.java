package com.join8.request;

public interface PARAMS {

	public static final String TAG_USERNAME = "username";
	public static final String TAG_PASSWORD = "password";

	// Registraiton webservice parameters
	public static final String TAG_TOKEN = "Token";
	public static final String TAG_DATA = "Data ";

	public static final String TAG_RECORDSTATUS = "RecordStatus";
	public static final String TAG_LOGINPASSWORD = "LoginPassword";
	public static final String TAG_PROVINCE = "Province";
	public static final String TAG_LANGUAGE = "UserPreferLanguage";
	public static final String TAG_PROFILE = "DefaultProfile";
	public static final String TAG_CITY = "City";
	public static final String TAG_FIRSTNAME = "FirstName";
	public static final String TAG_GENDER = "Gender";
	public static final String TAG_LASTNAME = "LastName";
	public static final String TAG_EMAIL = "Email";
	public static final String TAG_MOBILENO = "MobileNumber";
	public static final String TAG_LOGINPREFS = "LoginPreference";

	public static final String TAG_ACCESSTOKEN = "AccessToken";
	public static final String TAG_LOGINWITH = "LoginWith";
	public static final String TAG_TYPE = "Type";
	public static final String TAG_LOGINTYPE = "LoginType";

	public static final String TAG_CITYID = "CityId";
	public static final String TAG_COMPANYNAME = "CompanyName";
	public static final String TAG_COUNT = "Count";
	public static final String TAG_CUSINTYPE = "CuisineType";
	public static final String TAG_ITEMNAME = "ItemName";
	public static final String TAG_NOOFDAYS = "NoOfDays";
	public static final String TAG_STARTINDEX = "StartIndex";
	public static final String TAG_SEARCHTEXT = "SearchText";
	public static final String TAG_PARTITION_KEY = "PartitionKey";

	public static final String TAG_COMPANY_ID = "CompanyId";
	public static final String TAG_USER_ID = "UserId";
	public static final String TAG_MENU_ID = "MenuId";
	public static final String TAG_ADDED_ON = "Added_On";
	public static final String TAG_WATCHLIST_ID = "WatchListId";

	public static final String TAG_START_DATE = "StartDate";
	public static final String TAG_END_DATE = "EndDate";

	public static final String TAG_ORDER_STATUS = "OrderStatus";
	public static final String TAG_ORDER_ID = "OrderId";
	public static final String TAG_USER_PROFILE = "UserProfile";
	public static final String TAG_ORDER_DEADLINE = "OrderDeadline";
	public static final String TAG_IS_ORDER_CENCELLED = "IsOrderCancelled";
	public static final String TAG_IS_ORDER_DELIVERED = "IsOrderDelivered";
	public static final String TAG_IS_ORDER_PICKUPBYUSER = "IsOrderPickUpByUser";
	public static final String TAG_IS_ORDER_SHARED = "IsOrderShared";
	public static final String TAG_IS_TEMPLATE = "IsTemplate";
	public static final String TAG_DELETE_PREVIOUS_DETAILS = "DeletePreviousDetails";
	public static final String TAG_MENUDATA = "MenuData";
	public static final String TAG_LISTEMAILS = "listEmails";
	public static final String TAG_TABLEBOOKING_ID = "TableBookingId";
	public static final String TAG_BOOKING_STATUS = "BookingStatus";
	public static final String TAG_STATUS = "Status";
	public static final String TAG_DATETOBOOK = "DateToBook";
	public static final String TAG_DRESSCODEID = "DressCodeId";
	public static final String TAG_ISSHARED = "IsShared";
	public static final String TAG_REMARKS = "Remarks";
	public static final String TAG_SEATS = "Seats";
	public static final String TAG_SHAREDEMAILS = "SharedEmails";
	public static final String TAG_NAME = "Name";
	public static final String TAG_PHONE_NUMBER = "PhoneNumber";
	public static final String TAG_SUBJECT = "Subject";
	public static final String TAG_PROFILE_NAME = "ProfileName";
	public static final String TAG_BUSSINESS_PHONE = "BusinessPhone";
	public static final String TAG_JOB_TITLE = "JobTitle";
	public static final String TAG_ADDRESS1 = "Address1";
	public static final String TAG_ADDRESS2 = "Address2";
	public static final String TAG_ROWKEY = "RowKey";
	public static final String TAG_LOGIN_PREFERENCE = "LoginPreference";
	public static final String TAG_PHOTO = "Photo";
	public static final String TAG_BOOKING_NAME_EN = "BookingNameEn";
	public static final String TAG_BOOKING_DATE = "BookingDate";
	public static final String TAG_BOOKING_TYPE_ID = "BookingTypeId";
	public static final String TAG_BOOKING_TYPE = "BookingType";
	public static final String TAG_DEPOSIT = "Deposit";
	public static final String TAG_BOOKING_DATETIME = "BookingDateTime";
	public static final String TAG_COMPANY_NAME = "CompanyName";
	public static final String TAG_COMPANY_NAME_EN = "CompanyNameEN";
	public static final String TAG_MENU_ITEM_ID = "MenuItemId";
	public static final String TAG_BOOKING_ID = "BookingId";
	public static final String TAG_CURRENCY = "Currency";
	public static final String TAG_HANDLE_BY_USER_ID = "HandleByUserId";
	public static final String TAG_ORDER_PAY_OPTION = "OrderPayOption";
	public static final String TAG_ORDER_DATETIME = "OrderDatetime";
	public static final String TAG_UPDATED_TIME = "LastUpdatedTime";
	public static final String TAG_ORDER_SHARED_WITH = "OrderSharedWith";
	public static final String TAG_ORDER_PICKUP_TIME = "OrderEstimatedPickUpTime";
	public static final String TAG_TOTAL_AMOUNT = "TotalAmount";
	public static final String TAG_LAST_INSERTED_VALUE = "LastInsertedValue";
	public static final String TAG_ORDER_NAME = "OrderName";
	public static final String TAG_QUANTITY = "Quantity";
	public static final String TAG_FOODOPTION_ITEM_ID = "FoodOptionItemId";
	public static final String TAG_FOODOPTION_ITEM_NAME = "FoodOptionItemName";
	public static final String TAG_FOODOPTION_ITEM_NAME_EN = "FoodOptionItemNameEn";
	public static final String TAG_IS_FOODOPTION = "IsFoodOption";
	public static final String TAG_FOODOPTIONS = "foodoptions";
	public static final String TAG_DELIVER_ADDRESS = "DeliverAddress";
	public static final String TAG_TYPE_ID = "TypeId";
	public static final String TAG_AREA_ID = "AreaId";
	public static final String TAG_CULTURE = "Culture";
	public static final String TAG_USER_PREF_LANG = "UserPreferLanguage";
	public static final String TAG_ACTIVATION_CODE = "ActivationCode";
	 
	
	 

}
