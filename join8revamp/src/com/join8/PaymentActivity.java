package com.join8;

import org.json.JSONObject;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.TextView;

import com.join8.listeners.Requestlistener;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.Utils;

public class PaymentActivity extends ActionBarActivity implements Requestlistener, OnClickListener {

	private TextView txtActionTitle = null;
	private WebView webView = null;
	private String merchantId = "", orderRef = "", currCode = "", remark = "", companyId = "", language = "";
	private double amount = 0;

	private String access_token = "";
	private NetworkManager networManager;
	private CryptoManager prefManager;
	private int currencyCodeRequest = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_payment);

		initActionBar();

		webView = (WebView) findViewById(R.id.webView);

		orderRef = getIntent().getStringExtra("refId");
		companyId = getIntent().getStringExtra("companyId");
		remark = getIntent().getStringExtra("companyName");
		amount = getIntent().getDoubleExtra("amount", 0);

		networManager = NetworkManager.getInstance();
		prefManager = CryptoManager.getInstance(PaymentActivity.this);
		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");

		getCurrencyCode();

		webView.loadData("<html><table height=100% width=100%><tr><td><center><h1>Loading...</td></tr><table></html>", "text/html", "UTF-8");
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		webView.getSettings().setAllowFileAccess(true);
		webView.getSettings().setSupportMultipleWindows(true);
		webView.getSettings().setDomStorageEnabled(true);
		webView.getSettings().setUserAgentString("Mozilla/5.0 (Android; Tablet; rv:20.0) Gecko/20.0 Firefox/20.0");
		webView.getSettings().setUseWideViewPort(true);
	    webView.getSettings().setLoadWithOverviewMode(true);
	    webView.getSettings().setSupportZoom(true);
	    webView.getSettings().setBuiltInZoomControls(true);
	    webView.getSettings().setDisplayZoomControls(false);
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);

				if (url.startsWith("http://www.join8.com/en/Restaurant/ViewAdminRestaurant")) {
					Toast.displayText(PaymentActivity.this, getString(R.string.payment_success));
					setResult(RESULT_OK);
					finish();
				} else if (url.startsWith("http://www.join8.com/en/Restaurant/Fail")) {
					Toast.displayText(PaymentActivity.this, getString(R.string.payment_fail));
					setResult(RESULT_CANCELED);
					finish();
				} else if (url.startsWith("http://www.join8.com/en/Restaurant/Cancel")) {
					Toast.displayText(PaymentActivity.this, getString(R.string.payment_cancel));
					setResult(RESULT_CANCELED);
					finish();
				}
				return true;
			}
		});
		webView.setWebChromeClient(new WebChromeClient() {

			@Override
			public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
				return super.onJsAlert(view, url, message, result);
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	protected void onStop() {
		networManager.removeListeners(this);
		super.onStop();
	}

	private void initActionBar() {

		getSupportActionBar().setHomeButtonEnabled(false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		getSupportActionBar().setDisplayUseLogoEnabled(false);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);

		View view = getLayoutInflater().inflate(R.layout.actionview, null);
		getSupportActionBar().setCustomView(view);
		ImageButton imgBack = (ImageButton) view.findViewById(R.id.btn_back);
		imgBack.setOnClickListener(this);
		txtActionTitle = (TextView) view.findViewById(R.id.txtTitle);
		txtActionTitle.setText(getIntent().getStringExtra("title"));
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_back) {
			finish();
		}
	}

	private void getCurrencyCode() {
		networManager.isProgressVisible(true);
		currencyCodeRequest = networManager.addRequest(Join8RequestBuilder.getCurrencyCodeRequest(access_token, companyId), RequestMethod.POST, PaymentActivity.this, Join8RequestBuilder.METHOD_GET_PAYMENT_DETAILS_OF_CITY);
	}

	@Override
	public void onSuccess(int id, String response) {

		if (!Utils.isEmpty(response)) {

			try {

				if (id == currencyCodeRequest) {

					JSONObject jObj = new JSONObject(response);

					currCode = jObj.getString("CurrencyCode");
					merchantId = jObj.getString("MerchantId");
					
					if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
						language = "E";
					} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_SIMPLECHINSESE)) {
						language = "X";
					} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_TRADITIONALCHINESE)) {
						language = "C";
					}

					webView.loadUrl(String.format("file:///android_asset/PaymenForm.html?%1$s&%2$s&%3$s&%4$s&%5$s&%6$s", merchantId, amount, orderRef, currCode, remark, language));
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onError(int id, String message) {
		Toast.displayText(PaymentActivity.this, message);
	}
}
