package com.join8;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeoutException;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLPeerUnverifiedException;

import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.ConnectionPoolTimeoutException;

import android.accounts.NetworkErrorException;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;

public class Toast {

	@SuppressLint("ShowToast")
	public static void displayText(Context context, String str) {
		if (str != null && context != null) {

			if (str.contains(TimeoutException.class.getName()) || str.contains(UnknownHostException.class.getName()) || str.contains(ConnectTimeoutException.class.getName()) || str.contains(SocketTimeoutException.class.getName())
					|| str.contains(NetworkErrorException.class.getName()) || str.contains(SSLException.class.getName()) || str.contains(SSLPeerUnverifiedException.class.getName()) || str.contains(ConnectionPoolTimeoutException.class.getName())) {

				android.widget.Toast toast = android.widget.Toast.makeText(context, context.getString(R.string.nointerent), android.widget.Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			} else {

				android.widget.Toast toast = android.widget.Toast.makeText(context, str, android.widget.Toast.LENGTH_LONG);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
		}
	}

	@SuppressLint("ShowToast")
	public static void displayTextForShortTime(Context context, String str) {
		if (str != null && context != null) {
			android.widget.Toast toast = android.widget.Toast.makeText(context, str, android.widget.Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
		}
	}

}
