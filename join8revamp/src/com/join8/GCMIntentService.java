package com.join8;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.join8.utils.ConstantData;

public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "Join8-GCM";

	public GCMIntentService() {
		super(ConstantData.SENDER_ID);
	}

	/**
	 * Method called on device registered
	 **/
	@Override
	protected void onRegistered(Context context, String registrationId) {
		Log.i(TAG, "Join8 GCM registrationId : " + registrationId);

		ConstantData.RegistrationID = registrationId;
	}

	/**
	 * Method called on device unregistered
	 * */
	@Override
	protected void onUnregistered(Context context, String registrationId) {
		Log.i(TAG, "Device unregistered");
	}

	/**
	 * Method called on Receiving a new message
	 * */
	@Override
	protected void onMessage(Context context, Intent intent) {
		Log.i(TAG, "Received message : " + intent.toString());

		String message = intent.getExtras().getString("msg");
		String redirectTo = intent.getExtras().getString("redirectTo");
		String reference = intent.getExtras().getString("reference");

		Log.i(TAG, "Message ==> " + message);
		Log.i(TAG, "RedirectTo ==> " + redirectTo);
		Log.i(TAG, "Reference ==> " + reference);
	}

	/**
	 * Method called on receiving a deleted message
	 * */
	@Override
	protected void onDeletedMessages(Context context, int total) {
		Log.i(TAG, "Received deleted messages notification");
	}

	/**
	 * Method called on Error
	 * */
	@Override
	public void onError(Context context, String errorId) {
		Log.i(TAG, "Received error: " + errorId);
	}

	@Override
	protected boolean onRecoverableError(Context context, String errorId) {
		// log message
		Log.i(TAG, "Received recoverable error: " + errorId);
		return super.onRecoverableError(context, errorId);
	}
}
