package com.join8.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;

/**
 * This Class Handle image related things.
 * 
 * @author Navrattan Yadav
 */
public class ImageUtil {

	public static final String TAG = ImageUtil.class.getName();

	/**
	 * This method resize image
	 * 
	 * @param bitmap
	 *            : image
	 * @param width
	 *            :
	 * @param height
	 * @return : resized image / same image in case of error
	 */
	public static Bitmap resizeImage(Bitmap bitmap, int width, int height) {
		if (bitmap != null && width != 0 && height != 0 && bitmap.getWidth() != width && bitmap.getHeight() != height) {
			bitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
		}
		return bitmap;
	}

	/**
	 * This method Convert Image to byte array
	 * 
	 * @param bmp
	 * @return byte Array of BMP/ null
	 */
	public static byte[] bitmapToBytes(CompressFormat format, int quality, Bitmap bmp) {
		if (bmp != null) {
			try {
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bmp.compress(format, quality, stream);
				return stream.toByteArray();
			} catch (Exception e) {
				Log.e(TAG, "Error in Converting Image to Bytes", e);
			}
		}
		return null;
	}

	public static String getBase64(Bitmap bitmap) {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		bitmap.compress(CompressFormat.JPEG, 100, byteArrayOutputStream);
		byte[] byteArray = byteArrayOutputStream.toByteArray();
		return Base64.encodeToString(byteArray, Base64.DEFAULT);
	}

	public static byte[] bitmapJPEGToBytes(Bitmap bmp) {
		return bitmapToBytes(CompressFormat.JPEG, 100, bmp);
	}

	public static byte[] bitmapPNGToBytes(Bitmap bmp) {
		return bitmapToBytes(CompressFormat.PNG, 100, bmp);
	}

	/**
	 * This method convert bytes array to bitmap
	 * 
	 * @param bytes
	 * @return
	 */
	public static Bitmap bytesToBitmap(byte[] bytes) {
		if (bytes != null && bytes.length != 0) {
			try {
				return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
			} catch (Exception e) {
				Log.e(TAG, "Error in Converting Image to Bytes", e);
			}
		}
		return null;
	}

	/**
	 * This method gives bitmap after decoding given uri of image
	 * 
	 * @param context
	 * @param imageUri
	 * @param width
	 * @param height
	 * @return Bitmap
	 */
	public static Bitmap decodeImageUri(Context context, Uri imageUri, int width, int height) {
		try {
			// Decode image size
			Log.v("", "decode uri");
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;

			ContentResolver contentResolver = context.getContentResolver();

			BitmapFactory.decodeStream(contentResolver.openInputStream(imageUri), null, options);

			// The new size we want to scale to
			// Find the correct scale value. It should be the power of 2.
			int width_tmp = options.outWidth, height_tmp = options.outHeight;
			int scale = 1;
			while (width_tmp / 2 > width && height_tmp / 2 > height) {
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// Decode with inSampleSize
			options = new BitmapFactory.Options();
			options.inSampleSize = scale;
			return BitmapFactory.decodeStream(contentResolver.openInputStream(imageUri), null, options);
		} catch (FileNotFoundException e) {
			Log.e(TAG, "Image decode file not find exception", e);
		} catch (Exception e) {
			Log.e(TAG, "Image decode exception", e);
		}

		return null;
	}

	/**
	 * This method gives bitmap after decoding given uri of image
	 * 
	 * @param context
	 * @param imageUri
	 * @return Bitmap
	 */
	public static Bitmap decodeImageUri(Context context, Uri imageUri) {
		try {
			// Decode image size
			return BitmapFactory.decodeStream(context.getContentResolver().openInputStream(imageUri), null, null);
		} catch (FileNotFoundException e) {
			Log.e(TAG, "Image decode file not find exception", e);
		} catch (Exception e) {
			Log.e(TAG, "Image decode exception", e);
		}

		return null;
	}

	public static Bitmap roateImage(Bitmap bmp, int degree) {
		Matrix matrix = new Matrix();
		matrix.postRotate(degree);

		Bitmap rotatedBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

		bmp.recycle();

		return rotatedBitmap;
	}
	/*
	 * public static void loadImage(String imageUrl, ImageView imageView) {
	 * DisplayImageOptions options = universalImageLoaderDisplayOption();
	 * 
	 * com.nostra13.universalimageloader.core.ImageLoader.getInstance()
	 * .displayImage(imageUrl, imageView, options, new ImageLoadingListener() {
	 * 
	 * @Override public void onLoadingStarted(String imageUrl, View view) { }
	 * 
	 * @Override public void onLoadingFailed(String imageUrl, View view,
	 * FailReason failReason) { }
	 * 
	 * @Override public void onLoadingComplete(String imageUrl, View view,
	 * Bitmap loadedImage) { }
	 * 
	 * @Override public void onLoadingCancelled(String imageUrl, View view) { }
	 * 
	 * }); }
	 */

	/*
	 * @SuppressWarnings("deprecation") public static DisplayImageOptions
	 * universalImageLoaderDisplayOption() { DisplayImageOptions options = new
	 * DisplayImageOptions.Builder()
	 * .showStubImage(R.drawable.dark_default_image)
	 * .showImageForEmptyUri(R.drawable.dark_default_image)
	 * .resetViewBeforeLoading(false).cacheInMemory(true)
	 * .cacheOnDisc(true).imageScaleType(ImageScaleType.EXACTLY) // default
	 * .bitmapConfig(Bitmap.Config.ARGB_8888) // default .displayer(new
	 * RoundedBitmapDisplayer(20))// default .build(); return options; }
	 */

	public static String getImageName(File file) {
		String imagePath = file.toString();
		int index = imagePath.lastIndexOf("/");
		String imageName = imagePath.substring(index + 1, imagePath.length());
		return imageName;
	}

}
