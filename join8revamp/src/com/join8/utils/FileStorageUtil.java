/*
 *  Copyright @ CraterZone 2013 
 *
 */
package com.join8.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

/**
 * This Class Handle File Storage related things : Save and Get File It store
 * file in external and internal storage
 * 
 * @author Navrattan Yadav
 * 
 */
public class FileStorageUtil {

	public static final String TAG = FileStorageUtil.class.getName();

	public enum StorageType {
		EXTERNAL, INTERNAL, EXTERNAL_OR_INTERNAL;
	}

	/**
	 * Checking whether External Storage is Available or not
	 * 
	 * @return boolean available or not
	 */
	private static boolean isExternalStorageAvaliable() {
		return Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED);
	}

	/**
	 * Checking External Storage is Available for write
	 * 
	 * @return boolean can write or not
	 */
	private static boolean isExternalStorageWritable() {
		return Environment.getExternalStorageDirectory().canWrite();
	}

	/**
	 * This method
	 * 
	 * @param packageName
	 *            : App Package :
	 * @param folderName
	 * @return file
	 */
	public static File getOrCreateDirectory(String packageName,
			String folderName, StorageType storageType) {
		File dir = null;
		if (folderName != null && folderName.length() > 0) {
			switch (storageType) {
			case EXTERNAL:
				dir = getOrCreateDirectoryInExternalStorage(folderName);
				break;
			case INTERNAL:
				dir = getOrCreateDirectoryInInternalStorage(packageName,
						folderName);
				break;
			case EXTERNAL_OR_INTERNAL:
				if (isExternalStorageAvaliable() && isExternalStorageWritable()) {
					dir = getOrCreateDirectoryInExternalStorage(folderName);
				} else if (packageName != null && packageName.length() > 0) {
					dir = getOrCreateDirectoryInInternalStorage(packageName,
							folderName);
				}
				break;
			}
		}
		return dir;
	}

	public static File getOrCreateDirectoryInExternalStorage(String folderName) {
		File dir = null;
		try {
			Log.v("", "File directory not null");
			if (isExternalStorageAvaliable() && isExternalStorageWritable()
					&& folderName != null && folderName.length() > 0) {
				dir = new File(Environment.getExternalStorageDirectory(),
						folderName);
				if (!dir.exists())
					dir.mkdir();
			}
		} catch (Exception e) {
			Log.e(TAG, "Error in creating Directory in Excernal Storage", e);
		}

		return dir;
	}

	public static File getOrCreateDirectoryInInternalStorage(
			String packageName, String folderName) {
		File dir = null;
		if (packageName != null && packageName.length() > 0
				&& folderName != null && folderName.length() > 0) {
			dir = new File(Environment.getDataDirectory() + "//" + packageName
					+ "//" + folderName);
			if (!dir.exists()) {
				dir.mkdir();
			}
		}
		return dir;
	}

	/**
	 * Checking is File Exist or not
	 * 
	 * @param packageName
	 * @param folderName
	 * @param fileName
	 * @return boolean
	 */
	public static boolean isFileExist(StorageType storageType,
			String packageName, String folderName, String fileName) {
		if (folderName != null && folderName.length() > 0) {
			switch (storageType) {
			case EXTERNAL:
				return isFileExistInExternalStorage(folderName, fileName);
			case INTERNAL:
				return isFileExistInInternalStorage(packageName, folderName,
						fileName);
			case EXTERNAL_OR_INTERNAL:
				if (isFileExistInExternalStorage(folderName, fileName)) {
					return true;
				}
				return isFileExistInInternalStorage(packageName, folderName,
						fileName);
			}
		}
		return false;
	}

	public static boolean isFileExistInExternalStorage(String folderName,
			String fileName) {
		try {
			if (folderName != null && fileName != null
					&& isExternalStorageAvaliable()
					&& isExternalStorageWritable()) {
				File file = new File(Environment.getExternalStorageDirectory()
						+ "//" + folderName, fileName);
				if (file.exists())
					return true;
			}
		} catch (Exception e) {
			Log.e(TAG,
					"Error in Checking File Exist or not in External Storage",
					e);
		}
		return false;
	}

	public static File getFileIfExistFromExternalStorage(String folderName,
			String fileName) {
		try {
			if (folderName != null && fileName != null
					&& isExternalStorageAvaliable()
					&& isExternalStorageWritable()) {
				File file = new File(Environment.getExternalStorageDirectory()
						+ "//" + folderName, fileName);
				Log.v("", "file path in external" + folderName + "//"
						+ fileName);
				if (file.exists())
					return file;
			}
		} catch (Exception e) {
			Log.e(TAG, "Error in getting file from  External Storage", e);
		}
		return null;
	}

	public static boolean isFileExistInInternalStorage(String packageName,
			String folderName, String fileName) {
		try {
			if (packageName != null && folderName != null && fileName != null) {
				File file = new File(Environment.getDataDirectory() + "//"
						+ packageName + "//" + folderName + "//" + fileName);
				if (file.exists())
					return true;
			}
		} catch (Exception e) {
			Log.e(TAG,
					"Error in Checking File Exist or not in Internal Storage",
					e);
		}
		return false;
	}


	public static void copyStream(InputStream input, OutputStream output) {

		byte[] buffer = new byte[1024];
		int bytesRead;
		try {
			while ((bytesRead = input.read(buffer)) != -1) {
				output.write(buffer, 0, bytesRead);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}