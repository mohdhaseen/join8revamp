package com.join8.utils;

public class ConstantData {

	public static final String TAG = "AndroidOauth2";
	public static final int ACTIVITY_CAMERA_REQUEST = 10;
	public static final String USER_PREFERENCES = "join8prefs";
	public static final String WEBSERVICE_URL = "http://join8service.cloudapp.net/ServiceHandler.svc/";
	// public static final String WEBSERVICE_URL =
	// "http://77f097399ea345818759ff313714bb72.cloudapp.net/ServiceHandler.svc/";
	public static final String DefaultImage = "http://join8storage.blob.core.windows.net/advertisementimages/images/briefimage/852/join8-banner-300x250.gif";
	public static final String HongKongImage = "http://join8storage.blob.core.windows.net/advertisementimages/images/briefimage/852/default.jpg";
	public static final String MacauImage = "http://join8storage.blob.core.windows.net/advertisementimages/images/briefimage/853/farm%20house.jpg";

	public static final String DefaultBreifImage = "http://join8storage.blob.core.windows.net/advertisementimages/images/briefimage/852/join8-banner-300x250.gif";
	public static final String HongKongBriefImage = "http://join8storage.blob.core.windows.net/advertisementimages/images/briefimage/852/default.jpg";
	public static final String MacauBreifImage = " http://join8storage.blob.core.windows.net/advertisementimages/images/briefimage/853/farm%20house.jpg";

	public static final String USERNAME_FOR_TOKEN = "join8Admin";
	public static final String PASSWORD_FOR_TOKEN = "j01N@dm1n";

	public static final String WEBSERVICE_CONTENTTYPE = "text/xml; charset=utf-8";
	public static final String FACEBOOK_APPID = "383485348355400";

	public static final String PAYMENT_URL = "https://test.paydollar.com/b2cDemo/eng/directPay/payComp.jsp";
	public static final String ENCRPITION_KEY = "MAKV2SPBNI99212X";
	public static String LANG_ENGLISH = "en_US";
	public static String LANG_SIMPLECHINSESE = "zh_CN";
	public static String LANG_TRADITIONALCHINESE = "zh_TW";
	public static final String SENDER_ID = "1052360065327";


	public static final String method_post = "POST";

	public static final String method_get = "GET";

	public static final String CONTENT_TYPE_WAITER = "application/json";

	public static final String TOKEN_CLOUD = "token_cloud";
	public static final String USER_NAME = "user_name";
	public static final Object method_put = "PUT";
	public static String RegistrationID = "";
	public static String adMobKey = "ca-app-pub-1710502159178480/5207815854";
	// public static String adMobKey = "ca-app-pub-3692767588798415/9521416488";

	public static String HUB_NOTI_CONNECTION_STRING = "Endpoint=sb://hubnotification-ns.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=OqrKYquzqUfdrGgYUBWGBmLMKCof8QDZyvQI6rB3/Yw=";

	public static String BROADCAST_REFRESH_LIST = "com.join8.refresh_list";

}
