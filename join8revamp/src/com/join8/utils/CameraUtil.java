package com.join8.utils;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

/**
 * This Class Handle Camera related things.
 */
public class CameraUtil {
    public static final String TAG = CameraUtil.class.getName();

    private static final String CAMERA = "Camera";

    private static Uri lastImageUri;

    public static Uri newImageUri() {
        File cameraDirectory = FileStorageUtil.getOrCreateDirectoryInExternalStorage(CAMERA);

        if (cameraDirectory == null) {
            return null;
        }

        return Uri.fromFile(new File(cameraDirectory, System.currentTimeMillis() + ".png"));
    }


    public static void takePictureInExternalStorage(Activity activity, int errorId) {
        Log.v("", "camera util 1");
        takePictureInExternalStorage(activity, activity.getString(errorId));
    }

    public static void takePictureInExternalStorage(Activity activity, String error) {
        Uri cameraUri = newImageUri();

        if (cameraUri != null) {
            Log.v("", "camera util 2");
            Intent cameraIntent = new Intent("android.media.action.IMAGE_CAPTURE");
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
            activity.startActivityForResult(cameraIntent, ConstantData.ACTIVITY_CAMERA_REQUEST);

            lastImageUri = cameraUri;
        } else {
            Log.v("", "camera util3");
            Toast.makeText(activity, error, Toast.LENGTH_SHORT).show();
        }
    }

    public static Uri getLastImageUri() {
        return lastImageUri;
    }

    public static int getExifRotation(String imgPath) {
        try {
            ExifInterface exif = new ExifInterface(imgPath);
            String rotationAmount = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            if (!TextUtils.isEmpty(rotationAmount)) {
                int rotationParam = Integer.parseInt(rotationAmount);
                switch (rotationParam) {
                    case ExifInterface.ORIENTATION_NORMAL:
                        return 0;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        return 90;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        return 180;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        return 270;
                    default:
                        return 0;
                }
            } else {
                return 0;
            }
        } catch (Exception ex) {
            return 0;
        }
    }


}
