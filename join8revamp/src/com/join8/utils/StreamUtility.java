/*
 *  Copyright @ CraterZone 2013 
 *
 */
package com.join8.utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

/**
 * This class help in converting response entity(byte stream ) to string.
 * 
 * @author craterzone
 * 
 */
public class StreamUtility {
	public static final String TAG = StreamUtility.class.getName();

	/*
	 * Convert Stream to String To convert the InputStream to String we use the
	 * BufferedReader.readLine() method. We iterate until the BufferedReader
	 * return null which means there's no more data to read. Each line will
	 * appended to a StringBuilder and returned as String.
	 */
	public static String convertStreamToString(InputStream is) {
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			// Logger.e(TAG, "Error in converting strem to string", e);
			return null;
		} catch (IllegalStateException e) {
			// Logger.e(TAG, "Error in converting strem to string", e);
			return null;
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				// Logger.e(TAG, "Error in closing Input Stream", e);
			}
		}
		return sb.toString();
	}

	/**
	 * Getting data from JSON Response
	 * 
	 * @param response
	 * @return decoded JSON String or null if some error
	 */
	public static String getJSONStringFromResponse(HttpResponse response) {
		try {
			if (response != null) {
				HttpEntity entity = response.getEntity();
				if (entity != null) {
					InputStream is = entity.getContent();
					return StreamUtility.convertStreamToString(is);
				}
			}
		} catch (Exception e) {
			// Logger.e(TAG, "Unable convet stream to string ", e);
		}
		return null;
	}

	/**
	 * Getting data from bitmap
	 * 
	 * @param bitmap
	 * @return convert bitmap to bytes or return null
	 */
	public static byte[] getBytefromBitmap(Bitmap bitmap) {
		byte[] imageByte = null;
		try {
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
			imageByte = stream.toByteArray();
			return imageByte;
		} catch (Exception e) {
		}
		return imageByte;
	}

	public static File String_to_File(String img_url) {
		try {
			File rootSdDirectory = Environment.getExternalStorageDirectory();

			File casted_image = new File(rootSdDirectory, "attachment.jpg");
			if (casted_image.exists()) {
				casted_image.delete();
			}
			casted_image.createNewFile();

			FileOutputStream fos = new FileOutputStream(casted_image);
			URL url = new URL(img_url);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);
			connection.connect();
			InputStream in = connection.getInputStream();

			byte[] buffer = new byte[1024];
			int size = 0;
			while ((size = in.read(buffer)) > 0) {
				fos.write(buffer, 0, size);
			}
			fos.close();
			return casted_image;

		} catch (Exception e) {

			System.out.print(e);
			// e.printStackTrace();

		}
		return null;
	}

	public static Uri getImageContentUri(Context context, File imageFile) {
		String filePath = imageFile.getAbsolutePath();
		Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{MediaStore.Images.Media._ID}, MediaStore.Images.Media.DATA + "=? ", new String[]{filePath}, null);
		if (cursor != null && cursor.moveToFirst()) {
			int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
			Uri baseUri = Uri.parse("content://media/external/images/media");
			return Uri.withAppendedPath(baseUri, "" + id);
		} else {
			if (imageFile.exists()) {
				ContentValues values = new ContentValues();
				values.put(MediaStore.Images.Media.DATA, filePath);
				return context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
			} else {
				return null;
			}
		}
	}

	public static InputStream getStream(String url) {
		try {

			URL imageUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.setInstanceFollowRedirects(true);
			return conn.getInputStream();
		} catch (Exception e) {
			Log.e(TAG, "Unable to get input stream", e);
		}
		return null;
	}

}
