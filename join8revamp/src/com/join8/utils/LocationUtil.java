package com.join8.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Atiqul Alam on 10/10/15.
 */
public class LocationUtil {
	private static final String TAG = "getAddress";

	public static boolean isValidEmail(String email) {

		Pattern pattern;
		Matcher matcher;
		String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(email);
		return matcher.matches();

	}
	public static boolean isValidPassword(String password) {

		Pattern pattern;
		Matcher matcher;
		String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$";
		pattern = Pattern.compile(PASSWORD_PATTERN);
		matcher = pattern.matcher(password);
		return matcher.matches();

	}

	public static final boolean isValidPhoneNumber(String target) {
		return target.length() == 10;
	}

	public static String getAddress(Context con, double latitude, double longitude) {

		String fullAddress = null;
		Geocoder geocoder = new Geocoder(con, Locale.getDefault());

		// Address found using the Geocoder.
		List<Address> addresses = null;
		ArrayList<String> addressFragments = null;

		try {
			// Using getFromLocation() returns an array of Addresses for the
			// area immediately
			// surrounding the given latitude and longitude. The results are a
			// best guess and are
			// not guaranteed to be accurate.
			addresses = geocoder.getFromLocation(latitude, longitude,
			// In this sample, we get just a single address.
					1);
		} catch (IOException ioException) {
			// Catch network or other I/O problems.

			Log.e(TAG, "error", ioException);
		} catch (IllegalArgumentException illegalArgumentException) {
			// Catch invalid latitude or longitude values.

			Log.e(TAG, "address not found for . " + "Latitude = " + latitude + ", Longitude = " + longitude, illegalArgumentException);
		}

		// Handle case where no address was found.
		if (addresses == null || addresses.size() == 0) {
			Log.e(TAG, "address not found");
		} else {
			Address address = addresses.get(0);
			addressFragments = new ArrayList<String>();

			// Fetch the address lines using {@code getAddressLine},
			// join them, and send them to the thread. The {@link
			// android.location.address}
			// class provides other options for fetching address details that
			// you may prefer
			// to use. Here are some examples:
			String locality = address.getLocality();// ("Mountain View", for
													// example)
			for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
				addressFragments.add(address.getAddressLine(i));
			}
			// Log.i(TAG, fullAddress);
			fullAddress = addressFragments.get(0) + "," + addressFragments.get(1) + "," + locality;
		}
		return fullAddress;
	}

	public static String getAddress1(Context context, double LATITUDE, double LONGITUDE) {
		String strAdd = "";
		Geocoder geocoder = new Geocoder(context, Locale.getDefault());
		try {
			List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
			if (addresses != null) {
				Address returnedAddress = addresses.get(0);
				StringBuilder strReturnedAddress = new StringBuilder("");

				for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
					strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
				}
				strAdd = strReturnedAddress.toString();
				Log.w("address", "" + strReturnedAddress.toString());
			} else {
				Log.w("address", "No Address returned!");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.w("address", "Canont get Address!");
		}
		return strAdd;
	}

	public static boolean isPlayserviceAvilable(Context context) {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);

		return resultCode == ConnectionResult.SUCCESS;
	}

	public static int getResId(String resName, Class<?> c) {

		try {
			Field idField = c.getDeclaredField(resName);
			return idField.getInt(idField);
		} catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
	}

}
