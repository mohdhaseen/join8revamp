package com.join8.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.RadioButton;
import android.widget.TextView;

public class TypefaceUtils {

	public static final int NORMAL = 0;
	public static final int BOLD = 1;
	public static final int ITALIC = 2;
	public static final int BOLD_ITALIC = 3;
	public static final int NORMAL_SPLASH = 4;

	private static TypefaceUtils mInstance = null;
	private Typeface mTypeface;
	private Typeface mBoldTypeface;
	private Typeface mBoldItalicTypeface;
	private Typeface mItalicTypeface;
	private Typeface mSplashTypeface;

	public static TypefaceUtils getInstance(Context c) {
		// TODO Auto-generated method stub
		if (mInstance == null) {
			mInstance = new TypefaceUtils(c);
		}
		return mInstance;
	}

	private TypefaceUtils(Context c) {
		mTypeface = Typeface.createFromAsset(c.getAssets(), "font/Avenir.ttf");
		mBoldTypeface = Typeface.createFromAsset(c.getAssets(), "font/AvenirHeavy.ttf");
		// mBoldTypeface= Typeface.createFromAsset(c.getAssets(),
		// "font/Avenirnext.ttf");
	}

	private TypefaceUtils() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Method to set typeface on TextView or button
	 * 
	 * @param mTextView
	 * @param style
	 */
	public void applyTypeface(TextView mTextView, int style) {
		if (mTextView == null) {
			return;
		}
		switch (style) {
		case NORMAL:

			mTextView.setTypeface(mTypeface, Typeface.NORMAL);
			break;
		case BOLD:
			mTextView.setTypeface(mBoldTypeface, Typeface.BOLD);
			break;
		case ITALIC:
			mTextView.setTypeface(mItalicTypeface, Typeface.ITALIC);
			break;
		case BOLD_ITALIC:
			mTextView.setTypeface(mBoldItalicTypeface, Typeface.BOLD_ITALIC);
			break;
		case NORMAL_SPLASH:
			mTextView.setTypeface(mSplashTypeface, Typeface.NORMAL);
			break;
		default:
			mTextView.setTypeface(mTypeface, Typeface.NORMAL);
			break;
		}

	}

	/**
	 * Method to set typeface on TextView or button
	 * 
	 * @param mTextView
	 */
	public void applyTypeface(TextView mTextView) {
		this.applyTypeface(mTextView, Typeface.NORMAL);
	}

	public void applyTypeface(RadioButton radioButton) {
		if (radioButton != null) {
			radioButton.setTypeface(mTypeface, Typeface.NORMAL);
		}
	}
}
