package com.join8.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class CryptoManager {

	private static CryptoManager instance = null;
	private SharedPreferences prefs;

	public static CryptoManager getInstance(Context context) {
		if (instance == null)
			instance = new CryptoManager(context);
		return instance;
	}

	private CryptoManager(Context context) {
		prefs = new ObscuredSharedPreferences(context,
				context.getSharedPreferences(ConstantData.USER_PREFERENCES,
						Context.MODE_PRIVATE));
	}

	public SharedPreferences getPrefs() {
		return prefs;
	}

}
