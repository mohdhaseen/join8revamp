package com.join8.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.Date;

import android.graphics.Bitmap;
import android.os.Environment;

public class Utils {

	public static boolean checkEmail(String email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

	public static boolean checkDigit(String email) {
		if (email.matches("[0-9]+")) {
			return true;
		}

		return false;
	}

	public static boolean isEmpty(String text) {

		if (text != null && !text.equalsIgnoreCase("null") && !text.trim().equals("")) {
			return false;
		}
		return true;
	}

	public static String formatDecimal(String val) {

		try {

			DecimalFormat form = new DecimalFormat("* ##,##,#,##0.00");

			return form.format(Double.parseDouble(val.replace(",", ""))).trim();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";

	}

	public static Bitmap resizeBitmapWithRatio(Bitmap bitmap, int width) {

		int bwidth = bitmap.getWidth();
		int bheight = bitmap.getHeight();
		int height = (int) Math.floor((double) bheight * ((double) width / (double) bwidth));
		return Bitmap.createScaledBitmap(bitmap, width, height, true);
	}
	
	public static void addLog(String msg) {
        try {
            Date dt = new Date();
            File root = Environment.getExternalStorageDirectory();
            if (root.canWrite()) {
                File file = new File(root, "Join8Log.txt");
                FileWriter writer = new FileWriter(file, true);
                BufferedWriter out = new BufferedWriter(writer);
                out.append("\n [" + dt.toString() + "]   " + msg + "\n");
                out.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static String stackTraceToString(Throwable e) {
        StringBuilder sb = new StringBuilder();
        sb.append(e.toString() + "\n");
        for (StackTraceElement element : e.getStackTrace()) {
            sb.append(element.toString());
            sb.append("\n");
        }
        return sb.toString();
    }
    
    public static String convertStreamToString(InputStream is) {

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			StringBuilder sb = new StringBuilder();

			String line = null;
			try {
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return sb.toString();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
}
