package com.join8.utils;

public class UrlUtil {
	public static final String WEBSERVICE_URL_CLOUD_PRODUCTION = "http://40.114.3.67:89/V1/";
	private static final String WEBSERVICE_URL_CLOUD_DEVELOPMENT = "https://microsoft-apiapp27e77eac878c41d0ac257940759de23f.azurewebsites.net/V1/";
	private static final String WEBSERVICE_URL_SHOP_PRODUCTION = "http://40.114.3.67:85/V1/";
	private static final String acc = "Account/";
	private static final String shop = "Shop/";
	private static final String product = "Product/";
	private static final String CITY = "City/";
	private static final String PROMOTIONS = "Promotion/";
	public static String getUserInfoUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "UserInfo";
	}

	public static String getLogoutUrl(String userName) {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "Logout?UserName=" + userName;
	}

	public static String getFeeadBackUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "FeedbackUs";
	}

	public static String getManageInfoUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "ManageInfo";
	}

	public static String getChangePasswordUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "ChangePassword";
	}

	public static String getSetPasswordUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "SetPassword";
	}

	public static String getAddExternalLoginUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "AddExternalLogin";
	}

	public static String getRemovelLoginUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "RemovelLogin";
	}

	public static String getExternalLoginUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "ExternalLogin";
	}

	public static String getExternalLoginsUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "ExternalLogins";
	}

	public static String getVerifyUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "Verify";
	}
	public static String getVerificationCodeUrl(String phNo) {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "VerifyUserNumber?UserName=" + phNo;
	}
	public static String getPromotionsUrl(String cityId, int count, int startIndex) {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + PROMOTIONS + "GetPromotionList?CityID=" + cityId + "&Count=" + count + "&StartIndex=" + startIndex;
	}
	public static String getResetPwdUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "ResetOrChangePassword";
	}
	public static String getRegisterUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "Register";
	}

	public static String getUserLoginUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "UserLogin";
	}

	public static String getTokenUrlForCloud() {
		return "https://microsoft-apiapp27e77eac878c41d0ac257940759de23f.azurewebsites.net/GetToken";// hard
																										// coded
																										// because
																										// it
																										// does
		// not include v1 but
		// all others
	}
	public static String getCityListUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + CITY + "GetCityList";
	}
	public static String getRegisterExternalUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + acc + "RegisterExternal";
	}

	public static String getShopCategoryUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + shop + "GetAllShopCategory";
	}
	public static String getShopAllSubCategoryUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + shop + "GetAllShopSubCategory";
	}
	public static String getAddShopUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + shop + "AddShop";
	}
	public static String getParticularShopUrl(String shopId) {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + shop + "SearchParticularShop?ShopId=" + shopId;
	}

	public static String getUploadShopPhotoUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + shop + "UploadShopPhoto";
	}
	public static String getAreaListUrl() {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + CITY + "/GetAreaList";
	}
	public static String getAreaListByCityIdUrl(String cityId) {
		return WEBSERVICE_URL_CLOUD_DEVELOPMENT + CITY + "/GetAreaListByCityId?cityId=" + cityId;
	}
}
