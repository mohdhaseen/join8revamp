package com.join8.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.join8.model.TokenModel;

import android.content.Context;
import android.util.Log;

public class HttpClientUtil {

	public static final String TAG = HttpClientUtil.class.getName();

	public static final String HEADER_ACCEPT = "Accept";
	public static final String CONTENT_TYPE = "Content-type";
	public static final String ORDER_TIME_STAMP = "orderTimestamp";

	public static HttpResponse post(String url, String body, String contentType, String acceptType, HttpParams httpParams) {
		HttpResponse response = null;
		HttpClient httpclient = getHttpClient(httpParams);
		HttpPost httppost = new HttpPost(url);
		try {
			StringEntity se = new StringEntity(body);
			httppost.setEntity(se);
			httppost.setHeader(HEADER_ACCEPT, contentType);
			httppost.setHeader(CONTENT_TYPE, acceptType);
			response = httpclient.execute(httppost);
		} catch (Exception e) {
			Log.e(TAG, "Error POST", e);
		}
		return response;
	}

	private static String getTokenRequest() {
		return "{\"UserName\":\"join8PosAdmin\",\"Password\":\"j01Np05@dm1n\"}";
	}
	public static HttpResponse postWithAuth(Context c, String url, String body, String contentType, String acceptType, HashMap<String, String>[] auth) {
		HttpResponse response = null;
		HttpClient httpclient = getHttpClient(null);
		HttpPost httppost = new HttpPost(url);
		try {
			StringEntity se = new StringEntity(body, "UTF-8");
			httppost.setEntity(se);
			httppost.setHeader(HEADER_ACCEPT, contentType);
			httppost.setHeader(CONTENT_TYPE, acceptType);
			for (Map.Entry<String, String> entry : auth[0].entrySet()) {
				httppost.setHeader(entry.getKey(), entry.getValue());
			}
			response = httpclient.execute(httppost);
			if (response.getStatusLine().getStatusCode() == 401) {
				String token = refreshToken(c, url, contentType, acceptType);
				httppost.setHeader("Authorization", "Bearer " + token);
				response = httpclient.execute(httppost);
			}
		} catch (Exception e) {
			Log.e(TAG, "Error POST", e);
		}
		return response;
	}

	public static HttpResponse postWithAuth(Context c, String url, String body, String contentType, String acceptType) {
		HttpResponse response = null;
		HttpClient httpclient = getHttpClient(null);
		HttpPost httppost = new HttpPost(url);
		try {

			StringEntity se = new StringEntity(body, "UTF-8");
			httppost.setEntity(se);
			httppost.setHeader(HEADER_ACCEPT, contentType);
			httppost.setHeader(CONTENT_TYPE, acceptType);
			/*
			 * for (Map.Entry<String, String> entry : auth[0].entrySet()) {
			 * httppost.setHeader(entry.getKey(), entry.getValue()); }
			 */
			httppost.setHeader("Authorization", "bearer " + CryptoManager.getInstance(c).getPrefs().getString(ConstantData.TOKEN_CLOUD, ""));
			response = httpclient.execute(httppost);
			if (response.getStatusLine().getStatusCode() == 401) {
				String token = refreshToken(c, url, contentType, acceptType);
				httppost.setHeader("Authorization", "Bearer " + token);
				response = httpclient.execute(httppost);
			}
			Log.d("result", response.getStatusLine().getStatusCode() + "");
		} catch (Exception e) {
			Log.e(TAG, "Error POST", e);
		}
		return response;
	}

	public static HttpResponse putWithAuth(Context c, String url, String body, String contentType, String acceptType) {
		HttpResponse response = null;
		HttpClient httpclient = getHttpClient(null);
		HttpPut httppost = new HttpPut(url);
		try {

			StringEntity se = new StringEntity(body, "UTF-8");
			httppost.setEntity(se);
			httppost.setHeader(HEADER_ACCEPT, contentType);
			httppost.setHeader(CONTENT_TYPE, acceptType);
			/*
			 * for (Map.Entry<String, String> entry : auth[0].entrySet()) {
			 * httppost.setHeader(entry.getKey(), entry.getValue()); }
			 */
			httppost.setHeader("Authorization", "bearer " + CryptoManager.getInstance(c).getPrefs().getString(ConstantData.TOKEN_CLOUD, ""));
			response = httpclient.execute(httppost);
			if (response.getStatusLine().getStatusCode() == 401) {
				String token = refreshToken(c, url, contentType, acceptType);
				httppost.setHeader("Authorization", "Bearer " + token);
				response = httpclient.execute(httppost);
			}
			Log.d("result", response.getStatusLine().getStatusCode() + "");
		} catch (Exception e) {
			Log.e(TAG, "Error PUT", e);
		}
		return response;
	}

	private static String refreshToken(Context c, String url, String contentType, String acceptType) throws Exception {
		url = UrlUtil.getTokenUrlForCloud();
		HttpPost httppost = new HttpPost(url);
		httppost.setEntity(new StringEntity(getTokenRequest()));
		httppost.setHeader(HEADER_ACCEPT, contentType);
		httppost.setHeader(CONTENT_TYPE, acceptType);
		HttpResponse response = new DefaultHttpClient().execute(httppost);
		Gson gson = new Gson();
		TokenModel token = gson.fromJson(EntityUtils.toString(response.getEntity()), TokenModel.class);
		CryptoManager.getInstance(c).getPrefs().edit().putString(ConstantData.TOKEN_CLOUD, token.getAccess_token()).commit();
		return token.getAccess_token();

	}

	public static HttpResponse put(String url, String body, String contentType, String acceptType, HttpParams httpParams) {
		HttpResponse response = null;
		HttpClient httpclient = getHttpClient(httpParams);
		HttpPut httpput = new HttpPut(url);
		try {
			if (body != null) {
				StringEntity se = new StringEntity(body, "UTF-8");
				httpput.setEntity(se);
			}
			httpput.setHeader(CONTENT_TYPE, contentType);
			httpput.setHeader(HEADER_ACCEPT, acceptType);
			response = httpclient.execute(httpput);
		} catch (Exception e) {
			Log.e(TAG, "Error ", e);
		}
		return response;
	}

	public static HttpResponse get(String url, String acceptType, HttpParams httpParams) {
		HttpResponse response = null;
		HttpClient httpclient = new DefaultHttpClient(getHttpParams());
		HttpGet httpget = new HttpGet(url);
		try {
			httpget.setHeader(HEADER_ACCEPT, acceptType);
			response = httpclient.execute(httpget);
		} catch (Exception e) {
			Log.e(TAG, "Error ", e);
		}
		return response;
	}

	public static HttpResponse getWithAuth(Context c, String url, String acceptType) {
		HttpResponse response = null;
		HttpClient httpclient = new DefaultHttpClient(getHttpParams());
		HttpGet httpget = new HttpGet(url);
		try {
			httpget.setHeader(HEADER_ACCEPT, acceptType);
			httpget.setHeader("Authorization", "bearer " + CryptoManager.getInstance(c).getPrefs().getString(ConstantData.TOKEN_CLOUD, ""));
			response = httpclient.execute(httpget);
			if (response.getStatusLine().getStatusCode() == 401) {
				String token = refreshToken(c, url, CONTENT_TYPE, acceptType);
				httpget.setHeader("Authorization", "Bearer " + token);
				response = httpclient.execute(httpget);
			}
		} catch (Exception e) {
			Log.e(TAG, "Error ", e);
		}
		return response;
	}

	public static HttpResponse getWithAuth(Context c, String url, String acceptType, HashMap<String, String>[] auth) {
		HttpResponse response = null;
		HttpClient httpclient = new DefaultHttpClient(getHttpParams());
		HttpGet httpget = new HttpGet(url);
		try {
			httpget.setHeader(HEADER_ACCEPT, acceptType);
			for (Map.Entry<String, String> entry : auth[0].entrySet()) {
				httpget.setHeader(entry.getKey(), entry.getValue());
			}
			response = httpclient.execute(httpget);
			if (response.getStatusLine().getStatusCode() == 401) {
				String token = refreshToken(c, url, CONTENT_TYPE, acceptType);
				httpget.setHeader("Authorization", "Bearer " + token);
				response = httpclient.execute(httpget);
			}
		} catch (Exception e) {
			Log.e(TAG, "Error ", e);
		}
		return response;
	}
	private static HttpParams getHttpParams() {
		HttpParams httpParameters = new BasicHttpParams();
		// Set the timeout in milliseconds until a connection is established.
		// The default value is zero, that means the timeout is not used.
		int timeoutConnection = 2 * 60 * 1000;
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		// Set the default socket timeout (SO_TIMEOUT)
		// in milliseconds which is the timeout for waiting for data.
		int timeoutSocket = 2 * 60 * 1000;
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		return httpParameters;

	}

	private static HttpClient getHttpClient(HttpParams httpParams) {
		if (httpParams != null) {
			return new DefaultHttpClient(httpParams);
		}
		return new DefaultHttpClient();
	}
}
