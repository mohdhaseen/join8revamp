package com.join8.facebook;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashSet;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;
import com.join8.facebook.FacebookUser.WallPost;
import com.join8.facebook.SessionEvents.AuthListener;
import com.join8.facebook.SessionEvents.LogoutListener;

public class FacebookManager {

	private static final String TAG = FacebookManager.class.getSimpleName();
	private static FacebookManager instance = null;
	private Facebook facebook = null;
	private Facebook facebookapp = null;
	private Context context;
	private String[] permissions;
	private Handler mHandler;
	private Activity activity;
	private SessionListener mSessionListener;
	private FacebookUser mFacebookUser;
	private JSONObject metadataObject;
	private FacebookUser mFacebookEuro2012User;
	private HashSet<FacebookGraphAPIRequestListener> mGraphAPIListeners;

	private enum Event {
		FACEBOOK_USER_INFO_RECEIVED, FACEBOOK_EURO2012_WALL_POST_RECEIVED, FACEBOOK_POST_SENT, FACEBOOK_LIKE_DONE, FACEBOOK_PHOTO_UPLOADED, FACEBOOK_LOGGING_OUT_BEGIN, FACEBOOK_LOGGING_OUT_END, FACEBOOK_AUTH_FAILED, FACEBOOK_FREIND_RECIEVED
	}

	private static String EURO2012_ID = "uefaeuro2012";

	private FacebookManager(String appId, String[] permissions) {
		this.facebookapp = new Facebook(appId);
		this.facebook = new Facebook(appId);
		this.mGraphAPIListeners = new HashSet<FacebookManager.FacebookGraphAPIRequestListener>();
		this.mSessionListener = new SessionListener();

		SessionEvents.addAuthListener(mSessionListener);
		SessionEvents.addLogoutListener(mSessionListener);

		this.mFacebookEuro2012User = new FacebookUser();
		this.permissions = permissions;
	}

	public static FacebookManager getInstance(Object... optionnalParams) {
		if (instance == null)
			instance = new FacebookManager((String) optionnalParams[0], (String[]) optionnalParams[1]);
		return instance;
	}

	public void init(Context context) {
		// restore session if one exists
		SessionStore.restore(facebook, context);
		this.context = context;
		this.activity = (Activity) context;
		this.mHandler = new Handler();
	}

	public void login() {
		if (!facebook.isSessionValid()) {
			// facebook.authorize(this.activity, this.permissions,new
			// LoginDialogListener());
			facebook.authorize(this.activity, this.permissions, Facebook.FORCE_DIALOG_AUTH, new LoginDialogListener());
		}
	}

	public void logout() {
		SessionEvents.onLogoutBegin();
		AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(this.facebook);
		asyncRunner.logout(this.context, new LogoutRequestListener());
	}

	// public String getFacebookAppAccessToken(){
	// return Utility.getAppToken(Constants.FACEBOOK_APP_ID,
	// Constants.FACEBOOK_APP_SECRET);
	// }

	public void getUserInfo() {
		if (facebook.isSessionValid()) {
			
			AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(this.facebook);
			Bundle params = new Bundle();
			params.putString("fields", "name,first_name,last_name,picture,email");
			asyncRunner.request("me", params, new UserInfoRequestListener());
			// asyncRunner.request("me", new UserInfoRequestListener());
		} else {
			login();
		}
	}

	

	public void postMessageOnWall(final String msg) {
		if (facebook.isSessionValid()) {
			AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(this.facebook);
			String graphPath = "me/feed";
			Bundle params = new Bundle();
			params.putString("message", msg);
			// new SendFacebookPostRequestTask(graphPath, params).execute();
			asyncRunner.request(graphPath, params, "POST", new PostRequestListener(), null);
			notifySucribers(Event.FACEBOOK_POST_SENT);
			// try {
			// String response = facebook.request(graphPath, params, "POST");
			// System.out.println(response);
			// } catch (IOException e) {
			// e.printStackTrace();
			// }
			// facebook.dialog(context, graphPath, params, new
			// PostDialogListener());
		} else {

			SessionEvents.AuthListener listener = new SessionEvents.AuthListener() {

				@Override
				public void onAuthSucceed() {
					postMessageOnWall(msg);
				}

				@Override
				public void onAuthFail(String error) {

				}
			};
			SessionEvents.addAuthListener(listener);

			login();
		}
	}

	public void shareCommentOnPost(final String postId, final String message) {
		if (facebook.isSessionValid()) {
			AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(this.facebook);
			String graphPath = postId + "/comments";
			Bundle params = new Bundle();
			params.putString("message", message);
			// new SendFacebookPostRequestTask(graphPath, params).execute();
			asyncRunner.request(graphPath, params, "POST", new PostRequestListener(), null);
			notifySucribers(Event.FACEBOOK_POST_SENT);
			// try {
			// String response = facebook.request(graphPath, params, "POST");
			// System.out.println(response);
			// } catch (IOException e) {
			// e.printStackTrace();
			// }
			// facebook.dialog(context, graphPath, params, new
			// PostDialogListener());
		} else {

			SessionEvents.AuthListener listener = new SessionEvents.AuthListener() {

				@Override
				public void onAuthSucceed() {
					shareCommentOnPost(postId, message);
				}

				@Override
				public void onAuthFail(String error) {

				}
			};
			SessionEvents.addAuthListener(listener);

			login();
		}
	}

	public void likePost(final String postId) {
		if (facebook.isSessionValid()) {
			AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(this.facebook);
			String graphPath = postId + "/likes";
			asyncRunner.request(graphPath, new Bundle(), "POST", new LikeRequestListener(), null);
			// new SendFacebookPostRequestTask(graphPath, new
			// Bundle()).execute();
			// notifySucribers(Event.FACEBOOK_LIKE_DONE);
			// try {
			// String response = facebook.request(graphPath, new Bundle(),
			// "POST");
			// System.out.println(response);
			// } catch (IOException e) {
			// e.printStackTrace();
			// }
		} else {
			SessionEvents.AuthListener listener = new SessionEvents.AuthListener() {

				@Override
				public void onAuthSucceed() {
					likePost(postId);
				}

				@Override
				public void onAuthFail(String error) {

				}
			};
			SessionEvents.addAuthListener(listener);

			login();
		}
	}

	public void shareLinkOnWall(final String message, final String url) {
		if (facebook.isSessionValid()) {
			AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(this.facebook);
			String graphPath = "me/feed";
			Bundle params = new Bundle();
			params.putString("message", message);
			params.putString("link", url);
			// new SendFacebookPostRequestTask(graphPath, params).execute();
			asyncRunner.request(graphPath, params, "POST", new PostRequestListener(), null);
			notifySucribers(Event.FACEBOOK_POST_SENT);
			// try {
			// String response = facebook.request(graphPath, params, "POST");
			// System.out.println(response);
			// } catch (IOException e) {
			// e.printStackTrace();
			// }
			// facebook.dialog(context, graphPath, params, new
			// PostDialogListener());
		} else {

			SessionEvents.AuthListener listener = new SessionEvents.AuthListener() {

				@Override
				public void onAuthSucceed() {
					shareLinkOnWall(message, url);
				}

				@Override
				public void onAuthFail(String error) {

				}
			};
			SessionEvents.addAuthListener(listener);

			login();
		}
	}

	public void sharePictureOnWall(final String message, final Bitmap bitmap) {
		if (facebook.isSessionValid()) {
			AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(this.facebook);
			// String graphPath = "me/feed";
			Bundle params = new Bundle();
			params.putString("caption", message);
			params.putString("method", "photos.upload");

			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

			byte[] imageInByte = stream.toByteArray();
			params.putByteArray("picture", imageInByte);

			// URL uploadFileUrl = null;
			// try {
			// uploadFileUrl = new URL(
			// "http://www.facebook.com/images/devsite/iphone_connect_btn.jpg");
			// } catch (MalformedURLException e) {
			// e.printStackTrace();
			// }
			// try {
			// HttpURLConnection conn=
			// (HttpURLConnection)uploadFileUrl.openConnection();
			// conn.setDoInput(true);
			// conn.connect();
			// int length = conn.getContentLength();
			//
			// byte[] imgData =new byte[length];
			// InputStream is = conn.getInputStream();
			// is.read(imgData);
			// params.putByteArray("picture", imgData);
			//
			// } catch (IOException e) {
			// e.printStackTrace();
			// }

			// asyncRunner.request(null, params, "POST", new
			// SampleUploadListener(), null);

			// new SendFacebookPostRequestTask(graphPath, params).execute();
			asyncRunner.request(null, params, "POST", new SampleUploadListener(), null);
			notifySucribers(Event.FACEBOOK_PHOTO_UPLOADED);
			// try {
			// String response = facebook.request(graphPath, params, "POST");
			// System.out.println(response);
			// } catch (IOException e) {
			// e.printStackTrace();
			// }
			// facebook.dialog(context, graphPath, params, new
			// PostDialogListener());
		} else {

			SessionEvents.AuthListener listener = new SessionEvents.AuthListener() {

				@Override
				public void onAuthSucceed() {
					sharePictureOnWall(message, bitmap);
				}

				@Override
				public void onAuthFail(String error) {

				}
			};
			SessionEvents.addAuthListener(listener);

			login();
		}

	}

	public class PostRequestListener extends BaseRequestListener {
		public void onComplete(String response, final Object state) {
			// callback should be run in the original thread,
			// not the background thread

			mHandler.post(new Runnable() {
				public void run() {
					// Toast.makeText(context, "Comment posted !",
					// Toast.LENGTH_LONG).show();
				}
			});
			notifySucribers(Event.FACEBOOK_POST_SENT);
		}
	}

	public class LikeRequestListener extends BaseRequestListener {
		public void onComplete(String response, final Object state) {
			// callback should be run in the original thread, not the background
			// thread

			mHandler.post(new Runnable() {
				public void run() {
					// Toast.makeText(context, "You liked it!",
					// Toast.LENGTH_LONG).show();
				}
			});
			notifySucribers(Event.FACEBOOK_LIKE_DONE);
		}
	}

	public class SampleUploadListener extends BaseRequestListener {

		public void onComplete(final String response, final Object state) {

			try {
				// process the response here: (executed in background thread)

				JSONObject json = Util.parseJson(response);
				final String src = json.getString("src");

				// then post the processed result back to the UI thread
				// if we do not do this, an runtime exception will be generated
				// e.g. "CalledFromWrongThreadException: Only the original
				// thread that created a view hierarchy can touch its views."

				mHandler.post(new Runnable() {
					public void run() {
						// Toast.makeText(context,
						// "Your photo has been uploaded at \n" + src,
						// Toast.LENGTH_LONG).show();
					}
				});
				notifySucribers(Event.FACEBOOK_PHOTO_UPLOADED);
			} catch (JSONException e) {

			} catch (FacebookError e) {

			}
		}
	}

	private final class LoginDialogListener implements DialogListener {
		public void onComplete(Bundle values) {
			SessionEvents.onLoginSuccess();
		}

		public void onFacebookError(FacebookError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onError(DialogError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onCancel() {
			SessionEvents.onLoginError("Action Canceled");
		}
	}

	public class LogoutRequestListener extends BaseRequestListener {
		public void onComplete(String response, final Object state) {
			// callback should be run in the original thread,
			// not the background thread
			mHandler.post(new Runnable() {
				public void run() {
					SessionEvents.onLogoutFinish();
				}
			});
		}
	}

	private class SessionListener implements AuthListener, LogoutListener {

		public void onAuthSucceed() {
			SessionStore.save(facebook, context);
			getUserInfo();
		}

		public void onAuthFail(String error) {
			notifySucribers(Event.FACEBOOK_AUTH_FAILED);
		}

		public void onLogoutBegin() {
			notifySucribers(Event.FACEBOOK_LOGGING_OUT_BEGIN);
		}

		public void onLogoutFinish() {
			SessionStore.clear(context);
			mFacebookUser = null;

			notifySucribers(Event.FACEBOOK_LOGGING_OUT_END);
		}
	}

	public Facebook getFacebook() {
		return this.facebook;
	}

	public String[] getPermission() {
		return this.permissions;
	}

	public FacebookUser getUser() {
		return this.mFacebookUser;
	}

	public FacebookUser getEuro2012User() {
		return this.mFacebookEuro2012User;
	}

	public class UserInfoRequestListener extends BaseRequestListener {
		public void onComplete(String response, final Object state) {

			try {

				// process the response here: executed in background thread
				JSONObject json = Util.parseJson(response);

				mFacebookUser = new FacebookUser();

				mFacebookUser.setUserId(json.getString("id"));
				mFacebookUser.setName(json.getString("name"));
				mFacebookUser.setFirstName(json.getString("first_name"));
				mFacebookUser.setLastName(json.getString("last_name"));
				mFacebookUser.setPicUrl(json.getJSONObject("picture").getJSONObject("data").getString("url"));
				mFacebookUser.setmEmail(json.getString("email"));

				notifySucribers(Event.FACEBOOK_USER_INFO_RECEIVED);

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (FacebookError e) {
				e.printStackTrace();
			}

		}
	}

	public class PageRequestListener extends BaseRequestListener {
		public void onComplete(String response, final Object state) {

			try {

				// process the response here: executed in background thread
				JSONObject json = Util.parseJson(response);
				// JSONArray array = new JSONArray(response);
				JSONArray array = json.getJSONArray("data");
				for (int i = 0; i < array.length(); i++) {
					JSONObject row = array.getJSONObject(i);
					// i(TAG,"row : "+row);
					String id = row.getString("id");

					String name = null;
					String message = null;
					String created_time = null;
					String pictureUrl = null;
					int commentCount = 0;
					int likeCount = 0;

					if (row.has("name")) {
						name = row.getString("name");
					}

					if (row.has("message")) {
						message = row.getString("message");
					}

					if (row.has("created_time")) {
						created_time = row.getString("created_time");
					}

					if (row.has("picture")) {
						pictureUrl = row.getString("picture");

					}

					if (row.has("comments")) {
						JSONObject commentsJson = row.getJSONObject("comments");
						commentCount = commentsJson.getInt("count");
						// JSONArray commentsArray = json.getJSONArray("data");
						// for (int j = 0; j < commentsArray.length(); j++) {
						// JSONObject commentRow =
						// commentsArray.getJSONObject(j);
						// commentCount = commentRow.getInt("count");
						// }
					}

					if (row.has("likes")) {
						JSONObject likesJson = row.getJSONObject("likes");
						likeCount = likesJson.getInt("count");
						// JSONArray likesArray = json.getJSONArray("data");
						// for (int j = 0; j < likesArray.length(); j++) {
						// JSONObject likeRow = likesArray.getJSONObject(j);
						// likeCount = likeRow.getInt("count");
						// }
					}

					if (message.length() > 0 && name.length() > 0) {
						WallPost wallPost = mFacebookEuro2012User.new WallPost(id, name, message, stringToDateHelper(created_time), pictureUrl,
								likeCount, commentCount);
						mFacebookEuro2012User.addWallPost(id, wallPost);
					}
				}

				notifySucribers(Event.FACEBOOK_EURO2012_WALL_POST_RECEIVED);

			} catch (JSONException e) {

				// showError(getString(R.string.general_error));
			} catch (FacebookError e) {

				// showError(getString(R.string.general_error));
			}

		}
	}

	public interface FacebookGraphAPIRequestListener {
	}

	public interface FacebookSessionEventListener extends FacebookGraphAPIRequestListener {
		public void OnFacebookAuthFailed();

		public void OnFacebookUserInfoReceived();

		public void onFacebookFriendListReceived();

		public void OnFacebookLoggingOutBegin();

		public void OnFacebookLoggingOutEnd();
	}

	public interface FacebookEuro2012EventListener extends FacebookGraphAPIRequestListener {
		public void OnFacebookEuro2012WallPostReceived();

		public void OnFacebookPostSent();

		public void OnFacebookLikeDone();

		public void OnFacebookPhotoUploaded();
	}

	/**
	 * Register an news listener with this manager
	 * 
	 * @param listener
	 *            - the News to register
	 */
	public void addListener(FacebookGraphAPIRequestListener listener) {
		mGraphAPIListeners.add(listener);
	}

	/**
	 * Unregister an News listener
	 * 
	 * @param listener
	 *            - the listener to unregister
	 */
	public void removeListener(FacebookGraphAPIRequestListener listener) {
		mGraphAPIListeners.remove(listener);
	}

	public void notifySucribers(Event event, Object... optionalParams) {
		
		synchronized (mGraphAPIListeners) {
			try {
				for (FacebookGraphAPIRequestListener subscriber : mGraphAPIListeners) {
					FacebookGraphAPIRequestListener listener;

					switch (event) {

					case FACEBOOK_USER_INFO_RECEIVED:
						listener = (FacebookGraphAPIRequestListener) subscriber;
						if (listener instanceof FacebookSessionEventListener) {
							((FacebookSessionEventListener) listener).OnFacebookUserInfoReceived();
						}
						break;

					case FACEBOOK_AUTH_FAILED:
						listener = (FacebookGraphAPIRequestListener) subscriber;
						if (listener instanceof FacebookSessionEventListener) {
							((FacebookSessionEventListener) listener).OnFacebookAuthFailed();
						}
						break;

					case FACEBOOK_LOGGING_OUT_BEGIN:
						listener = (FacebookGraphAPIRequestListener) subscriber;
						if (listener instanceof FacebookSessionEventListener) {
							((FacebookSessionEventListener) listener).OnFacebookLoggingOutBegin();
						}
						break;

					case FACEBOOK_LOGGING_OUT_END:
						listener = (FacebookGraphAPIRequestListener) subscriber;
						if (listener instanceof FacebookSessionEventListener) {
							((FacebookSessionEventListener) listener).OnFacebookLoggingOutEnd();
						}
						break;

					case FACEBOOK_EURO2012_WALL_POST_RECEIVED:
						listener = (FacebookGraphAPIRequestListener) subscriber;
						if (listener instanceof FacebookEuro2012EventListener) {
							((FacebookEuro2012EventListener) listener).OnFacebookEuro2012WallPostReceived();
						}
						break;

					case FACEBOOK_POST_SENT:
						listener = (FacebookGraphAPIRequestListener) subscriber;
						if (listener instanceof FacebookEuro2012EventListener) {
							((FacebookEuro2012EventListener) listener).OnFacebookPostSent();
						}
						break;

					case FACEBOOK_LIKE_DONE:
						listener = (FacebookGraphAPIRequestListener) subscriber;
						if (listener instanceof FacebookEuro2012EventListener) {
							((FacebookEuro2012EventListener) listener).OnFacebookLikeDone();
						}
						break;

					case FACEBOOK_PHOTO_UPLOADED:
						listener = (FacebookGraphAPIRequestListener) subscriber;
						if (listener instanceof FacebookEuro2012EventListener) {
							((FacebookEuro2012EventListener) listener).OnFacebookPhotoUploaded();
						}
						break;
					case FACEBOOK_FREIND_RECIEVED:
						listener = (FacebookGraphAPIRequestListener) subscriber;
						if (listener instanceof FacebookSessionEventListener) {
							((FacebookSessionEventListener) listener).onFacebookFriendListReceived();
						}
						break;
					default:
						break;
					}
				}
			} catch (ConcurrentModificationException e) {
				e.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}

	public Date stringToDateHelper(String dateString) {
		// facebook date are 2011-03-03T00:02:15+0000
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss+SSSS");
		Date date = null;
		try {
			date = df.parse(dateString);
			// String newDateString = df.format(startDate);
			// System.out.println(newDateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// Calendar calendar =
		// Calendar.getInstance(TimeZone.getTimeZone("India"),Locale.getDefault());
		//
		// calendar.set(Calendar.DATE,date.getDate());
		// calendar.set(Calendar.MONTH,date.getMonth());
		// calendar.set(Calendar.YEAR, date.getYear()+1900);
		// calendar.set(Calendar.HOUR,date.getHours());
		// calendar.set(Calendar.MINUTE,date.getMinutes());
		// calendar.set(Calendar.SECOND,date.getSeconds());

		// final long current = System.currentTimeMillis();
		// final long update = calendar.getTimeInMillis();
		// final long timeago = Math.abs(current-update);

		Calendar calendar = Calendar.getInstance();
		TimeZone timezone = TimeZone.getDefault();
		TimeZone utcTimeZone = TimeZone.getTimeZone("UTC");

		int currentGMTOffset = timezone.getOffset(calendar.getTimeInMillis());
		int gmtOffset = utcTimeZone.getOffset(calendar.getTimeInMillis());

		calendar.setTimeInMillis(calendar.getTimeInMillis() + (gmtOffset - currentGMTOffset));

		final long current = calendar.getTimeInMillis();
		final long update = date.getTime();
		final long timeago = Math.abs(current - update);

		return date;

	}

	
	public void postToFriendWall(String uid, String targetId) {

		AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(this.facebook);
		if (facebook.isSessionValid()) {

			try {
				
				
				Log.d("psot", "post wall"+targetId);
				JSONArray jArray = new JSONArray();
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("text", "Download");
				jsonObject.put("href", "http://photos-g.ak.fbcdn.net/photos-ak-snc1/v43/185/283202920409/app_2_283202920409_2326.gif");
				jArray.put(jsonObject);
				Bundle parameters = new Bundle();

				parameters.putString("message", "Dial7 App!!");

				String href = "http://google.co.in";

				String logo = "http://ec2-107-20-68-200.compute-1.amazonaws.com/css/images/icon.png";
				String name = "Name";
				String caption = "Caption";
				String description = "Dial7";

				parameters.putString("attachment", "{\"name\":\"" + name + "\",\"href\":\"" + href + "\",\"caption\":\"" + caption
						+ "\",\"description\":\"" + description + "\",\"media\":[{\"type\":\"image\",\"src\":\"" + logo + "\",\"href\":\"" + logo
						+ "\"}]}");

				parameters.putString("auto_publish", "true");
				parameters.putString("method", "stream.publish");
				parameters.putString("target_id", String.valueOf(targetId));
				parameters.putString("uid", uid);
				parameters.putString("action_links", jArray.toString());
				Log.d("parameter", parameters.toString());

				String response = facebook.request(parameters);
				asyncRunner.request(response, new PostRequestListener());

				Log.d("post result", response);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

}
