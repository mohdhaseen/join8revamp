package com.join8;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.join8.fragments.BookingTableListFragment;
import com.join8.utils.TypefaceUtils;

public class SelectTableActivity extends ActionBarActivity implements OnClickListener {

	private ImageView imgBack;
	private TextView txtActionTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.container);

		Bundle bundle = new Bundle();
		bundle.putString("companyId", getIntent().getStringExtra("companyId"));

		BookingTableListFragment fragment = new BookingTableListFragment();
		fragment.setArguments(bundle);
		getSupportFragmentManager().beginTransaction().replace(R.id.frmContainer, fragment).addToBackStack(null).commit();
		initActionBar();
	}

	private void initActionBar() {

		getSupportActionBar().setHomeButtonEnabled(false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		getSupportActionBar().setDisplayUseLogoEnabled(false);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);

		View mActionbarView = getLayoutInflater().inflate(R.layout.actionview, (ViewGroup) findViewById(R.id.frmContainer), false);
		imgBack = (ImageButton) mActionbarView.findViewById(R.id.btn_back);
		imgBack.setOnClickListener(this);
		txtActionTitle = (TextView) mActionbarView.findViewById(R.id.txtTitle);
		txtActionTitle.setText(getIntent().getStringExtra("restaurantName"));
		getSupportActionBar().setCustomView(mActionbarView);
		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(this);
		mTypefaceUtils.applyTypeface(txtActionTitle);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_back) {
			if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
				finish();
			} else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
				getSupportFragmentManager().popBackStack();
			}
		}
	}

	@Override
	public void onBackPressed() {

		if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
			finish();
		} else {
			super.onBackPressed();
		}
	}

	public void setCustomTitle(String title) {
		txtActionTitle.setText(title);
	}
}
