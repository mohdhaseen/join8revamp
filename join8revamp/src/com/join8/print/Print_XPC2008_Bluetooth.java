package com.join8.print;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.model.BookingData;
import com.join8.model.FoodOptionItemData;
import com.join8.model.MenuItemData;
import com.join8.model.OrderData;
import com.join8.utils.ConstantData;
import com.join8.utils.Utils;

public class Print_XPC2008_Bluetooth {

	private Context mContext = null;
	private BluetoothService mService;
	private int charPerLine = 24;
	private SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.getDefault());
	private SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.getDefault());
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
	private SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());

	public Print_XPC2008_Bluetooth(Context mContext, BluetoothService mService) {
		this.mContext = mContext;
		this.mService = mService;
	}

	public void printBooking(BookingData data) {

		try {

			// Check that we're actually connected before trying
			// anything
			if (mService.getState() != BluetoothService.STATE_CONNECTED) {
				Toast.displayText(mContext, mContext.getString(R.string.not_connected));
				return;
			}

			String orderState = null;

			switch (data.getBookingStatus()) {
				case 10 :
					orderState = mContext.getString(R.string.pending);
					break;
				case 20 :
					orderState = mContext.getString(R.string.confirmed);
					break;
				case 30 :
					orderState = mContext.getString(R.string.cancelled);
					break;
				case 50 :
					orderState = mContext.getString(R.string.rejected);
					break;
				case 70 :
					orderState = mContext.getString(R.string.pickuped);
					break;
				case 80 :
					orderState = mContext.getString(R.string.delivered);
					break;
			}

			goToNextLine();
			setFontSizeNormal();
			printCenter(mContext.getString(R.string.booking_no) + " " + data.getTableBookingId() + " <<" + orderState + ">>");

			setFontSizeBig();
			goToNextLine();
			printCenter(dateFormat.format(data.getDateToBook()));
			printCenter(timeFormat.format(data.getDateToBook()));

			setFontSizeNormal();
			goToNextLine();
			printLeft(mContext.getString(R.string.number_of_people) + " " + data.getSeats());
			printLeft(mContext.getString(R.string.customer) + " " + data.getFirstName() + " " + data.getLastName());
			printLeft(mContext.getString(R.string.phone) + " " + data.getMobileNumber());

			printSeparatorLine();
			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				printLeft(mContext.getString(R.string.table_type_print) + ": " + data.getBookingNameEn());
			} else {
				printLeft(mContext.getString(R.string.table_type_print) + ": " + data.getBookingName());
			}
			printLeft(mContext.getString(R.string.deposit) + ": $" + Utils.formatDecimal("" + data.getDeposit()));

			if (!TextUtils.isEmpty(data.getRemarks())) {
				printLeft(mContext.getString(R.string.remarks_for_print) + " " + data.getRemarks());
			}
			goToNextLine();
			setFontSizeSmall();
			printCenter("www.join8.com 下載外賣app");
			goToNextLine();
			goToNextLine();
			goToNextLine();
			goToNextLine();
			resetPrinter();
			paperCut();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void printOrder(OrderData data, int copyNo) {

		try {

			String orderState = null;

			switch (data.getOrderStatus()) {
				case 10 :
					orderState = mContext.getString(R.string.pending);
					break;
				case 20 :
					orderState = mContext.getString(R.string.confirmed);
					break;
				case 30 :
					orderState = mContext.getString(R.string.cancelled);
					break;
				case 50 :
					orderState = mContext.getString(R.string.rejected);
					break;
				case 70 :
					orderState = mContext.getString(R.string.pickuped);
					break;
				case 80 :
					orderState = mContext.getString(R.string.delivered);
					break;
			}

			resetPrinter();

			setFontSizeBig();
			printCenter(mContext.getString(R.string.order_no) + " " + data.getOrderId());

			setFontSizeNormal();
			printCenter("<<" + mContext.getString(R.string.take_out_order) + " " + copyNo + ": " + orderState + ">>");

			goToNextLine();
			goToNextLine();
			if (data.isOrderPaid()) {
				printCenter("<<" + mContext.getString(R.string.paid) + ">>");
			}

			printCenter(dateProcess(data.getOrderDatetime()));

			if (data.isIsOrderPickUpByUser()) {
				printCenter("*" + mContext.getString(R.string.pickup) + "*");
				printCenter("(" + dateProcess(data.getOrderDeadline()) + ")");
			} else {
				printCenter("*" + mContext.getString(R.string.deliver) + "*");
				printCenter("(" + dateProcess(data.getOrderDeadline()) + ")");
			}

			printSeparatorLine();

			printLeft(mContext.getString(R.string.customer) + " " + data.getFirstName() + " " + data.getLastName());
			printLeft(mContext.getString(R.string.phone) + " " + data.getMobileNumber());
			printLeft(mContext.getString(R.string.address_for_print) + " " + data.getDeliverAddress());
			goToNextLine();
			goToNextLine();

			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				printCenter(data.getCompanyNameEN());
			} else {
				printCenter(data.getCompanyName());
			}

			printSeparatorLine();

			for (int i = 0; i < data.getMenuItem().size(); i++) {

				MenuItemData menuItemData = data.getMenuItem().get(i);
				double itemPrice = menuItemData.getPrice();

				for (int j = 0; j < menuItemData.getFoodOption().size(); j++) {

					for (int k = 0; k < menuItemData.getFoodOption().get(j).getFoodOptionItem().size(); k++) {

						FoodOptionItemData foodOptionItemData = data.getMenuItem().get(i).getFoodOption().get(j).getFoodOptionItem().get(k);
						itemPrice += foodOptionItemData.getExtraPrice();
					}
				}

				printMenuItem(menuItemData.getItemName(), menuItemData.getItemName_EN(), menuItemData.getQuantity(), itemPrice);

				for (int j = 0; j < menuItemData.getFoodOption().size(); j++) {

					for (int k = 0; k < menuItemData.getFoodOption().get(j).getFoodOptionItem().size(); k++) {

						FoodOptionItemData foodOptionItemData = menuItemData.getFoodOption().get(j).getFoodOptionItem().get(k);

						if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
							printLeft("  --" + foodOptionItemData.getFoodOptionItemNameEn());
						} else {
							printLeft("  --" + foodOptionItemData.getFoodOptionItemName());
						}
					}
				}

				goToNextLine();
			}

			printSeparatorLine();
			printChargesAndTotal(data);
			if (!TextUtils.isEmpty(data.getRemarks())) {
				goToNextLine();
				printLeft(mContext.getString(R.string.remarks_for_print) + " " + data.getRemarks());
			}
			goToNextLine();
			goToNextLine();
			setFontSizeSmall();
			printCenter("www.join8.com 下載外賣app");
			goToNextLine();
			goToNextLine();
			goToNextLine();
			goToNextLine();
			resetPrinter();
			paperCut();

		} catch (Exception e) {
			e.printStackTrace();
			Toast.displayText(mContext, "[printOrder]: " + e.toString());
		}
	}

	private void printChargesAndTotal(OrderData orderData) {

		try {

			double total = 0.0, deliveryFee = 0;

			if (orderData.getMenuItem() != null) {

				for (int i = 0; i < orderData.getMenuItem().size(); i++) {

					MenuItemData menuItemData = orderData.getMenuItem().get(i);
					double itemPrice = menuItemData.getPrice();

					for (int j = 0; j < orderData.getMenuItem().get(i).getFoodOption().size(); j++) {

						for (int k = 0; k < orderData.getMenuItem().get(i).getFoodOption().get(j).getFoodOptionItem().size(); k++) {

							FoodOptionItemData foodOptionItemData = orderData.getMenuItem().get(i).getFoodOption().get(j).getFoodOptionItem().get(k);
							itemPrice += foodOptionItemData.getExtraPrice();
						}
					}

					total += menuItemData.getQuantity() * itemPrice;
				}
			}

			if (!orderData.isIsOrderPickUpByUser()) {

				if (orderData.getDeliveryOption() == 1) {

					deliveryFee = orderData.getDeliveryFee();

				} else if (orderData.getDeliveryOption() == 2) {

					deliveryFee = total * orderData.getDeliveryFee() / 100;
				}
			}

			printJustify(mContext.getString(R.string.shipping_cost), String.format("%.1f", deliveryFee));
			printJustify(mContext.getString(R.string.total), String.format("%.1f", deliveryFee + total));

		} catch (Exception e) {
			e.printStackTrace();
			Toast.displayText(mContext, "[printChargesAndTotal]: " + e.toString());
		}
	}

	private void printMenuItem(String itemName, String itemNameEn, int qty, double price) {

		String pText = null, temp1, temp2;

		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
			temp1 = itemNameEn;
		} else {
			temp1 = itemName;
		}

		temp2 = String.format(Locale.getDefault(), "% 2d", qty) + String.format(Locale.getDefault(), "% 6.1f", price);

		pText = justifyString(temp1, temp2, charPerLine);
		printCenter(pText);

		// addWaiting();

		// PrintLeft(itemNameEn);
	}

	// print space line
	private void goToNextLine() {
		byte SendCut[] = {0x1B, 0x64, 0x01};
		printfData(SendCut);
	}

	private boolean printfData(byte[] data) {

		if (mService != null && mService.getState() != BluetoothService.STATE_CONNECTED) {
			Toast.displayText(mContext, mContext.getString(R.string.not_connected));
			return false;
		}
		mService.write(data);
		return true;
	}

	private void setFontSizeSmall() {
		printfData(new byte[]{0x1D, 0x21, 0x00});
	}

	private void setFontSizeNormal() {
		printfData(new byte[]{0x1D, 0x21, 0x11});
	}

	private void setFontSizeBig() {
		printfData(new byte[]{0x1D, 0x21, 0x22});
	}

	private void resetPrinter() {
		byte SendCut[] = {0x1b, 0x40};
		printfData(SendCut);
		byte SendCut1[] = {0x0a};
		printfData(SendCut1);
	}

	private synchronized void printString(String str) {
		try {
			if (mService != null && mService.getState() != BluetoothService.STATE_CONNECTED) {
				Toast.displayText(mContext, mContext.getString(R.string.not_connected));
				return;
			}
			mService.write(str.getBytes("Big5"));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	private void printLeft(String str) {
		byte SendCut[] = {0x0a, 0x1b, 0x61, 0x00};
		if (printfData(SendCut)) {
			printString(str);
		}
	}

	private void printCenter(String str) {
		byte SendCut[] = {0x0a, 0x1b, 0x61, 0x01};
		if (printfData(SendCut)) {
			printString(str);
		}
	}

	private void printSeparatorLine() {
		printCenter("========================");
	}

	private void paperCut() {
		byte SendCut[] = {0x0a, 0x1d, 0x56, 0x01, 0x00};
		if (printfData(SendCut)) {
			Log.e("PrintUtils", "cut succeed...");
		} else {
			Log.e("PrintUtils", "cut failed...");
		}
	}

	private void printJustify(String message, String message2) {
		String temp = justifyString(message, message2, charPerLine);
		printCenter(temp);

	}

	private String dateProcess(String stringDate) {
		try {
			Date date = sourceFormat.parse(stringDate);
			return destFormat.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stringDate;
	}

	private String justifyString(String text1, String text2, int width) {
		String ptext = "";
		int byteCount1 = getByte(text1);
		int byteCount2 = getByte(text2);

		ptext = text1;

		if (byteCount1 + byteCount2 <= width) {

			for (int i = 0; i < width - (byteCount1 + byteCount2); i++) {
				ptext = ptext + " ";
			}
		} else {
			for (int i = 0; i < width - ((byteCount1 + byteCount2) % width); i++) {
				ptext = ptext + " ";
			}
		}

		ptext = ptext + text2;

		return ptext;
	}

	private int getByte(String text) {
		String temp;
		int bytecount = 0;
		for (int i = 0; i < text.length(); i++) {
			temp = text.substring(i, i + 1);
			if (temp.getBytes().length == 1)
				bytecount = bytecount + 1;
			else
				bytecount = bytecount + 2;
		}

		return bytecount;
	}
}
