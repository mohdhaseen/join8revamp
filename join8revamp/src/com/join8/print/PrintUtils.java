package com.join8.print;

import android.content.Context;
import android.content.SharedPreferences;

import com.join8.model.BookingData;
import com.join8.model.OrderData;

public class PrintUtils {

	private Context mContext;
	private BluetoothService mService;
	private SocketManager mSocketManager;

	public PrintUtils(Context c, BluetoothService mService, SocketManager mSocketManager) {

		mContext = c;
		this.mService = mService;
		this.mSocketManager = mSocketManager;
	}

	public void printOrder(OrderData orderData, int copyNo) {

		try {

			SharedPreferences mPreferences = mContext.getSharedPreferences(mContext.getPackageName(), Context.MODE_PRIVATE);

			int printerTypeposition = mPreferences.getInt("printerTypePosition", 0);

			if (printerTypeposition == 0 && mService != null) {
				new Print_GPU80161(mContext, mService).printOrder(orderData, copyNo);
			} else if (printerTypeposition == 1 && mSocketManager != null) {
				new Print_XPC2008(mContext, mSocketManager).printOrder(orderData, copyNo);
			} else if (printerTypeposition == 2 && mService != null) {
				new Print_XPP100(mContext, mService).printOrder(orderData, copyNo);
			} else if (printerTypeposition == 3 && mService != null) {
				new Print_GP76IN(mContext, mService).printOrder(orderData, copyNo);
			} else if (printerTypeposition == 4 && mService != null) {
				new Print_XPC2008_Bluetooth(mContext, mService).printOrder(orderData, copyNo);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void printBooking(BookingData bookingData) {

		try {

			SharedPreferences mPreferences = mContext.getSharedPreferences(mContext.getPackageName(), Context.MODE_PRIVATE);

			int printerTypeposition = mPreferences.getInt("printerTypePosition", 0);

			if (printerTypeposition == 0 && mService != null) {
				new Print_GPU80161(mContext, mService).printBooking(bookingData);
			} else if (printerTypeposition == 1 && mSocketManager != null) {
				new Print_XPC2008(mContext, mSocketManager).printBooking(bookingData);
			} else if (printerTypeposition == 2 && mService != null) {
				new Print_XPP100(mContext, mService).printBooking(bookingData);
			} else if (printerTypeposition == 3 && mService != null) {
				new Print_GP76IN(mContext, mService).printBooking(bookingData);
			} else if (printerTypeposition == 4 && mService != null) {
				new Print_XPC2008_Bluetooth(mContext, mService).printBooking(bookingData);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String wrap(final String str, final int wrapLength) {
		return wrap(str, wrapLength, null, false);
	}

	private static String wrap(final String str, int wrapLength, String newLineStr, final boolean wrapLongWords) {
		if (str == null) {
			return null;
		}
		if (newLineStr == null) {
			newLineStr = System.getProperty("line.separator");
		}
		if (wrapLength < 1) {
			wrapLength = 1;
		}
		final int inputLineLength = str.length();
		int offset = 0;
		final StringBuilder wrappedLine = new StringBuilder(inputLineLength + 32);

		while (inputLineLength - offset > wrapLength) {
			if (str.charAt(offset) == ' ') {
				offset++;
				continue;
			}
			int spaceToWrapAt = str.lastIndexOf(' ', wrapLength + offset);

			if (spaceToWrapAt >= offset) {
				// normal case
				wrappedLine.append(str.substring(offset, spaceToWrapAt));
				wrappedLine.append(newLineStr);
				offset = spaceToWrapAt + 1;

			} else {
				// really long word or URL
				if (wrapLongWords) {
					// wrap really long word one line at a time
					wrappedLine.append(str.substring(offset, wrapLength + offset));
					wrappedLine.append(newLineStr);
					offset += wrapLength;
				} else {
					// do not wrap really long word, just extend beyond limit
					spaceToWrapAt = str.indexOf(' ', wrapLength + offset);
					if (spaceToWrapAt >= 0) {
						wrappedLine.append(str.substring(offset, spaceToWrapAt));
						wrappedLine.append(newLineStr);
						offset = spaceToWrapAt + 1;
					} else {
						wrappedLine.append(str.substring(offset));
						offset = inputLineLength;
					}
				}
			}
		}

		// Whatever is left in line is short enough to just pass through
		wrappedLine.append(str.substring(offset));

		return wrappedLine.toString();
	}
}
