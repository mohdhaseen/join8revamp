package com.join8.print;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class BluetoothService {

	private static final String TAG = BluetoothService.class.getSimpleName();

	private static BluetoothService bluetoothService = null;

	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	// // Key names received from the BluetoothService Handler
	public static final String DEVICE_NAME = "device_name";
	public static final String DEVICE_ADDRESS = "device_address";
	public static final String TOAST = "toast";

	// Intent request codes
	private static final int REQUEST_CONNECT_DEVICE = 1;
	private static final int REQUEST_ENABLE_BT = 2;

	private BluetoothAdapter mAdapter;
	private Handler mHandler = null;
	private int mState;

	// Name for the SDP record when creating server socket
	private static final String NAME = "BTPrinter";
	// UUID must be this Unique UUID for this application
	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

	// Constants that indicate the current connection state
	public static final int STATE_NONE = 0; // we're doing nothing

	public static final int STATE_LISTEN = 1; // now listening for incoming
												// connections
	public static final int STATE_CONNECTING = 2; // now initiating an outgoing
													// connection
	public static final int STATE_CONNECTED = 3; // now connected to a remote
													// device

	// The local server socket
	private BluetoothServerSocket mServerSocket = null;

	private BluetoothSocket mSocket;

	private AcceptThread mAcceptThread;
	private ConnectThread mConnectThread;
	private ConnectedThread mConnectedThread;

	public static BluetoothService getInstance(Context context) {

		if (bluetoothService == null) {
			bluetoothService = new BluetoothService(context);
		}
		return bluetoothService;
	}

	private BluetoothService(Context context) {
		mAdapter = BluetoothAdapter.getDefaultAdapter();
		mState = STATE_NONE;
	}

	public void setHandler(Handler handler) {
		if (handler != null) {
			mHandler = handler;
		}
	}

	private synchronized void setState(int state) {
		Log.d(TAG, "setState() " + mState + " -> " + state);
		mState = state;

		if (mHandler != null) {
			mHandler.obtainMessage(MESSAGE_STATE_CHANGE, state, -1).sendToTarget();
		}
	}

	public synchronized int getState() {
		return mState;
	}

	public String getDeviceName() {
		if (mSocket != null) {
			return mSocket.getRemoteDevice().getName();
		}
		return "";
	}

	public Boolean isDeviceConnected() {
		if (mSocket != null) {
			try {
				mSocket.connect();
				return true;
			} catch (IOException e) {
				try {
					mSocket.close();
					mSocket = null;
					setState(STATE_NONE);
					return false;
				} catch (Exception e2) {
				}
				mSocket = null;
				setState(STATE_NONE);
				return false;
			}
		} else {
			mSocket = null;
			setState(STATE_NONE);
			return false;
		}
	}

	public synchronized void start() {

		// Cancel any thread attempting to make a connection
		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}

		// Cancel any thread currently running a connection
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}

		// Start the thread to listen on a BluetoothServerSocket
		if (mAcceptThread == null) {
			mAcceptThread = new AcceptThread();
			mAcceptThread.start();
		}
		setState(STATE_LISTEN);
	}

	public synchronized void connect(BluetoothDevice device) {

		// if (mSocket != null && device.equals(mSocket.getRemoteDevice())) {
		//
		// Log.e(TAG, "=============> bond state : " +
		// mSocket.getRemoteDevice().getBondState());
		//
		// connected(mSocket, device);
		// return;
		// }

		// Cancel any thread attempting to make a connection
		if (mState == STATE_CONNECTING) {
			if (mConnectThread != null) {
				mConnectThread.cancel();
				mConnectThread = null;
			}
		}

		// Cancel any thread currently running a connection
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}

		// Start the thread to connect with the given device
		mConnectThread = new ConnectThread(device);
		mConnectThread.start();
		setState(STATE_CONNECTING);
	}

	public synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {

		// Cancel the thread that completed the connection
		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}

		// Cancel any thread currently running a connection
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}

		// Cancel the accept thread because we only want to connect to one
		// device
		if (mAcceptThread != null) {
			mAcceptThread.cancel();
			mAcceptThread = null;
		}

		// Start the thread to manage the connection and perform transmissions
		mConnectedThread = new ConnectedThread();
		mConnectedThread.start();

		// Send the name of the connected device back to the UI Activity
		Message msg = mHandler.obtainMessage(MESSAGE_DEVICE_NAME);
		Bundle bundle = new Bundle();
		bundle.putString(DEVICE_NAME, device.getName());
		bundle.putString(DEVICE_ADDRESS, device.getAddress());
		msg.setData(bundle);
		mHandler.sendMessage(msg);

		setState(STATE_CONNECTED);
	}

	public synchronized void stop() {

		setState(STATE_NONE);

		if (mSocket != null) {
			try {
				mSocket.close();
			} catch (Exception e) {
			}
			mSocket = null;
		}

		if (mServerSocket != null) {
			try {
				mServerSocket.close();
			} catch (Exception e) {
			}
			mServerSocket = null;
		}

		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}
		if (mConnectedThread != null) {
			mConnectedThread.cancel();
			mConnectedThread = null;
		}
		if (mAcceptThread != null) {
			mAcceptThread.cancel();
			mAcceptThread = null;
		}
	}

	public void write(byte[] out) {
		// Create temporary object
		ConnectedThread r;
		// Synchronize a copy of the ConnectedThread
		synchronized (this) {
			if (mState != STATE_CONNECTED)
				return;
			r = mConnectedThread;
		}
		// Perform the write unsynchronized
		// r.write(new byte[]{27,64});
		r.write(out);
		try {
			Thread.sleep(80);
		} catch (Exception e) {
		}

		// r.write(new byte[]{10});
		// r.write(new byte[]{27,100,4});
	}

	private void connectionFailed() {
		setState(STATE_LISTEN);
		if (mHandler != null) {
			// Send a failure message back to the Activity
			Message msg = mHandler.obtainMessage(MESSAGE_TOAST);
			Bundle bundle = new Bundle();
			bundle.putString(TOAST, "Unable to connect device");
			msg.setData(bundle);
			mHandler.sendMessage(msg);
		}
	}

	/**
	 * Indicate that the connection was lost and notify the UI Activity.
	 */
	private void connectionLost() {
		// setState(STATE_LISTEN);
		if (mHandler != null) {
			// Send a failure message back to the Activity
			Message msg = mHandler.obtainMessage(MESSAGE_TOAST);
			Bundle bundle = new Bundle();
			bundle.putString(TOAST, "Device connection was lost");
			msg.setData(bundle);
			mHandler.sendMessage(msg);
		}
	}

	/**
	 * This thread runs while listening for incoming connections. It behaves
	 * like a server-side client. It runs until a connection is accepted (or
	 * until cancelled).
	 */
	private class AcceptThread extends Thread {

		public AcceptThread() {

			if (mServerSocket == null) {
				try {
					mServerSocket = mAdapter.listenUsingRfcommWithServiceRecord(NAME, MY_UUID);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		@Override
		public void run() {

			setName("AcceptThread");
			BluetoothSocket socket = null;

			// Listen to the server socket if we're not connected
			while (mState != STATE_CONNECTED) {
				try {
					socket = mServerSocket.accept();
				} catch (IOException e) {
					Log.e(TAG, "accept() failed", e);
					break;
				}

				// If a connection was accepted
				if (socket != null) {
					synchronized (BluetoothService.this) {
						switch (mState) {
						case STATE_LISTEN:
						case STATE_CONNECTING:
							// Situation normal. Start the connected thread.
							connected(socket, socket.getRemoteDevice());
							break;
						case STATE_NONE:
						case STATE_CONNECTED:
							// Either not ready or already connected. Terminate
							// new socket.
							try {
								socket.close();
							} catch (IOException e) {
								Log.e(TAG, "Could not close unwanted socket", e);
							}
							break;
						}
					}
				}
			}
		}

		public void cancel() {
			try {
				if (mServerSocket != null) {
					mServerSocket.close();
				}
			} catch (IOException e) {
				Log.e(TAG, "close() of server failed", e);
			}
		}
	}

	/**
	 * This thread runs while attempting to make an outgoing connection with a
	 * device. It runs straight through; the connection either succeeds or
	 * fails.
	 */
	private class ConnectThread extends Thread {

		BluetoothDevice mDevice = null;

		public ConnectThread(BluetoothDevice device) {
			this.mDevice = device;
		}

		@Override
		public void run() {
			Log.i(TAG, "BEGIN mConnectThread");
			setName("ConnectThread");

			// Always cancel discovery because it will slow down a connection
			mAdapter.cancelDiscovery();

			// if (mSocket != null && mDevice.equals(mSocket.getRemoteDevice()))
			// {
			// Log.e(TAG, "=============> bond state : " +
			// mSocket.getRemoteDevice().getBondState());
			// connected(mSocket, mDevice);
			// return;
			// }

			if (mSocket == null) {
				try {
					mSocket = createSocket(mDevice);
				} catch (Exception e) {
					connectionFailed();
					return;
				}
			}

			if (mSocket != null) {
				try {
					mSocket.connect();
				} catch (IOException e) {
					try {
						mSocket.close();
						mSocket = null;
						Log.e(TAG, "disconnected", e);
						connectionFailed();
						BluetoothService.this.stop();
						return;
					} catch (Exception e2) {
					}
					Log.e(TAG, "disconnected", e);
					connectionFailed();
					return;
				}
			} else {
				connectionFailed();
				return;
			}

			// Reset the ConnectThread because we're done
			synchronized (BluetoothService.this) {
				mConnectThread = null;
			}

			// Start the connected thread
			connected(mSocket, mDevice);
		}

		public void cancel() {
			try {
				if (mSocket != null) {
					mSocket.close();
				}
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed", e);
			}
		}

		public BluetoothSocket createSocket(final BluetoothDevice device) throws IOException {
			BluetoothSocket socket = null;
			try {
				// Method m = device.getClass().getMethod("createRfcommSocket",
				// int.class);
				// socket = (BluetoothSocket) m.invoke(device, 1);

				socket = device.createRfcommSocketToServiceRecord(MY_UUID);

				// Method m =
				// device.getClass().getMethod("createInsecureRfcommSocket", new
				// Class[] { int.class });
				// socket = (BluetoothSocket) m.invoke(device,
				// Integer.valueOf(1));
			} catch (IOException ignore) {
				try {
					// socket =
					// device.createRfcommSocketToServiceRecord(MY_UUID);
					Method m = device.getClass().getMethod("createInsecureRfcommSocket", new Class[] { int.class });
					socket = (BluetoothSocket) m.invoke(device, Integer.valueOf(1));
				} catch (Exception e) {
				}
			} catch (Exception ignore) {
			}
			return socket;
		}
	}

	/**
	 * This thread runs during a connection with a remote device. It handles all
	 * incoming and outgoing transmissions.
	 */
	private class ConnectedThread extends Thread {

		private InputStream mInStream;
		private OutputStream mOutStream;

		public ConnectedThread() {

			try {

				if (mSocket != null) {
					mInStream = mSocket.getInputStream();
					mOutStream = mSocket.getOutputStream();
				} else {
					connectionFailed();
					return;
				}
			} catch (IOException e) {
				Log.e(TAG, "temp sockets not created", e);
			}
		}

		@Override
		public void run() {
			Log.i(TAG, "BEGIN mConnectedThread");
			int bytes;

			// Keep listening to the InputStream while connected
			while (true) {
				try {
					byte[] buffer = new byte[256];
					// Read from the InputStream
					bytes = mInStream.read(buffer);
					if (bytes > 0) {
						if (mHandler != null) {
							// Send the obtained bytes to the UI Activity
							mHandler.obtainMessage(MESSAGE_READ, bytes, -1, buffer).sendToTarget();
						}
					} else {
						Log.e(TAG, "disconnected");
						connectionLost();

						// add by chongqing jinou
						if (mState != STATE_NONE) {
							Log.e(TAG, "disconnected");
							// Start the service over to restart listening mode
							BluetoothService.this.start();
						}
						break;
					}
				} catch (IOException e) {
					e.printStackTrace();
					Log.e(TAG, "Connected Thread : " + e.toString());
					break;
				} catch (Exception e) {
					e.printStackTrace();
					Log.e(TAG, "disconnected", e);
					connectionLost();

					// add by chongqing jinou
					if (mState != STATE_NONE) {
						// Start the service over to restart listening mode
						BluetoothService.this.start();
					}
					break;
				}
			}
		}

		/**
		 * Write to the connected OutStream.
		 * 
		 * @param buffer
		 *            The bytes to write
		 */
		public void write(byte[] buffer) {
			try {
				mOutStream.write(buffer);
				Log.i("BTPWRITE", new String(buffer, "GBK"));
				if (mHandler != null) {
					// Share the sent message back to the UI Activity
					mHandler.obtainMessage(MESSAGE_WRITE, -1, -1, buffer).sendToTarget();
				}
			} catch (IOException e) {
				Log.e(TAG, "Exception during write", e);
			}
		}

		public void cancel() {
			try {
				if (mInStream != null) {
					mInStream.close();
				}
				if (mOutStream != null) {
					mOutStream.close();
				}
				if (mSocket != null) {
					mSocket.close();
				}
			} catch (IOException e) {
				Log.e(TAG, "close() of connect socket failed", e);
			}
		}
	}
}
