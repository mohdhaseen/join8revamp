package com.join8.print;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

public class SocketManager {

	private static final String TAG = SocketManager.class.getSimpleName();

	public static final int WIFI_PRINTER_CONNECTION_STATE = 11;

	public static final int STATE_CONNECTED = 1;
	public static final int STATE_ERROR = 2;
	public static final int STATE_WRITE_SUCCESS = 3;
	public static final int STATE_WRITE_ERROR = 4;

	private Socket mMyWifiSocket = null;
	private OutputStream mOutputStream = null;
	private boolean connected = false;

	public String mPrinterIP = "";
	private  int mPort = 9100;
	
	private Handler mHandler = null;

	public SocketManager(Context context) {
	}

	public void setHandler(Handler handler) {
		if (handler != null) {
			mHandler = handler;
		}
	}

	public boolean isConnected() {
		return connected;
	}

	private synchronized void setState(int state) {
		Log.d(TAG, "State -> " + state);

		if (mHandler != null) {
			mHandler.obtainMessage(WIFI_PRINTER_CONNECTION_STATE, state, -1).sendToTarget();
		}
	}

	public void threadconnect() {
		new ConnectThread();
	}

	public synchronized void threadconnectwrite(byte[] str) {
		new WriteThread(str);
	}

	public synchronized boolean connect() {

		try {

			if (mMyWifiSocket != null && mMyWifiSocket.isConnected()) {
				if (mOutputStream == null) {
					mOutputStream = mMyWifiSocket.getOutputStream();
				}
				return true;
			} else {
				close();				
//				mMyWifiSocket = new Socket();
//				mMyWifiSocket.bind(null);				
//				mMyWifiSocket.connect(new InetSocketAddress(mPrinterIP, mPort), TimeOut);
//				mOutputStream = mMyWifiSocket.getOutputStream();
				
				mMyWifiSocket = new Socket(mPrinterIP, mPort);
				mOutputStream = mMyWifiSocket.getOutputStream();			
				
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
			setState(STATE_ERROR);
			connected = false;
			mMyWifiSocket = null;
			return false;
		}
	}

	public synchronized boolean write(byte[] out) {
		if (mOutputStream != null) {
			try {
				mOutputStream.write(out);
				mOutputStream.flush();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}
	}

	public synchronized void close() {

		if (mOutputStream != null) {
			try {
				mOutputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			mOutputStream = null;
		}
		
		if (mMyWifiSocket != null) {
			try {
				mMyWifiSocket.close();				
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			mMyWifiSocket = null;
		}		
	}

	public synchronized boolean ConnectAndWrite(byte[] out) {
		if (connect()) {
			write(out);
			// close();
			setState(STATE_WRITE_SUCCESS);
			connected = true;
			return true;
		} else {
			setState(STATE_ERROR);
			connected = false;
			return false;
		}
	}

	private class ConnectThread extends Thread {

		public ConnectThread() {
			start();
		}

		public void run() {

			Looper.prepare();

			if (connect()) {
				setState(STATE_CONNECTED);
				connected = true;
			}
			// close();
		}
	}

	private class WriteThread extends Thread {
		byte[] out;

		public WriteThread(byte[] str) {
			out = str;
			start();
		}

		public void run() {

			try {

				Looper.prepare();

				if (mMyWifiSocket != null && mMyWifiSocket.isConnected()) {

					if (mOutputStream == null) {
						mOutputStream = mMyWifiSocket.getOutputStream();
					}

					write(out);
					setState(STATE_WRITE_SUCCESS);
					connected = true;

				} else {
					if (ConnectAndWrite(out)) {
						setState(STATE_WRITE_SUCCESS);
						connected = true;
					}
				}

				// if (ConnectAndWrite(out)) {
				// setState(STATE_WRITE_SUCCESS);
				// connected = true;
				// }

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void printData(byte[] byteData) {

		try {

			// Looper.prepare();
			
			try {
				Thread.sleep(50);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (mMyWifiSocket != null && mMyWifiSocket.isConnected()) {

				if (mOutputStream == null) {
					mOutputStream = mMyWifiSocket.getOutputStream();
				}

				write(byteData);
				setState(STATE_WRITE_SUCCESS);
				connected = true;

			} else {
				if (ConnectAndWrite(byteData)) {
					setState(STATE_WRITE_SUCCESS);
					connected = true;
				}
			}

			// if (ConnectAndWrite(out)) {
			// setState(STATE_WRITE_SUCCESS);
			// connected = true;
			// }

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
