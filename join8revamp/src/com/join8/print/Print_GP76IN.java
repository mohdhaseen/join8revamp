package com.join8.print;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.text.TextUtils;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.model.BookingData;
import com.join8.model.FoodOptionItemData;
import com.join8.model.MenuItemData;
import com.join8.model.OrderData;
import com.join8.utils.ConstantData;
import com.join8.utils.Utils;

public class Print_GP76IN {

	private Context mContext;
	private BluetoothService mService;
	private int charPerLine = 16;
	private SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.getDefault());
	private SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.getDefault());
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
	private SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());

	public Print_GP76IN(Context mContext, BluetoothService mService) {

		this.mContext = mContext;
		this.mService = mService;
	}

	public void printBooking(BookingData data) {

		try {

			// Check that we're actually connected before trying
			// anything
			if (mService.getState() != BluetoothService.STATE_CONNECTED) {
				Toast.displayText(mContext, mContext.getString(R.string.not_connected));
				return;
			}

			String orderState = null;

			switch (data.getBookingStatus()) {
				case 10 :
					orderState = mContext.getString(R.string.pending);
					break;
				case 20 :
					orderState = mContext.getString(R.string.confirmed);
					break;
				case 30 :
					orderState = mContext.getString(R.string.cancelled);
					break;
				case 50 :
					orderState = mContext.getString(R.string.rejected);
					break;
				case 70 :
					orderState = mContext.getString(R.string.pickuped);
					break;
				case 80 :
					orderState = mContext.getString(R.string.delivered);
					break;
			}

			goToNextLine();
			setFontSizeNormal();
			printCenter(mContext.getString(R.string.booking_no) + " " + data.getTableBookingId() + " <<" + orderState + ">>");

			setFontSizeBig();
			goToNextLine();
			printCenter(dateFormat.format(data.getDateToBook()));
			printCenter(timeFormat.format(data.getDateToBook()));

			setFontSizeNormal();
			goToNextLine();
			printString(mContext.getString(R.string.number_of_people) + " " + data.getSeats());
			printString(mContext.getString(R.string.customer) + " " + data.getFirstName() + " " + data.getLastName());
			printString(mContext.getString(R.string.phone) + " " + data.getMobileNumber());

			printSeparatorLine();
			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				printString(mContext.getString(R.string.table_type_print) + ": " + data.getBookingNameEn());
			} else {
				printString(mContext.getString(R.string.table_type_print) + ": " + data.getBookingName());
			}
			printString(mContext.getString(R.string.deposit) + ": $" + Utils.formatDecimal("" + data.getDeposit()));

			if (!TextUtils.isEmpty(data.getRemarks())) {
				printString(mContext.getString(R.string.remarks_for_print) + " " + data.getRemarks());
			}
			goToNextLine();
			setFontSizeNormal();
			printCenter("www.join8.com 下載外賣app");

			feedLines(5);
			cupPage();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void printOrder(final OrderData data, int copyNo) {

		try {

			// Check that we're actually connected before trying
			// anything
			if (mService.getState() != BluetoothService.STATE_CONNECTED) {
				Toast.displayText(mContext, mContext.getString(R.string.not_connected));
				return;
			}

			String orderState = null;

			switch (data.getOrderStatus()) {
				case 10 :
					orderState = mContext.getString(R.string.pending);
					break;
				case 20 :
					orderState = mContext.getString(R.string.confirmed);
					break;
				case 30 :
					orderState = mContext.getString(R.string.cancelled);
					break;
				case 50 :
					orderState = mContext.getString(R.string.rejected);
					break;
				case 70 :
					orderState = mContext.getString(R.string.pickuped);
					break;
				case 80 :
					orderState = mContext.getString(R.string.delivered);
					break;
			}

			initializePrinter();

			setFontSizeBig();
			printCenter(mContext.getString(R.string.order_no) + " " + data.getOrderId());

			printCenter("<<" + mContext.getString(R.string.take_out_order) + " " + copyNo + ": " + orderState + ">>");
			goToNextLine();
			goToNextLine();

			if (data.isOrderPaid()) {
				printCenter("<<" + mContext.getString(R.string.paid) + ">>");
			}

			setFontSizeNormal();
			printCenter(DateProcess(data.getOrderDatetime()));

			if (data.isIsOrderPickUpByUser()) {
				printCenter("*" + mContext.getString(R.string.pickup) + "*");
				printCenter("(" + DateProcess(data.getOrderDeadline()) + ")");
			} else {
				printCenter("*" + mContext.getString(R.string.deliver) + "*");
				printCenter("(" + DateProcess(data.getOrderDeadline()) + ")");
			}

			setFontSizeBig();
			printSeparatorLine();

			printString(mContext.getString(R.string.customer) + " " + data.getFirstName() + " " + data.getLastName());
			printString(mContext.getString(R.string.phone) + " " + data.getMobileNumber());
			printString(mContext.getString(R.string.address_for_print) + " " + data.getDeliverAddress());
			goToNextLine();
			goToNextLine();

			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				printCenter(data.getCompanyNameEN());
			} else {
				printCenter(data.getCompanyName());
			}

			printSeparatorLine();

			for (int i = 0; i < data.getMenuItem().size(); i++) {

				MenuItemData menuItemData = data.getMenuItem().get(i);
				double itemPrice = menuItemData.getPrice();

				for (int j = 0; j < menuItemData.getFoodOption().size(); j++) {

					for (int k = 0; k < menuItemData.getFoodOption().get(j).getFoodOptionItem().size(); k++) {

						FoodOptionItemData foodOptionItemData = data.getMenuItem().get(i).getFoodOption().get(j).getFoodOptionItem().get(k);
						itemPrice += foodOptionItemData.getExtraPrice();
					}
				}

				printMenuItem(menuItemData.getItemName(), menuItemData.getItemName_EN(), menuItemData.getQuantity(), itemPrice);

				for (int j = 0; j < menuItemData.getFoodOption().size(); j++) {

					for (int k = 0; k < menuItemData.getFoodOption().get(j).getFoodOptionItem().size(); k++) {

						FoodOptionItemData foodOptionItemData = menuItemData.getFoodOption().get(j).getFoodOptionItem().get(k);

						if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
							printString("  --" + foodOptionItemData.getFoodOptionItemNameEn());
						} else {
							printString("  --" + foodOptionItemData.getFoodOptionItemName());
						}
					}
				}

				goToNextLine();
			}

			printSeparatorLine();
			printChargesAndTotal(data);
			if (!TextUtils.isEmpty(data.getRemarks())) {
				goToNextLine();
				printString(mContext.getString(R.string.remarks_for_print) + " " + data.getRemarks());
			}
			goToNextLine();
			goToNextLine();
			setFontSizeNormal();
			printCenter("www.join8.com 下載外賣app");

			feedLines(5);
			cupPage();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void initializePrinter() {

		if (mService.getState() != BluetoothService.STATE_CONNECTED) {
			Toast.displayText(mContext, mContext.getString(R.string.not_connected));
			return;
		}

		mService.write(new byte[]{0x1b, 0x40});
		mService.write(new byte[]{0x0a});
	}

	private void setFontSizeNormal() {
		mService.write(new byte[]{0x1C, 0x21, 0x00, 0x1B, 0x21, 0x00});
	}

	private void setFontSizeBig() {
		mService.write(new byte[]{0x1C, 0x21, 0x0C, 0x1B, 0x21, 0x30});
	}

	private void printChargesAndTotal(OrderData orderData) {

		try {

			double total = 0.0, deliveryFee = 0;

			if (orderData.getMenuItem() != null) {

				for (int i = 0; i < orderData.getMenuItem().size(); i++) {

					MenuItemData menuItemData = orderData.getMenuItem().get(i);
					double itemPrice = menuItemData.getPrice();

					for (int j = 0; j < orderData.getMenuItem().get(i).getFoodOption().size(); j++) {

						for (int k = 0; k < orderData.getMenuItem().get(i).getFoodOption().get(j).getFoodOptionItem().size(); k++) {

							FoodOptionItemData foodOptionItemData = orderData.getMenuItem().get(i).getFoodOption().get(j).getFoodOptionItem().get(k);
							itemPrice += foodOptionItemData.getExtraPrice();
						}
					}

					total += menuItemData.getQuantity() * itemPrice;
				}
			}

			if (!orderData.isIsOrderPickUpByUser()) {

				if (orderData.getDeliveryOption() == 1) {

					deliveryFee = orderData.getDeliveryFee();

				} else if (orderData.getDeliveryOption() == 2) {

					deliveryFee = total * orderData.getDeliveryFee() / 100;
				}
			}

			PrintJustify(mContext.getString(R.string.shipping_cost), String.format("%.1f", deliveryFee));
			PrintJustify(mContext.getString(R.string.total), String.format("%.1f", deliveryFee + total));

		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	private void printMenuItem(String itemName, String itemNameEn, int qty, double price) {
		// Check that we're actually connected before trying anything
		if (mService.getState() != BluetoothService.STATE_CONNECTED) {
			Toast.displayText(mContext, mContext.getString(R.string.not_connected));
			return;
		}

		String pText = null, temp1, temp2;
		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
			temp1 = itemNameEn;
		} else {
			temp1 = itemName;
		}

		temp2 = String.format(Locale.getDefault(), "% 2d", qty) + String.format(Locale.getDefault(), "% 6.1f", price);

		pText = justifyString(temp1, temp2, charPerLine);
		printString(pText);
	}

	private void printCenter(String message) {
		if (mService.getState() != BluetoothService.STATE_CONNECTED) {
			Toast.displayText(mContext, mContext.getString(R.string.not_connected));
			return;
		}
		// Check that there's actually something to send
		if (message.length() > 0) {
			// Get the message bytes and tell the BluetoothService to write
			byte[] send;
			try {
				send = message.getBytes("Big5");
			} catch (UnsupportedEncodingException e) {
				send = message.getBytes();
			}

			// print center justify string
			mService.write(new byte[]{27, 97, 1});
			// print data
			mService.write(send);
			// print enter for new line
			mService.write(new byte[]{10});
		}
	}

	private void printSeparatorLine() {
		if (mService.getState() != BluetoothService.STATE_CONNECTED) {
			Toast.displayText(mContext, mContext.getString(R.string.not_connected));
			return;
		}
		printCenter("================");
	}

	private void PrintJustify(String str1, String str2) {
		if (mService.getState() != BluetoothService.STATE_CONNECTED) {
			Toast.displayText(mContext, mContext.getString(R.string.not_connected));
			return;
		}
		String temp = justifyString(str1, str2, charPerLine);
		printString(temp);
	}

	private void goToNextLine() {
		if (mService.getState() != BluetoothService.STATE_CONNECTED) {
			Toast.displayText(mContext, mContext.getString(R.string.not_connected));
			return;
		}
		// print new line
		mService.write(new byte[]{10});
	}

	private String DateProcess(String stringDate) {
		try {
			Date date = sourceFormat.parse(stringDate);
			return destFormat.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stringDate;
	}

	private void printString(String message) {
		// Check that we're actually connected before trying anything
		if (mService.getState() != BluetoothService.STATE_CONNECTED) {
			Toast.displayText(mContext, mContext.getString(R.string.not_connected));
			return;
		}

		// Check that there's actually something to send
		if (message.length() > 0) {
			// Get the message bytes and tell the BluetoothService to write
			byte[] send;
			try {
				send = message.getBytes("Big5");
			} catch (UnsupportedEncodingException e) {
				send = message.getBytes();
			}

			// print left justify string
			mService.write(new byte[]{27, 97, 0});
			// print data
			mService.write(send);
			// print enter for new line
			mService.write(new byte[]{10});
		}
	}

	private void feedLines(int nLineCount) {
		if (mService.getState() != BluetoothService.STATE_CONNECTED) {
			Toast.displayText(mContext, mContext.getString(R.string.not_connected));
			return;
		}

		// print and feed N line
		mService.write(new byte[]{27, 100, (byte) nLineCount});
	}

	private void cupPage() {
		// send page cut command
		mService.write(new byte[]{29, 86, 0});
	}

	private String justifyString(String text1, String text2, int width) {
		String ptext = "";
		int byteCount1 = getByte(text1);
		int byteCount2 = getByte(text2);

		ptext = text1;

		if (byteCount1 + byteCount2 <= width) {

			for (int i = 0; i < width - (byteCount1 + byteCount2); i++) {
				ptext = ptext + " ";
			}
		} else {
			for (int i = 0; i < width - ((byteCount1 + byteCount2) % width); i++) {
				ptext = ptext + " ";
			}
		}

		ptext = ptext + text2;

		return ptext;
	}

	private int getByte(String text) {
		String temp;
		int bytecount = 0;
		for (int i = 0; i < text.length(); i++) {
			temp = text.substring(i, i + 1);
			if (temp.getBytes().length == 1)
				bytecount = bytecount + 1;
			else
				bytecount = bytecount + 2;
		}

		return bytecount;
	}
}
