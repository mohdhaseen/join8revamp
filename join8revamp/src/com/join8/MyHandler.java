package com.join8;

import java.util.Random;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.join8.utils.ConstantData;
import com.microsoft.windowsazure.notifications.NotificationsHandler;

public class MyHandler extends NotificationsHandler {

	public static final int NOTIFICATION_ID = 1;
	private Context ctx = null;

	@Override
	public void onReceive(Context context, Bundle bundle) {
		ctx = context;

		String message = bundle.getString("msg");
		String redirectTo = bundle.getString("redirectTo");
		String reference = bundle.getString("reference");

		Log.i("Join8 Hub Notification", "Message ==> " + message);
		Log.i("Join8 Hub Notification", "RedirectTo ==> " + redirectTo);
		Log.i("Join8 Hub Notification", "Reference ==> " + reference);

		Intent intent = new Intent(ConstantData.BROADCAST_REFRESH_LIST);
		intent.putExtra("redirectTo", redirectTo);
		context.sendOrderedBroadcast(intent, null);

		sendNotification(message, redirectTo, reference);
	}

	private void sendNotification(String message, String redirectTo, String reference) {

		try {

			String title = ctx.getString(R.string.app_name);

			Intent notificationIntent = new Intent(ctx, HomeScreen.class);
			// set intent so it does not start a new activity
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			// notificationIntent.putExtra("isFromNotification", true);
			notificationIntent.putExtra("redirectTo", redirectTo);
			notificationIntent.putExtra("reference", reference);

			NotificationManager mNotificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
			PendingIntent contentIntent = PendingIntent.getActivity(ctx, new Random().nextInt(), notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

			if (redirectTo.equalsIgnoreCase("ConfirmBookingDetail") || redirectTo.equalsIgnoreCase("ConfirmOrderDetail")) {
				contentIntent = PendingIntent.getActivity(ctx, new Random().nextInt(), new Intent(), PendingIntent.FLAG_CANCEL_CURRENT);
			}

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(ctx).setSmallIcon(R.drawable.ic_launcher).setContentTitle(title).setStyle(new NotificationCompat.BigTextStyle().bigText(message)).setContentText(message);
			mBuilder.setContentIntent(contentIntent);
			mBuilder.setAutoCancel(true);
			Uri notificationSound = Uri.parse("android.resource://" + ctx.getPackageName() + "/" + R.raw.notification_sound);
			mBuilder.setSound(notificationSound);
			mBuilder.setVibrate(new long[] { 1000, 1000 });
			mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());

			if (redirectTo.equalsIgnoreCase("StaffOrderDetail") || redirectTo.equalsIgnoreCase("StaffBookingDetail")) {
				if (reference.equals("0") || TextUtils.isEmpty(reference)) {
					return;
				}
				Intent mIntent = new Intent(ctx, PrinterService.class);
				mIntent.putExtra("redirectTo", redirectTo);
				mIntent.putExtra("reference", reference);
				ctx.startService(mIntent);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
