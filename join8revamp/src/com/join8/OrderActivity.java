package com.join8;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.join8.fragments.OrderDetailFragment;
import com.join8.fragments.RestaurantDetailsFragment;
import com.join8.fragments.ViewOrderDetailFragment;
import com.join8.utils.TypefaceUtils;

public class OrderActivity extends ActionBarActivity {

	private ImageButton btnBack = null, btnFav = null;
	private TextView txtTitle = null, txtOpen = null;
	public static int restaurant_fragment_container = R.id.fragment_container;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_restaurant);

		getSupportActionBar().setHomeButtonEnabled(false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		getSupportActionBar().setDisplayUseLogoEnabled(false);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);

		View view = getLayoutInflater().inflate(R.layout.actionviewdetail, null);
		getSupportActionBar().setCustomView(view);

		btnBack = (ImageButton) view.findViewById(R.id.btnBack);
		btnFav = (ImageButton) view.findViewById(R.id.btnFav);
		txtTitle = (TextView) view.findViewById(R.id.txtTitle);
		txtOpen = (TextView) view.findViewById(R.id.txtOpen);
		txtOpen.setVisibility(View.GONE);
		btnFav.setVisibility(View.INVISIBLE);

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(this);
		mTypefaceUtils.applyTypeface(txtTitle);

		Bundle bundle = new Bundle();
		bundle.putSerializable("orderId", getIntent().getExtras().getString("orderId"));

		ViewOrderDetailFragment viewOrderDetailFragment = new ViewOrderDetailFragment();
		viewOrderDetailFragment.setArguments(bundle);

		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.replace(restaurant_fragment_container, viewOrderDetailFragment);
		ft.addToBackStack(OrderDetailFragment.class.getName());
		ft.commit();

		// Bundle bundle = new Bundle();
		// bundle.putSerializable("orderId",
		// getIntent().getExtras().getString("orderId"));
		//
		// OrderDetailFragment orderDetailFragment = new OrderDetailFragment();
		// orderDetailFragment.setArguments(bundle);
		//
		// FragmentTransaction ft =
		// getSupportFragmentManager().beginTransaction();
		// ft.replace(restaurant_fragment_container, orderDetailFragment);
		// ft.addToBackStack(OrderDetailFragment.class.getName());
		// ft.commit();

		btnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
					finish();
				} else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
					getSupportFragmentManager().popBackStack();
				}
			}
		});

		getSupportFragmentManager().addOnBackStackChangedListener(new OnBackStackChangedListener() {
			@Override
			public void onBackStackChanged() {

				if (getActiveFragment().equals(RestaurantDetailsFragment.class.getName())) {

					btnFav.setVisibility(View.VISIBLE);
					txtTitle.setVisibility(View.VISIBLE);
					txtOpen.setVisibility(View.VISIBLE);
				}
			}
		});
	}

	public String getActiveFragment() {

		if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
			return "";
		}
		return getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
	}

	@Override
	public void onBackPressed() {

		if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
			finish();
		} else {
			super.onBackPressed();
		}
	}

	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		super.onActivityResult(arg0, arg1, arg2);
	}

	public String getActivityTitle() {
		if (txtTitle != null) {
			return txtTitle.getText().toString();
		} else {
			return "";
		}
	}
}
