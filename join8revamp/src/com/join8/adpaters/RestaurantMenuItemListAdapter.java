package com.join8.adpaters;

import java.util.ArrayList;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.join8.FoodOptionActivity;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.fragments.OrderDetailFragment;
import com.join8.model.Registration;
import com.join8.model.RestaurantMenuItemData;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class RestaurantMenuItemListAdapter extends BaseAdapter {

	private LayoutInflater mInflater;
	private FragmentActivity context;
	private ArrayList<RestaurantMenuItemData> listRestaurantMenuItems;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private TypefaceUtils mTypefaceUtils;

	public RestaurantMenuItemListAdapter(FragmentActivity c, ArrayList<RestaurantMenuItemData> listRestaurantMenuItems) {
		mInflater = LayoutInflater.from(c);
		context = c;
		this.listRestaurantMenuItems = listRestaurantMenuItems;
		imageLoader = ImageLoader.getInstance();
		mTypefaceUtils = TypefaceUtils.getInstance(c);
		options = new DisplayImageOptions.Builder().showStubImage(R.drawable.default_image).showImageForEmptyUri(R.drawable.default_image).showImageOnFail(R.drawable.default_image).cacheInMemory(true).cacheOnDisc(true).build();

		mTypefaceUtils = TypefaceUtils.getInstance(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listRestaurantMenuItems.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return listRestaurantMenuItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.row_menu, parent, false);
			mHolder = new ViewHolder();

			mHolder.imvArrow = (ImageView) convertView.findViewById(R.id.imvArrow);
			mHolder.txtTitle = (TextView) convertView.findViewById(R.id.txt_title);
			mHolder.txtSubTitle = (TextView) convertView.findViewById(R.id.txt_subtitle);
			mHolder.txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
			mHolder.txtItemName = (TextView) convertView.findViewById(R.id.txt_itemName);
			mHolder.imgPhoto = (ImageView) convertView.findViewById(R.id.img_Restorent);
			mHolder.rltPrice = (RelativeLayout) convertView.findViewById(R.id.rltPrice);
			convertView.setTag(mHolder);
		} else {
			mHolder = (ViewHolder) convertView.getTag();
		}
		try {

			if (listRestaurantMenuItems.get(position).isOrderable()) {
				mHolder.rltPrice.setEnabled(true);
			} else {
				mHolder.rltPrice.setEnabled(false);
			}

			if (position == 0) {
				mHolder.imvArrow.setVisibility(View.VISIBLE);
			} else {
				mHolder.imvArrow.setVisibility(View.GONE);
			}

			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				mHolder.txtTitle.setText(listRestaurantMenuItems.get(position).getCompanyNameEN());

			} else {
				mHolder.txtTitle.setText(Html.fromHtml(listRestaurantMenuItems.get(position).getCompanyName()));

			}
			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				if (!Utils.isEmpty(listRestaurantMenuItems.get(position).getItemName())) {
					mHolder.txtSubTitle.setText(Html.fromHtml(listRestaurantMenuItems.get(position).getItemName()));
				}
				mHolder.txtItemName.setText(listRestaurantMenuItems.get(position).getItemName_EN());
			} else {
				if (!Utils.isEmpty(listRestaurantMenuItems.get(position).getItemName())) {
					mHolder.txtSubTitle.setText(Html.fromHtml(listRestaurantMenuItems.get(position).getItemName_EN()));
				}
				mHolder.txtItemName.setText(listRestaurantMenuItems.get(position).getItemName());
			}

			mHolder.txtPrice.setText("$" + Utils.formatDecimal("" + listRestaurantMenuItems.get(position).getPrice()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		imageLoader.displayImage(listRestaurantMenuItems.get(position).getPhoto(), mHolder.imgPhoto, options);
		mTypefaceUtils.applyTypeface(mHolder.txtTitle);
		mTypefaceUtils.applyTypeface(mHolder.txtSubTitle);
		mTypefaceUtils.applyTypeface(mHolder.txtItemName);
		mTypefaceUtils.applyTypeface(mHolder.txtPrice);

		if (OrderDetailFragment.listItemIds != null && OrderDetailFragment.listItemIds.contains(listRestaurantMenuItems.get(position).getMenuItemId())) {
			mHolder.rltPrice.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.menusearchorangebutton));
		} else {
			mHolder.rltPrice.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.menusearchbutton));
		}

		mHolder.rltPrice.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				/*
				 * Bundle bundle = new Bundle(); bundle.putSerializable("obj",
				 * listRestaurantMenuItems.get(position));
				 * 
				 * PromotionDetailsFromList obj = new
				 * PromotionDetailsFromList(); obj.setArguments(bundle);
				 * 
				 * FragmentTransaction ft =
				 * context.getSupportFragmentManager().beginTransaction();
				 * ft.addToBackStack(PromotionDetailsFromList.class.getName());
				 * ft.add(RestaurantActivity.restaurant_fragment_container,
				 * obj); ft.commit();
				 */

				CryptoManager prefManager = CryptoManager.getInstance(context);
				Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);

				if (registration == null) {
					Toast.displayText(context, context.getString(R.string.please_login));
				} else {

					if (!listRestaurantMenuItems.get(position).isHasFoodOption()) {
						
						RestaurantMenuItemData data  = RestaurantMenuItemData.copy(listRestaurantMenuItems.get(position));
						OrderDetailFragment.addOrder(data);
						v.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.menusearchorangebutton));

						Toast.displayTextForShortTime(context, context.getString(R.string.added));
						
					} else {

						Intent intent = new Intent(context, FoodOptionActivity.class);
						intent.putExtra("menuItemId", listRestaurantMenuItems.get(position).getMenuItemId());
						context.startActivity(intent);
					}
				}
			}
		});

		return convertView;
	}

	private static class ViewHolder {

		ImageView imvArrow;
		TextView txtTitle;
		TextView txtSubTitle;
		TextView txtItemName;
		ImageView imgPhoto;
		TextView txtPrice;
		RelativeLayout rltPrice;

	}
}
