package com.join8.adpaters;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.model.Promotion;
import com.join8.model.Promotions;
import com.join8.model.ResponsePromotions;
import com.join8.utils.ConstantData;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class PromotionListAdapterNew extends BaseAdapter {

	private LayoutInflater mInflater;
	private ArrayList<Promotion> promotionsData;
	private ImageLoader imageLoader;
	private DisplayImageOptions options, options1;
	private TypefaceUtils mTypefaceUtils;

	public PromotionListAdapterNew(Context context, int resource, ArrayList<Promotion> promotionsData) {
		mInflater = LayoutInflater.from(context);
		this.promotionsData = promotionsData;
		imageLoader = ImageLoader.getInstance();
		mTypefaceUtils = TypefaceUtils.getInstance(context);
		options = new DisplayImageOptions.Builder().showStubImage(R.drawable.banner_default).showImageForEmptyUri(R.drawable.banner_default).showImageOnFail(R.drawable.banner_default).cacheInMemory(true).cacheOnDisc(true).build();
		options1 = new DisplayImageOptions.Builder().showStubImage(R.drawable.default_image).showImageForEmptyUri(R.drawable.default_image).showImageOnFail(R.drawable.default_image).cacheInMemory(true).cacheOnDisc(true).build();
	}

	@Override
	public int getCount() {
		return promotionsData.size();
	}

	@Override
	public Object getItem(int position) {
		return promotionsData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.row_promotions, parent, false);
			mHolder = new ViewHolder();

			mHolder.txtTitle = (TextView) convertView.findViewById(R.id.txt_title);
			mHolder.txtSubTitle = (TextView) convertView.findViewById(R.id.txt_subtitle);
			mHolder.txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
			mHolder.imgBanner = (ImageView) convertView.findViewById(R.id.imgBanner);
			mHolder.imgIcon = (ImageView) convertView.findViewById(R.id.img_Restorent);
			convertView.setTag(mHolder);
		} else {
			mHolder = (ViewHolder) convertView.getTag();
		}
		try {

			mHolder.txtTitle.setText(Html.fromHtml(promotionsData.get(position).getPromotionName()));
			mHolder.txtPrice.setText("$" + Utils.formatDecimal("" + promotionsData.get(position).getPromotionAmount()));

			// mHolder.txtSubTitle.setText(Html.fromHtml(arrSearchData.get(
			// position).getItemName()));
			/*if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				mHolder.txtSubTitle.setText(promotionsData.get(position).getCompanyNameEN());
			} else {
				mHolder.txtSubTitle.setText(promotionsData.get(position).getCompanyName());
			}*/

		} catch (Exception e) {
			e.printStackTrace();
		}
		imageLoader.displayImage(promotionsData.get(position).getBanner(), mHolder.imgBanner, options);
		imageLoader.displayImage(promotionsData.get(position).getIcon(), mHolder.imgIcon, options);
		/*if (promotionsData.get(position).getCompanyPhoto().size() > 0) {
			imageLoader.displayImage(promotionsData.get(position).getCompanyPhoto().get(0), mHolder.imgIcon, options1);
		} else {
			imageLoader.displayImage(null, mHolder.imgIcon, options);
		}*/
		mTypefaceUtils.applyTypeface(mHolder.txtTitle);
		mTypefaceUtils.applyTypeface(mHolder.txtSubTitle);
		mTypefaceUtils.applyTypeface(mHolder.txtPrice);

		return convertView;
	}

	private static class ViewHolder {

		TextView txtTitle;
		TextView txtSubTitle;

		ImageView imgBanner, imgIcon;
		TextView txtPrice;

	}
}
