package com.join8.adpaters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.model.BookingData;
import com.join8.model.OrderData;
import com.join8.utils.ConstantData;
import com.join8.utils.TypefaceUtils;

public class BookingListAdapter extends BaseAdapter {

	private LayoutInflater mInflater = null;
	private Context mContext = null;
	private TypefaceUtils mTypefaceUtils = null;
	private ArrayList<BookingData> list = null;
	private SimpleDateFormat format = null;
	private OnBookingCancelListner onBookingCancelListner = null;

	public BookingListAdapter(Context context, ArrayList<BookingData> list, OnBookingCancelListner onOrderDeleteListner) {
		this.mContext = context;
		this.list = list;
		this.onBookingCancelListner = onOrderDeleteListner;
		mInflater = LayoutInflater.from(mContext);
		mTypefaceUtils = TypefaceUtils.getInstance(context);
//		format = new SimpleDateFormat("EEE dd MMM, yyyy", Locale.getDefault());
		format = new SimpleDateFormat("yyyy-MM-dd '-' hh:mm a", Locale.getDefault());
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder;

		if (convertView == null) {

			convertView = mInflater.inflate(R.layout.order_list_row, null);
			mHolder = new ViewHolder();

			mHolder.imvDelete = (ImageView) convertView.findViewById(R.id.imvDelete);
			mHolder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
			mHolder.txtTime = (TextView) convertView.findViewById(R.id.txtTime);
			mHolder.txtOrderStatus = (TextView) convertView.findViewById(R.id.txtOrderStatus);
			mHolder.txtContact = (TextView) convertView.findViewById(R.id.txtContact);

			mTypefaceUtils.applyTypeface(mHolder.txtTitle);
			mTypefaceUtils.applyTypeface(mHolder.txtTime);
			mTypefaceUtils.applyTypeface(mHolder.txtOrderStatus);
			mTypefaceUtils.applyTypeface(mHolder.txtContact);

			convertView.setTag(mHolder);

		} else {

			mHolder = (ViewHolder) convertView.getTag();
		}

		final BookingData data = list.get(position);

		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
			mHolder.txtTitle.setText(data.getCompanyNameEN());
		} else {
			mHolder.txtTitle.setText(data.getCompanyName());
		}

		mHolder.txtTime.setText(format.format(data.getDateToBook()));

		mHolder.txtOrderStatus.setText(OrderData.getOrderStatusString(mContext, data.getBookingStatus()));

		if (data.getBookingStatus() == 10 || data.getBookingStatus() == 20) {
			mHolder.imvDelete.setVisibility(View.VISIBLE);
		} else {
			mHolder.imvDelete.setVisibility(View.INVISIBLE);
		}

		mHolder.imvDelete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (data.getBookingStatus() == 10 || data.getBookingStatus() == 20) {
					onBookingCancelListner.onBookingCancelled(position, data.getTableBookingId());
				}
			}
		});

		return convertView;
	}

	public interface OnBookingCancelListner {
		public void onBookingCancelled(int position, String bookingId);
	}

	private static class ViewHolder {
		TextView txtTitle, txtTime, txtOrderStatus, txtContact;
		ImageView imvDelete;
	}
}
