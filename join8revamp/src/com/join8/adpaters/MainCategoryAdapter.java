package com.join8.adpaters;

import java.util.ArrayList;

import com.join8.R;
import com.join8.model.ShopCategory;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class MainCategoryAdapter extends BaseAdapter {
	private Context mContext;
	ArrayList<ShopCategory> list;
	public MainCategoryAdapter(Context c, ArrayList<ShopCategory> list) {
		mContext = c;
		this.list = list;
	}

	public int getCount() {
		return list.size();
	}

	public ShopCategory getItem(int position) {
		return list.get(position);
	}

	public long getItemId(int position) {
		return 0;
	}

	// create a new ImageView for each item referenced by the Adapter
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		if (convertView == null) {
			// if it's not recycled, initialize some attributes
			imageView = new ImageView(mContext);
			imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setPadding(8, 8, 8, 8);
		} else {
			imageView = (ImageView) convertView;
		}

		imageView.setImageResource(getImageResourceByType(list.get(position)));
		return imageView;
	}

	private int getImageResourceByType(ShopCategory shopCategory) {
		if (shopCategory.getShopCategoryId().equals("1"))
			return R.drawable.food;
		else if (shopCategory.getShopCategoryId().equals("2"))
			return R.drawable.shop;
		else if (shopCategory.getShopCategoryId().equals("3"))
			return R.drawable.beauty;
		else if (shopCategory.getShopCategoryId().equals("4"))
			return R.drawable.service;
		else if (shopCategory.getShopCategoryId().equals("5"))
			return R.drawable.digital;
		else
			return R.drawable.shop;

	}

}
