package com.join8.adpaters;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.join8.R;
import com.join8.utils.TypefaceUtils;

public class HorizontalGalleryAdapter extends BaseAdapter {

	private String[] data;
	private LayoutInflater mInflater;
	private TypefaceUtils mTypefaceUtils;

	public HorizontalGalleryAdapter(Context context, String[] items) {
		this.data = items;
		mInflater = LayoutInflater.from(context);
		mTypefaceUtils = TypefaceUtils.getInstance(context);
	}

	@Override
	public int getCount() {
		return data.length;
	}

	@Override
	public Object getItem(int position) {
		return data[position];
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder;

		if (convertView == null) {

			convertView = mInflater.inflate(R.layout.row_restaurant_gallery_service_hours, null);
			mHolder = new ViewHolder();
			mHolder.mTextView = (TextView) convertView.findViewById(R.id.text1);
			mHolder.mTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
			mTypefaceUtils.applyTypeface(mHolder.mTextView, TypefaceUtils.BOLD);
			convertView.setTag(mHolder);

		} else {

			mHolder = (ViewHolder) convertView.getTag();
		}

		mHolder.mTextView.setText(data[position]);

		return convertView;
	}

	private static class ViewHolder {
		TextView mTextView;
	}
}
