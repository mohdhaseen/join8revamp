package com.join8.adpaters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.model.RestaurantMenuItemData;
import com.join8.utils.ConstantData;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;

public class StaffOrderDetailsAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater mInflater;
	private TypefaceUtils mTypefaceUtils;
	private ArrayList<RestaurantMenuItemData> listData;

	public StaffOrderDetailsAdapter(Context c, ArrayList<RestaurantMenuItemData> list) {
		this.mContext = c;
		this.listData = list;
		mTypefaceUtils = TypefaceUtils.getInstance(mContext);
		mInflater = LayoutInflater.from(c);
	}

	@Override
	public int getCount() {
		return listData.size();
	}

	@Override
	public Object getItem(int position) {
		return listData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.row_order_details, null);
			mHolder = new ViewHolder();
			mHolder.btnCancel = (ImageButton) convertView.findViewById(R.id.btnCancel);
			mHolder.txtItemName = (TextView) convertView.findViewById(R.id.txtItemName);
			mHolder.txtQty = (TextView) convertView.findViewById(R.id.txtQty);
			mHolder.txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
			convertView.setTag(mHolder);

		} else {
			mHolder = (ViewHolder) convertView.getTag();
		}

		mHolder.btnCancel.setVisibility(View.INVISIBLE);
		mHolder.txtQty.setEnabled(false);

		final RestaurantMenuItemData data = (RestaurantMenuItemData) getItem(position);

		String itemName = "";

		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
			itemName = data.getItemName_EN();
		} else {
			itemName = data.getItemName();
		}

		double price = data.getPrice();

		if (data.getFoodOptions() != null && data.getFoodOptions().size() > 0) {

			for (int i = 0; i < data.getFoodOptions().size(); i++) {

				price += data.getFoodOptions().get(i).getExtraPrice();

				if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
					itemName += "\n--" + data.getFoodOptions().get(i).getFoodOptionItemNameEn();
				} else {
					itemName += "\n--" + data.getFoodOptions().get(i).getFoodOptionItemName();
				}
			}
		}

		mHolder.txtItemName.setText(itemName);
		mHolder.txtQty.setText("" + (data.getQuantity() == -1 ? 0 : data.getQuantity()));
		mHolder.txtPrice.setText("$" + Utils.formatDecimal("" + price));

		mTypefaceUtils.applyTypeface(mHolder.txtQty);
		mTypefaceUtils.applyTypeface(mHolder.txtItemName);
		mTypefaceUtils.applyTypeface(mHolder.txtPrice);

		return convertView;
	}

	private static class ViewHolder {
		TextView txtQty, txtItemName, txtPrice;
		ImageButton btnCancel;
	}
}
