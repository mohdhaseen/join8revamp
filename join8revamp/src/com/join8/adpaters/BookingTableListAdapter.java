package com.join8.adpaters;

import java.util.ArrayList;

import org.json.JSONArray;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.fragments.BookingTableDetailsFragment;
import com.join8.model.TableTypeData;
import com.join8.utils.ConstantData;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class BookingTableListAdapter extends BaseAdapter {

	private Context context = null;
	private LayoutInflater mInflater;
	private ArrayList<TableTypeData> list;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private TypefaceUtils mTypefaceUtils;

	public BookingTableListAdapter(Context context, ArrayList<TableTypeData> list) {
		this.context = context;
		this.list = list;

		mInflater = LayoutInflater.from(context);
		imageLoader = ImageLoader.getInstance();
		mTypefaceUtils = TypefaceUtils.getInstance(context);
		options = new DisplayImageOptions.Builder().showStubImage(R.drawable.default_image).showImageForEmptyUri(R.drawable.default_image).showImageOnFail(R.drawable.default_image).cacheInMemory(true).cacheOnDisc(true).build();
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		try {

			ViewHolder mHolder;

			if (convertView == null) {
				convertView = mInflater.inflate(R.layout.row_booking_table_list, parent, false);
				mHolder = new ViewHolder();

				mHolder.txtName = (TextView) convertView.findViewById(R.id.txtName);
				mHolder.txtDesc = (TextView) convertView.findViewById(R.id.txtDesc);
				mHolder.txtDeposit = (TextView) convertView.findViewById(R.id.txtDeposit);
				mHolder.txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
				mHolder.imvPhoto = (ImageView) convertView.findViewById(R.id.imvPhoto);
				mHolder.imvNext = (ImageView) convertView.findViewById(R.id.imvNext);
				convertView.setTag(mHolder);
			} else {
				mHolder = (ViewHolder) convertView.getTag();
			}

			final TableTypeData data = list.get(position);

			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				mHolder.txtName.setText(Html.fromHtml(data.getBookingNameEn()));
				if (!Utils.isEmpty(data.getTermsEn())) {
					mHolder.txtDesc.setText(Html.fromHtml(data.getTermsEn()));
				}
			} else {
				mHolder.txtName.setText(Html.fromHtml(data.getBookingName()));
				if (!Utils.isEmpty(data.getTerms())) {
					mHolder.txtDesc.setText(Html.fromHtml(data.getTerms()));
				}
			}

			mHolder.txtPrice.setText("$" + Utils.formatDecimal("" + data.getBookingDeposit()));

			JSONArray jArray = new JSONArray(data.getBookingImageLink());

			if (jArray.length() > 0) {
				imageLoader.displayImage(jArray.getString(0), mHolder.imvPhoto, options);
			}else {
				imageLoader.displayImage(null, mHolder.imvPhoto, options);
			}

			mHolder.imvNext.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					Bundle bundle = new Bundle();
					bundle.putSerializable("data", data);

					BookingTableDetailsFragment fragment = new BookingTableDetailsFragment();
					fragment.setArguments(bundle);

					FragmentTransaction ft = ((ActionBarActivity) context).getSupportFragmentManager().beginTransaction();
					ft.addToBackStack(null);
					ft.add(R.id.frmContainer, fragment);
					ft.commit();
				}
			});

			mTypefaceUtils.applyTypeface(mHolder.txtName, TypefaceUtils.BOLD);
			mTypefaceUtils.applyTypeface(mHolder.txtDesc);
			mTypefaceUtils.applyTypeface(mHolder.txtPrice);
			mTypefaceUtils.applyTypeface(mHolder.txtDeposit);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return convertView;
	}

	private static class ViewHolder {

		TextView txtName;
		TextView txtDesc;
		ImageView imvPhoto;
		TextView txtDeposit;
		TextView txtPrice;
		ImageView imvNext;

	}
}
