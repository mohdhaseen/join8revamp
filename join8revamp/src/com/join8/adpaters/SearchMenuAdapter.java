package com.join8.adpaters;

import java.util.ArrayList;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.RestaurantActivity;
import com.join8.fragments.OrderDetailFragment;
import com.join8.model.RestaurantData;
import com.join8.utils.ConstantData;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class SearchMenuAdapter extends BaseAdapter {

	private LayoutInflater mInflater;
	private FragmentActivity context;
	private ArrayList<RestaurantData> list;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private TypefaceUtils mTypefaceUtils;

	public SearchMenuAdapter(FragmentActivity context, ArrayList<RestaurantData> list) {
		mInflater = LayoutInflater.from(context);
		this.context = context;
		this.list = list;
		imageLoader = ImageLoader.getInstance();
		mTypefaceUtils = TypefaceUtils.getInstance(context);
		options = new DisplayImageOptions.Builder().showStubImage(R.drawable.default_image).showImageForEmptyUri(R.drawable.default_image).showImageOnFail(R.drawable.default_image).cacheInMemory(true).cacheOnDisc(true).build();

	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder;
		if (convertView == null) {

			convertView = mInflater.inflate(R.layout.row_search_menu, parent, false);
			mHolder = new ViewHolder();

			mHolder.txtTitle = (TextView) convertView.findViewById(R.id.txt_title);
			mHolder.txtSubTitle = (TextView) convertView.findViewById(R.id.txt_subtitle);
			mHolder.txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
			mHolder.txtItemName = (TextView) convertView.findViewById(R.id.txt_itemName);
			mHolder.imgPhoto = (ImageView) convertView.findViewById(R.id.img_Restorent);
			mHolder.rltPrice = (RelativeLayout) convertView.findViewById(R.id.rltPrice);
			convertView.setTag(mHolder);
		} else {
			mHolder = (ViewHolder) convertView.getTag();
		}
		try {

			if (list.get(position).isOrderable()) {
				mHolder.rltPrice.setEnabled(true);
			} else {
				mHolder.rltPrice.setEnabled(false);
			}

			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				mHolder.txtTitle.setText(list.get(position).getCompanyNameEN());

			} else {
				mHolder.txtTitle.setText(Html.fromHtml(list.get(position).getCompanyName()));

			}
			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				mHolder.txtSubTitle.setText(Html.fromHtml(list.get(position).getItemName()));
				mHolder.txtItemName.setText(list.get(position).getItemName_EN());
			} else {
				mHolder.txtSubTitle.setText(Html.fromHtml(list.get(position).getItemName_EN()));
				mHolder.txtItemName.setText(list.get(position).getItemName());
			}

			mHolder.txtPrice.setText("$" + Utils.formatDecimal(list.get(position).getPrice()));

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (list.get(position).getPhoto().size() > 0) {
			imageLoader.displayImage(list.get(position).getPhoto().get(0), mHolder.imgPhoto, options);
		} else {
			imageLoader.displayImage(null, mHolder.imgPhoto, options);
		}
		mTypefaceUtils.applyTypeface(mHolder.txtTitle);
		mTypefaceUtils.applyTypeface(mHolder.txtSubTitle);
		mTypefaceUtils.applyTypeface(mHolder.txtItemName);
		mTypefaceUtils.applyTypeface(mHolder.txtPrice);

		if (OrderDetailFragment.listItemIds != null && OrderDetailFragment.listItemIds.contains(list.get(position).getMenuItemId())) {
			mHolder.rltPrice.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.menusearchorangebutton));
		} else {
			mHolder.rltPrice.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.menusearchbutton));
		}

		mHolder.rltPrice.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				Intent intent = new Intent(context, RestaurantActivity.class);
				intent.putExtra("restaurantData", list.get(position));
				intent.putExtra("fromFlag", true);
				context.startActivityForResult(intent, 33);

			}
		});

		return convertView;
	}

	private static class ViewHolder {

		TextView txtTitle;
		TextView txtSubTitle;
		TextView txtItemName;
		ImageView imgPhoto;
		TextView txtPrice;
		RelativeLayout rltPrice;
	}
}
