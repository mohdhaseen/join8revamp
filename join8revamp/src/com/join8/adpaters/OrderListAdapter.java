package com.join8.adpaters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.model.OrderData;
import com.join8.utils.ConstantData;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;

public class OrderListAdapter extends BaseAdapter {

	private LayoutInflater mInflater = null;
	private Context mContext = null;
	private TypefaceUtils mTypefaceUtils = null;
	private ArrayList<OrderData> list = null;
	private SimpleDateFormat format = null;
	private OnOrderDeleteListner onOrderDeleteListner = null;

	public OrderListAdapter(Context context, ArrayList<OrderData> list, OnOrderDeleteListner onOrderDeleteListner) {
		this.mContext = context;
		this.list = list;
		this.onOrderDeleteListner = onOrderDeleteListner;
		mInflater = LayoutInflater.from(mContext);
		mTypefaceUtils = TypefaceUtils.getInstance(context);
//		format = new SimpleDateFormat("EEE dd MMM, yyyy", Locale.getDefault());
		format = new SimpleDateFormat("yyyy-MM-dd '-' hh:mm a", Locale.getDefault());
		
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder;

		if (convertView == null) {

			convertView = mInflater.inflate(R.layout.order_list_row, null);
			mHolder = new ViewHolder();

			mHolder.imvDelete = (ImageView) convertView.findViewById(R.id.imvDelete);
			mHolder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
			mHolder.txtTime = (TextView) convertView.findViewById(R.id.txtTime);
			mHolder.txtOrderStatus = (TextView) convertView.findViewById(R.id.txtOrderStatus);
			mHolder.txtOrderAmount = (TextView) convertView.findViewById(R.id.txtOrderAmount);

			mTypefaceUtils.applyTypeface(mHolder.txtTitle);
			mTypefaceUtils.applyTypeface(mHolder.txtTime);
			mTypefaceUtils.applyTypeface(mHolder.txtOrderStatus);
			mTypefaceUtils.applyTypeface(mHolder.txtOrderAmount);

			convertView.setTag(mHolder);

		} else {

			mHolder = (ViewHolder) convertView.getTag();
		}

		final OrderData data = list.get(position);

		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
			mHolder.txtTitle.setText(data.getOrderId() + " -- "+data.getCompanyNameEN());
		} else {
			mHolder.txtTitle.setText(data.getOrderId() + " -- "+data.getCompanyName());
		}

		if (data.getOrderDeadlineDate() != null) {
			mHolder.txtTime.setText(format.format(data.getOrderDeadlineDate()));
		} else {
			mHolder.txtTime.setText("");
		}

		mHolder.txtOrderStatus.setText(OrderData.getOrderStatusString(mContext, data.getOrderStatus()));
		mHolder.txtOrderAmount.setText("$" + Utils.formatDecimal("" + data.getTotalAmount()));

		if (data.getOrderStatus() == 10 || data.getOrderStatus() == 20) {
			mHolder.imvDelete.setVisibility(View.VISIBLE);
		} else {
			mHolder.imvDelete.setVisibility(View.INVISIBLE);
		}

		mHolder.imvDelete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (data.getOrderStatus() == 10 || data.getOrderStatus() == 20) {
					onOrderDeleteListner.onOrderDeleted(position, data.getOrderId());
				}
			}
		});

		return convertView;
	}

	public interface OnOrderDeleteListner {
		public void onOrderDeleted(int position, String orderId);
	}

	private static class ViewHolder {
		TextView txtTitle, txtTime, txtOrderStatus, txtOrderAmount;
		ImageView imvDelete;
	}
}
