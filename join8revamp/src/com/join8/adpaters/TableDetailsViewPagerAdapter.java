package com.join8.adpaters;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.join8.ImagePreviewScreen;
import com.join8.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class TableDetailsViewPagerAdapter extends PagerAdapter {

	private Context context = null;
	private ArrayList<String> list = null;
	private String title = "";
	private ImageLoader imageLoader = null;
	private DisplayImageOptions options = null;

	public TableDetailsViewPagerAdapter(Context context, ArrayList<String> list, String title) {
		this.context = context;
		this.list = list;
		this.title = title;
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().showStubImage(R.drawable.joinlogo).showImageForEmptyUri(R.drawable.joinlogo).showImageOnFail(R.drawable.joinlogo).cacheInMemory(true).cacheOnDisc(true).build();
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == ((ImageView) object);
	}

	@Override
	public Object instantiateItem(ViewGroup container, final int position) {

		ImageView imvPhoto = new ImageView(context);
		imvPhoto.setScaleType(ScaleType.CENTER_CROP);
		imageLoader.displayImage(list.get(position), imvPhoto, options);

		((ViewPager) container).addView(imvPhoto);

		imvPhoto.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				ArrayList<String> listImages = new ArrayList<String>();
				listImages.add(list.get(position));
				
				Intent intent = new Intent(context, ImagePreviewScreen.class);
				intent.putExtra("title", title);
				intent.putExtra("images", listImages);
				context.startActivity(intent);
			}
		});

		return imvPhoto;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// Remove viewpager_item.xml from ViewPager
		((ViewPager) container).removeView((ImageView) object);

	}
}