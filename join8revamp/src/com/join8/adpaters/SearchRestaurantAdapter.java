package com.join8.adpaters;

import java.util.ArrayList;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.model.RestaurantData;
import com.join8.utils.ConstantData;
import com.join8.utils.TypefaceUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class SearchRestaurantAdapter extends BaseAdapter {

	private LayoutInflater mInflater;
	private Context context;
	private ArrayList<RestaurantData> arrSearchData;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private TypefaceUtils mTypefaceUtils;

	public SearchRestaurantAdapter(Context c, ArrayList<RestaurantData> searchData) {
		mInflater = LayoutInflater.from(c);
		context = c;
		this.arrSearchData = searchData;
		imageLoader = ImageLoader.getInstance();
		mTypefaceUtils = TypefaceUtils.getInstance(c);
		options = new DisplayImageOptions.Builder().showStubImage(R.drawable.default_image).showImageForEmptyUri(R.drawable.default_image).showImageOnFail(R.drawable.default_image).cacheInMemory(true).cacheOnDisc(true).build();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrSearchData.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return arrSearchData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.row_search, parent, false);
			mHolder = new ViewHolder();

			mHolder.txtTitle = (TextView) convertView.findViewById(R.id.txt_title);
			mHolder.txtSubTitle = (TextView) convertView.findViewById(R.id.txt_subtitle);
			mHolder.imgPhoto = (ImageView) convertView.findViewById(R.id.img_Restorent);
			mHolder.imgDot = (ImageView) convertView.findViewById(R.id.img_dot);
			convertView.setTag(mHolder);
		} else {
			mHolder = (ViewHolder) convertView.getTag();
		}
		if (arrSearchData.get(position).getAvailability().equals("Red")) {
			mHolder.imgDot.setImageDrawable(context.getResources().getDrawable(R.drawable.redbullet));
		} else if (arrSearchData.get(position).getAvailability().equals("Orange")) {
			mHolder.imgDot.setImageDrawable(context.getResources().getDrawable(R.drawable.yellowbullet));
		} else if (arrSearchData.get(position).getAvailability().equals("Green")) {
			mHolder.imgDot.setImageDrawable(context.getResources().getDrawable(R.drawable.greenbullet));
		}

		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
			mHolder.txtTitle.setText(arrSearchData.get(position).getCompanyNameEN());
			mHolder.txtSubTitle.setText(Html.fromHtml(arrSearchData.get(position).getCompanyName()));
		} else {
			mHolder.txtTitle.setText(arrSearchData.get(position).getCompanyName());
			mHolder.txtSubTitle.setText(Html.fromHtml(arrSearchData.get(position).getCompanyNameEN()));
		}
		if (arrSearchData.get(position).getPhoto().size() > 0) {
			imageLoader.displayImage(arrSearchData.get(position).getPhoto().get(0), mHolder.imgPhoto, options);
		} else {
			imageLoader.displayImage(null, mHolder.imgPhoto, options);
		}
		mTypefaceUtils.applyTypeface(mHolder.txtTitle);
		mTypefaceUtils.applyTypeface(mHolder.txtSubTitle);
		return convertView;
	}

	private static class ViewHolder {

		TextView txtTitle;
		TextView txtSubTitle;
		ImageView imgPhoto;
		ImageView imgDot;

	}
}
