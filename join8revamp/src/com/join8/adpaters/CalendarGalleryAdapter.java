package com.join8.adpaters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.join8.R;
import com.join8.calendar.DateDayData;
import com.join8.utils.TypefaceUtils;

public class CalendarGalleryAdapter extends BaseAdapter {

	private Context context;
	private LayoutInflater mInflater;
	private ArrayList<DateDayData> list;

	public CalendarGalleryAdapter(Context context, ArrayList<DateDayData> list) {
		this.context = context;
		this.list = list;
		mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.row_gallery_calendar, parent, false);
			mHolder = new ViewHolder();

			mHolder.txtDay = (TextView) convertView.findViewById(R.id.txtDay);
			mHolder.txtDate = (TextView) convertView.findViewById(R.id.txtDate);

			convertView.setTag(mHolder);
		} else {
			mHolder = (ViewHolder) convertView.getTag();
		}

		convertView.setTag(position);
		
		DateDayData data = list.get(position);

		mHolder.txtDay.setText(data.getDay());
		mHolder.txtDate.setText("" + data.getDate());

		TypefaceUtils.getInstance(context).applyTypeface(mHolder.txtDay);
		TypefaceUtils.getInstance(context).applyTypeface(mHolder.txtDate);

		return convertView;
	}

	private static class ViewHolder {

		TextView txtDay;
		TextView txtDate;
	}
}
