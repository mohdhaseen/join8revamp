package com.join8.adpaters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.join8.R;
import com.join8.RestaurantActivity;
import com.join8.fragments.ViewHolidaysFragment;
import com.join8.utils.TypefaceUtils;

public class RestaurantServiceHoursAdapter extends BaseAdapter {

	private LayoutInflater mInflater;
	// private String value = "";
	private Context context = null;
	private TypefaceUtils mTypefaceUtils = null;
	private int companyId = -1;
	private ArrayList<String> listServiceHours = null;

	public RestaurantServiceHoursAdapter(Context context, int companyId, ArrayList<String> listServiceHours) {
		mInflater = LayoutInflater.from(context);
		this.listServiceHours = listServiceHours;
		this.context = context;
		this.companyId = companyId;
		mTypefaceUtils = TypefaceUtils.getInstance(context);
	}

	@Override
	public int getCount() {
		return 1;
	}

	@Override
	public Object getItem(int position) {
		return listServiceHours.get(0);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = convertView;

		if (view == null) {
			view = mInflater.inflate(R.layout.row_service_hours, parent, false);
		}

		TextView txtWorkingHours = (TextView) view.findViewById(R.id.txtWorkingHours);
		final ImageView imvViewHoliday = (ImageView) view.findViewById(R.id.imvViewHoliday);
		final TextView txtViewHoliday = (TextView) view.findViewById(R.id.txtViewHoliday);
		TextView txtWorkingHoursTitle = (TextView) view.findViewById(R.id.txtWorkingHoursTitle);
		RelativeLayout relViewHolidays = (RelativeLayout) view.findViewById(R.id.relViewHoliday);

		if (listServiceHours != null && listServiceHours.size() > 0) {
			txtWorkingHours.setText(listServiceHours.get(0));
		} else {
			txtWorkingHours.setText(context.getString(R.string.off));
		}

		mTypefaceUtils.applyTypeface(txtWorkingHours);
		mTypefaceUtils.applyTypeface(txtViewHoliday);
		mTypefaceUtils.applyTypeface(txtWorkingHoursTitle);

		relViewHolidays.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				if (event.getAction() == MotionEvent.ACTION_DOWN) {

					imvViewHoliday.setImageResource(R.drawable.holiday_white);
					txtViewHoliday.setTextColor(Color.WHITE);

				} else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_OUTSIDE) {

					imvViewHoliday.setImageResource(R.drawable.holiday);
					txtViewHoliday.setTextColor(context.getResources().getColor(R.color.orangebg));
				}
				return false;
			}
		});

		relViewHolidays.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				Bundle bundle = new Bundle();
				bundle.putSerializable("companyId", companyId);

				ViewHolidaysFragment obj = new ViewHolidaysFragment();
				obj.setArguments(bundle);

				FragmentTransaction ft = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
				ft.addToBackStack(ViewHolidaysFragment.class.getName());
				ft.add(RestaurantActivity.restaurant_fragment_container, obj);
				ft.commit();
			}
		});

		return view;
	}
}
