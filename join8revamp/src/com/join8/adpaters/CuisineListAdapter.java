package com.join8.adpaters;

import java.util.ArrayList;

import android.content.Context;
import android.text.TextUtils.TruncateAt;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.model.CuisineCategoryData;
import com.join8.model.CuisineData;
import com.join8.utils.ConstantData;
import com.join8.utils.TypefaceUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class CuisineListAdapter extends BaseAdapter {

	private LayoutInflater mInflater;
	private Context context;
	private ArrayList<CuisineCategoryData> list;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private TypefaceUtils mTypefaceUtils;
	private OnCuisineSelectListener onCuisineSelectListener;

	public CuisineListAdapter(Context context, ArrayList<CuisineCategoryData> list, OnCuisineSelectListener onCuisineSelectListener) {
		this.onCuisineSelectListener = onCuisineSelectListener;
		this.context = context;
		this.list = list;

		mInflater = LayoutInflater.from(context);

		imageLoader = ImageLoader.getInstance();
		mTypefaceUtils = TypefaceUtils.getInstance(context);
		options = new DisplayImageOptions.Builder().showStubImage(R.drawable.default_image).showImageForEmptyUri(R.drawable.default_image).showImageOnFail(R.drawable.default_image).cacheInMemory(true).cacheOnDisc(true).build();
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.row_cuisine_list, parent, false);
			mHolder = new ViewHolder();

			mHolder.txtCuisine = (TextView) convertView.findViewById(R.id.txtCuisine);
			mHolder.imvPhoto = (ImageView) convertView.findViewById(R.id.imvPhoto);
			mHolder.lnrCategory = (LinearLayout) convertView.findViewById(R.id.lnrCategory);
			convertView.setTag(mHolder);
		} else {
			mHolder = (ViewHolder) convertView.getTag();
		}

		CuisineCategoryData data = list.get(position);

		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
			mHolder.txtCuisine.setText(data.getCategoryEN());
		} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_SIMPLECHINSESE)) {
			mHolder.txtCuisine.setText(data.getCategorySC());
		} else {
			mHolder.txtCuisine.setText(data.getCategoryTC());
		}

		imageLoader.displayImage(data.getPhoto(), mHolder.imvPhoto, options);

		diplaycategory(mHolder.lnrCategory, data.getCuisineTypeList());

		mTypefaceUtils.applyTypeface(mHolder.txtCuisine, TypefaceUtils.BOLD);
		return convertView;
	}

	private static class ViewHolder {

		TextView txtCuisine;
		ImageView imvPhoto;
		LinearLayout lnrCategory;
	}

	private void diplaycategory(LinearLayout lnrContainer, ArrayList<CuisineData> list) {

		try {

			lnrContainer.removeAllViews();

			if (list != null && list.size() > 0) {

				int margin = context.getResources().getDimensionPixelOffset(R.dimen.margin_2);

				LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

				LinearLayout lnrRow = new LinearLayout(context);
				lnrRow.setLayoutParams(layoutParams);
				lnrRow.setWeightSum(2f);

				for (int i = 0; i < list.size(); i++) {

					LinearLayout lnrChild = new LinearLayout(context);
					LayoutParams layoutParams1 = new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1);
					lnrChild.setLayoutParams(layoutParams1);
					lnrChild.setPadding(margin, margin, margin, margin);

					TextView txtCat = new TextView(context);
					txtCat.setTextColor(context.getResources().getColor(R.color.orange));
					txtCat.setTag(list.get(i).getTypeId());
					mTypefaceUtils.applyTypeface(txtCat);
					txtCat.setEllipsize(TruncateAt.END);
					txtCat.setSingleLine();

					if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
						txtCat.setText(list.get(i).getTypeValueEN() + " (" + list.get(i).getTotalCount() + ")");
					} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_SIMPLECHINSESE)) {
						txtCat.setText(list.get(i).getTypeValueSC() + " (" + list.get(i).getTotalCount() + ")");
					} else {
						txtCat.setText(list.get(i).getTypeValueTC() + " (" + list.get(i).getTotalCount() + ")");
					}
					txtCat.setOnClickListener(onClickListener);

					lnrChild.addView(txtCat);

					lnrRow.addView(lnrChild);

					if ((i + 1) % 2 == 0) {
						lnrContainer.addView(lnrRow);
						lnrRow = new LinearLayout(context);
						lnrRow.setLayoutParams(layoutParams);
						lnrRow.setWeightSum(2f);
					}

					if ((i + 1) % 2 == 0) {
						lnrRow.setWeightSum(2f);
					}
				}

				if (list.size() % 2 != 0) {
					lnrContainer.addView(lnrRow);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {

			if (onCuisineSelectListener != null) {

				if (v.getTag() != null) {
					String name = ((TextView) v).getText().toString().trim();
					int count = Integer.parseInt(name.substring(name.indexOf("(") + 1, name.length() - 1));
					if (count > 0) {
						name = name.substring(0, name.indexOf("("));
						onCuisineSelectListener.onCuisineSelected(Integer.parseInt(v.getTag().toString()), name.trim());
					}
				}
			}
		}
	};

	public interface OnCuisineSelectListener {
		public void onCuisineSelected(int id, String category);
	}
}
