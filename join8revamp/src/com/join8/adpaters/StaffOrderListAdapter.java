package com.join8.adpaters;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.join8.R;
import com.join8.model.OrderData;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;

public class StaffOrderListAdapter extends BaseAdapter {

	private LayoutInflater mInflater = null;
	private Context mContext = null;
	private TypefaceUtils mTypefaceUtils = null;
	private ArrayList<OrderData> list = null;
	private SimpleDateFormat format = null;

	public StaffOrderListAdapter(Context context, ArrayList<OrderData> list) {
		this.mContext = context;
		this.list = list;

		mInflater = LayoutInflater.from(mContext);
		mTypefaceUtils = TypefaceUtils.getInstance(context);
		// format = new SimpleDateFormat("EEE dd MMM, yyyy",
		// Locale.getDefault());
		format = new SimpleDateFormat("yyyy-MM-dd '-' hh:mm a", Locale.getDefault());

	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder;

		if (convertView == null) {

			convertView = mInflater.inflate(R.layout.staff_order_list_row, null);
			mHolder = new ViewHolder();

			mHolder.txtTitle = (TextView) convertView.findViewById(R.id.txtTitle);
			mHolder.txtTime = (TextView) convertView.findViewById(R.id.txtTime);
			mHolder.txtUserName = (TextView) convertView.findViewById(R.id.txtUserName);
			mHolder.txtContact = (TextView) convertView.findViewById(R.id.txtContact);

			mTypefaceUtils.applyTypeface(mHolder.txtTitle);
			mTypefaceUtils.applyTypeface(mHolder.txtTime);
			mTypefaceUtils.applyTypeface(mHolder.txtUserName);
			mTypefaceUtils.applyTypeface(mHolder.txtContact);

			convertView.setTag(mHolder);

		} else {

			mHolder = (ViewHolder) convertView.getTag();
		}

		final OrderData data = list.get(position);

		// if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH))
		// {
		// if (!Utils.isEmpty(data.getCompanyNameEN())) {
		// mHolder.txtTitle.setText(data.getCompanyNameEN());
		// }
		// } else {
		// if (!Utils.isEmpty(data.getCompanyName())) {
		// mHolder.txtTitle.setText(data.getCompanyName());
		// }
		// }

		if (data.getOrderDeadlineDate() != null) {
			mHolder.txtTime.setText(format.format(data.getOrderDeadlineDate()));
		}

		String name = "";
		if (!Utils.isEmpty(data.getFirstName())) {
			name = data.getFirstName();
		}
		if (!Utils.isEmpty(data.getLastName())) {
			name += " " + data.getLastName();
		}
		// mHolder.txtUserName.setText(name);

		mHolder.txtTitle.setText(data.getOrderId() + " -- " + name);

		mHolder.txtUserName.setText("$" + Utils.formatDecimal("" + data.getTotalAmount()));

		if (!Utils.isEmpty(data.getMobileNumber())) {
			mHolder.txtContact.setText(data.getMobileNumber());
		} else {
			mHolder.txtContact.setText("");
		}

		return convertView;
	}

	private static class ViewHolder {
		TextView txtTitle, txtTime, txtUserName, txtContact;
	}
}
