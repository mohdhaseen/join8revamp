package com.join8.adpaters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.model.RestaurantMenuData;
import com.join8.utils.ConstantData;
import com.join8.utils.TypefaceUtils;

public class RestaurantMenuAdapter extends BaseAdapter {

	private LayoutInflater mInflater;
	private ArrayList<RestaurantMenuData> listData;
	private TypefaceUtils mTypefaceUtils;

	public RestaurantMenuAdapter(Context context, ArrayList<RestaurantMenuData> list) {
		mInflater = LayoutInflater.from(context);
		this.listData = list;

		mTypefaceUtils = TypefaceUtils.getInstance(context);
	}

	@Override
	public int getCount() {
		return listData.size();
	}

	@Override
	public Object getItem(int position) {
		return listData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View view = convertView;

		if (view == null) {
			view = mInflater.inflate(R.layout.row_restaurant_gallery_menu, parent, false);
		}

		RestaurantMenuData data = listData.get(position);

		if (data != null) {

			TextView txtName = (TextView) view.findViewById(R.id.txtName);

			mTypefaceUtils.applyTypeface(txtName);

			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				txtName.setText(data.getMenuNameEn());
			} else {
				txtName.setText(data.getMenuName());
			}
		}

		return view;
	}
}
