package com.join8.adpaters;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouchBase.DisplayType;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;

import com.join8.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

public class ImagePreviewAdapter extends PagerAdapter {

	private Context context = null;
	private ArrayList<String> listImages;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private LayoutInflater mLayoutInflater = null;

	public ImagePreviewAdapter(Context context, ArrayList<String> listImages) {
		this.context = context;
		this.listImages = listImages;

		mLayoutInflater = LayoutInflater.from(context);

		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().imageScaleType(ImageScaleType.EXACTLY).bitmapConfig(Bitmap.Config.RGB_565).showStubImage(R.drawable.trans).showImageForEmptyUri(R.drawable.trans).showImageOnFail(R.drawable.trans).cacheInMemory(false).cacheOnDisc(true).build();
	}

	@Override
	public int getCount() {
		return listImages.size();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return arg0 == ((View) arg1);
	}

	@Override
	public Object instantiateItem(View collection, int position) {

		View view = mLayoutInflater.inflate(R.layout.view_image_preview, null);

		ImageViewTouch imvPhoto = (ImageViewTouch) view.findViewById(R.id.imvPhoto);
		final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
		imvPhoto.setDisplayType(DisplayType.FIT_TO_SCREEN);
		imvPhoto.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		imageLoader.displayImage(listImages.get(position), imvPhoto, options, new ImageLoadingListener() {

			@Override
			public void onLoadingStarted(String imageUri, View view) {
				progressBar.setVisibility(View.VISIBLE);
			}

			@Override
			public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
				progressBar.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				progressBar.setVisibility(View.GONE);
			}

			@Override
			public void onLoadingCancelled(String imageUri, View view) {
				progressBar.setVisibility(View.GONE);
			}
		});
		((ViewPager) collection).addView(view, 0);
		return view;
	}

	@Override
	public void destroyItem(View container, int position, Object object) {
		((ViewPager) container).removeView((View) object);
	}
}
