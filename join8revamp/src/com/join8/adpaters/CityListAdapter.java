package com.join8.adpaters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.model.City;
import com.join8.utils.ConstantData;

public class CityListAdapter extends BaseAdapter {

	private LayoutInflater mInflater;
	private ArrayList<City> arrCity;

	public CityListAdapter(Context c, int resource, ArrayList<City> city) {
		mInflater = LayoutInflater.from(c);
		this.arrCity = city;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrCity.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return arrCity.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.row_city, parent, false);
			mHolder = new ViewHolder();

			mHolder.txtCity = (TextView) convertView.findViewById(R.id.txtCity);

			convertView.setTag(mHolder);
		} else {
			mHolder = (ViewHolder) convertView.getTag();
		}

		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
			mHolder.txtCity.setText(arrCity.get(position).getCityNameEN());
		} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_SIMPLECHINSESE)) {
			mHolder.txtCity.setText(arrCity.get(position).getCityNameSC());
		} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_TRADITIONALCHINESE)) {
			mHolder.txtCity.setText(arrCity.get(position).getCityNameTC());
		}

		return convertView;
	}

	private static class ViewHolder {
		TextView txtCity;
	}
}
