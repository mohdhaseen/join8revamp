package com.join8.adpaters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.join8.R;

import java.util.ArrayList;

/**
 * Created by craterzone on 28/1/16.
 */
public class ImageAdapter extends BaseAdapter {

	private ArrayList<Bitmap> images;
	private Context mContext;

	public ImageAdapter(FragmentActivity mContext, ArrayList<Bitmap> uris) {
		this.mContext = mContext;
		this.images = uris;
	}

	@Override
	public int getCount() {
		return images.size();
	}

	@Override
	public Object getItem(int position) {
		return images.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		LayoutInflater mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.image_item, null);
			holder = new ViewHolder();
			holder.imgCaptured = (ImageView) convertView.findViewById(R.id.img_item);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.imgCaptured.setImageBitmap(images.get(position));

		return convertView;
	}

	public class ViewHolder {
		protected ImageView imgCaptured;
		protected ImageView imgCancel;

	}

}
