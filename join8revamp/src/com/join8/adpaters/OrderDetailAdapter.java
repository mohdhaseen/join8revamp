package com.join8.adpaters;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.fragments.OrderDetailFragment;
import com.join8.model.RestaurantMenuItemData;
import com.join8.utils.ConstantData;
import com.join8.utils.DialogHelper;
import com.join8.utils.KeyboardUtils;
import com.join8.utils.Log;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;

public class OrderDetailAdapter extends BaseAdapter {

	private FragmentActivity mContext;
	private LayoutInflater mInflater;
	private TypefaceUtils mTypefaceUtils;
	private ArrayList<RestaurantMenuItemData> listItems;
	private ChangeCart changeCart;
	private OnItemDeleteListener onItemDeleteListener;
	private boolean editable = true;

	public OrderDetailAdapter(FragmentActivity c, ArrayList<RestaurantMenuItemData> listItems, ChangeCart changeCart, OnItemDeleteListener onItemDeleteListener) {
		this.mContext = c;
		mInflater = LayoutInflater.from(mContext);
		mTypefaceUtils = TypefaceUtils.getInstance(mContext);
		this.listItems = listItems;
		this.changeCart = changeCart;
		this.onItemDeleteListener = onItemDeleteListener;
	}

	public OrderDetailAdapter(FragmentActivity c, ArrayList<RestaurantMenuItemData> listItems, ChangeCart changeCart, boolean editable) {
		this.mContext = c;
		mInflater = LayoutInflater.from(mContext);
		mTypefaceUtils = TypefaceUtils.getInstance(mContext);
		this.listItems = listItems;
		this.changeCart = changeCart;
		this.editable = editable;
	}

	@Override
	public int getCount() {
		return listItems.size();
	}

	@Override
	public RestaurantMenuItemData getItem(int position) {
		return listItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.row_order_details, null);
			mHolder = new ViewHolder();
			mHolder.btnCancel = (ImageButton) convertView.findViewById(R.id.btnCancel);
			mHolder.txtItemName = (TextView) convertView.findViewById(R.id.txtItemName);
			mHolder.txtQty = (TextView) convertView.findViewById(R.id.txtQty);
			mHolder.txtPrice = (TextView) convertView.findViewById(R.id.txtPrice);
			convertView.setTag(mHolder);
		} else {
			mHolder = (ViewHolder) convertView.getTag();
		}

		if (editable) {
			mHolder.btnCancel.setVisibility(View.VISIBLE);
			mHolder.txtQty.setEnabled(true);
		} else {
			mHolder.btnCancel.setVisibility(View.INVISIBLE);
			mHolder.txtQty.setEnabled(false);
		}

		final RestaurantMenuItemData data = getItem(position);

		String itemName = "";

		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
			itemName = data.getItemName_EN();
		} else {
			itemName = data.getItemName();
		}

		double price = data.getPrice();

		if (data.getFoodOptions() != null && data.getFoodOptions().size() > 0) {

			for (int i = 0; i < data.getFoodOptions().size(); i++) {

				price += data.getFoodOptions().get(i).getExtraPrice();

				if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
					itemName += "\n--" + data.getFoodOptions().get(i).getFoodOptionItemNameEn();
				} else {
					itemName += "\n--" + data.getFoodOptions().get(i).getFoodOptionItemName();
				}
			}
		}

		mHolder.txtItemName.setText(itemName);
		mHolder.txtQty.setText("" + (data.getQuantity() == -1 ? 0 : data.getQuantity()));
		mHolder.txtPrice.setText("$" + Utils.formatDecimal("" + price));

		mTypefaceUtils.applyTypeface(mHolder.txtQty);
		mTypefaceUtils.applyTypeface(mHolder.txtItemName);
		mTypefaceUtils.applyTypeface(mHolder.txtPrice);
		
		mHolder.txtQty.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				KeyboardUtils.hideKeyboard(v);
				showPicker(position);
			}
		});

		mHolder.btnCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				showDeleteDialog(position);
			}
		});

		return convertView;
	}

	private static class ViewHolder {
		TextView txtQty, txtItemName, txtPrice;
		ImageButton btnCancel;
	}

	private void showPicker(final int position) {
		RelativeLayout linearLayout = new RelativeLayout(mContext);
		final NumberPicker aNumberPicker = new NumberPicker(mContext);
		aNumberPicker.setMaxValue(100);
		aNumberPicker.setMinValue(1);
		int quntity = listItems.get(position).getQuantity();
		aNumberPicker.setValue((quntity <= 0 || quntity > 100) ? 1 : quntity);

		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(50, 50);
		RelativeLayout.LayoutParams numPicerParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		numPicerParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

		linearLayout.setLayoutParams(params);
		linearLayout.addView(aNumberPicker, numPicerParams);

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
		alertDialogBuilder.setTitle(mContext.getString(R.string.select_qty));
		alertDialogBuilder.setView(linearLayout);
		alertDialogBuilder.setCancelable(false).setPositiveButton(mContext.getString(R.string.tvOk), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

				Log.e("update","po = "+position+" qry = "+aNumberPicker.getValue());
				
				OrderDetailFragment.listItems.get(position).setQuantity(aNumberPicker.getValue());
				OrderDetailAdapter.this.notifyDataSetChanged();
				changeCart.changeTotal();

			}
		}).setNegativeButton(mContext.getString(R.string.cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	public interface ChangeCart {
		public void changeTotal();
	}

	public interface OnItemDeleteListener {
		public void itemDeleted();
	}

	private void showDeleteDialog(final int position) {

		DialogHelper.popupMessage(mContext, mContext.getString(R.string.app_name), mContext.getString(R.string.delete_item_error), mContext.getString(R.string.yes), mContext.getString(R.string.no), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				try {

					OrderDetailFragment.listItemIds.remove(position);
					OrderDetailFragment.listItems.remove(position);
					OrderDetailAdapter.this.notifyDataSetChanged();
					if (changeCart != null) {
						changeCart.changeTotal();
					}
					if (onItemDeleteListener != null) {
						onItemDeleteListener.itemDeleted();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		}, true);
	}
}
