package com.join8.adpaters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.model.OrderData;
import com.join8.utils.ConstantData;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;

public class StaffOrderDetailsAdapter1 extends BaseAdapter {

	private Context mContext;
	private LayoutInflater mInflater;
	private TypefaceUtils mTypefaceUtils;
	private ArrayList<OrderData> orderList;

	public StaffOrderDetailsAdapter1(Context c, ArrayList<OrderData> orderList) {
		// TODO Auto-generated constructor stub
		this.mContext = c;
		this.orderList = orderList;
		mTypefaceUtils = TypefaceUtils.getInstance(mContext);
		mInflater = LayoutInflater.from(c);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return orderList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	private static class ViewHolder {
		TextView tvQty, tvItemName, tvItemSubTital, tvPrice;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder;

		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.staff_order_details_row, null);
			mHolder = new ViewHolder();
			mHolder.tvItemName = (TextView) convertView.findViewById(R.id.tvItemName);
			mHolder.tvItemSubTital = (TextView) convertView.findViewById(R.id.tvItemSubTital);
			mHolder.tvQty = (TextView) convertView.findViewById(R.id.tvQty);
			mHolder.tvPrice = (TextView) convertView.findViewById(R.id.tvPrice);
			convertView.setTag(mHolder);

		} else {
			mHolder = (ViewHolder) convertView.getTag();
		}
		final OrderData data = orderList.get(position);

		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
			mHolder.tvItemName.setText(data.getItemName_EN());
			mHolder.tvItemSubTital.setText(data.getItemName());
		} else {
			mHolder.tvItemName.setText(data.getItemName());
			mHolder.tvItemSubTital.setText(data.getItemName_EN());
		}

		mHolder.tvQty.setText("" + (data.getQuantity() == -1 ? 0 : data.getQuantity()));
		mHolder.tvPrice.setText("$" + Utils.formatDecimal("" + data.getPrice()));

		mTypefaceUtils.applyTypeface(mHolder.tvQty);
		mTypefaceUtils.applyTypeface(mHolder.tvItemName);
		mTypefaceUtils.applyTypeface(mHolder.tvItemSubTital);
		mTypefaceUtils.applyTypeface(mHolder.tvPrice);

		return convertView;

	}

}
