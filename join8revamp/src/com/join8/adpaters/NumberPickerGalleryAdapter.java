package com.join8.adpaters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.join8.R;
import com.join8.utils.TypefaceUtils;

public class NumberPickerGalleryAdapter extends BaseAdapter {

	private Context context;
	private LayoutInflater mInflater;
	private ArrayList<Integer> list;

	public NumberPickerGalleryAdapter(Context context, ArrayList<Integer> list) {
		this.context = context;
		this.list = list;
		mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder mHolder;
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.row_num_picker_gallery, parent, false);
			mHolder = new ViewHolder();

			mHolder.txtNo = (TextView) convertView.findViewById(R.id.textView);

			convertView.setTag(mHolder);
		} else {
			mHolder = (ViewHolder) convertView.getTag();
		}

		mHolder.txtNo.setText("" + list.get(position));

		TypefaceUtils.getInstance(context).applyTypeface(mHolder.txtNo);

		return convertView;
	}

	private static class ViewHolder {

		TextView txtNo;

	}
}
