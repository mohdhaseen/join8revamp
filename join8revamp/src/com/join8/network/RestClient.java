package com.join8.network;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import android.util.Log;

import com.join8.HomeScreen;
import com.join8.listeners.Requestlistener;
import com.join8.request.Join8RequestBuilder;
import com.join8.request.PARAMS;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.Utils;

public class RestClient {

	private static final String TAG = RestClient.class.getSimpleName();
	private Context context;
	private ArrayList<NameValuePair> params;
	private ArrayList<NameValuePair> headers;
	private Requestlistener listener;
	private String url;

	private int responseCode;
	private String message;
	private int requestId;
	private String response;

	public String getResponse() {
		return response;
	}

	public String getErrorMessage() {
		return message;
	}

	public int getResponseCode() {
		return responseCode;
	}

	public RestClient(Context context, String url, Requestlistener listener, int requestId) {
		this.context = context;
		this.url = url;
		this.listener = listener;
		this.requestId = requestId;
		params = new ArrayList<NameValuePair>();
		headers = new ArrayList<NameValuePair>();
	}

	public void AddParam(String name, String value) {
		params.add(new BasicNameValuePair(name, value));
	}

	public void AddHeader(String name, String value) {
		headers.add(new BasicNameValuePair(name, value));
	}

	public void execute(RequestMethod method) throws Exception {

		switch (method) {

		case GET: {

			refreshTokenIfRequired();

			// add parameters
			String combinedParams = "";
			if (!params.isEmpty()) {
				combinedParams += "?";
				for (NameValuePair p : params) {
					String paramString = p.getName() + "=" + URLEncoder.encode(p.getValue(), "UTF-8");
					if (combinedParams.length() > 1) {
						combinedParams += "&" + paramString;
					} else {
						combinedParams += paramString;
					}
				}
			}

			HttpGet request = new HttpGet(url + combinedParams);

			// add headers
			for (NameValuePair h : headers) {
				request.addHeader(h.getName(), h.getValue());
			}

			executeRequest(request, url);
			break;
		}

		case POST: {

			refreshTokenIfRequired();

			HttpPost request = new HttpPost(url);

			// add headers
			for (NameValuePair h : headers) {
				request.addHeader(h.getName(), h.getValue());

			}
			executeRequest(request, url);
			break;
		}
		}
	}

	private void executeRequest(HttpUriRequest request, String url) throws Exception {

		Log.i(TAG, "Url => " + url);

		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, 20000);
		HttpConnectionParams.setSoTimeout(httpParameters, 30000);

		HttpClient client = new DefaultHttpClient(httpParameters);

		HttpResponse httpResponse = client.execute(request);
		responseCode = httpResponse.getStatusLine().getStatusCode();
		message = httpResponse.getStatusLine().getReasonPhrase();

		Log.i(TAG, "HTTP Response Code = " + responseCode + " HTTP Response Message = " + message);

		HttpEntity entity = httpResponse.getEntity();

		if (entity != null) {
			response = Utils.convertStreamToString(entity.getContent());			
			Log.i(TAG, "Response => " + response);
		} else {
			Log.e(TAG, "Inputstream is null");
			listener.onError(requestId, "SocketTimeout exception");
		}
	}

	private void refreshTokenIfRequired() {
		try {

			SharedPreferences sharedPreferences = CryptoManager.getInstance(context).getPrefs();
			int expiryTime = sharedPreferences.getInt(HomeScreen.PARAM_TOKEN_EXPIRY_TIME, 0);
			long diffTime = Calendar.getInstance().getTimeInMillis() - sharedPreferences.getLong(HomeScreen.PARAM_TOKEN_RECEIVED_TIME, 0);
			diffTime = diffTime / (1000 * 60);

			if (diffTime >= expiryTime) {
				
				Log.e(TAG, "========== Access Token is Expired ==========");

				HttpPost request = new HttpPost(ConstantData.WEBSERVICE_URL + Join8RequestBuilder.METHOD_LOGIN);

				// add headers
				request.addHeader("Content-Type", ConstantData.WEBSERVICE_CONTENTTYPE);
				request.addHeader(PARAMS.TAG_USERNAME, ConstantData.USERNAME_FOR_TOKEN);
				request.addHeader(PARAMS.TAG_PASSWORD, ConstantData.PASSWORD_FOR_TOKEN);

				HttpParams httpParameters = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(httpParameters, 20000);
				HttpConnectionParams.setSoTimeout(httpParameters, 30000);

				HttpClient client = new DefaultHttpClient(httpParameters);
				HttpResponse httpResponse = client.execute(request);
				HttpEntity entity = httpResponse.getEntity();
				if (entity != null) {

					String res = Utils.convertStreamToString(entity.getContent());

					if (!TextUtils.isEmpty(res)) {

						JSONObject jObject = new JSONObject(res);
						
						if (jObject.has("access_token")) {

							Editor editor = CryptoManager.getInstance(context).getPrefs().edit();
							editor.putString(HomeScreen.PARAM_TOKEN, jObject.getString("access_token"));
							editor.putInt(HomeScreen.PARAM_TOKEN_EXPIRY_TIME, jObject.getInt("Expire_Time"));
							editor.putLong(HomeScreen.PARAM_TOKEN_RECEIVED_TIME, Calendar.getInstance().getTimeInMillis());
							editor.apply();

							Log.i(TAG, "AccessToken =>> " + jObject.getString("access_token"));
							Log.i(TAG, "Token Expiry Time =>> " + jObject.getString("Expire_Time"));

							if (params != null && params.size() > 0) {

								for (int i = 0; i < headers.size(); i++) {
									BasicNameValuePair bnvp = (BasicNameValuePair) headers.get(i);
									if (bnvp.getName().equalsIgnoreCase(PARAMS.TAG_TOKEN)) {
										headers.set(i, new BasicNameValuePair(PARAMS.TAG_TOKEN, jObject.getString("access_token")));
										break;
									}
								}
							}
						}
					}
				}
			}else{
				Log.i(TAG, "Access Token is Valid");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
