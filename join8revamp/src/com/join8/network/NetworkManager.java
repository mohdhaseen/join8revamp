package com.join8.network;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.join8.R;
import com.join8.listeners.Requestlistener;
import com.join8.utils.ConnectivityTools;
import com.join8.utils.FormatUtils;

public class NetworkManager implements Requestlistener {

	private static NetworkManager instance = null;
	ArrayList<Requestlistener> arrRequestlisteners = null;
	private int requestId;
	public boolean isAvailable = false;
	private Context context = null;

	public static NetworkManager getInstance() {
		if (instance == null)
			instance = new NetworkManager();
		return instance;
	}

	public synchronized int addRequest(HashMap<String, String> request, RequestMethod method, Context context, String ws_method) {
		return addRequest(request, method, context, ws_method, isAvailable);
	}

	public synchronized int addRequest(HashMap<String, String> request, Context context, boolean which, String url, String req, String method) {
		return addRequest(request, context, isAvailable, which, url, req, method);
	}
	@SuppressWarnings("unchecked")
	public synchronized int addRequest(HashMap<String, String> request, Context context, boolean isProgressVisible, boolean which, String url, String req, String method) {

		try {
			this.context = context;

			requestId = FormatUtils.getInstance().getUniqueId();

			if (ConnectivityTools.isNetworkAvailable(context)) {

				NetworkClient networkClient = new NetworkClient(requestId, this, context, isProgressVisible, which, url, req, method);
				networkClient.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, request);

			} else {
				onError(requestId, context.getString(R.string.nointerent));
			}
		} catch (Exception e) {
			e.printStackTrace();
			onError(requestId, e.toString());
		}

		return requestId;
	}
	@SuppressWarnings("unchecked")
	public synchronized int addRequest(HashMap<String, String> request, RequestMethod method, Context context, String ws_method, boolean isProgressVisible) {

		try {
			this.context = context;

			requestId = FormatUtils.getInstance().getUniqueId();

			if (ConnectivityTools.isNetworkAvailable(context)) {

				NetworkClient networkClient = new NetworkClient(requestId, this, method, ws_method, context, isProgressVisible);
				networkClient.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, request);

			} else {
				onError(requestId, context.getString(R.string.nointerent));
			}
		} catch (Exception e) {
			e.printStackTrace();
			onError(requestId, e.toString());
		}

		return requestId;
	}

	private NetworkManager() {
		arrRequestlisteners = new ArrayList<Requestlistener>();
	}

	public int getRequestId() {
		return requestId;

	}

	public void isProgressVisible(boolean isAvailable) {
		this.isAvailable = isAvailable;

	}

	public synchronized void addListener(Requestlistener listener) {
		if (!arrRequestlisteners.contains(listener))
			arrRequestlisteners.add(listener);
	}

	@Override
	public void onSuccess(int id, String response) {
		Log.d(NetworkManager.class.getSimpleName(), "[onSuccess] arrRequestlisteners=" + arrRequestlisteners.size());

		synchronized (arrRequestlisteners) {
			if (arrRequestlisteners != null && arrRequestlisteners.size() > 0) {
				for (Requestlistener listener : arrRequestlisteners) {
					if (listener != null)
						listener.onSuccess(id, response);
				}
			}
		}
	}

	@Override
	public void onError(final int id, final String message) {

		synchronized (arrRequestlisteners) {
			if (arrRequestlisteners != null && arrRequestlisteners.size() > 0) {
				for (final Requestlistener listener : arrRequestlisteners) {
					if (listener != null) {
						if (message == null || message.contains("SocketException") || message.contains("UnknownHostException") || message.contains("SocketTimeoutException")) {
							listener.onError(id, context.getString(R.string.nointerent));
						} else {
							listener.onError(id, message);
						}
					}
				}
			}
		}
	}

	public void removeListeners(Requestlistener listener) {
		arrRequestlisteners.remove(listener);
	}

	public int getListenerSize() {
		return arrRequestlisteners.size();
	}
}
