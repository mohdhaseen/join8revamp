package com.join8.network;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;

import android.content.Context;
import android.os.AsyncTask;

import com.join8.listeners.Requestlistener;
import com.join8.utils.ConstantData;
import com.join8.utils.HttpClientUtil;
import com.join8.utils.StreamUtility;

public class NetworkClient extends AsyncTask<HashMap<String, String>, String, HashMap<String, String>> {

	private static final String TAG = "NETWORK Client";
	private Requestlistener listener;
	private int requestId;
	private RequestMethod method;
	private String ws_method;
	private HashMap<String, String> response = null;
	private Context context;
	private boolean isProgressVisible = false;
	private com.join8.layout.MyProgressDialog progressDialog;
	private boolean newParam;
	private String url;
	private String req;
	public NetworkClient(int requestId, Requestlistener listener, RequestMethod method, String ws_method, Context context, boolean isProgressVisible) {
		this.listener = listener;
		this.requestId = requestId;
		this.method = method;
		this.ws_method = ws_method;
		this.context = context;
		this.isProgressVisible = isProgressVisible;
	}
	public NetworkClient(int requestId, Requestlistener listener, Context context, boolean isProgressVisible, boolean which, String url, String request, String req_method) {
		this.listener = listener;
		this.requestId = requestId;
		this.context = context;
		this.isProgressVisible = isProgressVisible;
		this.newParam = which;
		this.url = url;
		this.req = request;
		this.ws_method = req_method;
	}
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		try {
			if (isProgressVisible)
				showProgressDialog();
		} catch (Exception e) {
		}

	}

	@Override
	protected HashMap<String, String> doInBackground(HashMap<String, String>... request) {

		try {

			response = new HashMap<String, String>();
			HttpResponse res = null;
			if (newParam) {
				if (ws_method.equals(ConstantData.method_post)) {
					res = HttpClientUtil.postWithAuth(context, url, req, ConstantData.CONTENT_TYPE_WAITER, ConstantData.CONTENT_TYPE_WAITER);
				} else if (ws_method.equals(ConstantData.method_get)) {
					res = HttpClientUtil.getWithAuth(context, url, ConstantData.CONTENT_TYPE_WAITER);
				}
				else if (ws_method.equals(ConstantData.method_put)) {
					res = HttpClientUtil.putWithAuth(context, url, req, ConstantData.CONTENT_TYPE_WAITER, ConstantData.CONTENT_TYPE_WAITER);
				}
				response.put("status", String.valueOf(res.getStatusLine().getStatusCode()));
				response.put("result", StreamUtility.getJSONStringFromResponse(res));
				return response;
			}
			RestClient client = new RestClient(context, ConstantData.WEBSERVICE_URL + ws_method, listener, requestId);
			client.AddParam("Content-Type", ConstantData.WEBSERVICE_CONTENTTYPE);
			if (request.length > 0) {
				for (Map.Entry<String, String> entry : request[0].entrySet()) {
					client.AddHeader(entry.getKey(), entry.getValue());
				}
			}
			client.execute(method);
			String result = client.getResponse();
			// Log.d(TAG, "Response= " + client.getResponse());

			response.put("status", "1");
			response.put("result", result);

		} catch (Exception e) {
			e.printStackTrace();
			response.put("status", "0");
			response.put("result", e.toString());
		}

		return response;
	}

	@Override
	protected void onPostExecute(HashMap<String, String> response) {
		super.onPostExecute(response);

		dismissProgressDialog();

		if (response != null && response.size() > 0) {
			if (response.get("status").equals("201")) {
				listener.onSuccess(requestId, response.get("result"));
				return;
			} else if (response.get("status").equals("200")) {
				listener.onSuccess(requestId, response.get("result"));
				return;
			} else if (response.get("status").equals("204")) {
				listener.onSuccess(requestId, response.get("result"));
				return;
			} else if (response.get("status").equals("1")) {
				listener.onSuccess(requestId, response.get("result"));
			} else {
				listener.onError(requestId, response.get("result"));
			}
		}
	}

	public void showProgressDialog() {

		progressDialog = new com.join8.layout.MyProgressDialog(context);
	}

	/**
	 * Dismiss custom progress dialog
	 */
	public void dismissProgressDialog() {

		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}
}
