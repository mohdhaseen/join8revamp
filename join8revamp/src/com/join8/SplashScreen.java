package com.join8;

import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;
import com.join8.request.Join8RequestBuilder;
import com.join8.request.PARAMS;
import com.join8.utils.ConnectivityTools;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.Log;
import com.join8.utils.Utils;

public class SplashScreen extends Activity {

	private final static String TAG = SplashScreen.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setTheme(R.style.fullscreenTheme);
		setContentView(R.layout.splash);

		PackageInfo pInfo;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			String version = pInfo.versionName;
			((TextView) findViewById(R.id.txtVersion)).setText("V " + version);
		} catch (NameNotFoundException e1) {
			e1.printStackTrace();
		}

		// try {
		// PackageInfo info =
		// getPackageManager().getPackageInfo(getPackageName(),
		// PackageManager.GET_SIGNATURES);
		// for (Signature signature : info.signatures) {
		// MessageDigest md = MessageDigest.getInstance("SHA");
		// md.update(signature.toByteArray());
		// android.util.Log.e("HashKey", Base64.encodeToString(md.digest(),
		// Base64.DEFAULT));
		// // 2jmj7l5rSw0yVb/vlWAYkK/YBwk= // debug
		// // pGCwtDC7cAflc72/nIzRR48RICc= // live
		// }
		// } catch (NameNotFoundException e) {
		// } catch (NoSuchAlgorithmException e) {
		// }

		try {

			String defaultLanguage = CryptoManager.getInstance(this).getPrefs().getString("language", ConstantData.LANG_TRADITIONALCHINESE);
			String[] str = defaultLanguage.split("_");
			Locale locale = new Locale(str[0], str[1]);
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			getResources().updateConfiguration(config, getResources().getDisplayMetrics());

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {

			// NotificationsManager.handleNotifications(this,
			// Constants.SENDER_ID, MyHandler.class);
			//
			// String connectionString =
			// "Endpoint=sb://hubnotification-ns.servicebus.windows.net/;SharedAccessKeyName=DefaultListenSharedAccessSignature;SharedAccessKey=OqrKYquzqUfdrGgYUBWGBmLMKCof8QDZyvQI6rB3/Yw=";
			// hub = new NotificationHub("hubnotification", connectionString,
			// this);

			if (ConnectivityTools.isNetworkAvailable(this)) {
				// refresh app token if already expired
				refreshTokenIfRequired();
				// get GPS token asynchronously
				getRegisterIDForGCM();
				// Start Home Activity
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {

						Intent intent = new Intent(SplashScreen.this, HomeScreen.class);
						startActivity(intent);
						finish();
					}
				}, 2000);

			} else {
				showMessage(getString(R.string.app_name), getString(R.string.nointerent));
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Join8 Error", "Error in notification : " + e.toString());
		}
	}

	public void showMessage(final String title, final String message) {

		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setTitle(title);
		dialog.setMessage(message);
		dialog.setPositiveButton(getString(R.string.tvOk), new android.content.DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		dialog.show();

	}

	private void getRegisterIDForGCM() {

		try {

			GCMRegistrar.checkDevice(this);
			GCMRegistrar.checkManifest(this);
			String registerId = GCMRegistrar.getRegistrationId(this);

			if (!ConstantData.RegistrationID.equals(registerId)) {
				ConstantData.RegistrationID = "";
			}

			if (ConstantData.RegistrationID.equals("")) {
				GCMRegistrar.register(this, ConstantData.SENDER_ID);
			}

			Log.i(TAG, "Join8 GCM registrationId : " + ConstantData.RegistrationID);

		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Error in SplashScreen", e.toString());
		}
	}

	private void refreshTokenIfRequired() {

		new Thread(new Runnable() {
			@Override
			public void run() {

				try {

					SharedPreferences sharedPreferences = CryptoManager.getInstance(SplashScreen.this).getPrefs();
					int expiryTime = sharedPreferences.getInt(HomeScreen.PARAM_TOKEN_EXPIRY_TIME, 0);
					long diffTime = Calendar.getInstance().getTimeInMillis() - sharedPreferences.getLong(HomeScreen.PARAM_TOKEN_RECEIVED_TIME, 0);
					diffTime = diffTime / (1000 * 60);

					if (diffTime >= expiryTime) {

						Log.e(TAG, "========== Access Token is Expired ==========");

						HttpPost request = new HttpPost(ConstantData.WEBSERVICE_URL + Join8RequestBuilder.METHOD_LOGIN);

						// add headers
						request.addHeader("Content-Type", ConstantData.WEBSERVICE_CONTENTTYPE);
						request.addHeader(PARAMS.TAG_USERNAME, ConstantData.USERNAME_FOR_TOKEN);
						request.addHeader(PARAMS.TAG_PASSWORD, ConstantData.PASSWORD_FOR_TOKEN);

						HttpParams httpParameters = new BasicHttpParams();
						HttpConnectionParams.setConnectionTimeout(httpParameters, 20000);
						HttpConnectionParams.setSoTimeout(httpParameters, 30000);

						HttpClient client = new DefaultHttpClient(httpParameters);
						HttpResponse httpResponse = client.execute(request);
						HttpEntity entity = httpResponse.getEntity();
						if (entity != null) {

							String res = Utils.convertStreamToString(entity.getContent());

							if (!TextUtils.isEmpty(res)) {

								JSONObject jObject = new JSONObject(res);

								if (jObject.has("access_token")) {

									Editor editor = CryptoManager.getInstance(SplashScreen.this).getPrefs().edit();
									editor.putString(HomeScreen.PARAM_TOKEN, jObject.getString("access_token"));
									editor.putInt(HomeScreen.PARAM_TOKEN_EXPIRY_TIME, jObject.getInt("Expire_Time"));
									editor.putLong(HomeScreen.PARAM_TOKEN_RECEIVED_TIME, Calendar.getInstance().getTimeInMillis());
									editor.apply();

									Log.i(TAG, "AccessToken =>> " + jObject.getString("access_token"));
									Log.i(TAG, "Token Expiry Time =>> " + jObject.getString("Expire_Time"));
								}
							}
						}
					} else {
						Log.i(TAG, "Access Token is Valid");
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	@Override
	public void onBackPressed() {
		System.exit(0);
		super.onBackPressed();
	}

}
