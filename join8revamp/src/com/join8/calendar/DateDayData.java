package com.join8.calendar;

public class DateDayData {

	private int date = 0;
	private String day = "";

	public DateDayData(int date, String day) {
		super();
		this.date = date;
		this.day = day;
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

}
