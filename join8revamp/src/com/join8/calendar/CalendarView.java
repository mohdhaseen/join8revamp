package com.join8.calendar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.join8.R;
import com.join8.calendar.CalendarWrapper.OnDateChangedListener;
import com.join8.utils.TypefaceUtils;

public class CalendarView extends LinearLayout {

	private CalendarWrapper _calendar;
	private TableLayout _days;
	private TextView txtMonth;
	private ImageView _prev;
	private ImageView _next;
	private OnMonthChangedListener _onMonthChangedListener;
	private OnSelectedDayChangedListener _onSelectedDayChangedListener;
	private int _currentView;
	private int _currentYear;
	private int _currentMonth;
	private Context context;
	private TypefaceUtils mTypefaceUtils;

	public CalendarView(Context context) {
		super(context);
		this.context = context;

		init(context);
	}

	public CalendarView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;

		init(context);
	}

	public CalendarView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		this.context = context;

		init(context);
	}

	public interface OnMonthChangedListener {
		public void onMonthChanged(CalendarView view);
	}

	public void setOnMonthChangedListener(OnMonthChangedListener l) {
		_onMonthChangedListener = l;
	}

	public interface OnSelectedDayChangedListener {
		public void onSelectedDayChanged(CalendarView view);
	}

	public void setOnSelectedDayChangedListener(OnSelectedDayChangedListener l) {
		_onSelectedDayChangedListener = l;
	}

	public Calendar getVisibleStartDate() {
		return _calendar.getVisibleStartDate();
	}

	public Calendar getVisibleEndDate() {
		return _calendar.getVisibleEndDate();
	}

	public Calendar getSelectedDay() {
		return _calendar.getSelectedDay();
	}

	public int getCurrentSelectedYear() {

		return _calendar.getYear();
	}

	public int getCurrentSelectedMonth() {

		return _calendar.getMonth();
	}

	public String getStartDateOfMonth() {

		return _calendar.getStartDateOfMonth();
	}

	public String getEndDateOfMonth() {

		return _calendar.getEndDateOfMonth();
	}

	public int getEndDayOfMonth() {

		return _calendar.getEndDayOfMonth();
	}

	private void init(Context context) {

		View v = LayoutInflater.from(context).inflate(R.layout.view_calendar, this, true);

		_calendar = new CalendarWrapper();

		txtMonth = (TextView) findViewById(R.id.txtMonth);

		_days = (TableLayout) v.findViewById(R.id.days);
		_prev = (ImageView) v.findViewById(R.id.previous);
		_next = (ImageView) v.findViewById(R.id.next);

		setMonth();

		_days.setVisibility(View.VISIBLE);

		refreshCurrentDate();

		// Days Table
		String[] shortWeekDayNames = _calendar.getShortDayNames();

		for (int i = 0; i < 7; i++) { // Rows
			TableRow tr = (TableRow) _days.getChildAt(i);

			for (int j = 0; j < 7; j++) { // Columns
				Boolean header = i == 0; // First row is weekday headers
				TextView tv = (TextView) tr.getChildAt(j);

				if (header) {
					tv.setText(shortWeekDayNames[j]);
				}
			}
		}

		refreshDayCells(new ArrayList<Integer>());

		// Listeners
		_calendar.setOnDateChangedListener(_dateChanged);
		_prev.setOnClickListener(_incrementClicked);
		_next.setOnClickListener(_incrementClicked);

		mTypefaceUtils = TypefaceUtils.getInstance(context);
		mTypefaceUtils.applyTypeface(txtMonth);

	}

	public CalendarWrapper getCalendar() {

		return _calendar;
	}

	private OnDateChangedListener _dateChanged = new OnDateChangedListener() {
		public void onDateChanged(CalendarWrapper sc) {

			Boolean monthChanged = _currentYear != sc.getYear() || _currentMonth != sc.getMonth();

			if (monthChanged) {

				invokeMonthChangedListener();
			}

			refreshCurrentDate();
			setMonth();
		}
	};

	private OnClickListener _incrementClicked = new OnClickListener() {
		public void onClick(View v) {

			int inc = (v == _next ? 1 : -1);
			_calendar.addMonth(inc);
		}
	};

	public void refreshDayCells(ArrayList<Integer> markers) {

		int[] dayGrid = _calendar.get7x6DayArray();
		int monthAdd = -1;
		int row = 1; // Skip weekday header row
		int col = 0;
		// CalendarDayMarker [] markers = null;

		if (mTypefaceUtils == null) {
			mTypefaceUtils = TypefaceUtils.getInstance(context);
		}

		Calendar tempCal = _calendar.getVisibleStartDate();
		Calendar cal = Calendar.getInstance(Locale.getDefault());

		for (int i = 0; i < dayGrid.length; i++) {
			int day = dayGrid[i];

			if (day == 1)
				monthAdd++;

			TableRow tr = (TableRow) _days.getChildAt(row);
			TextView tv = (TextView) tr.getChildAt(col);

			mTypefaceUtils.applyTypeface(tv);

			// Clear current markers, if any.
			tv.setBackgroundDrawable(null);
			tv.setTextColor(Color.BLACK);

			tv.setText(dayGrid[i] + "");

			tv.setTag(new int[] { monthAdd, dayGrid[i], 0 });

			// Set current date

			if (tempCal.get(Calendar.YEAR) == cal.get(Calendar.YEAR) && tempCal.get(Calendar.MONTH) == cal.get(Calendar.MONTH) && day == cal.get(Calendar.DAY_OF_MONTH)) {
				tv.setBackgroundColor(getResources().getColor(R.color.orangebg));
			}

			// New added code

			if (monthAdd == 0) {

				// if (col == 0) {
				//
				// tv.setTextColor(getResources().getColor(R.color.gray_2));
				// } else {
				// tv.setTextColor(getResources().getColor(R.color.allday_font));
				// }

				if (tempCal.get(Calendar.YEAR) == cal.get(Calendar.YEAR) && tempCal.get(Calendar.MONTH) == cal.get(Calendar.MONTH) && day == cal.get(Calendar.DAY_OF_MONTH)) {
					// Set current date
					tv.setBackgroundColor(getResources().getColor(R.color.orangebg));
				} else {
					// Set normal date
					tv.setBackgroundColor(getResources().getColor(R.color.white));
				}

				if (markers != null && markers.size() > 0) {

					for (int j = 0; j < markers.size(); j++) {
						if (markers.get(j) == dayGrid[i]) {

							// tv.setTextColor(Color.WHITE);
							// tv.setBackgroundColor(Color.RED);
							tv.setBackgroundResource(R.drawable.selected_day_bg);
							tv.setTag(new int[] { monthAdd, dayGrid[i], 1 });
							break;
						}
					}
				}

			} else {
				if (tempCal.get(Calendar.YEAR) == cal.get(Calendar.YEAR) && tempCal.get(Calendar.MONTH) == cal.get(Calendar.MONTH) && day == cal.get(Calendar.DAY_OF_MONTH)) {
					// Set current date
					tv.setBackgroundColor(getResources().getColor(R.color.orangebg));
				} else {
					// set normal date
					tv.setBackgroundColor(Color.WHITE);
				}

				tv.setTextColor(getResources().getColor(R.color.gray_2));
				tv.setBackgroundColor(Color.parseColor("#EFF1F0"));
			}

			tempCal.add(Calendar.DAY_OF_MONTH, 1);
			col++;

			if (col == 7) {
				col = 0;
				row++;
			}
		}

		// setCurrentDayMarker(new CalendarDayMarker(Calendar.getInstance(),
		// getResources().getColor(com.silentringtone.activity.R.color.today_bg)));
	}

	private void setView(int view) {
		if (_currentView != view) {
			_currentView = view;
			// _days.setVisibility(_currentView == MONTH_VIEW ? View.VISIBLE :
			// View.GONE);
			// _up.setEnabled(_currentView != DAY_VIEW);

			setMonth();
		}
	}

	private void refreshCurrentDate() {
		_currentYear = _calendar.getYear();
		_currentMonth = _calendar.getMonth();
		_calendar.getDay();
	}

	private void setMonth() {

		try {

			SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
			String month = dateFormat.format(_calendar.getDateObj());

			txtMonth.setText(month);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void invokeMonthChangedListener() {
		if (_onMonthChangedListener != null)
			_onMonthChangedListener.onMonthChanged(this);
	}

	private void invokeSelectedDayChangedListener() {
		if (_onSelectedDayChangedListener != null)
			_onSelectedDayChangedListener.onSelectedDayChanged(this);
	}

}
