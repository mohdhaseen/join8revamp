package com.join8.calendar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import android.content.Context;
import android.os.Handler;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.join8.R;
import com.join8.Toast;
import com.join8.adpaters.CalendarGalleryAdapter;
import com.join8.utils.TypefaceUtils;

public class CalendarGalleryView extends LinearLayout {

	private Calendar calendar;
	private TextView txtDateTime;
	private TextView txtMonth;
	private ImageView _prev;
	private ImageView _next;
	private Gallery galleryDays;
	private TypefaceUtils mTypefaceUtils;
	private CalendarGalleryAdapter adapter;
	private ArrayList<DateDayData> list;
	private int lastSelectedPosition = 0;
	private Handler handler = null;
	private int galleryPosition = 0;
	private int selectedDate = 0;
	private boolean isTouch = false;

	private OnDayChangeListner onDayChangeListner = null;
	private OnMonthChangeListner onMonthChangeListner = null;

	private SimpleDateFormat monthFormat = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
	private SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE dd MMMM", Locale.getDefault());

	public CalendarGalleryView(Context context) {
		super(context);

		init(context);
	}

	public CalendarGalleryView(Context context, AttributeSet attrs) {
		super(context, attrs);

		init(context);
	}

	public CalendarGalleryView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		init(context);
	}

	private void init(Context context) {

		View view = LayoutInflater.from(context).inflate(R.layout.view_calendar_gallery, this, true);

		calendar = Calendar.getInstance();

		txtDateTime = (TextView) findViewById(R.id.txtDateTime);
		txtMonth = (TextView) findViewById(R.id.txtMonth);
		_prev = (ImageView) view.findViewById(R.id.previous);
		_next = (ImageView) view.findViewById(R.id.next);
		galleryDays = (Gallery) view.findViewById(R.id.galleryDays);

		list = new ArrayList<DateDayData>();
		adapter = new CalendarGalleryAdapter(getContext(), list);
		galleryDays.setAdapter(adapter);

		refreshDayCells();

		galleryDays.setSelection(getDay() - 1);
		lastSelectedPosition = galleryDays.getSelectedItemPosition();

		// Listeners
		_prev.setOnClickListener(_incrementClicked);
		_next.setOnClickListener(_incrementClicked);

		mTypefaceUtils = TypefaceUtils.getInstance(context);
		mTypefaceUtils.applyTypeface(txtMonth);
		mTypefaceUtils.applyTypeface(txtDateTime);
		mTypefaceUtils.applyTypeface((TextView) view.findViewById(R.id.lblDateTime));

		galleryDays.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, final View view, int position, long arg3) {

				galleryPosition = position;
				selectedDate = (Integer) view.getTag() + 1;

				if (handler == null) {
					handler = new Handler();
				}
				handler.removeCallbacks(runnable);
				handler.postDelayed(runnable, 1000);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		galleryDays.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				isTouch = true;
				return false;
			}
		});

	}

	private OnClickListener _incrementClicked = new OnClickListener() {
		public void onClick(View v) {

			int inc = (v == _next ? 1 : -1);

			Calendar tempCal = (Calendar) calendar.clone();
			tempCal.add(Calendar.MONTH, inc);

			if (Calendar.getInstance().get(Calendar.YEAR) > tempCal.get(Calendar.YEAR) || (Calendar.getInstance().get(Calendar.YEAR) == tempCal.get(Calendar.YEAR) && Calendar.getInstance().get(Calendar.MONTH) > tempCal.get(Calendar.MONTH))) {
				Toast.displayText(getContext(), getContext().getString(R.string.less_month));
			} else {
				addMonth(inc);
			}
		}
	};

	Runnable runnable = new Runnable() {
		@Override
		public void run() {

			Calendar calNow = Calendar.getInstance();
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.MONTH, getMonth());
			cal.set(Calendar.YEAR, getYear());
			cal.set(Calendar.DAY_OF_MONTH, selectedDate);

			if (cal.getTimeInMillis() >= calNow.getTimeInMillis()) {
				lastSelectedPosition = galleryPosition;
				setDay(selectedDate);
				txtDateTime.setText(dateFormat.format(calendar.getTime()));
				if (isTouch) {
					isTouch = false;
					if (onDayChangeListner != null) {
						onDayChangeListner.onDayChanged(calendar);
					}
				}
			} else {
				isTouch = false;
				galleryDays.setSelection(lastSelectedPosition);
				Toast.displayText(getContext(), getContext().getString(R.string.less_date));
			}
		}
	};

	public void refreshDayCells() {

		// Days Table
		String[] shortWeekDayNames = getShortDayNames();

		list.clear();

		Calendar tempCal = getDate();
		tempCal.set(Calendar.DAY_OF_MONTH, 1);
		int dayIndex = tempCal.get(Calendar.DAY_OF_WEEK) - 1;

		for (int i = 0; i < getEndDayOfMonth(); i++) {

			list.add(new DateDayData(i + 1, shortWeekDayNames[dayIndex % 7]));
			dayIndex++;
		}

		txtMonth.setText(monthFormat.format(calendar.getTime()));
		txtDateTime.setText(dateFormat.format(calendar.getTime()));

		adapter = new CalendarGalleryAdapter(getContext(), list);
		galleryDays.setAdapter(adapter);

		galleryDays.setSelection(getDay() - 1);

	}

	private String[] getShortDayNames() {

		String[] shortDayNames = new String[calendar.getActualMaximum(Calendar.DAY_OF_WEEK)];

		try {
			for (int i = 0; i < shortDayNames.length; i++) {
				shortDayNames[i] = DateUtils.getDayOfWeekString(i + 1, DateUtils.LENGTH_MEDIUM);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return shortDayNames;
	}

	private int getEndDayOfMonth() {
		int endDayOfMonth = 1;
		endDayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		return endDayOfMonth;
	}

	private int getDay() {
		return calendar.get(Calendar.DAY_OF_MONTH);
	}

	private int getMonth() {
		return calendar.get(Calendar.MONTH);
	}

	private int getYear() {
		return calendar.get(Calendar.YEAR);
	}

	public Calendar getDate() {
		return (Calendar) calendar.clone();
	}

	public void setDate(Calendar cal) {
		calendar = cal;
		refreshDayCells();
	}

	private void addMonth(int month) {
		calendar.add(Calendar.MONTH, month);

		if (Calendar.getInstance().get(Calendar.MONTH) == calendar.get(Calendar.MONTH) && Calendar.getInstance().get(Calendar.DAY_OF_MONTH) > calendar.get(Calendar.DAY_OF_MONTH)) {
			calendar.set(Calendar.DAY_OF_MONTH, Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		}

		refreshDayCells();
		if (onMonthChangeListner != null) {
			onMonthChangeListner.onMonthChanged((Calendar) calendar.clone());
		}
	}

	private void setDay(int date) {
		calendar.set(Calendar.DAY_OF_MONTH, date);
	}

	public interface OnDayChangeListner {
		public void onDayChanged(Calendar cal);
	}

	public interface OnMonthChangeListner {
		public void onMonthChanged(Calendar cal);
	}

	public void setOnDayChangeListner(OnDayChangeListner onDayChangeListner) {
		this.onDayChangeListner = onDayChangeListner;
	}

	public void setOnMonthChangeListner(OnMonthChangeListner onMonthChangeListner) {
		this.onMonthChangeListner = onMonthChangeListner;
	}
}
