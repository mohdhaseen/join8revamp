package com.join8;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.join8.fragments.SelectCityFragment;
import com.join8.fragments.SelectLanguageFragment;
import com.join8.model.City;
import com.join8.model.ResponseCity;

public class PreferenceActivty extends FragmentActivity {
	public static int PREFS_CONTAINER = R.id.frmprefContainer;
	public static String PARAM_PREF = "prefs";
	public static String PARAM_CITY = "city";
	public static String PARAM_LANG = "language";
	public static String PARAM_CITIES = "cities";
	public static String PARAM_CITIES_NEW = "citiesnew";
	public ArrayList<City> cities;
	public ArrayList<ResponseCity> citiesNew;
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.preference);

		if (getIntent().getExtras() == null)
			return;
		if (getIntent().getExtras().getString(PARAM_PREF, "").equals(PARAM_LANG)) {
			SelectLanguageFragment sel_lang = new SelectLanguageFragment();
			getSupportFragmentManager().beginTransaction().replace(PREFS_CONTAINER, sel_lang).commit();

		} else {
			cities = (ArrayList<City>) getIntent().getExtras().getSerializable(PARAM_CITIES);
			citiesNew=getIntent().getExtras().getParcelableArrayList(PARAM_CITIES_NEW);
			SelectCityFragment sel_city = new SelectCityFragment();
			getSupportFragmentManager().beginTransaction().replace(PREFS_CONTAINER, sel_city).commit();

		}

	}

}
