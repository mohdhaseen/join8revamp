package com.join8.fragments;

import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.listeners.Requestlistener;
import com.join8.model.City;
import com.join8.model.RequestRegisteration;
import com.join8.model.ResponseCity;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.UrlUtil;

@SuppressLint("ShowToast")
public class RegistrationFormFragment extends Fragment implements OnClickListener, Requestlistener {

	private static final String TAG = RegistrationFormFragment.class.getSimpleName();
	public static final String PARAMS_TOKEN = "token";

	private String access_token = "";
	private RadioButton radioLangButton;
	private RadioGroup radioLanguageGroup;
	private EditText edtFirst, edtLast, edtPhoneNo, edtVerificationCode, edtPassword, edtCnfmPass;
	private RadioButton rdoEnglish, rdoTC, rdoSC;
	private TextView btnCreate, btnGetVerificationCode;
	private int verification_code_request = -1, registration_request = -1;
	private NetworkManager manager;
	private CryptoManager prefsManager;
	private String phoneCode = "";
	private ResponseCity city;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			if (savedInstanceState.containsKey(PARAMS_TOKEN)) {
				access_token = savedInstanceState.getString(PARAMS_TOKEN);
			}
		} else {
			if (getArguments() != null) {
				if (getArguments().containsKey(PARAMS_TOKEN)) {
					access_token = getArguments().getString(PARAMS_TOKEN);
				}
			}
		}

		prefsManager = CryptoManager.getInstance(getActivity());
		manager = NetworkManager.getInstance();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.registration, container, false);
		radioLanguageGroup = (RadioGroup) v.findViewById(R.id.radioLangauge);
		rdoEnglish = (RadioButton) v.findViewById(R.id.chkEnglish);
		rdoTC = (RadioButton) v.findViewById(R.id.chkChinese1);
		rdoSC = (RadioButton) v.findViewById(R.id.chkChinese2);
		edtFirst = (EditText) v.findViewById(R.id.edtFirstName);
		edtLast = (EditText) v.findViewById(R.id.edtLastName);
		edtPhoneNo = (EditText) v.findViewById(R.id.edtPhoneNo);
		edtVerificationCode = (EditText) v.findViewById(R.id.edtVerificationCode);
		edtPassword = (EditText) v.findViewById(R.id.edtPassword);
		edtCnfmPass = (EditText) v.findViewById(R.id.edtConfirmPassword);
		btnCreate = (TextView) v.findViewById(R.id.btnCreateAccount);
		btnGetVerificationCode = (TextView) v.findViewById(R.id.btnGetVerificationCode);
		btnCreate.setOnClickListener(this);
		btnGetVerificationCode.setOnClickListener(this);

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(edtFirst);
		mTypefaceUtils.applyTypeface(edtLast);
		mTypefaceUtils.applyTypeface(edtPhoneNo);
		mTypefaceUtils.applyTypeface(edtVerificationCode);
		mTypefaceUtils.applyTypeface(edtPassword);
		mTypefaceUtils.applyTypeface(edtCnfmPass);
		mTypefaceUtils.applyTypeface(btnCreate);
		mTypefaceUtils.applyTypeface(btnGetVerificationCode);
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.txtPreferredLanguage));
		mTypefaceUtils.applyTypeface((RadioButton) v.findViewById(R.id.chkChinese1));
		mTypefaceUtils.applyTypeface((RadioButton) v.findViewById(R.id.chkChinese2));
		mTypefaceUtils.applyTypeface((RadioButton) v.findViewById(R.id.chkEnglish));

		RelativeLayout rltMain = (RelativeLayout) v.findViewById(R.id.rltMain);
		rltMain.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(edtFirst.getWindowToken(), 0);
				}
				return false;
			}
		});

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (!getActivity().isFinishing()) {
			((HomeScreen) getActivity()).changeActionItems(true);
		}

		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_TRADITIONALCHINESE)) {
			rdoTC.setChecked(true);
		} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_SIMPLECHINSESE)) {
			rdoSC.setChecked(true);
		} else {
			rdoEnglish.setChecked(true);
		}

		phoneCode = prefsManager.getPrefs().getString(SelectCityFragment.PARAM_PHONE_CODE, "");

		if (!getActivity().isFinishing()) {

			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				((HomeScreen) getActivity()).setCustomTitle(getString(R.string.tvregister) + " - " + ((HomeScreen) getActivity()).getCityData().getNameEN(), Typeface.BOLD);
			} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_SIMPLECHINSESE)) {
				((HomeScreen) getActivity()).setCustomTitle(getString(R.string.tvregister) + " - " + ((HomeScreen) getActivity()).getCityData().getName(), Typeface.BOLD);
			} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_TRADITIONALCHINESE)) {
				((HomeScreen) getActivity()).setCustomTitle(getString(R.string.tvregister) + " - " + ((HomeScreen) getActivity()).getCityData().getName(), Typeface.BOLD);
			}
		}

		city = new Gson().fromJson(prefsManager.getPrefs().getString(SelectCityFragment.PARAM_CITYDATA_NEW, ""), ResponseCity.class);

	}

	@Override
	public void onResume() {
		super.onResume();
		manager.addListener(this);

		IntentFilter filter = new IntentFilter();
		filter.addAction("android.provider.Telephony.SMS_RECEIVED");
		getActivity().registerReceiver(mSMSReceivedReceiver, filter);
	}

	@Override
	public void onStop() {
		super.onStop();
		manager.removeListeners(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(PARAMS_TOKEN, access_token);
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.btnCreateAccount) {

			validateData();

		} else if (v.getId() == R.id.btnGetVerificationCode) {

			if (TextUtils.isEmpty(edtPhoneNo.getText().toString())) {
				Toast.displayText(getActivity(), getString(R.string.phonerequirederror));
			} else if (edtPhoneNo.getText().toString().length() < 8 || edtPhoneNo.getText().toString().length() > 11) {
				Toast.displayText(getActivity(), getString(R.string.enterPhone));
			} else {
				getVerificationCode();
			}
		}
	}

	private void validateData() {
		if (TextUtils.isEmpty(edtFirst.getText().toString())) {
			Toast.displayText(getActivity(), getString(R.string.firstnamerequirederror));
		} else if (TextUtils.isEmpty(edtLast.getText().toString())) {
			Toast.displayText(getActivity(), getString(R.string.lastnamerequirederror));
		} else if (TextUtils.isEmpty(edtPhoneNo.getText().toString())) {
			Toast.displayText(getActivity(), getString(R.string.phonerequirederror));
		} else if (edtPhoneNo.getText().toString().length() < 8 || edtPhoneNo.getText().toString().length() > 11) {
			Toast.displayText(getActivity(), getString(R.string.enterPhone));
		} else if (TextUtils.isEmpty(edtVerificationCode.getText().toString())) {
			Toast.displayText(getActivity(), getString(R.string.verificationcoderequirederror));
		} else if (TextUtils.isEmpty(edtPassword.getText().toString())) {
			Toast.displayText(getActivity(), getString(R.string.passwordrequirederror));
		} else if (TextUtils.isEmpty(edtCnfmPass.getText().toString())) {
			Toast.displayText(getActivity(), getString(R.string.repeatpasswordrequirederror));
		} else if (!edtPassword.getText().toString().equals(edtCnfmPass.getText().toString())) {
			Toast.displayText(getActivity(), getString(R.string.samepassword));
		} else {

			callRegistration();
		}
	}

	private void callRegistration() {

		radioLangButton = (RadioButton) getView().findViewById(radioLanguageGroup.getCheckedRadioButtonId());

		manager.isProgressVisible(true);
		RequestRegisteration req = new RequestRegisteration();
		req.setFirstName(edtFirst.getText().toString().trim());
		req.setLastName(edtLast.getText().toString().trim());
		req.setMobilePhoneNumber(city.getPhoneCode() + "-" + edtPhoneNo.getText().toString().trim());
		req.setPreferLanguage(radioLangButton.getTag().toString().trim());
		req.setFk_CityCode(city.getCityCode());
		req.setActivationCode(edtVerificationCode.getText().toString().trim());
		req.setPassword(edtPassword.getText().toString());
		registration_request = manager.addRequest(null, getActivity(), true, UrlUtil.getRegisterUrl(), Join8RequestBuilder.getRegisterRequest(req), ConstantData.method_post);

	}

	private void getVerificationCode() {
		radioLangButton = (RadioButton) getView().findViewById(radioLanguageGroup.getCheckedRadioButtonId());
		manager.isProgressVisible(true);
		verification_code_request = manager.addRequest(new HashMap<String, String>(), getActivity(), true, UrlUtil.getVerificationCodeUrl(city.getPhoneCode() + "-" + edtPhoneNo.getText().toString()), "", ConstantData.method_get);
		/*
		 * verification_code_request =
		 * manager.addRequest(Join8RequestBuilder.getVerificationCodeRequest
		 * (access_token, phoneCode, edtPhoneNo.getText().toString().trim(),
		 * city.getCountryCode(), radioLangButton.getTag().toString().trim()),
		 * RequestMethod.POST, getActivity(),
		 * Join8RequestBuilder.METHOD_GET_VERIFICATION_CODE);
		 */

	}

	@Override
	public void onSuccess(int id, String response) {

		if (response != null) {

			Log.e(TAG, response);

			if (id == registration_request) {

				if (response.toLowerCase(Locale.getDefault()).startsWith("successfully registered")) {

					Toast.displayText(getActivity(), getString(R.string.register_success));

					if (!getActivity().isFinishing()) {
						((HomeScreen) getActivity()).clearBackStackEntry();
						HomeFragment homeFragment = new HomeFragment();
						getActivity().getSupportFragmentManager().beginTransaction().add(HomeScreen.FRAGMENT_CONTAINER, homeFragment).addToBackStack(null).commit();
					}

				} else if (response.toLowerCase(Locale.getDefault()).startsWith("youarenotvaliduser")) {

					Toast.displayText(getActivity(), getString(R.string.wrong_verification_code));

				} else {

					Toast.displayText(getActivity(), getString(R.string.reg_error));
				}

			} else if (id == verification_code_request) {

				if (response.toLowerCase(Locale.getDefault()).startsWith("mobilenoalreadyexist")) {

					Toast.displayText(getActivity(), getString(R.string.mobilealreadyexist));

				} else if (response.toLowerCase(Locale.getDefault()).startsWith("twillioerror")) {

					Toast.displayText(getActivity(), getString(R.string.sms_send_error));

				} else if (response.toLowerCase(Locale.getDefault()).contains("successfully")) {

					Toast.displayText(getActivity(), getString(R.string.sms_sent));

				}
			}
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		getActivity().unregisterReceiver(mSMSReceivedReceiver);
	}

	BroadcastReceiver mSMSReceivedReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			final Bundle bundle = intent.getExtras();

			try {

				if (bundle != null) {

					final Object[] pdusObj = (Object[]) bundle.get("pdus");

					for (int i = 0; i < pdusObj.length; i++) {

						SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
						String phoneNumber = currentMessage.getDisplayOriginatingAddress();

						if (phoneNumber.endsWith("16197276282")) {

							String message = currentMessage.getDisplayMessageBody();

							String verificationCode = null;
							Pattern p = Pattern.compile(" {1}[0-9]{4}");
							Matcher m = p.matcher(message);
							while (m.find()) {
								verificationCode = new String(m.group().trim());
							}

							if (!TextUtils.isEmpty(verificationCode)) {
								edtVerificationCode.setText(verificationCode);
							}
						}
					}
				}

			} catch (Exception e) {
				Log.e("SmsReceiver", "Exception smsReceiver" + e);
			}
		}
	};
}
