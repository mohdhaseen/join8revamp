package com.join8.fragments;

import java.util.ArrayList;

import com.costum.android.widget.LoadMoreListView;
import com.costum.android.widget.LoadMoreListView.OnLoadMoreListener;
import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.adpaters.PromotionListAdapter;
import com.join8.adpaters.PromotionListAdapterNew;
import com.join8.listeners.Requestlistener;
import com.join8.model.Promotion;
import com.join8.model.Promotions;
import com.join8.model.Registration;
import com.join8.model.ResponseCity;
import com.join8.model.ResponsePromotions;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.KeyboardUtils;
import com.join8.utils.UrlUtil;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class PromotionFragmentNew extends Fragment implements Requestlistener {
	public static final String PARAM_PROMOTION = null;
	private int totalrecords;
	private NetworkManager networManager;
	private int promotion_request = -1;
	private CryptoManager prefManager;
	private int starIndex = 0;
	private int count = 25;
	private LoadMoreListView lstPromotions;
	private TextView tvresults;
	private ArrayList<Promotion> arrPromotionsData;
	private ResponseCity city;
	private PromotionListAdapterNew promotionListAdapter;
	private int loadmore_request = -1;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.promotionsnew, container, false);
		lstPromotions = (LoadMoreListView) view.findViewById(R.id.lstPromotions);
		tvresults = (TextView) view.findViewById(R.id.tvresults);
		lstPromotions.setEmptyView(tvresults);
		city = ((HomeScreen) getActivity()).getCityData();
		arrPromotionsData = new ArrayList<Promotion>();
		doLoadData();
		return view;
	}
	@Override
	public void onStart() {
		super.onStart();

		lstPromotions.setOnLoadMoreListener(new OnLoadMoreListener() {

			@Override
			public void onLoadMore() {
				Log.e("on Load More", "in Load mOrew");
				if (arrPromotionsData.size() <= totalrecords) {

					starIndex = arrPromotionsData.size();
					doLoadMore();
				}

			}
		});

		lstPromotions.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				KeyboardUtils.hideKeyboard(view);
				Promotion promotion = (Promotion) parent.getAdapter().getItem(position);
				Bundle b = new Bundle();
				b.putString(PARAM_PROMOTION, new Gson().toJson(promotion));
				FragmentPromotionDetails promotiondetaisFragment = new FragmentPromotionDetails();
				promotiondetaisFragment.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(PromotionDetails.class.getName()).add(HomeScreen.FRAGMENT_CONTAINER, promotiondetaisFragment).commit();

			}
		});
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		networManager = NetworkManager.getInstance();
		prefManager = CryptoManager.getInstance(getActivity());
		networManager.addListener(this);
	}
	public void doLoadMore() {
		networManager.isProgressVisible(false);
		loadmore_request = networManager.addRequest(null, getActivity(), true, UrlUtil.getPromotionsUrl(city.getCityCode(), count, starIndex), "", ConstantData.method_get);

	}
	@Override
	public void onSuccess(int id, String response) {
		if (id == promotion_request) {
			ResponsePromotions responsePromotions = new Gson().fromJson(response, ResponsePromotions.class);
			promotionListAdapter = new PromotionListAdapterNew(getActivity(), R.layout.row_promotions, responsePromotions.getPromotionList());
			lstPromotions.setAdapter(promotionListAdapter);
			lstPromotions.setVisibility(View.VISIBLE);
		} else if (id == loadmore_request) {
			ResponsePromotions responsePromotions = new Gson().fromJson(response, ResponsePromotions.class);
			if (responsePromotions != null && responsePromotions.getPromotionList() != null && responsePromotions.getPromotionList().size() > 0)
				totalrecords = responsePromotions.getPromotionList().get(0).getTotalRecords();
		}
	}
	@Override
	public void onDestroy() {
		super.onStop();
		networManager.removeListeners(this);
	}

	public void doLoadData() {
		lstPromotions.onLoadMoreComplete();
		arrPromotionsData.clear();
		networManager.isProgressVisible(true);
		if (city != null && city.getNameEN().length() > 0) {
			promotion_request = networManager.addRequest(null, getActivity(), true, UrlUtil.getPromotionsUrl(city.getCityCode(), count, starIndex), "", ConstantData.method_get);
		} else {
			Toast.displayText(getActivity(), getString(R.string.selectCity));
			networManager.isProgressVisible(false);
		}

	}

	@Override
	public void onError(int id, String message) {
		// TODO Auto-generated method stub

	}
}
