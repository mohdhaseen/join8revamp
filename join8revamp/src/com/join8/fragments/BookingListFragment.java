package com.join8.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;
import android.widget.TextView;

import com.costum.android.widget.LoadMoreListView;
import com.costum.android.widget.LoadMoreListView.OnLoadMoreListener;
import com.google.gson.Gson;
import com.join8.BookingActivity;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.adpaters.BookingListAdapter;
import com.join8.adpaters.BookingListAdapter.OnBookingCancelListner;
import com.join8.adpaters.HorizontalGalleryAdapter;
import com.join8.listeners.Requestlistener;
import com.join8.model.BookingData;
import com.join8.model.Registration;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.quickaction.ActionItem;
import com.join8.quickaction.QuickAction;
import com.join8.quickaction.QuickAction.OnActionItemClickListener;
import com.join8.quickaction.QuickAction.OnDismissListener;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.Log;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;

public class BookingListFragment extends Fragment implements OnClickListener, Requestlistener {

	private static final String TAG = BookingListFragment.class.getSimpleName();
	private static final int COUNT_PER_PAGE = 25;

	private int prePosition = -1, bookingListMoredataRequestId = -1, cancelBookingRequestId = -1;

	private Gallery mGallery = null;
	private LoadMoreListView listView = null;
	private TextView txtBookingType = null;

	private NetworkManager networManager = null;
	private int bookingListRequestId = -1;
	private CryptoManager prefManager = null;
	private String access_token = "";

	private ArrayList<BookingData> arrBookingList = null;
	private BookingListAdapter adapterBookingList = null;
	private int starIndex = 0;
	private int totalRecords = 0;
	private int cancelledPosition = -1;

	private QuickAction mQuickAction = null;

	private String[] bookingTypes = null, bookingTypesValue = null;
	private TextView tvresults;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments() != null) {

			if (getArguments().getString("redirectTo").equalsIgnoreCase("BookingDetail")) {

				String tableBookingId = getArguments().getString("reference");

				Intent intent = new Intent(getActivity(), BookingActivity.class);
				intent.putExtra("companyId", "-1");
				intent.putExtra("tableBookingId", tableBookingId);
				startActivity(intent);
			}
		}

		networManager = NetworkManager.getInstance();
		networManager.isProgressVisible(true);
		prefManager = CryptoManager.getInstance(getActivity());
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);

		IntentFilter filter = new IntentFilter();
		filter.addAction(ConstantData.BROADCAST_REFRESH_LIST);
		getActivity().registerReceiver(listRefreshReceiver, filter);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.order_list_fragment, container, false);
		mappingWidgets(mView);
		addListeners();
		init();
		return mView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
		if (!getActivity().isFinishing()) {
			((HomeScreen) getActivity()).setCustomTitle(getString(R.string.my_booking), Typeface.NORMAL);
			((HomeScreen) getActivity()).changeActionItems(true);
		}
		loadBookingList();
	}

	private void mappingWidgets(View mView) {

		mGallery = (Gallery) mView.findViewById(R.id.gallery);
		listView = (LoadMoreListView) mView.findViewById(android.R.id.list);
		listView.setVisibility(View.GONE);

		txtBookingType = (TextView) mView.findViewById(R.id.txtOrderType);
		txtBookingType.setSelected(false);

		tvresults = (TextView) mView.findViewById(R.id.tvresults);

		listView.setEmptyView(tvresults);

		TextView tvOrderListTitle = (TextView) mView.findViewById(R.id.tvOrderListTitle);
		tvOrderListTitle.setText(getString(R.string.my_booking_list));

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(txtBookingType);
		mTypefaceUtils.applyTypeface(tvOrderListTitle);
		mTypefaceUtils.applyTypeface(tvresults);

	}

	private void addListeners() {

		mGallery.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					prePosition = mGallery.getSelectedItemPosition();
				} else if (event.getAction() == MotionEvent.ACTION_UP) {

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {

							try {

								if (prePosition != mGallery.getSelectedItemPosition()) {

									loadBookingList();
								}

							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}, 500);
				}
				return false;
			}
		});

		listView.setOnLoadMoreListener(new OnLoadMoreListener() {
			@Override
			public void onLoadMore() {

				if (arrBookingList.size() < totalRecords) {

					starIndex = arrBookingList.size();
					loadMoreBookingList();
				}
			}
		});
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				// if (arrBookingList.get(position).getBookingStatus() == 10) {

				Intent intent = new Intent(getActivity(), BookingActivity.class);
				intent.putExtra("companyId", arrBookingList.get(position).getCompanyId());
				intent.putExtra("tableBookingId", arrBookingList.get(position).getTableBookingId());
				startActivity(intent);
				// }
			}
		});

		txtBookingType.setOnClickListener(this);
	}

	private void init() {

		mGallery.setAdapter(new HorizontalGalleryAdapter(getActivity(), getResources().getStringArray(R.array.order_days)));
		mGallery.setSelection(4);

		arrBookingList = new ArrayList<BookingData>();
		bookingTypes = getResources().getStringArray(R.array.booking_types);
		bookingTypesValue = getResources().getStringArray(R.array.booking_types_value);

		initQuickAction();

		txtBookingType.setText(bookingTypes[0]);
		txtBookingType.setTag(bookingTypesValue[0]);
	}

	private void loadBookingList() {

		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		String partitionKey = registration.getPartitionKey();

		String startDate = "", endDate = "";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());

		switch (mGallery.getSelectedItemPosition()) {
			case 0 :
				break;

			case 1 :

				Calendar c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, -30);
				startDate = format.format(c.getTime());
				endDate = format.format(new Date());
				break;

			case 2 :
				c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, -7);
				startDate = format.format(c.getTime());
				endDate = format.format(new Date());
				break;

			case 3 :
				startDate = format.format(new Date());
				endDate = format.format(new Date());
				break;

			case 4 :
				startDate = format.format(new Date());
				c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, 7);
				endDate = format.format(c.getTime());
				break;

			case 5 :
				startDate = format.format(new Date());
				c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, 30);
				endDate = format.format(c.getTime());
				break;
		}

		networManager.isProgressVisible(true);
		bookingListRequestId = networManager.addRequest(Join8RequestBuilder.getOrderListRequest(access_token, partitionKey, txtBookingType.getTag().toString(), starIndex, COUNT_PER_PAGE, startDate, endDate), RequestMethod.POST, getActivity(),
				Join8RequestBuilder.METHOD_USER_BOOKINGLIST);
	}

	public void loadMoreBookingList() {

		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		String partitionKey = registration.getPartitionKey();

		String startDate = "", endDate = "";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());

		switch (mGallery.getSelectedItemPosition()) {
			case 0 :
				break;

			case 1 :

				Calendar c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, -30);
				startDate = format.format(c.getTime());
				endDate = format.format(new Date());
				break;

			case 2 :
				c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, -7);
				startDate = format.format(c.getTime());
				endDate = format.format(new Date());
				break;

			case 3 :
				startDate = format.format(new Date());
				endDate = format.format(new Date());
				break;

			case 4 :
				startDate = format.format(new Date());
				c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, 7);
				endDate = format.format(c.getTime());
				break;

			case 5 :
				startDate = format.format(new Date());
				c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, 30);
				endDate = format.format(c.getTime());
				break;
		}

		networManager.isProgressVisible(false);
		bookingListMoredataRequestId = networManager.addRequest(Join8RequestBuilder.getOrderListRequest(access_token, partitionKey, txtBookingType.getTag().toString(), starIndex, COUNT_PER_PAGE, startDate, endDate), RequestMethod.POST, getActivity(),
				Join8RequestBuilder.METHOD_USER_BOOKINGLIST);
	}

	public void cancelBooking(String bookingId) {

		networManager.isProgressVisible(true);
		cancelBookingRequestId = networManager.addRequest(Join8RequestBuilder.cancelBooking(access_token, bookingId), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_USER_BOOKING_CANCEL);
	}

	private void initQuickAction() {

		mQuickAction = new QuickAction(getActivity(), QuickAction.VERTICAL);

		for (int i = 0; i < bookingTypes.length; i++) {
			ActionItem mItem = new ActionItem(i, bookingTypes[i]);
			mItem.setColor(getResources().getColor(R.color.orangebg));
			mQuickAction.addActionItem(mItem);
		}
		mQuickAction.setOnActionItemClickListener(new OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction source, int pos, int actionId) {

				txtBookingType.setText(bookingTypes[pos]);
				txtBookingType.setTag(bookingTypesValue[pos]);

				txtBookingType.setSelected(false);
				txtBookingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_orange_down, 0);
				txtBookingType.setTextColor(getResources().getColor(R.color.orangebg));

				loadBookingList();
			}
		});

		mQuickAction.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss() {

				txtBookingType.setSelected(false);
				txtBookingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_orange_down, 0);
				txtBookingType.setTextColor(getResources().getColor(R.color.orangebg));
			}
		});
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

			case R.id.txtOrderType :

				mQuickAction.show(v);

				if (!txtBookingType.isSelected()) {
					txtBookingType.setSelected(true);
					txtBookingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up_white, 0);
					txtBookingType.setTextColor(getResources().getColor(android.R.color.white));
				} else {
					txtBookingType.setSelected(false);
				}

				break;
		}
	}

	OnBookingCancelListner onBookingCancelListner = new OnBookingCancelListner() {

		@Override
		public void onBookingCancelled(int position, final String bookingId) {

			cancelledPosition = position;

			if (!Utils.isEmpty(bookingId)) {

				AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
				dialog.setMessage(getString(R.string.cancel_booking));
				dialog.setPositiveButton(getString(R.string.yes), new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						cancelBooking(bookingId);
					}
				});
				dialog.setNegativeButton(getString(R.string.no), new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				});
				dialog.show();
			}
		}
	};

	@Override
	public void onSuccess(int id, String response) {

		try {

			Log.i(getTag(), "Result => " + response);

			if (response != null && !response.equals("")) {

				if (id == bookingListRequestId) {

					arrBookingList = new ArrayList<BookingData>();

					if (response.toLowerCase().startsWith("no record found")) {

						// Toast.makeText(getActivity(),
						// getString(R.string.norecorderror));
						//
						// listView.setVisibility(View.GONE);
						adapterBookingList = new BookingListAdapter(getActivity(), arrBookingList, onBookingCancelListner);
						listView.setAdapter(adapterBookingList);
						return;

					} else {

						JSONArray jArray = new JSONArray(response);

						if (jArray != null && jArray.length() > 0) {

							totalRecords = jArray.getJSONObject(0).getInt("TotalRecords");

							for (int i = 0; i < jArray.length(); i++) {
								BookingData data = new Gson().fromJson(jArray.getString(i).toString(), BookingData.class);
								arrBookingList.add(data);
							}

							listView.setVisibility(View.VISIBLE);
							adapterBookingList = new BookingListAdapter(getActivity(), arrBookingList, onBookingCancelListner);
							listView.setAdapter(adapterBookingList);
						}
					}

				} else if (id == bookingListMoredataRequestId) {

					if (!response.toLowerCase().startsWith("no record found")) {

						JSONArray jArray = new JSONArray(response);

						if (jArray != null && jArray.length() > 0) {

							totalRecords = jArray.getJSONObject(0).getInt("TotalRecords");

							for (int i = 0; i < jArray.length(); i++) {
								BookingData data = new Gson().fromJson(jArray.getString(i).toString(), BookingData.class);
								arrBookingList.add(data);
							}

							adapterBookingList.notifyDataSetChanged();
							// listView.invalidate();
							listView.onLoadMoreComplete();
						}
					}
				} else if (id == cancelBookingRequestId) {

					if (response.toLowerCase().startsWith("cancelled")) {

						arrBookingList.get(cancelledPosition).setBookingStatus(30);

						adapterBookingList.notifyDataSetChanged();

					} else if (response.toLowerCase().startsWith("failed")) {

						Toast.displayText(getActivity(), getString(R.string.failed));
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	private BroadcastReceiver listRefreshReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			try {

				String redirectTo = intent.getStringExtra("redirectTo");

				if (redirectTo.equalsIgnoreCase("BookingDetail")) {

					starIndex = 0;
					loadBookingList();
				}

			} catch (Exception e) {
				e.printStackTrace();
				Log.e("BookingList", "BroadcastReceiver : " + e.toString());
			}
		}
	};
}
