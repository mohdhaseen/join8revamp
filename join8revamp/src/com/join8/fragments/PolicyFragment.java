package com.join8.fragments;

import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.layout.MyProgressDialog;
import com.join8.utils.ConstantData;

@SuppressLint("ShowToast")
public class PolicyFragment extends Fragment {

	private static final String TAG = PolicyFragment.class.getSimpleName();
	private WebView webView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.termsofservice, container, false);

		webView = (WebView) v.findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);
//		webView.getSettings().setLoadWithOverviewMode(true);
//		webView.getSettings().setUseWideViewPort(true);
		webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		webView.setScrollbarFadingEnabled(false);
		
		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_TRADITIONALCHINESE)) {
			webView.loadUrl("file:///android_asset/Terms_TC.html");
		}else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_SIMPLECHINSESE)) {
			webView.loadUrl("file:///android_asset/Terms_SC.html");
		}else{
			webView.loadUrl("file:///android_asset/Terms_EN.html");	
		}
			
		final MyProgressDialog myProgressDialog = new MyProgressDialog(getActivity());
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				myProgressDialog.dismiss();
			}
		});

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		((HomeScreen) getActivity()).initActionBar();

		if (!getActivity().isFinishing()) {
			((HomeScreen) getActivity()).setCustomTitle(getString(R.string.terms), Typeface.NORMAL);
			((HomeScreen) getActivity()).changeActionItems(true);
		}

	}
}
