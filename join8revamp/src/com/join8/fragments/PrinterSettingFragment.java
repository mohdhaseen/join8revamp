package com.join8.fragments;

import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.join8.R;
import com.join8.Toast;
import com.join8.print.BluetoothService;
import com.join8.print.DeviceListActivity;
import com.join8.print.SocketManager;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;

public class PrinterSettingFragment extends Fragment implements OnClickListener {

	private static final String tag = PrinterSettingFragment.class.getSimpleName();

	private TextView mConnectionStatus, mConnectButton, txtPrinterType, txt_printer_type_label;

	private BluetoothAdapter mBluetoothAdapter;

	private BluetoothService mService;
	private SocketManager mSocketManager;
	private SocketManager mSocketManager1;

	private static final int REQUEST_CONNECT_DEVICE = 123;
	private static final int REQUEST_ENABLE_BT = 124;

	public static final String DEVICE_NAME = "device_name";
	public static final String DEVICE_ADDRESS = "device_address";
	public static final String TOAST = "toast";

	// Message types sent from the BluetoothService Handler
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	private String mConnectedDeviceName = null;
	private String[] printerTypes = null;
	private int selectedPosition = 0;

	private SharedPreferences mPreferences;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.fragment_printer_setting, container, false);
		mConnectionStatus = (TextView) mView.findViewById(R.id.txt_connection_status);
		mConnectButton = (TextView) mView.findViewById(R.id.btnConnect);
		txt_printer_type_label = (TextView) mView.findViewById(R.id.txt_printer_type_label);
		txtPrinterType = (TextView) mView.findViewById(R.id.txtPrinterType);
		printerTypes = getResources().getStringArray(R.array.printer_type);

		mPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
		selectedPosition = mPreferences.getInt("printerTypePosition", 0);
		txtPrinterType.setText(printerTypes[selectedPosition]);

		mConnectButton.setOnClickListener(this);

		txtPrinterType.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
				mBuilder.setSingleChoiceItems(printerTypes, selectedPosition, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

						if (selectedPosition != which) {

							if (mService != null && mService.getState() == BluetoothService.STATE_CONNECTED) {
								mService.stop();
								mService = null;
								mConnectButton.setEnabled(true);
								mConnectionStatus.setText(R.string.title_not_connected);
								mConnectButton.setText(R.string.connect);
								mConnectButton.setTag("not_connected");
								SharedPreferences mPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
								mPreferences.edit().remove("btAddress").commit();
							}

							if (mSocketManager != null) {
								mSocketManager.close();
								mSocketManager = null;
								mConnectButton.setEnabled(true);
								mConnectionStatus.setText(R.string.title_not_connected);
								mConnectButton.setText(R.string.connect);
								mConnectButton.setTag("not_connected");
							}

							txtPrinterType.setText(printerTypes[which]);
							mPreferences.edit().putInt("printerTypePosition", which).commit();
							selectedPosition = which;
							dialog.dismiss();

							if (which == 1) {
								displayIPDialog();
							}
						}
					}
				});

				mBuilder.setTitle(R.string.printer_type);
				mBuilder.create().show();
			}
		});

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(mConnectButton);
		mTypefaceUtils.applyTypeface(mConnectionStatus);
		mTypefaceUtils.applyTypeface(txtPrinterType);
		mTypefaceUtils.applyTypeface(txt_printer_type_label);

		return mView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mSocketManager = new SocketManager(getActivity());
		mSocketManager.setHandler(mHandlerWifi);

		mSocketManager1 = new SocketManager(getActivity());
		mSocketManager1.setHandler(mHandlerWifi1);

		if (selectedPosition == 1) {

			String ip = mPreferences.getString("printerIP", "");
			String ip1 = mPreferences.getString("printerIP1", "");

			if (TextUtils.isEmpty(ip) && TextUtils.isEmpty(ip1)) {
				displayIPDialog();
			} else {
				if (!TextUtils.isEmpty(ip)) {
					connectWifiPrinter(ip);
				}
				if (!TextUtils.isEmpty(ip1)) {
					connectWifiPrinter1(ip1);
				}
			}

		} else {
			connectBluetoothPrinter();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d(tag, "onActivityResult");

		if (requestCode == REQUEST_ENABLE_BT) {
			if (resultCode == Activity.RESULT_OK) {
				initBluetooth();
				connectPreviouslyPairedDevice();
			}
		} else if (requestCode == REQUEST_CONNECT_DEVICE) {
			if (resultCode == Activity.RESULT_OK) {
				if (data != null && data.getExtras() != null) {
					if (data.getExtras().containsKey(DeviceListActivity.EXTRA_DEVICE_ADDRESS)) {
						String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
						if (BluetoothAdapter.checkBluetoothAddress(address)) {
							BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
							mService.connect(device);
						}
					}
				}

			} else {
				if (mService.getState() != BluetoothService.STATE_NONE) {
					mService.stop();
					mService = null;
				}
			}
		}
	}

	@Override
	public void onStop() {

		if (mSocketManager != null) {
			mSocketManager.close();
			mSocketManager = null;
		}

		if (mSocketManager1 != null) {
			mSocketManager1.close();
			mSocketManager1 = null;
		}

		super.onStop();
	}

	private void connectBluetoothPrinter() {

		if (mBluetoothAdapter == null) {
			mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			if (mBluetoothAdapter == null) {
				Toast.displayText(getActivity(), getString(R.string.bluetooth_not_available));
				return;
			}
		}

		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			if (getParentFragment() != null) {
				getParentFragment().startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			} else {
				startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			}

		} else {
			if (mService == null) {
				initBluetooth();
			}
		}
	}

	public void connectWifiPrinter(String printerIp) {

		if (mSocketManager != null) {
			mSocketManager.close();
			mSocketManager = null;
		}

		if (mSocketManager == null) {
			mSocketManager = new SocketManager(getActivity());
			mSocketManager.setHandler(mHandlerWifi);
		}

		mSocketManager.mPrinterIP = printerIp;
		mSocketManager.threadconnect();
	}

	public void connectWifiPrinter1(String printerIp) {

		if (mSocketManager1 != null) {
			mSocketManager1.close();
			mSocketManager1 = null;
		}

		if (mSocketManager1 == null) {
			mSocketManager1 = new SocketManager(getActivity());
			mSocketManager1.setHandler(mHandlerWifi1);
		}

		mSocketManager1.mPrinterIP = printerIp;
		mSocketManager1.threadconnect();
	}

	private void connectPreviouslyPairedDevice() {
		SharedPreferences mPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
		if (mPreferences.contains("btAddress")) {
			String deviceId = mPreferences.getString("btAddress", "");
			if (!TextUtils.isEmpty(deviceId)) {
				Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
				if (pairedDevices != null && pairedDevices.size() > 0) {
					for (BluetoothDevice device : pairedDevices) {
						if (device.getAddress().equals(deviceId)) {
							mService.connect(device);
							return;
						}
					}
				}
			}
		}
	}

	private void initBluetooth() {
		// Initialize the BluetoothService to perform bluetooth connections
		if (mService == null) {
			mService = BluetoothService.getInstance(getActivity());
			mService.setHandler(mHandler);
		}

		if (mService.getState() == BluetoothService.STATE_NONE) {
			mService.start();
			connectPreviouslyPairedDevice();
		} else if (mService.getState() == BluetoothService.STATE_CONNECTED) {
			mConnectedDeviceName = mService.getDeviceName();
			mConnectionStatus.setText(R.string.title_connected_to);
			mConnectionStatus.append(TextUtils.isEmpty(mConnectedDeviceName) ? "" : " " + mConnectedDeviceName);
			mConnectButton.setText(R.string.disconnect);
			mConnectButton.setTag("connected");
			mConnectButton.setEnabled(true);
			return;
		}
	}

	private void displayIPDialog() {

		try {

			if (isAdded()) {

				View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_ip_address, null);
				final EditText edtIP = (EditText) view.findViewById(R.id.edtIP);
				final EditText edtIP1 = (EditText) view.findViewById(R.id.edtIP1);
				edtIP.setText(mPreferences.getString("printerIP", ""));
				edtIP1.setText(mPreferences.getString("printerIP1", ""));

				AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
				mBuilder.setView(view);
				mBuilder.setTitle(printerTypes[selectedPosition]);
				mBuilder.setPositiveButton(R.string.tvOk, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

						String ip = edtIP.getText().toString().trim();
						String ip1 = edtIP1.getText().toString().trim();
						Editor editor = mPreferences.edit();
						editor.putString("printerIP", ip);
						editor.putString("printerIP1", ip1);
						editor.commit();

						if (!Utils.isEmpty(ip)) {
							connectWifiPrinter(ip);
						}

						if (!Utils.isEmpty(ip1)) {
							connectWifiPrinter1(ip1);
						}
					}
				});
				mBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				});
				mBuilder.create().show();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			if (isAdded()) {

				switch (msg.what) {
				case MESSAGE_STATE_CHANGE:
					switch (msg.arg1) {
					case BluetoothService.STATE_CONNECTED:
						Log.e("status", "========== Connected ===========");
						mConnectionStatus.setText(R.string.title_connected_to);
						mConnectionStatus.append(" " + mConnectedDeviceName);
						mConnectButton.setText(R.string.disconnect);
						mConnectButton.setTag("connected");
						mConnectButton.setEnabled(true);
						break;
					case BluetoothService.STATE_CONNECTING:
						mConnectionStatus.setText(R.string.title_connecting);
						mConnectButton.setEnabled(false);
						break;
					case BluetoothService.STATE_LISTEN:
					case BluetoothService.STATE_NONE:
						Log.e("status", "========== Not connected ===========");
						mConnectionStatus.setText(R.string.title_not_connected);
						mConnectButton.setEnabled(true);
						mConnectButton.setText(R.string.connect);
						mConnectButton.setTag("not_connected");
						break;
					}
					break;
				case MESSAGE_WRITE:
					break;
				case MESSAGE_READ:
					break;
				case MESSAGE_DEVICE_NAME:
					Log.e("status", "========== Connected Name ===========");
					mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
					mPreferences.edit().putString("btAddress", msg.getData().getString(DEVICE_ADDRESS)).commit();
					break;
				case MESSAGE_TOAST:
					// startBluetoothDeviceDiscovery();
					break;
				}
			}
		}
	};

	private final Handler mHandlerWifi = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			if (isAdded() && msg.what == SocketManager.WIFI_PRINTER_CONNECTION_STATE) {

				switch (msg.arg1) {

				case SocketManager.STATE_CONNECTED:
					Toast.displayText(getActivity(), getString(R.string.wifi_printer_connected) + " : Printer-1");
					mConnectionStatus.setText(R.string.wifi_printer_connected);
					mConnectButton.setEnabled(true);
					mConnectButton.setText(R.string.disconnect);
					mConnectButton.setTag("connected");
					break;

				case SocketManager.STATE_ERROR:
					Toast.displayText(getActivity(), getString(R.string.wifi_printer_conn_error) + " : Printer-1");
					mConnectionStatus.setText(R.string.title_not_connected);
					mConnectButton.setEnabled(true);
					mConnectButton.setText(R.string.connect);
					mConnectButton.setTag("not_connected");
					break;
				}
			}
		}
	};

	private final Handler mHandlerWifi1 = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			if (isAdded() && msg.what == SocketManager.WIFI_PRINTER_CONNECTION_STATE) {

				switch (msg.arg1) {

				case SocketManager.STATE_CONNECTED:
					Toast.displayText(getActivity(), getString(R.string.wifi_printer_connected) + " : Printer-2");
					// mConnectionStatus.setText(R.string.wifi_printer_connected);
					// mConnectButton.setEnabled(true);
					// mConnectButton.setText(R.string.disconnect);
					// mConnectButton.setTag("connected");
					break;

				case SocketManager.STATE_ERROR:
					Toast.displayText(getActivity(), getString(R.string.wifi_printer_conn_error) + " : Printer-2");
					// mConnectionStatus.setText(R.string.title_not_connected);
					// mConnectButton.setEnabled(true);
					// mConnectButton.setText(R.string.connect);
					// mConnectButton.setTag("not_connected");
					break;
				}
			}
		}
	};

	private void startBluetoothDeviceDiscovery() {
		Intent serverIntent = new Intent(getActivity(), DeviceListActivity.class);
		if (getParentFragment() != null) {
			getParentFragment().startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
		} else {
			startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
		}
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.btnConnect:

			if (v.getTag() != null && v.getTag().equals("connected")) {

				if (mService != null && mService.getState() == BluetoothService.STATE_CONNECTED) {
					mService.stop();
					mService = null;
					mConnectButton.setEnabled(true);
					mConnectionStatus.setText(R.string.title_not_connected);
					mConnectButton.setText(R.string.connect);
					mConnectButton.setTag("not_connected");
					SharedPreferences mPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
					mPreferences.edit().remove("btAddress").commit();
				}

				if (mSocketManager != null) {
					mSocketManager.close();
					mSocketManager = null;
					mConnectButton.setEnabled(true);
					mConnectionStatus.setText(R.string.title_not_connected);
					mConnectButton.setText(R.string.connect);
					mConnectButton.setTag("not_connected");
				}

			} else {

				if (mPreferences.getInt("printerTypePosition", 0) == 1) {

					String ip = mPreferences.getString("printerIP", "");
					String ip1 = mPreferences.getString("printerIP1", "");

					if (TextUtils.isEmpty(ip) && TextUtils.isEmpty(ip1)) {
						displayIPDialog();
						mConnectButton.setEnabled(true);
						mConnectionStatus.setText(R.string.title_not_connected);
						mConnectButton.setText(R.string.connect);
						mConnectButton.setTag("not_connected");
					} else {

						if (!Utils.isEmpty(ip)) {
							connectWifiPrinter(ip);
						}

						if (!Utils.isEmpty(ip1)) {
							connectWifiPrinter1(ip1);
						}
					}

				} else {

					if (mBluetoothAdapter == null) {
						mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
						if (mBluetoothAdapter == null) {
							Toast.displayText(getActivity(), getString(R.string.bluetooth_not_available));
							return;
						}
					}

					if (!mBluetoothAdapter.isEnabled()) {
						Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
						if (getParentFragment() != null) {
							getParentFragment().startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
						} else {
							startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
						}

					} else {

						if (mService == null) {
							mConnectButton.setEnabled(true);
							mConnectionStatus.setText(R.string.title_not_connected);
							mConnectButton.setText(R.string.connect);
							mConnectButton.setTag("not_connected");
							initBluetooth();
							startBluetoothDeviceDiscovery();
							return;
						}

						if (mService.getState() == BluetoothService.STATE_CONNECTED) {
							mService.stop();
							mService = null;
							mConnectButton.setEnabled(true);
							mConnectionStatus.setText(R.string.title_not_connected);
							mConnectButton.setText(R.string.connect);
							mConnectButton.setTag("not_connected");
							mPreferences.edit().remove("btAddress").commit();
						} else {
							startBluetoothDeviceDiscovery();
						}
					}
				}
				break;
			}
		}
	}
}
