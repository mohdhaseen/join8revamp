package com.join8.fragments;

import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.listeners.Requestlistener;
import com.join8.model.City;
import com.join8.model.RequestResetPassword;
import com.join8.model.ResponseCity;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.SymmetricEncryption;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.UrlUtil;

@SuppressLint("ShowToast")
public class FragmentChangePassword extends Fragment implements OnClickListener, Requestlistener {

	private static final String TAG = RegistrationFormFragment.class.getSimpleName();
	private EditText edtPhoneNo, edtVerificationCode, edtPassword, edtCnfmPass;
	private TextView btnCreate, btnGetVerificationCode;
	private int verification_code_request = -1, reset_password_request = -1;
	private NetworkManager mNetworkManager;
	private CryptoManager prefsManager;
	private ResponseCity city;
	private RequestResetPassword req;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		prefsManager = CryptoManager.getInstance(getActivity());
		mNetworkManager = NetworkManager.getInstance();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.reset_password, container, false);
		edtPhoneNo = (EditText) v.findViewById(R.id.edtPhoneNo);
		edtVerificationCode = (EditText) v.findViewById(R.id.edtVerificationCode);
		edtPassword = (EditText) v.findViewById(R.id.edtPassword);
		edtCnfmPass = (EditText) v.findViewById(R.id.edtConfirmPassword);
		btnCreate = (TextView) v.findViewById(R.id.btnCreateAccount);
		btnCreate.setText(getResources().getString(R.string.tvchangepassword));
		btnGetVerificationCode = (TextView) v.findViewById(R.id.btnGetVerificationCode);
		btnCreate.setOnClickListener(this);
		btnGetVerificationCode.setOnClickListener(this);

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(edtPhoneNo);
		mTypefaceUtils.applyTypeface(edtVerificationCode);
		mTypefaceUtils.applyTypeface(edtPassword);
		mTypefaceUtils.applyTypeface(edtCnfmPass);
		mTypefaceUtils.applyTypeface(btnCreate);
		mTypefaceUtils.applyTypeface(btnGetVerificationCode);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (!getActivity().isFinishing()) {
			((HomeScreen) getActivity()).changeActionItems(true);
		}

		if (!getActivity().isFinishing()) {

			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				((HomeScreen) getActivity()).setCustomTitle(getString(R.string.tvchangepassword) + " - " + ((HomeScreen) getActivity()).getCityData().getNameEN(), Typeface.BOLD);
			} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_SIMPLECHINSESE)) {
				((HomeScreen) getActivity()).setCustomTitle(getString(R.string.tvchangepassword) + " - " + ((HomeScreen) getActivity()).getCityData().getName(), Typeface.BOLD);
			} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_TRADITIONALCHINESE)) {
				((HomeScreen) getActivity()).setCustomTitle(getString(R.string.tvchangepassword) + " - " + ((HomeScreen) getActivity()).getCityData().getName(), Typeface.BOLD);
			}
		}

		city = new Gson().fromJson(prefsManager.getPrefs().getString(SelectCityFragment.PARAM_CITYDATA_NEW, ""), ResponseCity.class);

	}

	@Override
	public void onResume() {
		super.onResume();
		mNetworkManager.addListener(this);
		IntentFilter filter = new IntentFilter();
		filter.addAction("android.provider.Telephony.SMS_RECEIVED");
		getActivity().registerReceiver(mSMSReceivedReceiver, filter);
	}

	@Override
	public void onStop() {
		super.onStop();
		mNetworkManager.removeListeners(this);
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.btnCreateAccount) {
			validateData();

		} else if (v.getId() == R.id.btnGetVerificationCode) {

			if (TextUtils.isEmpty(edtPhoneNo.getText().toString())) {
				Toast.displayText(getActivity(), getString(R.string.phonerequirederror));
			} else if (edtPhoneNo.getText().toString().length() < 8 || edtPhoneNo.getText().toString().length() > 12) {
				Toast.displayText(getActivity(), getString(R.string.enterPhone));
			} else {
				getVerificationCode();
			}
		}
	}

	private void validateData() {
		if (TextUtils.isEmpty(edtPhoneNo.getText().toString())) {
			Toast.displayText(getActivity(), getString(R.string.phonerequirederror));
		} else if (edtPhoneNo.getText().toString().length() < 8 || edtPhoneNo.getText().toString().length() > 12) {
			Toast.displayText(getActivity(), getString(R.string.enterPhone));
		} else if (TextUtils.isEmpty(edtVerificationCode.getText().toString())) {
			Toast.displayText(getActivity(), getString(R.string.verificationcoderequirederror));
		} else if (TextUtils.isEmpty(edtPassword.getText().toString())) {
			Toast.displayText(getActivity(), getString(R.string.passwordrequirederror));
		} else if (TextUtils.isEmpty(edtCnfmPass.getText().toString())) {
			Toast.displayText(getActivity(), getString(R.string.repeatpasswordrequirederror));
		} else if (!edtPassword.getText().toString().equals(edtCnfmPass.getText().toString())) {
			Toast.displayText(getActivity(), getString(R.string.samepassword));
		} else {
			req = new RequestResetPassword();
			req.setActivationCode(edtVerificationCode.getText().toString());
			req.setUserName(edtPhoneNo.getText().toString());
			try {
				req.setPassword(SymmetricEncryption.encrypt(edtPassword.getText().toString(), ConstantData.ENCRPITION_KEY));
			} catch (Exception e) {
				e.printStackTrace();
			}
			reset_password_request = mNetworkManager.addRequest(new HashMap<String, String>(), getActivity(), true, UrlUtil.getResetPwdUrl(), Join8RequestBuilder.getResetPwdRequest(req), ConstantData.method_post);
		}
	}
	private void getVerificationCode() {
		verification_code_request = mNetworkManager.addRequest(new HashMap<String, String>(), getActivity(), true, UrlUtil.getVerificationCodeUrl(edtPhoneNo.getText().toString()), "", ConstantData.method_get);
	}

	@Override
	public void onSuccess(int id, String response) {

		if (response != null) {

			Log.e(TAG, response);

			if (id == reset_password_request) {
				Toast.displayText(getActivity(), response);
			} else if (id == verification_code_request) {
				Toast.displayText(getActivity(), response);
			}
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		getActivity().unregisterReceiver(mSMSReceivedReceiver);
	}

	BroadcastReceiver mSMSReceivedReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {

			final Bundle bundle = intent.getExtras();

			try {

				if (bundle != null) {

					final Object[] pdusObj = (Object[]) bundle.get("pdus");

					for (int i = 0; i < pdusObj.length; i++) {

						SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
						String phoneNumber = currentMessage.getDisplayOriginatingAddress();

						if (phoneNumber.endsWith("16197276282")) {

							String message = currentMessage.getDisplayMessageBody();

							String verificationCode = null;
							Pattern p = Pattern.compile(" {1}[0-9]{4}");
							Matcher m = p.matcher(message);
							while (m.find()) {
								verificationCode = new String(m.group().trim());
							}

							if (!TextUtils.isEmpty(verificationCode)) {
								edtVerificationCode.setText(verificationCode);
							}
						}
					}
				}

			} catch (Exception e) {
				Log.e("SmsReceiver", "Exception smsReceiver" + e);
			}
		}
	};
}
