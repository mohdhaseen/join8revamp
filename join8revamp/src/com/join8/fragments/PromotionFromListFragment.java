package com.join8.fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.costum.android.widget.LoadMoreListView;
import com.costum.android.widget.LoadMoreListView.OnLoadMoreListener;
import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.RestaurantActivity;
import com.join8.Toast;
import com.join8.adpaters.PromotionListAdapter;
import com.join8.listeners.Requestlistener;
import com.join8.model.City;
import com.join8.model.Promotions;
import com.join8.model.Registration;
import com.join8.model.ResponseCity;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.CryptoManager;
import com.join8.utils.KeyboardUtils;
import com.join8.utils.TypefaceUtils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class PromotionFromListFragment extends Fragment implements Requestlistener {
	
	private static final String TAG = PromotionFromListFragment.class.getSimpleName();
	
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	public boolean isPromotion = false;
	private int promotion_request = -1;
	private int loadmore_request = -1;

	private NetworkManager networManager;
	private int starIndex = 0;
	private int count = 25;
	private CryptoManager prefManager;
	private String access_token = "";
	public ResponseCity city;
	private String searchtext = "";
	private ArrayList<Promotions> arrPromotionsData;
	public static final String PARAM_PROMOTION = "promotion";
	private String strCuisinType = "0";
	private String strNewDishType = "0";
	private PromotionListAdapter promotionListAdapter;
	private LoadMoreListView lstSearch;
	private int totalrecords = 0;
	private String breifUrl = "";
	private TextView tvresults;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e("Create", "create");
		imageLoader = ImageLoader.getInstance();
		options = new DisplayImageOptions.Builder().showStubImage(R.drawable.noimage).showImageForEmptyUri(R.drawable.noimage).showImageOnFail(R.drawable.noimage).cacheInMemory(true).cacheOnDisc(true).build();
		networManager = NetworkManager.getInstance();
		prefManager = CryptoManager.getInstance(getActivity());
		if (getArguments() != null) {
			searchtext = getArguments().getString("name");
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.e("Create view", "create view");
		View view = getActivity().getActionBar().getCustomView();
		((ImageButton) view.findViewById(R.id.btnFav)).setVisibility(View.INVISIBLE);
		((TextView) view.findViewById(R.id.txtTitle)).setText(searchtext);
		((TextView) view.findViewById(R.id.txtOpen)).setVisibility(View.GONE);

		View v = inflater.inflate(R.layout.promotions, container, false);

		lstSearch = (LoadMoreListView) v.findViewById(R.id.lstRestorent);

		((TextView) v.findViewById(R.id.tvresults)).setVisibility(View.GONE);
		((LinearLayout) v.findViewById(R.id.lnrSearch)).setVisibility(View.GONE);
		((LinearLayout) v.findViewById(R.id.lnrButton)).setVisibility(View.GONE);

		((LinearLayout) v.findViewById(R.id.lnrSearchText)).setVisibility(View.GONE);

		tvresults = (TextView) v.findViewById(R.id.tvresults);

		lstSearch.setEmptyView(tvresults);

		lstSearch.setOnLoadMoreListener(new OnLoadMoreListener() {

			@Override
			public void onLoadMore() {
				Log.e("on Load More", "in Load mOrew");
				if (arrPromotionsData.size() <= totalrecords) {

					starIndex = arrPromotionsData.size();
					doLoadMore(searchtext);
				}

			}
		});
		lstSearch.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				KeyboardUtils.hideKeyboard(view);
				Promotions promotions = (Promotions) parent.getAdapter().getItem(position);
				Bundle b = new Bundle();
				b.putString(PARAM_PROMOTION, new Gson().toJson(promotions));
				b.putBoolean("flag", true);
				PromotionDetails promotiondetaisFragment = new PromotionDetails();
				promotiondetaisFragment.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(PromotionFromListFragment.class.getName()).replace(RestaurantActivity.restaurant_fragment_container, promotiondetaisFragment).commit();

			}
		});

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());

		mTypefaceUtils.applyTypeface(tvresults);

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.e("on Activity Create", "on Activity Create");
		super.onActivityCreated(savedInstanceState);

		city = new Gson().fromJson(prefManager.getPrefs().getString(SelectCityFragment.PARAM_CITYDATA_NEW, ""), ResponseCity.class);// ((HomeScreen)
																												// getActivity()).getCityData();
		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
		arrPromotionsData = new ArrayList<Promotions>();
		doLoadData(searchtext);
		// LoadDishTypes();

	}

	public void doLoadData(String searchtext) {
		lstSearch.onLoadMoreComplete();
		arrPromotionsData.clear();
		networManager.isProgressVisible(true);
		if (city != null && city.getNameEN().length() > 0) {
			lstSearch.setVisibility(View.GONE);
			Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
			String userId = "";
			if (registration != null) {
				userId = registration.getPartitionKey();
			}
			promotion_request = networManager.addRequest(Join8RequestBuilder.getPromotionsRequest(access_token, city != null ? city.getPhoneCode() : "852", searchtext, "" + count, strCuisinType, strNewDishType, "" + starIndex, userId), RequestMethod.POST, getActivity(),
					isPromotion ? Join8RequestBuilder.METHOD_FAVORITEPROMOTIONS : Join8RequestBuilder.METHOD_PROMOTIONS);

		} else {
			Toast.displayText(getActivity(), getString(R.string.selectCity));
		}

	}

	public void doLoadMore(String searchtext) {
		networManager.isProgressVisible(false);
		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		String userId = "";
		if (registration != null)
			userId = registration.getPartitionKey();
		loadmore_request = networManager.addRequest(Join8RequestBuilder.getPromotionsRequest(access_token, city != null ? city.getPhoneCode() : "852", searchtext, "" + count, strCuisinType, strNewDishType, "" + starIndex, userId), RequestMethod.POST, getActivity(),
				isPromotion ? Join8RequestBuilder.METHOD_FAVORITEPROMOTIONS : Join8RequestBuilder.METHOD_PROMOTIONS);

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		networManager.removeListeners(this);
	}

	@Override
	public void onSuccess(int id, String response) {

		if (response != null) {

			if (id == promotion_request) {

				Log.e(getTag(), "" + response);

				if (response.toLowerCase().startsWith("no record found")) {

					return;

				} else {
					try {

						JSONArray jsonArray = new JSONArray(response);

						if (jsonArray != null && jsonArray.length() > 0) {

							totalrecords = Integer.parseInt(jsonArray.getJSONObject(0).getString("TotalRecords"));
							for (int i = 0; i < jsonArray.length(); i++) {
								Promotions searchData = new Gson().fromJson(jsonArray.getJSONObject(i).toString(), Promotions.class);
								arrPromotionsData.add(searchData);
							}

						}

						if (arrPromotionsData.size() > 0) {
							Log.e(getTag(), "in if");
							promotionListAdapter = new PromotionListAdapter(getActivity(), R.layout.row_promotions, arrPromotionsData);
							lstSearch.setAdapter(promotionListAdapter);
							lstSearch.setVisibility(View.VISIBLE);
						} else {
							Log.e(getTag(), "in else");
							arrPromotionsData.clear();
							promotionListAdapter.notifyDataSetChanged();
							lstSearch.invalidate();

						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						if (arrPromotionsData != null && promotionListAdapter != null) {
							arrPromotionsData.clear();
							promotionListAdapter.notifyDataSetChanged();
							lstSearch.invalidate();
						}

					}
				}
			} else if (id == loadmore_request) {
				Log.e(getTag(), "" + response);
				try {

					JSONArray jsonArray = new JSONArray(response);

					if (jsonArray != null && jsonArray.length() > 0) {

						totalrecords = Integer.parseInt(jsonArray.getJSONObject(0).getString("TotalRecords"));
						for (int i = 0; i < jsonArray.length(); i++) {
							Promotions searchData = new Gson().fromJson(jsonArray.getJSONObject(i).toString(), Promotions.class);
							arrPromotionsData.add(searchData);
						}
					}

					if (arrPromotionsData.size() > 0) {
						Log.e(getTag(), "in if");
						promotionListAdapter.notifyDataSetChanged();
						lstSearch.invalidate();
						lstSearch.onLoadMoreComplete();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

}
