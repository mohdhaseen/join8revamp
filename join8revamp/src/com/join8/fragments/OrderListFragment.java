package com.join8.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;
import android.widget.TextView;

import com.costum.android.widget.LoadMoreListView;
import com.costum.android.widget.LoadMoreListView.OnLoadMoreListener;
import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.OrderActivity;
import com.join8.R;
import com.join8.Toast;
import com.join8.adpaters.HorizontalGalleryAdapter;
import com.join8.adpaters.OrderListAdapter;
import com.join8.adpaters.OrderListAdapter.OnOrderDeleteListner;
import com.join8.listeners.Requestlistener;
import com.join8.model.OrderData;
import com.join8.model.Registration;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.quickaction.ActionItem;
import com.join8.quickaction.QuickAction;
import com.join8.quickaction.QuickAction.OnActionItemClickListener;
import com.join8.quickaction.QuickAction.OnDismissListener;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.Log;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;

public class OrderListFragment extends Fragment implements OnClickListener, Requestlistener {

	private static final String TAG = OrderListFragment.class.getSimpleName();
	private static final int COUNT_PER_PAGE = 25;

	private int prePosition = -1, orderListMoredataRequestId = -1, deleteOrderRequestId = -1;

	private Gallery mGallery = null;
	private LoadMoreListView listView = null;
	private TextView txtOrderType = null;

	private NetworkManager networManager = null;
	private int orderListRequestId = -1;
	private CryptoManager prefManager = null;
	private String access_token = "";

	private ArrayList<OrderData> arrOrderList = null;
	private OrderListAdapter adapterOrderList = null;
	private int starIndex = 0;
	private int totalRecords = 0;
	private int deletedPosition = -1;

	private QuickAction mQuickAction = null;

	private String[] orderTypes = null, orderTypesValue = null;
	private TextView tvresults;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments() != null) {

			if (getArguments().getString("redirectTo").equalsIgnoreCase("OrderDetail")) {

				String reference = getArguments().getString("reference");

				Intent intent = new Intent(getActivity(), OrderActivity.class);
				intent.putExtra("orderId", reference);
				getActivity().startActivity(intent);
			}
		}

		networManager = NetworkManager.getInstance();
		networManager.isProgressVisible(true);
		prefManager = CryptoManager.getInstance(getActivity());
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
		
		IntentFilter filter = new IntentFilter();
		filter.addAction(ConstantData.BROADCAST_REFRESH_LIST);
		getActivity().registerReceiver(listRefreshReceiver, filter);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.order_list_fragment, container, false);
		mappingWidgets(mView);
		addListeners();
		init();
		return mView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
		if (!getActivity().isFinishing()) {
			((HomeScreen) getActivity()).setCustomTitle(getString(R.string.my_order), Typeface.NORMAL);
			((HomeScreen) getActivity()).changeActionItems(true);
		}
		loadOrderList();
	}

	private void mappingWidgets(View mView) {

		mGallery = (Gallery) mView.findViewById(R.id.gallery);
		listView = (LoadMoreListView) mView.findViewById(android.R.id.list);
		listView.setVisibility(View.GONE);

		txtOrderType = (TextView) mView.findViewById(R.id.txtOrderType);
		txtOrderType.setSelected(false);
		tvresults = (TextView) mView.findViewById(R.id.tvresults);

		listView.setEmptyView(tvresults);

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(txtOrderType);
		mTypefaceUtils.applyTypeface((TextView) mView.findViewById(R.id.tvOrderListTitle));
		mTypefaceUtils.applyTypeface(tvresults);

	}

	private void addListeners() {

		mGallery.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					prePosition = mGallery.getSelectedItemPosition();
				} else if (event.getAction() == MotionEvent.ACTION_UP) {

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {

							try {

								if (prePosition != mGallery.getSelectedItemPosition()) {

									loadOrderList();
								}

							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}, 500);
				}
				return false;
			}
		});

		listView.setOnLoadMoreListener(new OnLoadMoreListener() {
			@Override
			public void onLoadMore() {

				if (arrOrderList.size() < totalRecords) {

					starIndex = arrOrderList.size();
					loadMoreOrderList();
				}
			}
		});
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				// if (arrOrderList.get(position).getOrderStatus() == 10) {

				OrderDetailFragment.clearOrderList();

				Intent intent = new Intent(getActivity(), OrderActivity.class);
				intent.putExtra("orderId", arrOrderList.get(position).getOrderId());
				getActivity().startActivity(intent);
				// }
			}
		});

		txtOrderType.setOnClickListener(this);
	}

	private void init() {

		mGallery.setAdapter(new HorizontalGalleryAdapter(getActivity(), getResources().getStringArray(R.array.order_days)));
		mGallery.setSelection(4);

		arrOrderList = new ArrayList<OrderData>();
		orderTypes = getResources().getStringArray(R.array.order_types);
		orderTypesValue = getResources().getStringArray(R.array.order_types_value);

		initQuickAction();

		txtOrderType.setText(orderTypes[0]);
		txtOrderType.setTag(orderTypesValue[0]);
	}

	private void loadOrderList() {

		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		String partitionKey = registration.getPartitionKey();

		String startDate = "", endDate = "";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());

		switch (mGallery.getSelectedItemPosition()) {
		case 0:
			break;

		case 1:

			Calendar c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, -30);
			startDate = format.format(c.getTime());
			endDate = format.format(new Date());
			break;

		case 2:
			c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, -7);
			startDate = format.format(c.getTime());
			endDate = format.format(new Date());
			break;

		case 3:
			startDate = format.format(new Date());
			endDate = format.format(new Date());
			break;

		case 4:
			startDate = format.format(new Date());
			c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, 7);
			endDate = format.format(c.getTime());
			break;

		case 5:
			startDate = format.format(new Date());
			c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, 30);
			endDate = format.format(c.getTime());
			break;
		}

		networManager.isProgressVisible(true);
		orderListRequestId = networManager.addRequest(Join8RequestBuilder.getOrderListRequest(access_token, partitionKey, txtOrderType.getTag().toString(), starIndex, COUNT_PER_PAGE, startDate, endDate), RequestMethod.POST, getActivity(),
				Join8RequestBuilder.METHOD_USER_ORDERLIST);
	}

	public void loadMoreOrderList() {

		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		String partitionKey = registration.getPartitionKey();

		String startDate = "", endDate = "";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());

		switch (mGallery.getSelectedItemPosition()) {
		case 0:
			break;

		case 1:

			Calendar c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, -30);
			startDate = format.format(c.getTime());
			endDate = format.format(new Date());
			break;

		case 2:
			c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, -7);
			startDate = format.format(c.getTime());
			endDate = format.format(new Date());
			break;

		case 3:
			startDate = format.format(new Date());
			endDate = format.format(new Date());
			break;

		case 4:
			startDate = format.format(new Date());
			c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, 7);
			endDate = format.format(c.getTime());
			break;

		case 5:
			startDate = format.format(new Date());
			c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, 30);
			endDate = format.format(c.getTime());
			break;
		}

		networManager.isProgressVisible(false);
		orderListMoredataRequestId = networManager.addRequest(Join8RequestBuilder.getOrderListRequest(access_token, partitionKey, txtOrderType.getTag().toString(), starIndex, COUNT_PER_PAGE, startDate, endDate), RequestMethod.POST, getActivity(),
				Join8RequestBuilder.METHOD_USER_ORDERLIST);
	}

	public void deleteOrder(String orderId) {

		networManager.isProgressVisible(true);
		deleteOrderRequestId = networManager.addRequest(Join8RequestBuilder.getDeleteOrder(access_token, orderId), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_DELETE_ORDER);

	}

	private void initQuickAction() {

		mQuickAction = new QuickAction(getActivity(), QuickAction.VERTICAL);

		for (int i = 0; i < orderTypes.length; i++) {
			ActionItem mItem = new ActionItem(i, orderTypes[i]);
			mItem.setColor(getResources().getColor(R.color.orangebg));
			mQuickAction.addActionItem(mItem);
		}

		mQuickAction.setOnActionItemClickListener(new OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction source, int pos, int actionId) {

				txtOrderType.setText(orderTypes[pos]);
				txtOrderType.setTag(orderTypesValue[pos]);

				txtOrderType.setSelected(false);
				txtOrderType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_orange_down, 0);
				txtOrderType.setTextColor(getResources().getColor(R.color.orangebg));

				loadOrderList();
			}
		});

		mQuickAction.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss() {

				txtOrderType.setSelected(false);
				txtOrderType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_orange_down, 0);
				txtOrderType.setTextColor(getResources().getColor(R.color.orangebg));
			}
		});
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.txtOrderType:

			mQuickAction.show(v);

			if (!txtOrderType.isSelected()) {
				txtOrderType.setSelected(true);
				txtOrderType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up_white, 0);
				txtOrderType.setTextColor(getResources().getColor(android.R.color.white));
			} else {
				txtOrderType.setSelected(false);
			}

			break;
		}
	}

	OnOrderDeleteListner onOrderDeleteListner = new OnOrderDeleteListner() {
		@Override
		public void onOrderDeleted(int position, final String orderId) {

			deletedPosition = position;

			if (!Utils.isEmpty(orderId)) {

				AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
				dialog.setMessage(getString(R.string.delete_order));
				dialog.setPositiveButton(getString(R.string.yes), new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						deleteOrder(orderId);
					}
				});
				dialog.setNegativeButton(getString(R.string.no), new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				});
				dialog.show();
			}
		}
	};

	@Override
	public void onSuccess(int id, String response) {

		try {

			Log.i(getTag(), "Result => " + response);

			if (response != null && !response.equals("")) {

				if (id == orderListRequestId) {

					arrOrderList = new ArrayList<OrderData>();

					if (response.toLowerCase().startsWith("no record found")) {

						// Toast.makeText(getActivity(),
						// getString(R.string.norecorderror));
						//
						// listView.setVisibility(View.GONE);
						adapterOrderList = new OrderListAdapter(getActivity(), arrOrderList, onOrderDeleteListner);
						listView.setAdapter(adapterOrderList);
						return;

					} else {

						JSONArray jArray = new JSONArray(response);

						if (jArray != null && jArray.length() > 0) {

							totalRecords = jArray.getJSONObject(0).getInt("TotalRecords");

							for (int i = 0; i < jArray.length(); i++) {
								OrderData data = new Gson().fromJson(jArray.getString(i).toString(), OrderData.class);
								arrOrderList.add(data);
							}

							listView.setVisibility(View.VISIBLE);
							adapterOrderList = new OrderListAdapter(getActivity(), arrOrderList, onOrderDeleteListner);
							listView.setAdapter(adapterOrderList);
						}
					}

				} else if (id == orderListMoredataRequestId) {

					if (!response.toLowerCase().startsWith("no record found")) {

						JSONArray jArray = new JSONArray(response);

						if (jArray != null && jArray.length() > 0) {

							totalRecords = jArray.getJSONObject(0).getInt("TotalRecords");

							for (int i = 0; i < jArray.length(); i++) {
								OrderData data = new Gson().fromJson(jArray.getString(i), OrderData.class);
								arrOrderList.add(data);
							}

							adapterOrderList.notifyDataSetChanged();
							// listView.invalidate();
							listView.onLoadMoreComplete();
						}
					}
				} else if (id == deleteOrderRequestId) {

					if (response.toLowerCase().startsWith("success")) {

						arrOrderList.remove(deletedPosition);

						adapterOrderList.notifyDataSetChanged();

					} else if (response.toLowerCase().startsWith("failed")) {

						Toast.displayText(getActivity(), getString(R.string.failed));
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}
	
	private BroadcastReceiver listRefreshReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			try {

				String redirectTo = intent.getStringExtra("redirectTo");

				if (redirectTo.equalsIgnoreCase("OrderDetail")) {

					starIndex = 0;
					loadOrderList();
				}

			} catch (Exception e) {
				e.printStackTrace();
				Log.e("OrderList", "BroadcastReceiver : " + e.toString());
			}
		}
	};
}
