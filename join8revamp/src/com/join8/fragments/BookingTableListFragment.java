package com.join8.fragments;

import java.util.ArrayList;

import org.json.JSONArray;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.adpaters.BookingTableListAdapter;
import com.join8.listeners.Requestlistener;
import com.join8.model.TableTypeData;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.TypefaceUtils;

public class BookingTableListFragment extends Fragment implements Requestlistener {

	private static final String TAG = BookingTableListFragment.class.getSimpleName();
	
	private ListView listView = null;
	private TextView lblDateTime = null;

	private NetworkManager networManager = null;
	private CryptoManager prefManager;
	private int tableTypeRequestId = -1;

	private ArrayList<TableTypeData> listTableType = null;
	private String companyId = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		companyId = getArguments().getString("companyId");

		networManager = NetworkManager.getInstance();
		prefManager = CryptoManager.getInstance(getActivity());
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.booking_table_list_fragment, container, false);

		listView = (ListView) mView.findViewById(R.id.listView);
		listView.setEmptyView(mView.findViewById(R.id.txtNoResult));
		lblDateTime = (TextView) mView.findViewById(R.id.lblDateTime);

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(lblDateTime);

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

				Intent intent = new Intent();
				if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
					intent.putExtra("tableType", listTableType.get(position).getBookingNameEn());
				} else {
					intent.putExtra("tableType", listTableType.get(position).getBookingName());
				}
				intent.putExtra("tableTypeEn", listTableType.get(position).getBookingNameEn());
				intent.putExtra("deposit", listTableType.get(position).getBookingDeposit());
				getActivity().setResult(Activity.RESULT_OK, intent);
				getActivity().finish();
			}
		});

		return mView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		getTableInfo();
	}

	private void getTableInfo() {

		String access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");

		networManager.isProgressVisible(true);
		tableTypeRequestId = networManager.addRequest(Join8RequestBuilder.getTableType(access_token, companyId), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_GET_TABLE_TYPE);
	}

	@Override
	public void onSuccess(int id, String response) {

		if (id == tableTypeRequestId) {

			try {

				listTableType = new ArrayList<TableTypeData>();

				JSONArray jArray = new JSONArray(response);

				for (int i = 0; i < jArray.length(); i++) {

					TableTypeData data = new Gson().fromJson(jArray.getJSONObject(i).toString(), TableTypeData.class);
					listTableType.add(data);
				}

				listView.setAdapter(new BookingTableListAdapter(getActivity(), listTableType));

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}
}
