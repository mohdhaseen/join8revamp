package com.join8.fragments;

import java.util.HashMap;

import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.ImagePreviewScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.listeners.Requestlistener;
import com.join8.model.FoodOptionData;
import com.join8.model.FoodOptionItemData;
import com.join8.model.Registration;
import com.join8.model.RestaurantData;
import com.join8.model.RestaurantMenuItemData;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.Log;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class FoodOptionFragment extends Fragment implements Requestlistener, OnClickListener {

	private static final String TAG = FoodOptionFragment.class.getSimpleName();

	private TextView txtRestaurantName, txtItemName = null, txtItemPrice = null, txtDesc = null, txtBasePrice = null, txtExtras = null, txtTotal = null, btnAddToOrder = null;
	private TextView lblBasicPrice, lblExtras, lblTotal;
	private ImageView imgPhoto = null;
	private LinearLayout lnrOptionContainer = null;

	private NetworkManager networManager = null;
	private int foodOptionRequestId = -1;
	private CryptoManager prefManager = null;

	private LayoutInflater mInflater = null;

	private RestaurantData restaurantData = null;

	private ImageLoader imageLoader = null;
	private DisplayImageOptions options = null;

	private TypefaceUtils typefaceUtils = null;

	private HashMap<String, CheckBox> hashMapViews = null;

	private double basePrice = 0, extraPrice = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		imageLoader = ImageLoader.getInstance();
		DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();
		int roundSize = (int) (dm.density * getResources().getDimension(R.dimen.image_height));
		options = new DisplayImageOptions.Builder().displayer(new RoundedBitmapDisplayer(roundSize)).showStubImage(R.drawable.default_image_round).showImageForEmptyUri(R.drawable.default_image_round).showImageOnFail(R.drawable.default_image_round).cacheInMemory(true)
				.cacheOnDisc(true).build();

		mInflater = LayoutInflater.from(getActivity());

		networManager = NetworkManager.getInstance();
		networManager.isProgressVisible(true);
		prefManager = CryptoManager.getInstance(getActivity());
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.food_option_fragment, container, false);

		txtRestaurantName = (TextView) mView.findViewById(R.id.txtRestaurantName);
		txtItemName = (TextView) mView.findViewById(R.id.txtItemName);
		txtItemPrice = (TextView) mView.findViewById(R.id.txtItemPrice);
		txtDesc = (TextView) mView.findViewById(R.id.txtDescription);
		txtBasePrice = (TextView) mView.findViewById(R.id.txtBasicPrice);
		txtExtras = (TextView) mView.findViewById(R.id.txtExtras);
		txtTotal = (TextView) mView.findViewById(R.id.txtTotal);
		btnAddToOrder = (TextView) mView.findViewById(R.id.btnAddToOrder);
		imgPhoto = (ImageView) mView.findViewById(R.id.imgPhoto);
		lnrOptionContainer = (LinearLayout) mView.findViewById(R.id.lnrOptionContainer);
		lblBasicPrice = (TextView) mView.findViewById(R.id.lblBasicPrice);
		lblExtras = (TextView) mView.findViewById(R.id.lblExtras);
		lblTotal = (TextView) mView.findViewById(R.id.lblTotal);

		imgPhoto.setOnClickListener(this);
		btnAddToOrder.setOnClickListener(this);

		typefaceUtils = TypefaceUtils.getInstance(getActivity());
		typefaceUtils.applyTypeface(txtRestaurantName);
		typefaceUtils.applyTypeface(txtItemName);
		typefaceUtils.applyTypeface(txtItemPrice);
		typefaceUtils.applyTypeface(txtDesc);
		typefaceUtils.applyTypeface(txtBasePrice);
		typefaceUtils.applyTypeface(txtExtras);
		typefaceUtils.applyTypeface(txtTotal);
		typefaceUtils.applyTypeface(btnAddToOrder);
		typefaceUtils.applyTypeface(lblBasicPrice);
		typefaceUtils.applyTypeface(lblExtras);
		typefaceUtils.applyTypeface(lblTotal);

		return mView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (isAdded()) {

			getFoodOption(getArguments().getInt("menuItemId"));
		}

	}

	private void setTitle(String title) {

		View view = getActivity().getActionBar().getCustomView();
		((TextView) view.findViewById(R.id.txtTitle)).setText(title);
	}

	private void getFoodOption(int menuItemId) {

		String access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");

		networManager.isProgressVisible(true);
		foodOptionRequestId = networManager.addRequest(Join8RequestBuilder.getFoodOptionRequest(access_token, menuItemId), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_GET_FOOD_OPTION);
	}

	@Override
	public void onSuccess(int id, String response) {

		try {

			if (isAdded() && response != null && !response.equals("")) {

				if (id == foodOptionRequestId) {

					JSONObject jObj = new JSONObject(response);

					restaurantData = new Gson().fromJson(jObj.toString(), RestaurantData.class);

					if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
						txtRestaurantName.setText(restaurantData.getCompanyNameEN());
						txtItemName.setText(restaurantData.getItemName_EN());
						txtDesc.setText(restaurantData.getDescription_EN());
						setTitle(restaurantData.getCompanyNameEN());
					} else {
						txtRestaurantName.setText(restaurantData.getCompanyName());
						txtItemName.setText(restaurantData.getItemName());
						txtDesc.setText(restaurantData.getDescription());
						setTitle(restaurantData.getCompanyName());
					}

					try {
						basePrice = Double.parseDouble(restaurantData.getPrice());
					} catch (Exception e) {
					}

					txtItemPrice.setText("$" + Utils.formatDecimal("" + basePrice));
					txtBasePrice.setText("$" + Utils.formatDecimal("" + basePrice));
					txtExtras.setText("$0.00");
					if (restaurantData.getPhoto().size() > 0) {
						imageLoader.displayImage(restaurantData.getPhoto().get(0), imgPhoto, options);
					}

					hashMapViews = new HashMap<String, CheckBox>();

					for (int i = 0; i < restaurantData.getFoodOption().size(); i++) {
						displayFoodOption(i, restaurantData.getFoodOption().get(i));
					}

					txtExtras.setText("$" + Utils.formatDecimal("" + extraPrice));
					txtTotal.setText("$" + Utils.formatDecimal(String.valueOf(basePrice + extraPrice)));

					if (!restaurantData.isHasFoodOption()) {
						lblExtras.setVisibility(View.GONE);
						txtExtras.setVisibility(View.GONE);
						lblTotal.setVisibility(View.GONE);
						txtTotal.setVisibility(View.GONE);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	private void displayFoodOption(int index, FoodOptionData data) {

		try {

			int margin = getResources().getDimensionPixelOffset(R.dimen.margin_5);

			LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			layoutParams.setMargins(margin, margin, margin, margin);

			View view = mInflater.inflate(R.layout.view_food_option, lnrOptionContainer, false);

			TextView txtHeader = (TextView) view.findViewById(R.id.txtHeader);
			TextView txtToppingPrice = (TextView) view.findViewById(R.id.txtToppingPrice);
			LinearLayout lnrContainerRow = (LinearLayout) view.findViewById(R.id.lnrContainerRow);
			typefaceUtils.applyTypeface(txtHeader);
			typefaceUtils.applyTypeface(txtToppingPrice);

			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				txtHeader.setText(data.getFoodOptionNameEN());
			} else {
				txtHeader.setText(data.getFoodOptionName());
			}
			txtToppingPrice.setTag(index);

			if (data.getBasePrice() > 0) {
				txtToppingPrice.setText("$" + Utils.formatDecimal("" + data.getBasePrice()));
				extraPrice += data.getBasePrice();
			} else {
				txtToppingPrice.setText("");
			}

			LinearLayout lnrRow = new LinearLayout(getActivity());
			lnrRow.setLayoutParams(layoutParams);
			lnrRow.setWeightSum(2f);

			int size = data.getFoodOptionItem().size();

			for (int i = 0; i < size; i++) {

				LinearLayout lnrChild = new LinearLayout(getActivity());
				LayoutParams layoutParams1 = new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1);
				lnrChild.setLayoutParams(layoutParams1);
				lnrChild.setPadding(margin, margin, margin, margin);

				FoodOptionItemData itemData = data.getFoodOptionItem().get(i);

				CheckBox chk = getCheckBox(data.getMaxSelection());
				chk.setTag(new int[] { index, i });
				hashMapViews.put(index + "_" + i, chk);

				if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
					if (itemData.getExtraPrice() > 0) {
						chk.setText(itemData.getFoodOptionItemNameEn() + " $" + itemData.getExtraPrice());
					} else {
						chk.setText(itemData.getFoodOptionItemNameEn());
					}
				} else {
					if (itemData.getExtraPrice() > 0) {
						chk.setText(itemData.getFoodOptionItemName() + " $" + itemData.getExtraPrice());
					} else {
						chk.setText(itemData.getFoodOptionItemName());
					}
				}
				chk.setOnClickListener(onClickListener);

				lnrChild.addView(chk);

				lnrRow.addView(lnrChild);

				if ((i + 1) % 2 == 0) {
					lnrContainerRow.addView(lnrRow);
					lnrRow = new LinearLayout(getActivity());
					lnrRow.setLayoutParams(layoutParams);
					lnrRow.setWeightSum(2f);
				}

				if ((i + 1) % 2 == 0) {
					lnrRow.setWeightSum(2f);
				}
			}

			if (size % 2 != 0) {
				lnrContainerRow.addView(lnrRow);
			}

			lnrOptionContainer.addView(view);

		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, e.toString());
		}
	}

	private CheckBox getCheckBox(int type) {

		CheckBox chk = new CheckBox(getActivity());
		chk.setButtonDrawable(R.drawable.trans);
		if (type == 1) {
			chk.setCompoundDrawablesWithIntrinsicBounds(R.drawable.theme_rbt, 0, 0, 0);
		} else {
			chk.setCompoundDrawablesWithIntrinsicBounds(R.drawable.theme_checkbox, 0, 0, 0);
		}
		chk.setCompoundDrawablePadding((int) getResources().getDimension(R.dimen.margin_8));
		chk.setTextColor(Color.BLACK);
		chk.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.font_signin_mid));
		chk.setPadding(2, 2, 2, 2);
		typefaceUtils.applyTypeface(chk);
		return chk;

	}

	OnClickListener onClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {

			CheckBox chk = (CheckBox) v;
			int[] tag = (int[]) chk.getTag();

			FoodOptionData foodOptionData = restaurantData.getFoodOption().get(tag[0]);
			FoodOptionItemData foodOptionItemData = restaurantData.getFoodOption().get(tag[0]).getFoodOptionItem().get(tag[1]);
			int itemSize = restaurantData.getFoodOption().get(tag[0]).getFoodOptionItem().size();

			if (chk.isChecked()) {

				if (foodOptionData.getMaxSelection() == 1) {

					foodOptionData.setTotalSelection(foodOptionData.getTotalSelection() + 1);
					extraPrice += foodOptionItemData.getExtraPrice();
					foodOptionItemData.setAdded(true);

					for (int i = 0; i < itemSize; i++) {

						if (hashMapViews.containsKey(tag[0] + "_" + i)) {

							CheckBox chkTemp = hashMapViews.get(tag[0] + "_" + i);
							if (chkTemp != chk && foodOptionData.getFoodOptionItem().get(i).isAdded()) {
								chkTemp.setChecked(false);
								extraPrice -= foodOptionData.getFoodOptionItem().get(i).getExtraPrice();
								foodOptionData.getFoodOptionItem().get(i).setAdded(false);
							}
						}
					}

				} else if (foodOptionData.getTotalSelection() < foodOptionData.getMaxSelection()) {

					foodOptionData.setTotalSelection(foodOptionData.getTotalSelection() + 1);
					extraPrice += foodOptionItemData.getExtraPrice();
					foodOptionItemData.setAdded(true);

				} else {
					chk.setChecked(false);
					if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
						Toast.displayText(getActivity(), String.format(getString(R.string.max_selection), foodOptionData.getMaxSelection(), foodOptionData.getFoodOptionNameEN()));
					} else {
						Toast.displayText(getActivity(), String.format(getString(R.string.max_selection), foodOptionData.getMaxSelection(), foodOptionData.getFoodOptionName()));
					}
				}
			} else {

				foodOptionData.setTotalSelection(foodOptionData.getTotalSelection() - 1);
				extraPrice -= foodOptionItemData.getExtraPrice();
				foodOptionItemData.setAdded(false);
			}

			txtExtras.setText("$" + Utils.formatDecimal("" + extraPrice));
			txtTotal.setText("$" + Utils.formatDecimal(String.valueOf(basePrice + extraPrice)));
		}
	};

	@Override
	public void onClick(View v) {

		try {

			if (v.getId() == R.id.btnAddToOrder) {

				Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);

				if (registration == null) {
					Toast.displayText(getActivity(), getString(R.string.please_login));
				} else {

					boolean isFound = false;
					for (int i = 0; i < restaurantData.getFoodOption().size(); i++) {

						if (restaurantData.getFoodOption().get(i).getTotalSelection() < restaurantData.getFoodOption().get(i).getMandatory()) {
							isFound = true;
							Toast.displayText(getActivity(), String.format(getString(R.string.min_selection), restaurantData.getFoodOption().get(i).getMandatory(), restaurantData.getFoodOption().get(i).getFoodOptionName()));
							break;
						}
					}

					if (!isFound) {

						RestaurantMenuItemData data = new RestaurantMenuItemData();

						data.setCompanyId(restaurantData.getCompanyId());
						data.setMenuId(restaurantData.getMenuId());
						data.setMenuItemId(restaurantData.getMenuItemId());
						data.setItemName(restaurantData.getItemName());
						data.setItemName_EN(restaurantData.getItemName_EN());
						data.setPrice(basePrice);
						data.setQuantity(1);
						data.setHasFoodOption(restaurantData.isHasFoodOption());

						if (restaurantData.getFoodOption().size() > 0) {

							for (int i = 0; i < restaurantData.getFoodOption().size(); i++) {

								double basePrice = restaurantData.getFoodOption().get(i).getBasePrice();

								for (int j = 0; j < restaurantData.getFoodOption().get(i).getFoodOptionItem().size(); j++) {

									FoodOptionItemData foodOptionItemData = restaurantData.getFoodOption().get(i).getFoodOptionItem().get(j);

									if (foodOptionItemData.isAdded()) {

										foodOptionItemData.setIsFoodOption(restaurantData.getFoodOption().get(i).getMandatory());
										foodOptionItemData.setExtraPrice(foodOptionItemData.getExtraPrice() + basePrice);
										basePrice = 0;
										data.getFoodOptions().add(foodOptionItemData);
									}
								}
							}
						} else {

							data.setMenuId(restaurantData.getMenuId());
							data.setMenuItemId(restaurantData.getMenuItemId());
							data.setItemName(restaurantData.getItemName());
							data.setItemName_EN(restaurantData.getItemName_EN());
							data.setPrice(basePrice + extraPrice);
							data.setQuantity(1);
							data.setHasFoodOption(restaurantData.isHasFoodOption());
						}

						OrderDetailFragment.addOrder(data);
						Toast.displayTextForShortTime(getActivity(), getString(R.string.added));

						getActivity().finish();

						// Bundle bundle = new Bundle();
						// bundle.putSerializable("restaurantData",
						// restaurantData);
						//
						// OrderDetailFragment fragment = new
						// OrderDetailFragment();
						// fragment.setArguments(bundle);
						// getActivity().getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,
						// fragment).addToBackStack(OrderConfirmationFragment.class.getName()).commit();
					}
				}

			} else if (v.getId() == R.id.imgPhoto) {

				if (restaurantData.getPhoto() == null && restaurantData.getPhoto().size() <= 0) {
					Toast.displayText(getActivity(), getString(R.string.no_image));
				} else {

					String title = "";

					if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
						title = restaurantData.getCompanyNameEN();
					} else {
						title = restaurantData.getCompanyName();
					}

					Intent intent = new Intent(getActivity(), ImagePreviewScreen.class);
					intent.putExtra("title", title);
					intent.putExtra("images", restaurantData.getPhoto());
					startActivity(intent);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
