package com.join8.fragments;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.listeners.Requestlistener;
import com.join8.model.Registration;
import com.join8.model.RequestFeedBack;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.UrlUtil;
import com.join8.utils.Utils;

@SuppressLint("ShowToast")
public class FeedbackFragment extends Fragment implements OnClickListener, Requestlistener {

	private static final String TAG = FeedbackFragment.class.getSimpleName();

	private EditText edtName, edtEmail, edtPhone, edtSubject;
	private TextView btnSendForm;

	private NetworkManager networManager = null;
	private CryptoManager prefManager = null;
	private int sendFormRequestId = -1;
	private String access_token = "";

	private String phoneCode = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		networManager = NetworkManager.getInstance();
		networManager.isProgressVisible(true);
		prefManager = CryptoManager.getInstance(getActivity());
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		((HomeScreen) getActivity()).initActionBar();
		((HomeScreen) getActivity()).hideNewActionBar();
		View v = inflater.inflate(R.layout.feedback, container, false);

		edtName = (EditText) v.findViewById(R.id.edtName);
		edtEmail = (EditText) v.findViewById(R.id.edtEmail);
		edtPhone = (EditText) v.findViewById(R.id.edtPhone);
		edtSubject = (EditText) v.findViewById(R.id.edtRemark);
		btnSendForm = (TextView) v.findViewById(R.id.btnSendForm);
		btnSendForm.setOnClickListener(this);

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(edtName);
		mTypefaceUtils.applyTypeface(edtEmail);
		mTypefaceUtils.applyTypeface(edtPhone);
		mTypefaceUtils.applyTypeface(edtSubject);
		mTypefaceUtils.applyTypeface(btnSendForm);

		ScrollView scrollView = (ScrollView) v.findViewById(R.id.scrollView);
		scrollView.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(edtName.getWindowToken(), 0);
				}
				return false;
			}
		});

		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		if (registration != null) {
			edtName.setText(registration.getFirstName() + " " + registration.getLastName());
			edtEmail.setText(registration.getEmail());

			String phone = registration.getMobileNumber();
			if (!Utils.isEmpty(phone)) {
				if (phone.contains("|")) {
					phone = phone.replaceAll("\\|", "-");
				}

				if (phone.contains("-")) {

					String[] str = phone.split("-");
					edtPhone.setText(str[str.length - 1]);

				} else {
					edtPhone.setText(phone);
				}
			}
		}

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
		phoneCode = prefManager.getPrefs().getString(SelectCityFragment.PARAM_PHONE_CODE, "");

		if (!getActivity().isFinishing()) {
			((HomeScreen) getActivity()).setCustomTitle(getString(R.string.feedback_title), Typeface.NORMAL);
			((HomeScreen) getActivity()).changeActionItems(true);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

	}

	private void sendFeedbackForm() {

		String phone = edtPhone.getText().toString().trim();
		if (!phone.equals("")) {
			phone = phoneCode + "-" + phone;
		}

		networManager.isProgressVisible(true);
		/*sendFormRequestId = networManager.addRequest(Join8RequestBuilder.sendFeedbackRequest(access_token, edtName.getText().toString().trim(), edtEmail.getText().toString().trim(), phone, edtSubject.getText().toString().trim()), RequestMethod.POST, getActivity(),
				Join8RequestBuilder.METHOD_SEND_FEEDBACK);*/
		sendFormRequestId=networManager.addRequest(null, getActivity(), true, UrlUtil.getFeeadBackUrl(), getFeedBackRequest(phone), ConstantData.method_post);
	}

	private String getFeedBackRequest(String PhoneNumber) {
		RequestFeedBack req=new RequestFeedBack();
		req.setName(edtName.getText().toString());
		req.setEmail(edtEmail.getText().toString());
		req.setPhoneNumber(PhoneNumber);
		req.setSubject(edtSubject.getText().toString());
		return new Gson().toJson(req,RequestFeedBack.class);
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.btnSendForm) {

			if (TextUtils.isEmpty(edtName.getText().toString())) {
				Toast.displayText(getActivity(), getString(R.string.name_required));
			} else if (TextUtils.isEmpty(edtEmail.getText().toString())) {
				Toast.displayText(getActivity(), getString(R.string.emailrequirederror));
			} else if (TextUtils.isEmpty(edtSubject.getText().toString())) {
				Toast.displayText(getActivity(), getString(R.string.subject_required));
			} else if (!Utils.checkEmail(edtEmail.getText().toString().trim())) {
				Toast.displayText(getActivity(), getString(R.string.emailnovalideerror));
			} else {

				sendFeedbackForm();
			}
		}
	}

	@Override
	public void onSuccess(int id, String response) {

		try {

			Log.i(getTag(), "Result => " + response);

			if (response != null && !response.equals("")) {

				if (id == sendFormRequestId) {

					if (response.toLowerCase().contains(("success"))) {
						Toast.displayText(getActivity(), getString(R.string.feedback_success));
					} else {
						Toast.displayText(getActivity(), getString(R.string.failed));
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		networManager.removeListeners(this);
	}
}
