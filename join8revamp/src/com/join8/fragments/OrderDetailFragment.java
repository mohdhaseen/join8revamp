package com.join8.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.PaymentActivity;
import com.join8.R;
import com.join8.Toast;
import com.join8.adpaters.OrderDetailAdapter;
import com.join8.adpaters.OrderDetailAdapter.ChangeCart;
import com.join8.adpaters.OrderDetailAdapter.OnItemDeleteListener;
import com.join8.listeners.Requestlistener;
import com.join8.model.City;
import com.join8.model.FoodOptionItemData;
import com.join8.model.OrderData;
import com.join8.model.Registration;
import com.join8.model.ResponseCity;
import com.join8.model.RestaurantData;
import com.join8.model.RestaurantMenuItemData;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.request.PARAMS;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.Log;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;

public class OrderDetailFragment extends Fragment implements ChangeCart, Requestlistener, OnClickListener, OnCheckedChangeListener {

	private static final String TAG = OrderDetailFragment.class.getSimpleName();
	private static final int INTENT_PAYMENT = 1;

	public static ArrayList<RestaurantMenuItemData> listItems = null;
	public static ArrayList<Integer> listItemIds = null;
	public static int companyId = -1;

	private ListView mListView;
	private RadioButton rbtCollect, rbtDeliver, rbtPayNow, rbtPayOnCollection;
	private RadioGroup rbgDeliveryType;
	private TextView txtOrderDate, txtChangeUserProfile, txtRestAddress, txtUserName, txtMobilePhone;
	private EditText edtRemark, edtAddress;
	private TextView txtDeliveryCharge, txtTotal, txtPlaceOrder;

	private NetworkManager networManager;
	private CryptoManager prefManager;
	private RestaurantData restaurantData;
	private String access_token = "";
	private int get_Order = -1, userProfile_request = -1, sendOrderRequest = -1;
	private String orderId = "0";

	private ArrayList<Registration> userProfileList = null;
	private String[] strArrayUserProfile = null;
	private int selectedUserProfileIndex = -1;
	private OrderData orderData = null;
	private String restAddress = "";
	private long selectedDate = 0;
	private int year = -1, month, day, hours, minute;
	private double total = 0, deliveryFee = 0;
	private OnItemDeleteListener onItemDeleteListener = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		networManager = NetworkManager.getInstance();
		prefManager = CryptoManager.getInstance(getActivity());
		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");

		if (savedInstanceState != null) {
			if (savedInstanceState.containsKey("restaurantData")) {
				restaurantData = (RestaurantData) savedInstanceState.getSerializable("restaurantData");
			} else if (savedInstanceState.containsKey("orderId")) {
				orderId = savedInstanceState.getString("orderId");
			}
		} else {
			if (getArguments() != null) {
				if (getArguments().containsKey("restaurantData")) {
					restaurantData = (RestaurantData) getArguments().getSerializable("restaurantData");
				} else if (getArguments().containsKey("orderId")) {
					orderId = getArguments().getString("orderId");
				}
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	public static void addOrder(RestaurantMenuItemData restaurantMenuItemData) {
		if (companyId == -1) {
			companyId = restaurantMenuItemData.getCompanyId();
		} else if (companyId != restaurantMenuItemData.getCompanyId() && listItems != null) {
			clearOrderList();
			companyId = restaurantMenuItemData.getCompanyId();
		}
		if (listItems == null) {
			listItems = new ArrayList<RestaurantMenuItemData>();
			listItemIds = new ArrayList<Integer>();
		}
		if (restaurantMenuItemData.isHasFoodOption()) {
			listItems.add(restaurantMenuItemData);
			listItemIds.add(restaurantMenuItemData.getMenuItemId());
		} else {

			boolean isFound = false;
			int index = 0;

			for (int i = 0; i < listItemIds.size(); i++) {

				if (restaurantMenuItemData.getMenuItemId() == listItemIds.get(i)) {
					isFound = true;
					index = i;
					break;
				}
			}

			if (isFound) {
				RestaurantMenuItemData tempData = listItems.get(index);
				tempData.setQuantity(tempData.getQuantity() + 1);
				listItems.set(index, tempData);
			} else {
				listItems.add(restaurantMenuItemData);
				listItemIds.add(restaurantMenuItemData.getMenuItemId());
			}
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (Utils.isEmpty(orderId) || orderId.equals("0")) {
			outState.putSerializable("restaurantData", restaurantData);
		} else {
			outState.putSerializable("orderId", orderId);
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.order_details_fragment, container, false);
		mappingWidgets(mView);
		init();
		return mView;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == INTENT_PAYMENT && resultCode == Activity.RESULT_OK) {

			OrderDetailFragment.clearOrderList();
			OrderDetailFragment.companyId = -1;
			getActivity().setResult(Activity.RESULT_OK);
			getActivity().finish();
		}
	}

	private void mappingWidgets(View mView) {

		mListView = (ListView) mView.findViewById(android.R.id.list);

		View mHeaderView = LayoutInflater.from(getActivity()).inflate(R.layout.order_details_header, null);
		mListView.addHeaderView(mHeaderView);

		txtRestAddress = (TextView) mHeaderView.findViewById(R.id.txtRestAddress);
		rbtCollect = (RadioButton) mHeaderView.findViewById(R.id.rbtCollect);
		rbtDeliver = (RadioButton) mHeaderView.findViewById(R.id.rbtDeliver);
		txtChangeUserProfile = (TextView) mHeaderView.findViewById(R.id.txtChangeUserProfile);
		txtUserName = (TextView) mHeaderView.findViewById(R.id.txtUserName);
		txtMobilePhone = (TextView) mHeaderView.findViewById(R.id.txtPhone);
		txtOrderDate = (TextView) mHeaderView.findViewById(R.id.txtOrderDate);
		edtAddress = (EditText) mHeaderView.findViewById(R.id.edtAddress);
		edtRemark = (EditText) mHeaderView.findViewById(R.id.edtRemark);
		rbgDeliveryType = (RadioGroup) mHeaderView.findViewById(R.id.rbgDeliveryType);

		View mFooterView = LayoutInflater.from(getActivity()).inflate(R.layout.order_details_footer, null);
		mListView.addFooterView(mFooterView);

		rbtPayNow = (RadioButton) mFooterView.findViewById(R.id.rbtPayNow);
		rbtPayOnCollection = (RadioButton) mFooterView.findViewById(R.id.rbtPayOnCollection);
		txtPlaceOrder = (TextView) mFooterView.findViewById(R.id.txtPlaceOrder);
		txtTotal = (TextView) mFooterView.findViewById(R.id.txtTotal);
		txtDeliveryCharge = (TextView) mFooterView.findViewById(R.id.txtDeliveryCharge);

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface((TextView) mFooterView.findViewById(R.id.lblDeliveryCharge));
		mTypefaceUtils.applyTypeface((TextView) mFooterView.findViewById(R.id.lblTotal));
		mTypefaceUtils.applyTypeface(rbtCollect);
		mTypefaceUtils.applyTypeface(rbtDeliver);
		mTypefaceUtils.applyTypeface(txtRestAddress);
		mTypefaceUtils.applyTypeface(txtChangeUserProfile);
		mTypefaceUtils.applyTypeface(txtUserName);
		mTypefaceUtils.applyTypeface(txtMobilePhone);
		mTypefaceUtils.applyTypeface(txtOrderDate);
		mTypefaceUtils.applyTypeface(rbtPayNow);
		mTypefaceUtils.applyTypeface(rbtPayOnCollection);
		mTypefaceUtils.applyTypeface(txtDeliveryCharge);
		mTypefaceUtils.applyTypeface(txtTotal);
		mTypefaceUtils.applyTypeface(txtPlaceOrder);

		rbgDeliveryType.setOnCheckedChangeListener(this);
		txtChangeUserProfile.setOnClickListener(this);
		txtOrderDate.setOnClickListener(this);
		txtPlaceOrder.setOnClickListener(this);
		txtMobilePhone.setOnClickListener(this);

		// if (!editable) {
		//
		// txtChangeUserProfile.setEnabled(false);
		// txtOrderDate.setEnabled(false);
		// rbtCollect.setEnabled(false);
		// rbtDeliver.setEnabled(false);
		// rbtPayNow.setEnabled(false);
		// rbtPayOnCollection.setEnabled(false);
		// txtPlaceOrder.setVisibility(View.GONE);
		// edtAddress.setEnabled(false);
		// edtRemark.setEnabled(false);
		// txtChangeUserProfile.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0,
		// 0);
		// txtOrderDate.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		// }

		// edtRemark.setOnFocusChangeListener(new OnFocusChangeListener() {
		// @Override
		// public void onFocusChange(View v, boolean hasFocus) {
		//
		// if (!hasFocus) {
		// KeyboardUtils.hideKeyboard(edtRemark);
		// }
		// }
		// });

	}

	private void init() {

		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
			setTitle(restaurantData.getCompanyNameEN());
		} else {
			setTitle(restaurantData.getCompanyName());
		}

		txtChangeUserProfile.setVisibility(View.GONE);
		txtUserName.setVisibility(View.GONE);
		txtMobilePhone.setVisibility(View.GONE);
		
		if(restaurantData.getDeliveryOption() == 0){
			rbtDeliver.setEnabled(false);
		}

		if (restaurantData.getTakeOutPayOptionOrder() == 1) {
			rbtPayNow.setChecked(true);
			rbtPayNow.setEnabled(false);
			rbtPayOnCollection.setVisibility(View.GONE);
			rbtPayOnCollection.setEnabled(false);
		} else if (restaurantData.getTakeOutPayOptionOrder() == 2) {
			rbtPayOnCollection.setChecked(true);
			rbtPayNow.setEnabled(false);
			rbtPayNow.setVisibility(View.GONE);
			rbtPayOnCollection.setEnabled(false);
		} else if (restaurantData.getTakeOutPayOptionOrder() == 3) {
			rbtPayNow.setChecked(true);
		}

		if (listItems == null) {
			changeTotal();
			OrderDetailAdapter mAdapter = new OrderDetailAdapter(getActivity(), new ArrayList<RestaurantMenuItemData>(), this, onItemDeleteListener);
			mListView.setAdapter(mAdapter);
		}

		if (Utils.isEmpty(orderId) && listItems != null) {

			changeTotal();
			OrderDetailAdapter mAdapter = new OrderDetailAdapter(getActivity(), listItems, this, onItemDeleteListener);
			mListView.setAdapter(mAdapter);
		} else {
			if (listItems == null) {
				loadOrder(orderId);
			} else {
				changeTotal();
				OrderDetailAdapter mAdapter = new OrderDetailAdapter(getActivity(), listItems, this, onItemDeleteListener);
				mListView.setAdapter(mAdapter);
			}
		}

		loadUserProfile();

		if (restaurantData != null) {

			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				restAddress = restaurantData.getCompanyNameEN();
				restAddress += "\n" + restaurantData.getAddress2();
			} else {
				restAddress = restaurantData.getCompanyName();
				restAddress += "\n" + restaurantData.getAddress1();
			}

			if (!Utils.isEmpty(restaurantData.getPhone())) {
				if (!Utils.isEmpty(restAddress)) {
					restAddress += "\n" + getString(R.string.tel) + " " + restaurantData.getPhone();
				} else {
					restAddress = getString(R.string.tel) + " " + restaurantData.getPhone();
				}
			}
			txtRestAddress.setText(restAddress);
			
		}

		try {
			Calendar cal = Calendar.getInstance();
			year = cal.get(Calendar.YEAR);
			day = cal.get(Calendar.DAY_OF_MONTH);
			month = cal.get(Calendar.MONTH);
			hours = cal.get(Calendar.HOUR);
			minute = cal.get(Calendar.MINUTE);

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd - hh:mm a", Locale.getDefault());
			String dateTime = format.format(cal.getTime());
			txtOrderDate.setText(dateTime);
			format = new SimpleDateFormat("EEEE yyyy-MM-dd HH:mm:ss", Locale.getDefault());
			getUTCTIME(format.format(cal.getTime()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setTitle(String title) {

		View view = getActivity().getActionBar().getCustomView();
		((ImageButton) view.findViewById(R.id.btnFav)).setVisibility(View.INVISIBLE);
		((TextView) view.findViewById(R.id.txtTitle)).setText(title);
		((TextView) view.findViewById(R.id.txtOpen)).setVisibility(View.GONE);
	}

	public void setOnItemDeleteListener(OnItemDeleteListener onItemDeleteListener) {
		this.onItemDeleteListener = onItemDeleteListener;
	}

	@Override
	public void changeTotal() {
		try {

			total = 0.0;

			if (listItems != null) {

				for (int i = 0; i < listItems.size(); i++) {

					total += (listItems.get(i).getQuantity() * listItems.get(i).getPrice());

					for (int j = 0; j < listItems.get(i).getFoodOptions().size(); j++) {

						total += listItems.get(i).getQuantity() * listItems.get(i).getFoodOptions().get(j).getExtraPrice();
					}
				}
			}

			deliveryFee = 0;

			if (!rbtCollect.isChecked()) {

				if (restaurantData.getDeliveryOption() == 1) {

					deliveryFee = restaurantData.getDeliveryFee();

				} else if (restaurantData.getDeliveryOption() == 2) {

					deliveryFee = total * restaurantData.getDeliveryFee() / 100;
				}
			}

			txtDeliveryCharge.setText("$" + Utils.formatDecimal(String.valueOf(deliveryFee)));
			txtTotal.setText("$" + Utils.formatDecimal(String.valueOf(deliveryFee + total)));

		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	private void setUserData(Registration registration) {

		String profileName = "";
		if (registration.getProfileName().contains("-")) {
			profileName = registration.getProfileName().substring(registration.getProfileName().lastIndexOf("-") + 1);
		} else {
			profileName = registration.getProfileName();
		}

		txtChangeUserProfile.setText(profileName);
		txtUserName.setText(registration.getFirstName() + " " + registration.getLastName());

		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
			edtAddress.setText(registration.getAddress2());
		} else {
			edtAddress.setText(registration.getAddress1());
		}

		if (!Utils.isEmpty(registration.getMobileNumber())) {

			String mobileNumber = registration.getMobileNumber();

			if (mobileNumber.contains("|")) {
				mobileNumber = mobileNumber.replaceAll("\\|", "-");
			}

			if (mobileNumber.contains("-")) {
				String[] str = mobileNumber.split("-");
				txtMobilePhone.setText(str[str.length - 1]);
			} else {
				txtMobilePhone.setText(mobileNumber);
			}

			txtMobilePhone.setCompoundDrawablesWithIntrinsicBounds(R.drawable.theme_btn_call, 0, 0, 0);

		} else {
			txtMobilePhone.setText(getString(R.string.no_phono_number));
			txtMobilePhone.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		}
	}

	private void showUserProfilePopup() {

		AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
		ab.setTitle(getString(R.string.change_user_profile));
		ab.setSingleChoiceItems(strArrayUserProfile, selectedUserProfileIndex, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface d, int choice) {
				selectedUserProfileIndex = choice;
				setUserData(userProfileList.get(choice));
				((Dialog) d).dismiss();
			}
		});

		ab.show();
	}

	private void showDateTimeDialog(final TextView t) {

		LayoutInflater li = LayoutInflater.from(getActivity());
		final View view = li.inflate(R.layout.datetime_dialog, null);

		Builder dialog = new AlertDialog.Builder(getActivity());
		dialog.setTitle(getString(R.string.select_date_and_time));

		final DatePicker dtPicker = (DatePicker) view.findViewById(R.id.datePicker);
		final TimePicker tmPicker = (TimePicker) view.findViewById(R.id.timePicker);

		dtPicker.setCalendarViewShown(false);

		if (year != -1) {
			// dtPicker.updateDate(year,month,day,null);

			dtPicker.updateDate(year, month, day);
			tmPicker.setCurrentHour(hours);
			tmPicker.setCurrentMinute(minute);
		}
		dialog.setView(view);

		dialog.setPositiveButton(getString(R.string.set), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

				Calendar cal = Calendar.getInstance();
				cal.set(dtPicker.getYear(), dtPicker.getMonth(), dtPicker.getDayOfMonth(), tmPicker.getCurrentHour(), tmPicker.getCurrentMinute(), 59);

				if (cal.getTimeInMillis() >= Calendar.getInstance().getTimeInMillis()) {
					try {
						year = dtPicker.getYear();
						day = dtPicker.getDayOfMonth();
						month = dtPicker.getMonth();
						hours = tmPicker.getCurrentHour();
						minute = tmPicker.getCurrentMinute();

						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd - hh:mm a", Locale.getDefault());
						String dateTime = format.format(cal.getTime());
						t.setText(dateTime);
						format = new SimpleDateFormat("EEEE yyyy-MM-dd HH:mm:ss", Locale.getDefault());
						getUTCTIME(format.format(cal.getTime()));
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					Toast.displayText(getActivity(), getString(R.string.error_past_date));
				}

			}
		});

		dialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

			}
		});
		dialog.show();
	}

	private void getUTCTIME(String date) {
		try {

			SimpleDateFormat sourceFormat = new SimpleDateFormat("EEEE yyyy-MM-dd HH:mm:ss", Locale.getDefault());
			sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date parsed = sourceFormat.parse(date); // => Date is in UTC now
			Log.d("CurrentDate", "" + parsed);
			selectedDate = parsed.getTime();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Calendar getUTCCalendar() {

		Calendar cal = Calendar.getInstance();

		try {

			SimpleDateFormat format = new SimpleDateFormat("EEEE yyyy-MM-dd HH:mm:ss", Locale.getDefault());
			String date = format.format(cal.getTime());
			format.setTimeZone(TimeZone.getTimeZone("UTC"));
			cal.setTime(format.parse(date));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return cal;
	}

	private void loadOrder(String orderId) {
		networManager.isProgressVisible(true);
		get_Order = networManager.addRequest(Join8RequestBuilder.getParticularOrder(access_token, orderId), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_GET_PARTICULAR_ORDER);
	}

	private void loadUserProfile() {
		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		networManager.isProgressVisible(true);
		userProfile_request = networManager.addRequest(Join8RequestBuilder.getProfileOfUser(access_token, registration.getPartitionKey()), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_GET_PROFILES_OF_USER);
	}

	private void placeOrder() {

		try {

			Registration reg = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
			ResponseCity cityData = new Gson().fromJson(prefManager.getPrefs().getString(SelectCityFragment.PARAM_CITYDATA_NEW, ""), ResponseCity.class);
			String currency = "HKD";
			if (cityData.getPhoneCode().equals("852")) {
				currency = "HKD";
			} else if (cityData.getPhoneCode().equals("853")) {
				currency = "MOP";
			}

			String orderName = companyId + "_" + getUTCCalendar().getTimeInMillis();

			String address = edtAddress.getText().toString().trim();

			JSONArray jArrayMenuData = new JSONArray();

			if (listItems != null) {

				for (int i = 0; i < listItems.size(); i++) {

					JSONArray jArrayFoodOption = new JSONArray();

					for (int j = 0; j < listItems.get(i).getFoodOptions().size(); j++) {

						FoodOptionItemData data = listItems.get(i).getFoodOptions().get(j);

						JSONObject jObjFO = new JSONObject();
						jObjFO.put(PARAMS.TAG_FOODOPTION_ITEM_ID, data.getFoodOptionItemId());
						jObjFO.put(PARAMS.TAG_FOODOPTION_ITEM_NAME, data.getFoodOptionItemName());
						jObjFO.put(PARAMS.TAG_FOODOPTION_ITEM_NAME_EN, data.getFoodOptionItemNameEn());
						jObjFO.put(PARAMS.TAG_IS_FOODOPTION, data.getIsFoodOption());

						jArrayFoodOption.put(jObjFO);
					}

					JSONObject jObj = new JSONObject();
					jObj.put(PARAMS.TAG_MENU_ID, listItems.get(i).getMenuId());
					jObj.put(PARAMS.TAG_MENU_ITEM_ID, listItems.get(i).getMenuItemId());
					jObj.put(PARAMS.TAG_QUANTITY, listItems.get(i).getQuantity());
					jObj.put(PARAMS.TAG_FOODOPTIONS, jArrayFoodOption);

					jArrayMenuData.put(jObj);
				}
			}

			networManager.isProgressVisible(true);

			// if (isDraft) {
			//
			// draftOrderRequest =
			// networManager.addRequest(Join8RequestBuilder.placeOrderRequest(access_token,
			// reg.getPartitionKey(), 90,
			// userProfileList.get(selectedUserProfileIndex).getFirstName(),
			// userProfileList.get(selectedUserProfileIndex).getLastName(),
			// orderId,
			// userProfileList.get(selectedUserProfileIndex).getProfileName(),
			// "/Date(" + selectedDate + ")/", companyId,
			// rbtCollect.isChecked(), false, false, jArrayMenuData,
			// userProfileList.get(selectedUserProfileIndex).getEmail(),
			// userProfileList.get(selectedUserProfileIndex).getMobileNumber(),
			// orderData, edtRemark.getText().toString().trim(), currency, "",
			// rbtPayNow.isChecked() ? 1 : 2, "/Date(" +
			// getUTCCalendar().getTimeInMillis() + ")/", "/Date("
			// + getUTCCalendar().getTimeInMillis() + ")/", "", total +
			// deliveryFee, 0, orderName, address), RequestMethod.POST,
			// getActivity(),
			// Join8RequestBuilder.METHOD_CREATE_UPDATE_DRAFT_ORDER);
			// } else {

			sendOrderRequest = networManager.addRequest(Join8RequestBuilder.placeOrderRequest(access_token, reg.getPartitionKey(), 90, userProfileList.get(selectedUserProfileIndex).getFirstName(), userProfileList.get(selectedUserProfileIndex).getLastName(), orderId,
					userProfileList.get(selectedUserProfileIndex).getProfileName(), "/Date(" + selectedDate + ")/", companyId, rbtCollect.isChecked(), false, false, jArrayMenuData, userProfileList.get(selectedUserProfileIndex).getEmail(),
					userProfileList.get(selectedUserProfileIndex).getMobileNumber(), orderData, edtRemark.getText().toString().trim(), currency, "", rbtPayNow.isChecked() ? 1 : 2, "/Date(" + getUTCCalendar().getTimeInMillis() + ")/", "/Date(" + getUTCCalendar().getTimeInMillis()
							+ ")/", "", total + deliveryFee, 0, orderName, address), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_CREATE_UPDATE_DRAFT_ORDER);
			// }

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSuccess(int id, String response) {

		try {

			if (id == get_Order) {

				if (response.toLowerCase(Locale.getDefault()).startsWith("failure")) {

					Toast.displayText(getActivity(), getString(R.string.norecorderror));

				} else {

					JSONArray jsonArray = new JSONArray(response);
					listItems = new ArrayList<RestaurantMenuItemData>();
					listItemIds = new ArrayList<Integer>();

					for (int i = 0; i < jsonArray.length(); i++) {

						orderData = new Gson().fromJson(jsonArray.getJSONObject(i).toString(), OrderData.class);

						RestaurantMenuItemData data = new RestaurantMenuItemData();
						data.setCompanyId(orderData.getCompanyId());
						data.setQuantity(orderData.getQuantity());
						data.setPrice(orderData.getPrice());
						data.setItemName(orderData.getItemName());
						data.setItemName_EN(orderData.getItemName_EN());
						data.setMenuItemId(orderData.getMenuItemId());
						data.setHasFoodOption(orderData.isHasFoodOption());

						listItems.add(data);
						listItemIds.add(data.getMenuItemId());

						if (orderData.getOrderDeadlineDate() != null) {
							try {
								selectedDate = orderData.getOrderDeadlineDate().getTime();
								Calendar calendar = Calendar.getInstance();
								calendar.setTimeInMillis(selectedDate);
								year = calendar.get(Calendar.YEAR);
								month = calendar.get(Calendar.MONTH);
								day = calendar.get(Calendar.DAY_OF_MONTH);
								hours = calendar.get(Calendar.HOUR_OF_DAY);
								minute = calendar.get(Calendar.MINUTE);
								SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd - hh:mm a", Locale.getDefault());
								String dateTime = format.format(orderData.getOrderDeadlineDate());
								txtOrderDate.setText(dateTime);
							} catch (Exception e) {
							}
						}
						if (i == 0) {

							try {

								String name = "";
								if (!Utils.isEmpty(orderData.getFirstName())) {
									name = orderData.getFirstName();
								}
								if (!Utils.isEmpty(orderData.getLastName())) {
									name += " " + orderData.getLastName();
								}
								txtUserName.setText(name);

								if (!Utils.isEmpty(orderData.getMobileNumber())) {
									txtMobilePhone.setText(orderData.getMobileNumber());
								}

								if (!Utils.isEmpty(orderData.getRemarks())) {
									edtRemark.setText(orderData.getRemarks());
								}

							} catch (Exception e) {
								e.printStackTrace();
							}
						}

						// setTitle(OrderData.getOrderStatusString(getActivity(),
						// orderData.getOrderStatus()));
					}

					changeTotal();

					OrderDetailAdapter mAdapter = new OrderDetailAdapter(getActivity(), listItems, this, false);
					mListView.setAdapter(mAdapter);
				}

			} else if (id == userProfile_request) {

				JSONArray jsonArray = new JSONArray(response);
				if (jsonArray.length() > 0) {
					userProfileList = new ArrayList<Registration>();
					strArrayUserProfile = new String[jsonArray.length()];
					for (int i = 0; i < jsonArray.length(); i++) {
						Registration registration = new Gson().fromJson(jsonArray.getJSONObject(i).toString(), Registration.class);
						userProfileList.add(registration);

						if (registration.getProfileName().contains("-")) {
							strArrayUserProfile[i] = registration.getProfileName().substring(registration.getProfileName().lastIndexOf("-") + 1);
						} else {
							strArrayUserProfile[i] = registration.getProfileName();
						}

						// strArrayUserProfile[i] =
						// registration.getProfileName().substring(registration.getEmail().length()
						// + 1).replace("-", "");

						if (orderData == null) {
							if (registration.isDefaultProfile()) {
								selectedUserProfileIndex = i;
								setUserData(registration);
							}
						} else {
							if (registration.getProfileName().equalsIgnoreCase(orderData.getUserProfile())) {
								selectedUserProfileIndex = i;
								setUserData(registration);
							}
						}
					}

					if (selectedUserProfileIndex == -1) {
						for (int i = 0; i < userProfileList.size(); i++) {
							Registration registration = userProfileList.get(i);
							userProfileList.add(registration);
							strArrayUserProfile[i] = registration.getProfileName().substring(registration.getEmail().length() + 1).replace("-", "");
							if (registration.isDefaultProfile()) {
								selectedUserProfileIndex = i;
								setUserData(registration);
								break;
							}
						}
					}
				}

				// } else if (id == draftOrderRequest) {
				//
				// if
				// (response.toLowerCase(Locale.getDefault()).startsWith("failure"))
				// {
				// Toast.displayText(getActivity(),
				// getString(R.string.placing_order_error));
				// } else {
				//
				// JSONObject jObj = new JSONObject(response);
				// orderId = jObj.getString("LastInsertedValue");
				//
				// placeOrder();
				// }

			} else if (id == sendOrderRequest) {

				if (response.toLowerCase(Locale.getDefault()).startsWith("failure")) {
					Toast.displayText(getActivity(), getString(R.string.placing_order_error));
				} else {

					JSONObject jObj = new JSONObject(response);
					orderId = jObj.getString("LastInsertedValue");

					if (rbtPayNow.isChecked()) {

						String restaurantName = "";
						if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
							restaurantName = restaurantData.getCompanyNameEN();
						} else {
							restaurantName = restaurantData.getCompanyName();
						}

						String refId = orderId + "_Ord_" + Calendar.getInstance().get(Calendar.YEAR);

						Intent intent = new Intent(getActivity(), PaymentActivity.class);
						intent.putExtra("amount", total + deliveryFee);
						intent.putExtra("refId", refId);
						intent.putExtra("companyId", String.valueOf(companyId));
						intent.putExtra("companyName", restaurantName);
						intent.putExtra("title", ((TextView) getActivity().getActionBar().getCustomView().findViewById(R.id.txtTitle)).getText());
						startActivityForResult(intent, INTENT_PAYMENT);

					} else {

						OrderDetailFragment.clearOrderList();
						OrderDetailFragment.companyId = -1;

						getActivity().setResult(Activity.RESULT_OK);
						getActivity().finish();
					}
				}
			}

		} catch (Exception e) {
			Log.e("Error", e.toString());
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	public static void clearOrderList() {
		if (listItems != null) {
			listItems.clear();
			listItems = null;
		}
		if (listItemIds != null) {
			listItemIds.clear();
			listItemIds = null;
		}
	}

	@Override
	public void onDestroy() {
		if (!Utils.isEmpty(orderId) && !orderId.equals("0") && listItems != null) {
			clearOrderList();
		}

		super.onDestroy();
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {

		if (checkedId == R.id.rbtCollect) {

			txtRestAddress.setVisibility(View.VISIBLE);
			txtChangeUserProfile.setVisibility(View.GONE);
			txtUserName.setVisibility(View.GONE);
			txtMobilePhone.setVisibility(View.GONE);

		} else {

			txtRestAddress.setVisibility(View.GONE);
			txtChangeUserProfile.setVisibility(View.VISIBLE);
			txtUserName.setVisibility(View.VISIBLE);
			txtMobilePhone.setVisibility(View.VISIBLE);
		}

		changeTotal();
	}

	@Override
	public void onClick(View v) {

		if (v == txtChangeUserProfile) {
			showUserProfilePopup();
		} else if (v == txtOrderDate) {
			showDateTimeDialog(txtOrderDate);
		} else if (v == txtMobilePhone) {
			if (!Utils.isEmpty(userProfileList.get(selectedUserProfileIndex).getPhoneNumber())) {
				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel:" + PhoneNumberUtils.stringFromStringAndTOA(PhoneNumberUtils.stripSeparators(userProfileList.get(selectedUserProfileIndex).getPhoneNumber()), PhoneNumberUtils.TOA_International)));
				startActivity(intent);
			}
		} else if (v == txtPlaceOrder) {

			if (listItems != null && listItems.size() > 0) {

				if (rbtDeliver.isChecked() && restaurantData.getDeliveryOption() == 0) {
					Toast.displayText(getActivity(), getString(R.string.no_delivery_option));
				} else if (rbtDeliver.isChecked() && selectedUserProfileIndex < 0) {
					Toast.displayText(getActivity(), getString(R.string.select_user_profile));
				} else if (txtOrderDate.getText().toString().trim().equals(getString(R.string.select_date))) {
					Toast.displayText(getActivity(), getString(R.string.datetimerequirederror));
					showDateTimeDialog(txtOrderDate);
				} else if (rbtDeliver.isChecked() && edtAddress.getText().toString().trim().equals("")) {
					Toast.displayText(getActivity(), getString(R.string.address_required));
				} else {
					placeOrder();
				}
			} else {
				Toast.displayText(getActivity(), getString(R.string.no_iteams_error));
			}
		}
	}
}
