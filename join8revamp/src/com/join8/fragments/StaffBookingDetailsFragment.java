package com.join8.fragments;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.fragments.StaffFragmentContainer.SendBack;
import com.join8.listeners.Requestlistener;
import com.join8.model.BookingData;
import com.join8.model.OrderData;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.print.BluetoothService;
import com.join8.print.DeviceListActivity;
import com.join8.print.PrintUtils;
import com.join8.print.SocketManager;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class StaffBookingDetailsFragment extends Fragment implements Requestlistener, OnClickListener {

	private static final String TAG = StaffBookingDetailsFragment.class.getSimpleName();

	private TextView txtName = null, txtDateToBook = null, txtSeats = null, txtMobilePhone = null, txtRemarks = null, txtTableType = null, txtDeposit = null, txtAccept = null, txtReject = null, txtPrint = null;
	private ImageView imvUser = null;

	private CryptoManager prefsManager = null;
	private NetworkManager networManager = null;
	private int bookingDetailsRequestId = -1, acceptRejectRequestId = -1;
	private String access_token = "";
	private BookingData bookingData = null;

	private String tableBookingId = "";
	private int status = 0;
	private ImageLoader imageLoader = null;
	private DisplayImageOptions options = null;
	private SendBack sendBack;

	private BluetoothAdapter mBluetoothAdapter;

	private BluetoothService mService;
	private SocketManager mSocketManager;
	private SocketManager mSocketManager1;

	private static final int REQUEST_CONNECT_DEVICE = 123;
	private static final int REQUEST_ENABLE_BT = 124;

	private static final String DEVICE_ADDRESS = "device_address";

	// Message types sent from the BluetoothService Handler
	private static final int MESSAGE_STATE_CHANGE = 1;
	private static final int MESSAGE_READ = 2;
	private static final int MESSAGE_WRITE = 3;
	private static final int MESSAGE_DEVICE_NAME = 4;
	private static final int MESSAGE_TOAST = 5;

	public void setSendBack(SendBack sendBack) {
		this.sendBack = sendBack;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		prefsManager = CryptoManager.getInstance(getActivity());
		networManager = NetworkManager.getInstance();
		networManager.addListener(this);
		networManager.isProgressVisible(true);

		setActionBarTitle("");

		mSocketManager = new SocketManager(getActivity());
		mSocketManager.setHandler(mHandlerWifi);

		mSocketManager1 = new SocketManager(getActivity());
		mSocketManager1.setHandler(mHandlerWifi1);

		tableBookingId = getArguments().getString("tableBookingId");
		imageLoader = ImageLoader.getInstance();
		DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();
		int roundSize = (int) (dm.density * getResources().getDimension(R.dimen.image_height));
		options = new DisplayImageOptions.Builder().displayer(new RoundedBitmapDisplayer(roundSize)).showStubImage(R.drawable.default_user).showImageForEmptyUri(R.drawable.default_user).showImageOnFail(R.drawable.default_user).cacheInMemory(true).cacheOnDisc(true).build();
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);

		if (mSocketManager != null) {
			mSocketManager.close();
			mSocketManager = null;
		}

		if (mSocketManager1 != null) {
			mSocketManager1.close();
			mSocketManager1 = null;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.staff_booking_details_fragment, container, false);

		imvUser = (ImageView) v.findViewById(R.id.imvUser);
		txtName = (TextView) v.findViewById(R.id.txtName);
		txtDateToBook = (TextView) v.findViewById(R.id.txtDateToBook);
		txtSeats = (TextView) v.findViewById(R.id.txtSeats);
		// txtDressCode = (TextView) v.findViewById(R.id.txtDressCode);
		txtMobilePhone = (TextView) v.findViewById(R.id.txtMobilePhone);
		txtRemarks = (TextView) v.findViewById(R.id.txtRemarks);
		txtTableType = (TextView) v.findViewById(R.id.txtTableType);
		txtDeposit = (TextView) v.findViewById(R.id.txtDeposit);
		txtAccept = (TextView) v.findViewById(R.id.txtAccept);
		txtReject = (TextView) v.findViewById(R.id.txtReject);
		txtPrint = (TextView) v.findViewById(R.id.txtPrint);

		txtAccept.setOnClickListener(this);
		txtReject.setOnClickListener(this);
		txtPrint.setOnClickListener(this);
		txtMobilePhone.setOnClickListener(this);

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(txtDateToBook);
		mTypefaceUtils.applyTypeface(txtSeats);
		// mTypefaceUtils.applyTypeface(txtDressCode);
		mTypefaceUtils.applyTypeface(txtMobilePhone);
		mTypefaceUtils.applyTypeface(txtRemarks);
		mTypefaceUtils.applyTypeface(txtAccept);
		mTypefaceUtils.applyTypeface(txtReject);
		mTypefaceUtils.applyTypeface(txtPrint);

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		access_token = prefsManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");

		loadBookingDetailsData();
		// getDressCodeName();
	}

	private void loadBookingDetailsData() {

		networManager.isProgressVisible(true);
		bookingDetailsRequestId = networManager.addRequest(Join8RequestBuilder.getBookingDetails(access_token, tableBookingId), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_STAFF_BOOKING_DETAILS);
	}

	private void acceptRejectBooking(boolean isAccept) {

		networManager.isProgressVisible(true);
		if (isAccept) {
			acceptRejectRequestId = networManager.addRequest(Join8RequestBuilder.acceptRejectBooking(access_token, status, tableBookingId, 20), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_ACCEPT_REJECT_BOOKING);
		} else {
			acceptRejectRequestId = networManager.addRequest(Join8RequestBuilder.acceptRejectBooking(access_token, status, tableBookingId, 50), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_ACCEPT_REJECT_BOOKING);
		}
	}

	// private void getDressCodeName() {
	// networManager.isProgressVisible(false);
	// getDressCodeRequestId =
	// networManager.addRequest(Join8RequestBuilder.getCityList(access_token),
	// RequestMethod.POST, getActivity(),
	// Join8RequestBuilder.METHOD_GET_ALL_DRESSCODES);
	// }

	@Override
	public void onClick(View v) {

		if (v == txtAccept) {

			acceptRejectBooking(true);

		} else if (v == txtReject) {

			acceptRejectBooking(false);

		} else if (v == txtMobilePhone) {

			try {

				if (TextUtils.isEmpty(txtMobilePhone.getText().toString().trim())) {

					Toast.displayText(getActivity(), getActivity().getString(R.string.no_phono_number));

				} else {

					Intent intent = new Intent(Intent.ACTION_CALL);
					intent.setData(Uri.parse("tel:" + PhoneNumberUtils.stringFromStringAndTOA(PhoneNumberUtils.stripSeparators(txtMobilePhone.getText().toString().trim()), PhoneNumberUtils.TOA_International)));
					startActivity(intent);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (v == txtPrint) {

			try {

				if (bookingData != null) {

					if (!print()) {
						if (sendBack != null) {
							sendBack.onBack();
						}
						((StaffFragmentContainer) getParentFragment()).popBackStack();

					}
					return;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void setActionBarTitle(String bookingStatus) {

		if (!bookingStatus.equals("")) {
			bookingStatus = "-" + bookingStatus;
		}

		((StaffFragmentContainer) getParentFragment()).setActionBarTitle(getString(R.string.booking_details) + bookingStatus);
	}

	@Override
	public void onSuccess(int id, String response) {

		try {

			Log.e(getTag(), "Response" + response);

			if (response != null && !response.equals("")) {

				if (id == bookingDetailsRequestId) {

					try {

						bookingData = new Gson().fromJson(response, BookingData.class);

						String name = "";
						if (!Utils.isEmpty(bookingData.getFirstName())) {
							name = bookingData.getFirstName();
						}
						if (!Utils.isEmpty(bookingData.getLastName())) {
							name += " " + bookingData.getLastName();
						}
						txtName.setText(name);

						if (bookingData.getDateToBook() != null) {
							try {
								SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd - hh:mm a", Locale.getDefault());
								txtDateToBook.setText(format.format(bookingData.getDateToBook()));
							} catch (Exception e) {
							}
						}

						txtSeats.setText("" + bookingData.getSeats());
						if (!Utils.isEmpty(bookingData.getUserProfile())) {
							imageLoader.displayImage(bookingData.getPhoto(), imvUser, options);
						}

						if (!Utils.isEmpty(bookingData.getMobileNumber())) {
							txtMobilePhone.setText(bookingData.getMobileNumber());
						}

						if (!Utils.isEmpty(bookingData.getRemarks())) {
							txtRemarks.setText(bookingData.getRemarks());
						}

						if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {

							if (!Utils.isEmpty(bookingData.getBookingNameEn())) {
								txtTableType.setText(bookingData.getBookingNameEn());
							}

						} else {

							if (!Utils.isEmpty(bookingData.getBookingName())) {
								txtTableType.setText(bookingData.getBookingName());
							}
						}

						txtDeposit.setText("$" + Utils.formatDecimal("" + bookingData.getDeposit()));

						status = bookingData.getBookingStatus();

						if (status != 10) {

							txtAccept.setVisibility(View.GONE);
							txtReject.setVisibility(View.GONE);
						}

						setActionBarTitle(OrderData.getOrderStatusString(getActivity(), status));

					} catch (Exception e) {
						e.printStackTrace();
					}

				} else if (id == acceptRejectRequestId) {

					if (response.toLowerCase().startsWith("confirmed")) {
						Toast.displayText(getActivity(), getString(R.string.confirmed));
						((StaffFragmentContainer) getParentFragment()).popBackStack();
					} else if (response.toLowerCase().startsWith("rejected")) {
						Toast.displayText(getActivity(), getString(R.string.confirmed));
						((StaffFragmentContainer) getParentFragment()).popBackStack();
					}
					sendBack.onBack();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Error in web service call", e.toString());
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	private boolean print() {

		if (bookingData == null) {
			Log.d(TAG, "No data to print");
			return false;
		}

		if (!isAdded()) {
			return false;
		}

		SharedPreferences mPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);

		if (mPreferences.getInt("printerTypePosition", 0) == 1) {

			String ip = mPreferences.getString("printerIP", "");
			String ip1 = mPreferences.getString("printerIP1", "");
			if (TextUtils.isEmpty(ip) && TextUtils.isEmpty(ip1)) {
				displayIPDialog();
			} else {

				if (mSocketManager != null && mSocketManager.isConnected()) {
					printBooking(mSocketManager);
				} else {
					if (!TextUtils.isEmpty(ip)) {
						connectWifiPrinter(ip);
					}
				}

				if (mSocketManager1 != null && mSocketManager1.isConnected()) {
					printBooking(mSocketManager1);
				} else {
					if (!TextUtils.isEmpty(ip1)) {
						connectWifiPrinter1(ip1);
					}
				}
			}

		} else {

			return connectBluetoothPrinter();

		}
		return true;
	}

	private boolean connectBluetoothPrinter() {

		if (mBluetoothAdapter == null) {
			mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			if (mBluetoothAdapter == null) {
				Toast.displayText(getActivity(), getString(R.string.bluetooth_not_available));
				return false;
			}
		}

		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			if (getParentFragment() != null) {
				getParentFragment().startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			} else {
				startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			}

		} else {
			if (mService == null) {
				initBluetooth();
				connectPreviouslyPairedDevice();
			} else if (mService.getState() == BluetoothService.STATE_CONNECTED) {
				printBooking(null);
			}
		}

		return true;
	}

	private void initBluetooth() {
		// Initialize the BluetoothService to perform bluetooth connections
		if (mService == null) {
			mService = BluetoothService.getInstance(getActivity());
			mService.setHandler(mHandler);
		}

		if (mService.getState() == BluetoothService.STATE_NONE) {
			mService.start();
		} else if (mService.getState() == BluetoothService.STATE_CONNECTED) {
			printBooking(null);
		}
	}

	private void connectPreviouslyPairedDevice() {
		SharedPreferences mPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
		if (mPreferences.contains("btAddress")) {
			String deviceId = mPreferences.getString("btAddress", "");
			if (!TextUtils.isEmpty(deviceId)) {
				Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
				if (pairedDevices != null && pairedDevices.size() > 0) {
					for (BluetoothDevice device : pairedDevices) {
						if (device.getAddress().equals(deviceId)) {
							mService.connect(device);
							return;
						}
					}
				}
			}
		}
	}

	private void displayIPDialog() {

		try {

			final SharedPreferences mPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);

			View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_ip_address, null);
			final EditText edtIP = (EditText) view.findViewById(R.id.edtIP);
			edtIP.setText(mPreferences.getString("printerIP", ""));
			final EditText edtIP1 = (EditText) view.findViewById(R.id.edtIP1);
			edtIP1.setText(mPreferences.getString("printerIP1", ""));

			AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
			mBuilder.setView(view);
			mBuilder.setTitle(getResources().getStringArray(R.array.printer_type)[1]);
			mBuilder.setPositiveButton(R.string.tvOk, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

					String ip = edtIP.getText().toString().trim();
					mPreferences.edit().putString("printerIP", ip).commit();
					String ip1 = edtIP1.getText().toString().trim();
					mPreferences.edit().putString("printerIP1", ip1).commit();
				}
			});
			mBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			});
			mBuilder.create().show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void connectWifiPrinter(String printerIp) {
		mSocketManager.mPrinterIP = printerIp;
		mSocketManager.threadconnect();
	}

	private void connectWifiPrinter1(String printerIp) {
		mSocketManager1.mPrinterIP = printerIp;
		mSocketManager1.threadconnect();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d(TAG, "onActivityResult");

		if (requestCode == REQUEST_ENABLE_BT) {
			if (resultCode == Activity.RESULT_OK) {
				initBluetooth();
			}
		} else if (requestCode == REQUEST_CONNECT_DEVICE) {
			if (resultCode == Activity.RESULT_OK) {

				if (data != null && data.getExtras() != null) {
					if (data.getExtras().containsKey(DeviceListActivity.EXTRA_DEVICE_ADDRESS)) {
						String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
						if (BluetoothAdapter.checkBluetoothAddress(address)) {
							BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
							mService.connect(device);
						}
					}
				}

			} else {
				// if (mService != null) {
				// mService.stop();
				// mService = null;
				// }

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if (sendBack != null) {
							sendBack.onBack();
						}
						((StaffFragmentContainer) getParentFragment()).popBackStack();
					}
				}, 1000);
			}
		}
	}

	private void printBooking(SocketManager mSocketManager) {

		try {

			if (bookingData != null) {
				new PrintUtils(getActivity(), mService, mSocketManager).printBooking(bookingData);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "Json Exception");
		}
	}

	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (!isAdded())
				return;
			switch (msg.what) {
				case MESSAGE_STATE_CHANGE :
					switch (msg.arg1) {
						case BluetoothService.STATE_CONNECTED :
							// mTitle.setText(R.string.title_connected_to);
							// mTitle.append(mConnectedDeviceName);
							break;
						case BluetoothService.STATE_CONNECTING :
							// mTitle.setText(R.string.title_connecting);
							break;
						case BluetoothService.STATE_LISTEN :
						case BluetoothService.STATE_NONE :
							// Toast.displayText(getActivity(),
							// getString(R.string.title_not_connected));
							break;
					}
					break;
				case MESSAGE_WRITE :
					break;
				case MESSAGE_READ :
					break;
				case MESSAGE_DEVICE_NAME :
					SharedPreferences mPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
					mPreferences.edit().putString("btAddress", msg.getData().getString(DEVICE_ADDRESS)).commit();
					printBooking(null);
					break;
				case MESSAGE_TOAST :

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							if (sendBack != null) {
								sendBack.onBack();
							}
							((StaffFragmentContainer) getParentFragment()).popBackStack();
						}
					}, 1000);
					// startBluetoothDeviceDiscovery();
					break;
			}
		}
	};

	private final Handler mHandlerWifi = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			if (msg.what == SocketManager.WIFI_PRINTER_CONNECTION_STATE) {

				switch (msg.arg1) {

					case SocketManager.STATE_CONNECTED :
						printBooking(mSocketManager);
						break;

					case SocketManager.STATE_ERROR :
						if (isAdded()) {
							Toast.displayText(getActivity(), getString(R.string.wifi_printer_conn_error) + " : Printer - 1");
						}
						break;
				}
			}
		}
	};

	private final Handler mHandlerWifi1 = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			if (msg.what == SocketManager.WIFI_PRINTER_CONNECTION_STATE) {

				switch (msg.arg1) {

					case SocketManager.STATE_CONNECTED :
						printBooking(mSocketManager1);
						break;

					case SocketManager.STATE_ERROR :
						if (isAdded()) {
							Toast.displayText(getActivity(), getString(R.string.wifi_printer_conn_error) + " : Printer - 2");
						}
						break;
				}
			}
		}
	};
}
