package com.join8.fragments;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.fragments.ProfileFragment.OnProfileDeleteListner;
import com.join8.listeners.Requestlistener;
import com.join8.model.Registration;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.Log;
import com.join8.utils.TypefaceUtils;

public class UserProfileFragment extends Fragment implements OnClickListener, OnProfileDeleteListner, Requestlistener {

	private static final String TAG = UserProfileFragment.class.getSimpleName();

	private ViewPager viewPager = null;
	private MyFragmentPagerAdapter mMyFragmentPagerAdapter;

	private int profileRequestId = -1, cityListRequestId = -1;

	public static ImageButton btnAddProfile = null;

	private NetworkManager networManager = null;
	private CryptoManager prefManager = null;
	private String access_token = "";

	public static JSONArray jArrayUserProfile = new JSONArray();
	public static boolean isNewProfile = false;
	public static HashMap<Integer, String> mapCityList = new HashMap<Integer, String>();

	private int visibleFragmentIndex = 0, prevVisibleFragmentIndex = 0;
	private boolean isTouch = false;
	private TextView tvresults;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		networManager = NetworkManager.getInstance();
		networManager.isProgressVisible(true);
		prefManager = CryptoManager.getInstance(getActivity());

	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		((HomeScreen) getActivity()).initActionBar();
		((HomeScreen) getActivity()).changeActionItems(true);
		((HomeScreen) getActivity()).hideNewActionBar();
		View mView = inflater.inflate(R.layout.profile_fragment, container, false);
		mappingWidgets(mView);
		addListeners();
		init();
		return mView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");

		loadCityData();
		loadProfileData();
	}

	private void mappingWidgets(View mView) {

		viewPager = (ViewPager) mView.findViewById(R.id.viewPager);
		tvresults = (TextView) mView.findViewById(R.id.tvresults);
		tvresults.setVisibility(View.GONE);

		btnAddProfile = ((HomeScreen) getActivity()).getAddMenu();
		btnAddProfile.setVisibility(View.GONE);
		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());

		mTypefaceUtils.applyTypeface(tvresults);

	}

	private void addListeners() {

		btnAddProfile.setOnClickListener(this);

		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {

				prevVisibleFragmentIndex = visibleFragmentIndex;
				visibleFragmentIndex = arg0;

				Log.e("pager index", "current = " + visibleFragmentIndex + " | old = " + prevVisibleFragmentIndex + " | cur = " + viewPager.getCurrentItem());

				if (isTouch) {

					final ProfileFragment fragment = getVisibleFragment(prevVisibleFragmentIndex);

					isTouch = false;

					if (fragment.isEdited) {

						// Toast.makeText(getActivity(), "index=" +
						// prevVisibleFragmentIndex + " | " +
						// viewPager.getCurrentItem());

						// new Handler().postDelayed(new Runnable() {
						// @Override
						// public void run() {

						viewPager.setCurrentItem(prevVisibleFragmentIndex);
						viewPager.refreshDrawableState();
						viewPager.invalidate();
						// }
						// }, 100);

						AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
						dialog.setMessage(getString(R.string.save_profile));
						dialog.setPositiveButton(getString(R.string.yes), new android.content.DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								fragment.pageChanged(true);
							}
						});
						dialog.setNegativeButton(getString(R.string.no), new android.content.DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {

								fragment.pageChanged(false);
								fragment.isEdited = false;

								if (isNewProfile) {
									isNewProfile = false;

									jArrayUserProfile = removeObjFromJArray(jArrayUserProfile, jArrayUserProfile.length() - 1);

									mMyFragmentPagerAdapter = new MyFragmentPagerAdapter(getActivity().getSupportFragmentManager(), jArrayUserProfile.length(), UserProfileFragment.this);
									viewPager.setAdapter(mMyFragmentPagerAdapter);
									viewPager.setCurrentItem(visibleFragmentIndex, true);

									((HomeScreen) getActivity()).setCustomTitle(getString(R.string.editprofile), TypefaceUtils.NORMAL);
								}
							}
						});
						dialog.show();

					}
				}
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});

		viewPager.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				if (event.getAction() == MotionEvent.ACTION_UP) {

					isTouch = true;
				}
				return false;
			}
		});

		// viewPager.setOnTouchListener(new OnTouchListener() {
		// @Override
		// public boolean onTouch(View v, MotionEvent event) {
		//
		// if (event.getAction() == MotionEvent.ACTION_UP) {
		//
		// // new Handler().postDelayed(new Runnable() {
		// //
		// // @Override
		// // public void run() {
		//
		// visibleFragmentIndex = viewPager.getCurrentItem();
		//
		// final ProfileFragment fragment =
		// getVisibleFragment(visibleFragmentIndex);
		//
		// Log.e("pager index", "prev = " + prevVisibleFragmentIndex +
		// " | next = " + visibleFragmentIndex);
		//
		// if (fragment.isEdited || isNewProfile) {
		//
		// Toast.makeText(getActivity(), "index=" + prevVisibleFragmentIndex +
		// " | " + viewPager.getCurrentItem());
		//
		// new Handler().postDelayed(new Runnable() {
		// @Override
		// public void run() {
		//
		// viewPager.setCurrentItem(prevVisibleFragmentIndex);
		// viewPager.refreshDrawableState();
		// viewPager.invalidate();
		// }
		// }, 100);
		//
		// AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
		// dialog.setMessage(getString(R.string.save_profile));
		// dialog.setPositiveButton(getString(R.string.yes), new
		// android.content.DialogInterface.OnClickListener() {
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// fragment.pageChanged();
		// }
		// });
		// dialog.setNegativeButton(getString(R.string.no), new
		// android.content.DialogInterface.OnClickListener() {
		// @Override
		// public void onClick(DialogInterface dialog, int which) {
		// fragment.isEdited = false;
		// }
		// });
		// dialog.show();
		//
		// return true;
		// }
		//
		// // }
		// // }, 100);
		// }
		// return false;
		// }
		// });
	}

	private void init() {

		isNewProfile = false;

		viewPager.setOffscreenPageLimit(1);

		((HomeScreen) getActivity()).setCustomTitle(getString(R.string.editprofile), TypefaceUtils.NORMAL);

		btnAddProfile.setEnabled(true);

		mMyFragmentPagerAdapter = new MyFragmentPagerAdapter(getActivity().getSupportFragmentManager(), jArrayUserProfile.length(), UserProfileFragment.this);
	}

	private void loadCityData() {

		networManager.isProgressVisible(true);
		cityListRequestId = networManager.addRequest(Join8RequestBuilder.getCityList(access_token), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_GETCITY);
	}

	private void loadProfileData() {

		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);

		networManager.isProgressVisible(true);
		profileRequestId = networManager.addRequest(Join8RequestBuilder.getProfileOfUser(access_token, registration.getPartitionKey()), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_GET_PROFILES_OF_USER);
	}

	@Override
	public void onClick(View v) {

		if (v == btnAddProfile) {

			btnAddProfile.setEnabled(false);
			isNewProfile = true;
			jArrayUserProfile.put(new JSONObject());
			mMyFragmentPagerAdapter = new MyFragmentPagerAdapter(getActivity().getSupportFragmentManager(), jArrayUserProfile.length(), UserProfileFragment.this);
			viewPager.setAdapter(mMyFragmentPagerAdapter);
			viewPager.setCurrentItem(jArrayUserProfile.length() - 1, true);

			((HomeScreen) getActivity()).setCustomTitle(getString(R.string.add_new_profile), TypefaceUtils.NORMAL);
		}
	}

	@Override
	public void onSuccess(int id, String response) {

		try {

			Log.i(getTag(), "Result => " + response);

			if (response != null && !response.equals("")) {

				if (id == profileRequestId) {

					if (response.toLowerCase().startsWith("no record found")) {

						// Toast.makeText(getActivity(),
						// getString(R.string.norecorderror));
						tvresults.setVisibility(View.VISIBLE);
						viewPager.setVisibility(View.GONE);

					} else {
						tvresults.setVisibility(View.GONE);
						viewPager.setVisibility(View.VISIBLE);
						jArrayUserProfile = new JSONArray(response);

						mMyFragmentPagerAdapter = new MyFragmentPagerAdapter(getActivity().getSupportFragmentManager(), jArrayUserProfile.length(), UserProfileFragment.this);
						viewPager.setAdapter(mMyFragmentPagerAdapter);

						// if (jsonArray.length() > 0) {
						//
						// userProfileList = new ArrayList<Registration>();
						//
						// for (int i = 0; i < jsonArray.length(); i++) {
						//
						// Registration registration = new
						// Gson().fromJson(jsonArray.getJSONObject(i).toString(),
						// Registration.class);
						// userProfileList.add(registration);
						// }
						//
						// mMyFragmentPagerAdapter = new
						// MyFragmentPagerAdapter(getActivity().getSupportFragmentManager(),
						// userProfileList, UserProfileFragment.this);
						// viewPager.setAdapter(mMyFragmentPagerAdapter);
						// }
					}
				} else if (id == cityListRequestId) {

					JSONArray jArray = new JSONArray(response);
					mapCityList = new HashMap<Integer, String>();

					for (int i = 0; i < jArray.length(); i++) {

						JSONObject jObj = jArray.getJSONObject(i);

						if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
							mapCityList.put(jObj.getInt("CountryCode"), jObj.getString("CityNameEN"));
						} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_TRADITIONALCHINESE)) {
							mapCityList.put(jObj.getInt("CountryCode"), jObj.getString("CityNameTC"));
						} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_SIMPLECHINSESE)) {
							mapCityList.put(jObj.getInt("CountryCode"), jObj.getString("CityNameSC"));
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	private static class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {

		private int size = 0;
		private OnProfileDeleteListner onProfileDeleteListner = null;

		public MyFragmentPagerAdapter(FragmentManager fm, int size, OnProfileDeleteListner onProfileDeleteListner) {
			super(fm);
			this.size = size;
			this.onProfileDeleteListner = onProfileDeleteListner;

			if (isNewProfile) {
				btnAddProfile.setEnabled(false);
			} else {
				btnAddProfile.setEnabled(true);
			}
		}

		@Override
		public Fragment getItem(int index) {

			Registration registration = null;

			try {
				registration = new Gson().fromJson(jArrayUserProfile.getJSONObject(index).toString(), Registration.class);
			} catch (Exception e) {
			}

			ProfileFragment fragment = new ProfileFragment();
			Bundle args = new Bundle();
			args.putInt("index", index);
			args.putInt("size", size);
			args.putSerializable("data", registration);
			fragment.setArguments(args);
			fragment.setOnProfileDeleteListner(onProfileDeleteListner);

			return fragment;
		}

		@Override
		public int getCount() {

			return jArrayUserProfile.length();
		}
	}

	private ProfileFragment getVisibleFragment(int index) {
		return (ProfileFragment) mMyFragmentPagerAdapter.instantiateItem(viewPager, index);
	}

	private JSONArray removeObjFromJArray(JSONArray jArray, int index) {

		try {

			ArrayList<JSONObject> list = new ArrayList<JSONObject>();
			for (int i = 0; i < jArray.length(); i++) {
				list.add(jArray.getJSONObject(i));
			}

			list.remove(index);

			jArray = new JSONArray();

			for (int j = 0; j < list.size(); j++) {
				jArray.put(list.get(j));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return jArray;
	}

	@Override
	public void onProfileDeleted(final int index) {

		try {

			visibleFragmentIndex = index;

			jArrayUserProfile = removeObjFromJArray(jArrayUserProfile, index);

			if (index > 0) {
				visibleFragmentIndex--;
			}

			mMyFragmentPagerAdapter = new MyFragmentPagerAdapter(getActivity().getSupportFragmentManager(), jArrayUserProfile.length(), UserProfileFragment.this);
			viewPager.setAdapter(mMyFragmentPagerAdapter);
			viewPager.setCurrentItem(visibleFragmentIndex, true);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSwipeLeft(int index) {

		visibleFragmentIndex = index;
		viewPager.setCurrentItem(visibleFragmentIndex, true);

	}

	@Override
	public void onSwipeRight(int index) {

		visibleFragmentIndex = index;
		viewPager.setCurrentItem(visibleFragmentIndex, true);
	}
}
