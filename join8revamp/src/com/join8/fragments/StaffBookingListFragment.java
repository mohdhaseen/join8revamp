package com.join8.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.TextView;

import com.costum.android.widget.LoadMoreListView;
import com.costum.android.widget.LoadMoreListView.OnLoadMoreListener;
import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.adpaters.StaffBookingListAdapter;
import com.join8.adpaters.StaffHorizontalGalleryAdapter;
import com.join8.adpaters.StaffOrderListAdapter;
import com.join8.fragments.StaffFragmentContainer.SendBack;
import com.join8.listeners.Requestlistener;
import com.join8.model.BookingData;
import com.join8.model.OrderData;
import com.join8.model.Registration;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.quickaction.ActionItem;
import com.join8.quickaction.QuickAction;
import com.join8.quickaction.QuickAction.OnActionItemClickListener;
import com.join8.quickaction.QuickAction.OnDismissListener;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.KeyboardUtils;
import com.join8.utils.Log;
import com.join8.utils.TypefaceUtils;

public class StaffBookingListFragment extends Fragment implements OnClickListener, Requestlistener, SendBack {

	private static final String TAG = BookingListFragment.class.getSimpleName();
	private static final int COUNT_PER_PAGE = 25;

	private int bookingListRequestId = -1, bookingListMoredataRequestId = -1;

	private Gallery mGallery = null;
	private LoadMoreListView listView = null;
	private TextView txtBookingType = null, txtPageHeader = null;
	private EditText edtSearch = null;

	private NetworkManager networManager = null;
	private CryptoManager prefManager = null;
	private String access_token = "";

	private ArrayList<BookingData> arrBookingList = null;
	private ArrayList<BookingData> arrBackupList = null;
	private StaffBookingListAdapter adapterBookingList = null;
	private int starIndex = 0;
	private int totalRecords = 0;
	private int prePosition = -1;

	private QuickAction mQuickAction = null;
	private String[] bookingTypes = null, bookingTypesValue = null;
	private TextView tvresults;
	private boolean isLoadMoreEnable = true;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments() != null) {

			if (getArguments().getString("redirectTo").equalsIgnoreCase("StaffBookingDetail")) {

				String tableBookingId = getArguments().getString("reference");

				Bundle bundle = new Bundle();
				bundle.putString("tableBookingId", tableBookingId);

				StaffBookingDetailsFragment fragment = new StaffBookingDetailsFragment();
				fragment.setArguments(bundle);
				fragment.setSendBack(StaffBookingListFragment.this);

				((StaffFragmentContainer) getParentFragment()).addFragment(fragment, true);
			}
		}

		networManager = NetworkManager.getInstance();
		networManager.addListener(this);
		networManager.isProgressVisible(true);
		prefManager = CryptoManager.getInstance(getActivity());
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);

		IntentFilter filter = new IntentFilter();
		filter.addAction(ConstantData.BROADCAST_REFRESH_LIST);
		getActivity().registerReceiver(listRefreshReceiver, filter);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.staff_order_list_fragment, container, false);
		mappingWidgets(mView);
		addListeners();
		init();
		return mView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");

		loadBookingList();
	}

	private void mappingWidgets(View mView) {

		txtPageHeader = (TextView) mView.findViewById(R.id.tvOrderListTitle);
		txtPageHeader.setText(getString(R.string.booking_list));
		mGallery = (Gallery) mView.findViewById(R.id.gallery);
		listView = (LoadMoreListView) mView.findViewById(android.R.id.list);
		listView.setBackgroundResource(R.drawable.list_bg_blue);
		listView.setDivider(getResources().getDrawable(R.drawable.divider_blue_list));
		listView.setVisibility(View.GONE);

		tvresults = (TextView) mView.findViewById(R.id.tvresults);
		edtSearch = (EditText) mView.findViewById(R.id.edtSearch);

		listView.setEmptyView(tvresults);

		txtBookingType = (TextView) mView.findViewById(R.id.txtOrderType);
		txtBookingType.setBackgroundResource(R.drawable.blue_dropdown_bg);
		txtBookingType.setTextColor(getResources().getColor(R.color.blue_dropdown_border_color));
		txtBookingType.setSelected(false);
		txtBookingType.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.arrow_blue_down), null);

		Button btnSearch = (Button) mView.findViewById(R.id.btnSearch);
		btnSearch.setOnClickListener(this);

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(txtBookingType);
		mTypefaceUtils.applyTypeface(txtPageHeader);
		mTypefaceUtils.applyTypeface(tvresults);
		mTypefaceUtils.applyTypeface(btnSearch);
		mTypefaceUtils.applyTypeface(edtSearch);

	}

	private void addListeners() {

		mGallery.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					prePosition = mGallery.getSelectedItemPosition();
				} else if (event.getAction() == MotionEvent.ACTION_UP) {

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {

							try {

								if (prePosition != mGallery.getSelectedItemPosition()) {

									loadBookingList();
								}

							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}, 500);
				}
				return false;
			}
		});

		listView.setOnLoadMoreListener(new OnLoadMoreListener() {
			@Override
			public void onLoadMore() {

				if (arrBookingList.size() < totalRecords) {

					if (isLoadMoreEnable) {
						starIndex = arrBookingList.size();
						loadMoreBookingList();
					}
				}
			}
		});

		txtBookingType.setOnClickListener(this);

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				Bundle bundle = new Bundle();
				bundle.putString("tableBookingId", arrBookingList.get(position).getTableBookingId());

				StaffBookingDetailsFragment fragment = new StaffBookingDetailsFragment();
				fragment.setSendBack(StaffBookingListFragment.this);
				fragment.setArguments(bundle);

				((StaffFragmentContainer) getParentFragment()).addFragment(fragment, true);
			}
		});

		edtSearch.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

				if (event != null && event.getAction() != KeyEvent.ACTION_DOWN) {
					return false;
				}

				if (actionId == EditorInfo.IME_ACTION_SEARCH) {

					filterList();
					return true;
				}

				return false;
			}
		});
	}

	private void init() {

		mGallery.setAdapter(new StaffHorizontalGalleryAdapter(getActivity(), getResources().getStringArray(R.array.order_days)));
		mGallery.setSelection(4);

		arrBookingList = new ArrayList<BookingData>();
		adapterBookingList = new StaffBookingListAdapter(getActivity(), arrBookingList);
		listView.setAdapter(adapterBookingList);

		bookingTypes = getResources().getStringArray(R.array.booking_types);
		bookingTypesValue = getResources().getStringArray(R.array.booking_types_value);

		initQuickAction();

		txtBookingType.setText(bookingTypes[1]);
		txtBookingType.setTag(bookingTypesValue[1]);
	}

	private void loadBookingList() {

		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		String partitionKey = registration.getPartitionKey();

		String startDate = "", endDate = "";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());

		switch (mGallery.getSelectedItemPosition()) {
			case 0 :
				break;

			case 1 :

				Calendar c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, -30);
				startDate = format.format(c.getTime());
				endDate = format.format(new Date());
				break;

			case 2 :
				c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, -7);
				startDate = format.format(c.getTime());
				endDate = format.format(new Date());
				break;

			case 3 :
				startDate = format.format(new Date());
				endDate = format.format(new Date());
				break;

			case 4 :
				startDate = format.format(new Date());
				c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, 7);
				endDate = format.format(c.getTime());
				break;

			case 5 :
				startDate = format.format(new Date());
				c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, 30);
				endDate = format.format(c.getTime());
				break;
		}

		networManager.isProgressVisible(true);
		bookingListRequestId = networManager.addRequest(Join8RequestBuilder.getOrderListRequest(access_token, partitionKey, txtBookingType.getTag().toString(), starIndex, COUNT_PER_PAGE, startDate, endDate), RequestMethod.POST, getActivity(),
				Join8RequestBuilder.METHOD_STAFF_BOOKINGLIST);
	}

	public void loadMoreBookingList() {

		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		String partitionKey = registration.getPartitionKey();

		String startDate = "", endDate = "";
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.getDefault());

		switch (mGallery.getSelectedItemPosition()) {
			case 0 :
				break;

			case 1 :

				Calendar c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, -30);
				startDate = format.format(c.getTime());
				endDate = format.format(new Date());
				break;

			case 2 :
				c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, -7);
				startDate = format.format(c.getTime());
				endDate = format.format(new Date());
				break;

			case 3 :
				startDate = format.format(new Date());
				endDate = format.format(new Date());
				break;

			case 4 :
				startDate = format.format(new Date());
				c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, 7);
				endDate = format.format(c.getTime());
				break;

			case 5 :
				startDate = format.format(new Date());
				c = Calendar.getInstance();
				c.add(Calendar.DAY_OF_MONTH, 30);
				endDate = format.format(c.getTime());
				break;
		}

		networManager.isProgressVisible(false);
		bookingListMoredataRequestId = networManager.addRequest(Join8RequestBuilder.getOrderListRequest(access_token, partitionKey, txtBookingType.getTag().toString(), starIndex, COUNT_PER_PAGE, startDate, endDate), RequestMethod.POST, getActivity(),
				Join8RequestBuilder.METHOD_STAFF_BOOKINGLIST);
	}

	private void initQuickAction() {

		mQuickAction = new QuickAction(getActivity(), QuickAction.VERTICAL, getResources().getDrawable(R.drawable.box_blue_border));

		for (int i = 0; i < bookingTypes.length; i++) {
			ActionItem mItem = new ActionItem(i, bookingTypes[i]);
			mItem.setColor(getResources().getColor(R.color.blue_text_color));
			mQuickAction.addActionItem(mItem);
		}
		mQuickAction.setOnActionItemClickListener(new OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction source, int pos, int actionId) {

				txtBookingType.setText(bookingTypes[pos]);
				txtBookingType.setTag(bookingTypesValue[pos]);

				txtBookingType.setSelected(false);
				txtBookingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_blue_down, 0);
				txtBookingType.setTextColor(getResources().getColor(R.color.blue_dropdown_border_color));

				loadBookingList();
			}
		});

		mQuickAction.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss() {

				txtBookingType.setSelected(false);
				txtBookingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_blue_down, 0);
				txtBookingType.setTextColor(getResources().getColor(R.color.blue_dropdown_border_color));
			}
		});
	}

	private void filterList() {

		try {

			KeyboardUtils.hideKeyboard(edtSearch);
			String keyword = edtSearch.getText().toString().trim();

			if (TextUtils.isEmpty(keyword)) {

				isLoadMoreEnable = true;
				arrBookingList.clear();
				arrBookingList.addAll(arrBackupList);
				listView.onLoadMoreComplete();

			} else {

				isLoadMoreEnable = false;
				arrBookingList.clear();

				for (int i = 0; i < arrBackupList.size(); i++) {

					if (arrBackupList.get(i).getMobileNumber().contains(keyword)) {
						arrBookingList.add(arrBackupList.get(i));
					}
				}
			}

			adapterBookingList.notifyDataSetChanged();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

			case R.id.txtOrderType :

				mQuickAction.show(v);

				if (!txtBookingType.isSelected()) {
					txtBookingType.setSelected(true);
					txtBookingType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up_white, 0);
					txtBookingType.setTextColor(getResources().getColor(android.R.color.white));
				} else {
					txtBookingType.setSelected(false);
				}

				break;

			case R.id.btnSearch :
				filterList();
				break;

		}
	}

	@Override
	public void onSuccess(int id, String response) {

		try {

			Log.i(getTag(), "Result => " + response);

			if (response != null && !response.equals("")) {

				if (id == bookingListRequestId) {

					arrBookingList.clear();

					if (response.toLowerCase(Locale.getDefault()).startsWith("no record found")) {

						// Toast.makeText(getActivity(),
						// getString(R.string.norecorderror));

						// listView.setVisibility(View.GONE);
						// adapterBookingList = new
						// StaffBookingListAdapter(getActivity(),
						// arrBookingList);
						// listView.setAdapter(adapterBookingList);
						adapterBookingList.notifyDataSetChanged();

					} else {

						JSONArray jArray = new JSONArray(response);

						if (jArray != null && jArray.length() > 0) {

							totalRecords = jArray.getJSONObject(0).getInt("TotalRecords");

							for (int i = 0; i < jArray.length(); i++) {
								BookingData data = new Gson().fromJson(jArray.getString(i).toString(), BookingData.class);
								arrBookingList.add(data);
							}

							listView.setVisibility(View.VISIBLE);
							adapterBookingList.notifyDataSetChanged();
							// adapterBookingList = new
							// StaffBookingListAdapter(getActivity(),
							// arrBookingList);
							// listView.setAdapter(adapterBookingList);
						}
					}

					arrBackupList = new ArrayList<BookingData>();
					arrBackupList.addAll(arrBookingList);

				} else if (id == bookingListMoredataRequestId) {

					if (!response.toLowerCase(Locale.getDefault()).startsWith("no record found")) {

						JSONArray jArray = new JSONArray(response);

						if (jArray != null && jArray.length() > 0) {

							totalRecords = jArray.getJSONObject(0).getInt("TotalRecords");

							for (int i = 0; i < jArray.length(); i++) {
								BookingData data = new Gson().fromJson(jArray.getString(i).toString(), BookingData.class);
								arrBookingList.add(data);
							}

							adapterBookingList.notifyDataSetChanged();
							listView.onLoadMoreComplete();

							arrBackupList.clear();
							arrBackupList.addAll(arrBookingList);
						}
					}
				}
			}

		} catch (Exception e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	@Override
	public void onBack() {
		loadBookingList();
	}

	private BroadcastReceiver listRefreshReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {

			try {

				String redirectTo = intent.getStringExtra("redirectTo");

				if (redirectTo.equalsIgnoreCase("StaffBookingDetail")) {

					starIndex = 0;
					loadBookingList();
				}

			} catch (Exception e) {
				e.printStackTrace();
				Log.e("StaffBookingList", "BroadcastReceiver : " + e.toString());
			}
		}
	};
}
