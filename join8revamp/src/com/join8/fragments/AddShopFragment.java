package com.join8.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.Location;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.facebook.android.FacebookError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.adpaters.ImageAdapter;
import com.join8.controller.ImageController;
import com.join8.listeners.Requestlistener;
import com.join8.model.Area;
import com.join8.model.RequestAddShop;
import com.join8.model.ShopCategory;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.runnable.ConvertImageRunnable;
import com.join8.utils.BackgroundExecutor;
import com.join8.utils.CameraUtil;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.Log;
import com.join8.utils.UrlUtil;
import com.join8.views.HorizontalListView;
import com.join8.views.MyEditText;
import com.join8.views.MyTextView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by craterzone on 8/1/16.
 */
public class AddShopFragment extends Fragment implements View.OnClickListener, Requestlistener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

	private final String TAG = this.getClass().getName();
	private MyEditText etNameEn, etNameCh, etCountryCode, etPhoneNumber, etContactPerson, etAddress1, etAddress2;
	private MyTextView txtLocation;
	private Spinner spnShopCategory, spnArea;
	private NetworkManager mNetworkManager;
	private int mReqSHopCAtegory = -1;
	private int mReqAreaList = -1;
	private CryptoManager prefsManager;
	private int mReqAddShop = -1;
	private ArrayList<Bitmap> images = new ArrayList<Bitmap>();
	private ImageAdapter mImageAdapter;
	private HorizontalListView mListView;
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
	private String tokenCloud = null;
	private Location mLastLocation;
	private HashMap<String, String> auth = null;
	// Google client to interact with Google API
	private GoogleApiClient mGoogleApiClient;

	// boolean flag to toggle periodic location updates
	/*
	 * private boolean mRequestingLocationUpdates = false;
	 * 
	 * private LocationRequest mLocationRequest;
	 * 
	 * // Location updates intervals in sec private static int UPDATE_INTERVAL =
	 * 10000; // 10 sec private static int FATEST_INTERVAL = 5000; // 5 sec
	 * private static int DISPLACEMENT = 10; // 10 meters
	 */
	private RequestAddShop req;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mNetworkManager = NetworkManager.getInstance();
		prefsManager = CryptoManager.getInstance(getActivity());
		mNetworkManager.isProgressVisible(true);
		tokenCloud = prefsManager.getPrefs().getString(ConstantData.TOKEN_CLOUD, null);
		auth = new HashMap<String, String>();
		auth.put("Authorization", "bearer " + tokenCloud);
		mReqSHopCAtegory = mNetworkManager.addRequest(auth, getActivity(), true, UrlUtil.getShopCategoryUrl(), "", ConstantData.method_get);
		mReqAreaList = mNetworkManager.addRequest(auth, getActivity(), true, UrlUtil.getAreaListUrl(), "", ConstantData.method_get);
		if (checkPlayServices()) {

			// Building the GoogleApi client
			buildGoogleApiClient();
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		mNetworkManager.addListener(this);
		if (mGoogleApiClient != null) {
			mGoogleApiClient.connect();
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		mNetworkManager.removeListeners(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mNetworkManager.removeListeners(this);
		ImageController.getInstance().getImages().clear();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		@SuppressLint("InflateParams")
		View view = inflater.inflate(R.layout.fragment_add_shop, null);
		initView(view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (!getActivity().isFinishing()) {
			((HomeScreen) getActivity()).setCustomTitle(getString(R.string.add_shop), Typeface.NORMAL);
			((HomeScreen) getActivity()).changeActionItems(true);
		}
	}

	private void initView(View view) {
		etNameEn = (MyEditText) view.findViewById(R.id.etNameEN);
		etNameCh = (MyEditText) view.findViewById(R.id.etNameCH);
		spnShopCategory = (Spinner) view.findViewById(R.id.spn_shop_category);
		etCountryCode = (MyEditText) view.findViewById(R.id.etCountryCode);
		etPhoneNumber = (MyEditText) view.findViewById(R.id.etPhoneNumber);
		etContactPerson = (MyEditText) view.findViewById(R.id.et_in_charge);
		spnArea = (Spinner) view.findViewById(R.id.spn_area);
		etAddress1 = (MyEditText) view.findViewById(R.id.et_address1);
		etAddress2 = (MyEditText) view.findViewById(R.id.et_address2);
		txtLocation = (MyTextView) view.findViewById(R.id.txtLocation);
		view.findViewById(R.id.btn_save).setOnClickListener(this);
		view.findViewById(R.id.btn_cancel).setOnClickListener(this);
		view.findViewById(R.id.txtLocation).setOnClickListener(this);
		view.findViewById(R.id.txt_photo_upload).setOnClickListener(this);
		mListView = (HorizontalListView) view.findViewById(R.id.hrListView);
		mImageAdapter = new ImageAdapter(getActivity(), images);
		mListView.setAdapter(mImageAdapter);

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
			case R.id.btn_cancel :
				clearAllFields();
				break;

			case R.id.btn_save :
				if (isValidData()) {
					saveDetails();
					mReqAddShop = mNetworkManager.addRequest(auth, getActivity(), true, UrlUtil.getAddShopUrl(), Join8RequestBuilder.getAddShopRequest(req), ConstantData.method_post);
				}
				break;
			case R.id.txtLocation :
				displayLocation();
				break;
			case R.id.txt_photo_upload :
				CameraUtil.takePictureInExternalStorage(getActivity(), R.string.camera_error);
				break;
		}
	}

	/**
	 * Creating google api client object
	 */
	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addConnectionCallbacks(this).addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
	}

	@Override
	public void onResume() {
		super.onResume();
		checkPlayServices();
	}

	/**
	 * Method to verify google play services on the device
	 */
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Toast.makeText(getActivity(), "This device is not supported.", Toast.LENGTH_LONG).show();
			}
			return false;
		}
		return true;
	}

	private void displayLocation() {

		mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

		if (mLastLocation != null) {
			double latitude = mLastLocation.getLatitude();
			double longitude = mLastLocation.getLongitude();

			txtLocation.setText(latitude + ", " + longitude);

		} else {

			Toast.makeText(getActivity(), "Couldn't get the location. Make sure location is enabled on the device", Toast.LENGTH_SHORT).show();
		}
	}

	private boolean isValidData() {
		if (etNameEn.length() < 2) {
			Toast.makeText(getActivity(), "Please insert name in english", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (etNameEn.length() < 2) {
			Toast.makeText(getActivity(), "Please insert name in chinese", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (spnShopCategory.getSelectedItemPosition() < 1) {
			Toast.makeText(getActivity(), "Please select Chop Category", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (etNameEn.length() < 2) {
			Toast.makeText(getActivity(), "Please insert country code", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (etNameEn.length() < 2) {
			Toast.makeText(getActivity(), "Please insert phone number", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (etPhoneNumber.length() < 8) {
			Toast.makeText(getActivity(), "Phone number Length should be eight ", Toast.LENGTH_SHORT).show();
			return false;
		}

		if (etNameEn.length() < 2) {
			Toast.makeText(getActivity(), "Please insert contact person", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (spnArea.getSelectedItemPosition() < 1) {
			Toast.makeText(getActivity(), "Please select Area", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (etNameEn.length() < 2) {
			Toast.makeText(getActivity(), "Please insert address 1", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (etNameEn.length() < 2) {
			Toast.makeText(getActivity(), "Please insert address 2", Toast.LENGTH_SHORT).show();
			return false;
		}
		if (txtLocation.getText().toString() == null) {
			Toast.makeText(getActivity(), "Please set Location", Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (resultCode == Activity.RESULT_OK) {
			Bitmap bitmap;
			Bitmap b = null;
			if (requestCode == ConstantData.ACTIVITY_CAMERA_REQUEST) {
				try {
					bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), CameraUtil.getLastImageUri());
					b = ThumbnailUtils.extractThumbnail(bitmap, 100, 100);
					images.add(b);
					// mImageAdapter.addItem(b);
				} catch (IOException e) {

					e.printStackTrace();
				}
				mImageAdapter = new ImageAdapter(getActivity(), images);
				mListView.setAdapter(mImageAdapter);
				BackgroundExecutor.getInstance().execute(new ConvertImageRunnable(b));
			}

		}
		super.onActivityResult(requestCode, resultCode, data);

	}

	private void saveDetails() {
		String nameEn = etNameEn.getText().toString().trim();
		String nameCh = etNameCh.getText().toString().trim();
		String shopCategoryId = ((ShopCategory) spnShopCategory.getSelectedItem()).getShopCategoryId();
		String phNumber = etCountryCode.getText().toString() + "-" + etPhoneNumber.getText().toString();
		String contactPerson = etContactPerson.getText().toString().trim();
		String area = ((Area) spnArea.getSelectedItem()).getAreaId();
		String add1 = etAddress1.getText().toString();
		String add2 = etAddress2.getText().toString();
		String location = txtLocation.getText().toString();
		req = new RequestAddShop();
		req.setShopNameEN(nameEn);
		req.setShopName(nameCh);
		// req.setDescription("description");
		// req.setDescriptionEN("descriptionen");
		req.setFk_City("1");
		req.setFk_ShopCategoryID(shopCategoryId);
		req.setPhone(phNumber);
		req.setArea(area);
		req.setAddress(add1 + add2);
		req.setLatitude(location.split(",")[0]);
		req.setLongitude(location.split(",")[1]);
		req.setFk_AddedBy("49");

	}

	private void clearAllFields() {
		ImageController.getInstance().getImages().clear();
		images = new ArrayList<Bitmap>();
		mListView.setAdapter(new ImageAdapter(getActivity(), images));
		etNameEn.getText().clear();
		etNameCh.getText().clear();
		spnShopCategory.setSelection(0);
		spnArea.setSelection(0);
		etCountryCode.getText().clear();
		etPhoneNumber.getText().clear();
		etContactPerson.getText().clear();
		etAddress1.getText().clear();
		etAddress2.getText().clear();
		txtLocation.setText("");

	}

	@Override
	public void onConnected(Bundle bundle) {
		// displayLocation();
	}

	@Override
	public void onConnectionSuspended(int i) {
		mGoogleApiClient.connect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
	}

	@Override
	public void onSuccess(int id, String response) {
		Gson gson = new Gson();
		if (isAdded() && !TextUtils.isEmpty(response)) {

			if (id == mReqSHopCAtegory) {
				ArrayList<ShopCategory> list = gson.fromJson(response, new TypeToken<List<ShopCategory>>() {
				}.getType());
				ShopCategory shopcat = new ShopCategory();
				shopcat.setShopType("Shop Category");
				shopcat.setShopTypeEn("Shop Category");
				list.add(0, shopcat);
				ArrayAdapter<ShopCategory> adapter = new ArrayAdapter<ShopCategory>(getActivity(), R.layout.item_spinner, list);
				spnShopCategory.setAdapter(adapter);
			} else if (id == mReqAreaList) {
				ArrayList<Area> list = gson.fromJson(response, new TypeToken<List<Area>>() {
				}.getType());
				Area area = new Area();
				area.setAreaNameEn("Area");
				area.setAreaName("Area");
				list.add(0, area);
				ArrayAdapter<Area> adapter = new ArrayAdapter<Area>(getActivity(), R.layout.item_spinner, list);
				spnArea.setAdapter(adapter);
			} else if (id == mReqAddShop) {
				try {
					JSONObject jObject = new JSONObject(response);
					if (jObject.has("ShopId")) {
						String shopId = jObject.getString("ShopId");
						ImageController.getInstance().setShopId(shopId);
						for (int i = 0; i < ImageController.getInstance().getImages().size(); i++) {
							mNetworkManager.addRequest(auth, getActivity(), true, UrlUtil.getUploadShopPhotoUrl(), Join8RequestBuilder.getUploadPhotoRequest(ImageController.getInstance().getImages().get(i)), ConstantData.method_post);
						}
					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Toast.makeText(getActivity(), response, Toast.LENGTH_SHORT).show();
				clearAllFields();
			}
		}
	}
	@Override
	public void onError(int id, String message) {
		// TODO Auto-generated method stub

	}

}
