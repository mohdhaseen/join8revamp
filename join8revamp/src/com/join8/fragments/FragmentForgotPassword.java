package com.join8.fragments;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.network.NetworkManager;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.UrlUtil;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class FragmentForgotPassword extends Fragment implements OnClickListener {
	private EditText edtEmail;
	private NetworkManager mNetworkManager;
	private int mReqAddShop = -1;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.forgot_password, container, false);
		init(v);
		return v;
	}

	private void init(View v) {
		edtEmail = (EditText) v.findViewById(R.id.edtEmail);
		v.findViewById(R.id.btnSignin).setOnClickListener(this);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (!getActivity().isFinishing()) {
			((HomeScreen) getActivity()).setCustomTitle(getString(R.string.forgot_pass), Typeface.NORMAL);
			((HomeScreen) getActivity()).changeActionItems(true);
		}
	}

	@Override
	public void onClick(View v) {
		if (!TextUtils.isEmpty(edtEmail.getText())) {
			//mReqAddShop = mNetworkManager.addRequest(auth, getActivity(), true, UrlUtil.getAddShopUrl(), Join8RequestBuilder.getAddShopRequest(req), ConstantData.method_post);
		}
	}
}
