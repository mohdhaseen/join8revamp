package com.join8.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.adpaters.StaffOrderDetailsAdapter;
import com.join8.fragments.StaffFragmentContainer.SendBack;
import com.join8.listeners.Requestlistener;
import com.join8.model.FoodOptionItemData;
import com.join8.model.OrderData;
import com.join8.model.RestaurantMenuItemData;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.print.BluetoothService;
import com.join8.print.DeviceListActivity;
import com.join8.print.PrintUtils;
import com.join8.print.SocketManager;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.CryptoManager;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class StaffOrderDetailFragment extends Fragment implements OnClickListener, Requestlistener {

	private static final String TAG = StaffOrderDetailFragment.class.getSimpleName();

	private TextView txtOrderId = null, txtName = null, txtDateToBook = null, txtMobilePhone = null, txtAddress = null, txtRemarks = null, txtDeliveryCharge = null, txtTotal = null;
	private ImageView imvUser = null;
	private ListView mListView;

	private TextView mAcceptButton, mRejectButton, mPrintButton;
	private NetworkManager networManager;
	private CryptoManager prefManager;
	private String access_token = "";
	private int get_Order = -1, accept = -1, reject = -1, delivered = -1, cancel = -1;
	private String orderId;

	private ImageLoader imageLoader = null;
	private DisplayImageOptions options = null;

	private ArrayList<RestaurantMenuItemData> listItems = null;
	private OrderData orderData = null;
	private double total = 0, deliveryFee = 0;
	private SendBack sendBack;

	private BluetoothAdapter mBluetoothAdapter;

	private BluetoothService mService;
	private SocketManager mSocketManager;
	private SocketManager mSocketManager1;

	private static final int REQUEST_CONNECT_DEVICE = 123;
	private static final int REQUEST_ENABLE_BT = 124;

	private static final String DEVICE_ADDRESS = "device_address";

	// Message types sent from the BluetoothService Handler
	private static final int MESSAGE_STATE_CHANGE = 1;
	private static final int MESSAGE_READ = 2;
	private static final int MESSAGE_WRITE = 3;
	private static final int MESSAGE_DEVICE_NAME = 4;
	private static final int MESSAGE_TOAST = 5;

	public void setSendBack(SendBack sendBack) {
		this.sendBack = sendBack;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		networManager = NetworkManager.getInstance();
		prefManager = CryptoManager.getInstance(getActivity());
		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
		if (savedInstanceState != null) {
			orderId = savedInstanceState.getString("orderId");
		} else {
			orderId = getArguments().getString("orderId");
		}

		mSocketManager = new SocketManager(getActivity());
		mSocketManager.setHandler(mHandlerWifi);

		mSocketManager1 = new SocketManager(getActivity());
		mSocketManager1.setHandler(mHandlerWifi1);

		imageLoader = ImageLoader.getInstance();
		DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();
		int roundSize = (int) (dm.density * getResources().getDimension(R.dimen.image_height));
		options = new DisplayImageOptions.Builder().displayer(new RoundedBitmapDisplayer(roundSize)).showStubImage(R.drawable.default_user).showImageForEmptyUri(R.drawable.default_user).showImageOnFail(R.drawable.default_user).cacheInMemory(true).cacheOnDisc(true).build();
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {

		networManager.removeListeners(this);

		if (mSocketManager != null) {
			mSocketManager.close();
			mSocketManager = null;
		}

		if (mSocketManager1 != null) {
			mSocketManager1.close();
			mSocketManager1 = null;
		}
		super.onStop();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putString("orderId", orderId);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.order_details_fragment, container, false);
		mappingWidgets(mView);
		addListeners();
		initFragment();
		return mView;
	}

	private void mappingWidgets(View mView) {

		mListView = (ListView) mView.findViewById(android.R.id.list);
		mListView.setBackgroundColor(getResources().getColor(R.color.staff_order_details_bg_color));
		mListView.setDivider(getResources().getDrawable(R.drawable.divider_blue_list));
		mListView.setSelector(R.drawable.trans);

		View mHeaderView = getActivity().getLayoutInflater().inflate(R.layout.staff_order_details_header, null);
		mListView.addHeaderView(mHeaderView);

		View mFooterView = getActivity().getLayoutInflater().inflate(R.layout.staff_order_details_footer, null);
		mListView.addFooterView(mFooterView);

		txtDeliveryCharge = (TextView) mFooterView.findViewById(R.id.txtDeliveryCharge);
		txtTotal = (TextView) mFooterView.findViewById(R.id.txtTotal);
		mAcceptButton = (TextView) mFooterView.findViewById(R.id.btnAccept);
		mRejectButton = (TextView) mFooterView.findViewById(R.id.btnReject);
		mPrintButton = (TextView) mFooterView.findViewById(R.id.btnPrint);

		mPrintButton.setVisibility(View.GONE);

		imvUser = (ImageView) mHeaderView.findViewById(R.id.imvUser);
		txtName = (TextView) mHeaderView.findViewById(R.id.txtName);
		txtOrderId = (TextView) mHeaderView.findViewById(R.id.txtOrderId);
		txtDateToBook = (TextView) mHeaderView.findViewById(R.id.txtDateToBook);
		txtMobilePhone = (TextView) mHeaderView.findViewById(R.id.txtMobilePhone);
		txtAddress = (TextView) mHeaderView.findViewById(R.id.txtAddress);
		txtRemarks = (TextView) mHeaderView.findViewById(R.id.txtRemarks);

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface((TextView) mFooterView.findViewById(R.id.lblDeliveryCharge));
		mTypefaceUtils.applyTypeface((TextView) mFooterView.findViewById(R.id.lblTotal));
		mTypefaceUtils.applyTypeface(txtOrderId);
		mTypefaceUtils.applyTypeface(txtDateToBook);
		mTypefaceUtils.applyTypeface(txtMobilePhone);
		mTypefaceUtils.applyTypeface(txtAddress);
		mTypefaceUtils.applyTypeface(txtRemarks);
		mTypefaceUtils.applyTypeface(mAcceptButton);
		mTypefaceUtils.applyTypeface(mRejectButton);
		mTypefaceUtils.applyTypeface(mPrintButton);
		mTypefaceUtils.applyTypeface(txtDeliveryCharge);
		mTypefaceUtils.applyTypeface(txtTotal);
	}

	private void addListeners() {

		mAcceptButton.setOnClickListener(this);
		mRejectButton.setOnClickListener(this);
		mPrintButton.setOnClickListener(this);

		txtMobilePhone.setOnClickListener(this);
	}

	private void initFragment() {

		changeActionTitle("");

		loadOrder(orderId);

	}

	private void loadOrder(String orderId) {
		networManager.isProgressVisible(true);
		get_Order = networManager.addRequest(Join8RequestBuilder.getParticularOrder(access_token, orderId), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_GET_PARTICULAR_ORDER);
	}

	@Override
	public void onClick(View v) {

		if (v.getTag() != null && v.getTag().equals("accept")) {

			networManager.isProgressVisible(true);
			accept = networManager.addRequest(Join8RequestBuilder.changeOrderStatus(access_token, orderId, 20), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_CHANGE_ORDER_STATUS);

		} else if (v.getTag() != null && v.getTag().equals("reject")) {

			networManager.isProgressVisible(true);
			reject = networManager.addRequest(Join8RequestBuilder.changeOrderStatus(access_token, orderId, 50), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_CHANGE_ORDER_STATUS);

		} else if (v.getTag() != null && v.getTag().equals("delivered")) {

			networManager.isProgressVisible(true);
			delivered = networManager.addRequest(Join8RequestBuilder.changeOrderStatus(access_token, orderId, 80), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_CHANGE_ORDER_STATUS);

		} else if (v.getTag() != null && v.getTag().equals("cancel")) {

			networManager.isProgressVisible(true);
			cancel = networManager.addRequest(Join8RequestBuilder.changeOrderStatus(access_token, orderId, 30), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_CHANGE_ORDER_STATUS);

		} else if (v == txtMobilePhone) {

			try {

				if (TextUtils.isEmpty(txtMobilePhone.getText().toString().trim())) {

					Toast.displayText(getActivity(), getActivity().getString(R.string.no_phono_number));

				} else {

					Intent intent = new Intent(Intent.ACTION_CALL);
					intent.setData(Uri.parse("tel:" + PhoneNumberUtils.stringFromStringAndTOA(PhoneNumberUtils.stripSeparators(txtMobilePhone.getText().toString().trim()), PhoneNumberUtils.TOA_International)));
					startActivity(intent);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (v == mPrintButton) {

			try {

				if (orderData != null) {

					if (!print()) {
						if (sendBack != null) {
							sendBack.onBack();
						}
						((StaffFragmentContainer) getParentFragment()).popBackStack();

					}
					return;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private boolean print() {

		if (orderData == null) {
			Log.d(TAG, "No data to print");
			return false;
		}

		if (!isAdded()) {
			return false;
		}

		SharedPreferences mPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);

		if (mPreferences.getInt("printerTypePosition", 0) == 1) {

			String ip = mPreferences.getString("printerIP", "");
			String ip1 = mPreferences.getString("printerIP1", "");
			if (TextUtils.isEmpty(ip) && TextUtils.isEmpty(ip1)) {
				displayIPDialog();
			} else {

				if (mSocketManager != null && mSocketManager.isConnected()) {
					printOrder(mSocketManager);
				} else {
					if (!TextUtils.isEmpty(ip)) {
						connectWifiPrinter(ip);
					}
				}

				if (mSocketManager1 != null && mSocketManager1.isConnected()) {
					printOrder(mSocketManager1);
				} else {
					if (!TextUtils.isEmpty(ip1)) {
						connectWifiPrinter1(ip1);
					}
				}
			}

		} else {

			return connectBluetoothPrinter();

		}
		return true;
	}

	private boolean connectBluetoothPrinter() {

		if (mBluetoothAdapter == null) {
			mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			if (mBluetoothAdapter == null) {
				Toast.displayText(getActivity(), getString(R.string.bluetooth_not_available));
				return false;
			}
		}

		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			if (getParentFragment() != null) {
				getParentFragment().startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			} else {
				startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
			}

		} else {
			if (mService == null) {
				initBluetooth();
				connectPreviouslyPairedDevice();
			} else if (mService.getState() == BluetoothService.STATE_CONNECTED) {
				printOrder(null);
			}
		}

		return true;
	}

	private void initBluetooth() {
		// Initialize the BluetoothService to perform bluetooth connections
		if (mService == null) {
			mService = BluetoothService.getInstance(getActivity());
			mService.setHandler(mHandler);
		}

		if (mService.getState() == BluetoothService.STATE_NONE) {
			mService.start();
		} else if (mService.getState() == BluetoothService.STATE_CONNECTED) {
			printOrder(null);
		}
	}

	private void connectPreviouslyPairedDevice() {
		SharedPreferences mPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
		if (mPreferences.contains("btAddress")) {
			String deviceId = mPreferences.getString("btAddress", "");
			if (!TextUtils.isEmpty(deviceId)) {
				Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
				if (pairedDevices != null && pairedDevices.size() > 0) {
					for (BluetoothDevice device : pairedDevices) {
						if (device.getAddress().equals(deviceId)) {
							mService.connect(device);
							return;
						}
					}
				}
			}
		}
	}

	private void displayIPDialog() {

		try {

			final SharedPreferences mPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);

			View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_ip_address, null);
			final EditText edtIP = (EditText) view.findViewById(R.id.edtIP);
			edtIP.setText(mPreferences.getString("printerIP", ""));
			final EditText edtIP1 = (EditText) view.findViewById(R.id.edtIP1);
			edtIP1.setText(mPreferences.getString("printerIP1", ""));

			AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
			mBuilder.setView(view);
			mBuilder.setTitle(getResources().getStringArray(R.array.printer_type)[1]);
			mBuilder.setPositiveButton(R.string.tvOk, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

					String ip = edtIP.getText().toString().trim();
					mPreferences.edit().putString("printerIP", ip).commit();
					String ip1 = edtIP1.getText().toString().trim();
					mPreferences.edit().putString("printerIP1", ip1).commit();
				}
			});
			mBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			});
			mBuilder.create().show();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void connectWifiPrinter(String printerIp) {
		mSocketManager.mPrinterIP = printerIp;
		mSocketManager.threadconnect();
	}

	private void connectWifiPrinter1(String printerIp) {
		mSocketManager1.mPrinterIP = printerIp;
		mSocketManager1.threadconnect();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d(TAG, "onActivityResult");

		if (requestCode == REQUEST_ENABLE_BT) {
			if (resultCode == Activity.RESULT_OK) {
				initBluetooth();
			}
		} else if (requestCode == REQUEST_CONNECT_DEVICE) {
			if (resultCode == Activity.RESULT_OK) {

				if (data != null && data.getExtras() != null) {
					if (data.getExtras().containsKey(DeviceListActivity.EXTRA_DEVICE_ADDRESS)) {
						String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
						if (BluetoothAdapter.checkBluetoothAddress(address)) {
							BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
							mService.connect(device);
						}
					}
				}

			} else {
				// if (mService != null) {
				// mService.stop();
				// mService = null;
				// }

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if (sendBack != null) {
							sendBack.onBack();
						}
						((StaffFragmentContainer) getParentFragment()).popBackStack();
					}
				}, 1000);
			}
		}
	}

	private void printOrder(SocketManager mSocketManager) {

		try {

			if (orderData != null) {
				new PrintUtils(getActivity(), mService, mSocketManager).printOrder(orderData, 1);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "Json Exception");
		}
	}

	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (!isAdded())
				return;
			switch (msg.what) {
				case MESSAGE_STATE_CHANGE :
					switch (msg.arg1) {
						case BluetoothService.STATE_CONNECTED :
							// mTitle.setText(R.string.title_connected_to);
							// mTitle.append(mConnectedDeviceName);
							break;
						case BluetoothService.STATE_CONNECTING :
							// mTitle.setText(R.string.title_connecting);
							break;
						case BluetoothService.STATE_LISTEN :
						case BluetoothService.STATE_NONE :
							// Toast.displayText(getActivity(),
							// getString(R.string.title_not_connected));
							break;
					}
					break;
				case MESSAGE_WRITE :
					break;
				case MESSAGE_READ :
					break;
				case MESSAGE_DEVICE_NAME :
					SharedPreferences mPreferences = getActivity().getSharedPreferences(getActivity().getPackageName(), Context.MODE_PRIVATE);
					mPreferences.edit().putString("btAddress", msg.getData().getString(DEVICE_ADDRESS)).commit();
					printOrder(null);
					break;
				case MESSAGE_TOAST :

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							if (sendBack != null) {
								sendBack.onBack();
							}
							((StaffFragmentContainer) getParentFragment()).popBackStack();
						}
					}, 1000);
					// startBluetoothDeviceDiscovery();
					break;
			}
		}
	};

	private void changeActionTitle(String orderStatus) {

		if (!orderStatus.equals("")) {
			orderStatus = "-" + orderStatus;
		}

		((StaffFragmentContainer) getParentFragment()).setActionBarTitle(getString(R.string.order_details) + orderStatus);
	}

	public void changeTotal() {

		try {

			total = 0.0;

			if (listItems != null) {

				for (int i = 0; i < listItems.size(); i++) {
					total += (listItems.get(i).getQuantity() * listItems.get(i).getPrice());

					for (int j = 0; j < listItems.get(i).getFoodOptions().size(); j++) {

						total += listItems.get(i).getQuantity() * listItems.get(i).getFoodOptions().get(j).getExtraPrice();
					}
				}
			}

			deliveryFee = 0;

			if (!orderData.isIsOrderPickUpByUser()) {

				if (orderData.getDeliveryOption() == 1) {

					deliveryFee = orderData.getDeliveryFee();

				} else if (orderData.getDeliveryOption() == 2) {

					deliveryFee = total * orderData.getDeliveryFee() / 100;
				}
			}

			txtDeliveryCharge.setText("$" + Utils.formatDecimal("" + deliveryFee));
			txtTotal.setText("$" + Utils.formatDecimal(String.valueOf(deliveryFee + total)));

		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onSuccess(int id, String response) {

		if (id == get_Order) {

			try {

				JSONObject jObj = new JSONObject(response);
				listItems = new ArrayList<RestaurantMenuItemData>();

				orderData = new Gson().fromJson(jObj.toString(), OrderData.class);

				if (orderData.getMenuItem().size() > 0) {

					for (int i = 0; i < orderData.getMenuItem().size(); i++) {

						RestaurantMenuItemData data = new RestaurantMenuItemData();

						data.setCompanyId(orderData.getCompanyId());
						data.setMenuItemId(orderData.getMenuItem().get(i).getMenuItemId());
						data.setItemName(orderData.getMenuItem().get(i).getItemName());
						data.setItemName_EN(orderData.getMenuItem().get(i).getItemName_EN());
						data.setPrice(orderData.getMenuItem().get(i).getPrice());
						data.setQuantity(orderData.getMenuItem().get(i).getQuantity());
						data.setHasFoodOption(orderData.isHasFoodOption());

						for (int j = 0; j < orderData.getMenuItem().get(i).getFoodOption().size(); j++) {

							double basePrice = orderData.getMenuItem().get(i).getFoodOption().get(j).getBasePrice();

							for (int k = 0; k < orderData.getMenuItem().get(i).getFoodOption().get(j).getFoodOptionItem().size(); k++) {

								FoodOptionItemData foodOptionItemData = orderData.getMenuItem().get(i).getFoodOption().get(j).getFoodOptionItem().get(k);
								foodOptionItemData.setExtraPrice(foodOptionItemData.getExtraPrice() + basePrice);
								basePrice = 0;
								data.getFoodOptions().add(foodOptionItemData);
							}
						}

						listItems.add(data);
					}
				}

				mListView.setAdapter(new StaffOrderDetailsAdapter(getActivity(), listItems));

				// orderList.add(orderData);
				// total += orderData.getAmount();

				try {

					String orderId = orderData.getOrderId() + "--";

					if (orderData.isIsOrderPickUpByUser()) {
						orderId += " (" + getString(R.string.pickup) + ")";
					} else {
						orderId += " (" + getString(R.string.deliver) + ")";
					}
					txtOrderId.setText(orderId);

					String name = "";
					if (!Utils.isEmpty(orderData.getFirstName())) {
						name = orderData.getFirstName();
					}
					if (!Utils.isEmpty(orderData.getLastName())) {
						name += " " + orderData.getLastName();
					}
					txtName.setText(name);

					if (!Utils.isEmpty(orderData.getPhoto())) {
						imageLoader.displayImage(orderData.getPhoto(), imvUser, options);
					}
					if (orderData.getOrderDeadlineDate() != null) {
						try {
							SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd - hh:mm a", Locale.getDefault());
							txtDateToBook.setText(format.format(orderData.getOrderDeadlineDate()));
						} catch (Exception e) {
						}
					}

					if (!Utils.isEmpty(orderData.getMobileNumber())) {
						txtMobilePhone.setText(orderData.getMobileNumber());
					}

					if (!Utils.isEmpty(orderData.getDeliverAddress())) {
						txtAddress.setText(orderData.getDeliverAddress());
					}

					if (!Utils.isEmpty(orderData.getRemarks())) {
						txtRemarks.setText(orderData.getRemarks());
					}

					if (orderData.getOrderStatus() == 10) { // pending

						mAcceptButton.setVisibility(View.VISIBLE);
						mRejectButton.setVisibility(View.VISIBLE);
						mAcceptButton.setText(getString(R.string.accept));
						mRejectButton.setText(getString(R.string.reject));
						mAcceptButton.setTag("accept");
						mRejectButton.setTag("reject");

					} else if (orderData.getOrderStatus() == 20 || orderData.getOrderStatus() == 60) { // confirmed

						mAcceptButton.setVisibility(View.VISIBLE);
						mRejectButton.setVisibility(View.VISIBLE);
						mAcceptButton.setText(getString(R.string.delivered));
						mRejectButton.setText(getString(R.string.cancel));
						mAcceptButton.setTag("delivered");
						mRejectButton.setTag("cancel");

					} else {

						mAcceptButton.setVisibility(View.GONE);
						mRejectButton.setVisibility(View.GONE);
					}

					if (orderData != null) {
						mPrintButton.setVisibility(View.VISIBLE);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				changeActionTitle(OrderData.getOrderStatusString(getActivity(), orderData.getOrderStatus()));

				changeTotal();

				// StaffOrderDetailsAdapter mAdapter = new
				// StaffOrderDetailsAdapter(getActivity(), orderList);
				// mListView.setAdapter(mAdapter);

				// txtTotal.setText("$" + Utils.formatDecimal("" + total));

			} catch (JSONException e) {
				e.printStackTrace();
			}

		} else if (id == accept) {

			if (response.equalsIgnoreCase("failure")) {
				Toast.displayText(getActivity(), getString(R.string.failed));
			} else {

				if (orderData != null) {
					orderData.setOrderStatus(20);
				}

				// try {
				//
				// if (orderData != null) {
				//
				// orderData.setOrderStatus(20);
				//
				// if (!print()) {
				// Toast.displayText(getActivity(),
				// getString(R.string.not_connected));
				// }
				// }
				// } catch (Exception e) {
				// e.printStackTrace();
				// }

				if (sendBack != null) {
					sendBack.onBack();
				}
				((StaffFragmentContainer) getParentFragment()).popBackStack();
			}
		} else if (id == reject) {
			if (response.equalsIgnoreCase("failure")) {
				Toast.displayText(getActivity(), getString(R.string.failed));
			} else {
				if (sendBack != null) {
					sendBack.onBack();
				}
				((StaffFragmentContainer) getParentFragment()).popBackStack();
			}
		} else if (id == delivered) {
			if (response.equalsIgnoreCase("failure")) {
				Toast.displayText(getActivity(), getString(R.string.failed));
			} else {
				if (sendBack != null) {
					sendBack.onBack();
				}
				((StaffFragmentContainer) getParentFragment()).popBackStack();
			}
		} else if (id == cancel) {
			if (response.equalsIgnoreCase("failure")) {
				Toast.displayText(getActivity(), getString(R.string.failed));
			} else {
				if (sendBack != null) {
					sendBack.onBack();
				}
				((StaffFragmentContainer) getParentFragment()).popBackStack();
			}
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	private final Handler mHandlerWifi = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			if (msg.what == SocketManager.WIFI_PRINTER_CONNECTION_STATE) {

				switch (msg.arg1) {

					case SocketManager.STATE_CONNECTED :
						printOrder(mSocketManager);
						break;

					case SocketManager.STATE_ERROR :
						if (isAdded()) {
							Toast.displayText(getActivity(), getString(R.string.wifi_printer_conn_error) + " : Printer - 1");
						}
						break;
				}
			}
		}
	};

	private final Handler mHandlerWifi1 = new Handler() {
		@Override
		public void handleMessage(Message msg) {

			if (msg.what == SocketManager.WIFI_PRINTER_CONNECTION_STATE) {

				switch (msg.arg1) {

					case SocketManager.STATE_CONNECTED :
						printOrder(mSocketManager1);
						break;

					case SocketManager.STATE_ERROR :
						if (isAdded()) {
							Toast.displayText(getActivity(), getString(R.string.wifi_printer_conn_error) + " : Printer - 2");
						}
						break;
				}
			}
		}
	};
}
