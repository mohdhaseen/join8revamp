package com.join8.fragments;

import java.util.Locale;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.TypefaceUtils;

public class SelectLanguageFragment extends Fragment implements OnClickListener {
	private TextView tvEnglish, tvSimpleCh, tvTraditional;
	private CryptoManager prefsManager;
	public static String PARAM_LANG = "language";
	private boolean isFirstTime = false;
	private ImageView imgBack;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		prefsManager = CryptoManager.getInstance(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.selectlanguage, container, false);
		tvEnglish = (TextView) v.findViewById(R.id.btnenglish);
		tvSimpleCh = (TextView) v.findViewById(R.id.btnSimplified);
		tvTraditional = (TextView) v.findViewById(R.id.btnTraditional);
		imgBack = (ImageView) v.findViewById(R.id.imgBack);
		imgBack.setOnClickListener(this);
		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(tvEnglish);
		mTypefaceUtils.applyTypeface(tvSimpleCh);
		mTypefaceUtils.applyTypeface(tvTraditional);

		tvEnglish.setOnClickListener(this);
		tvSimpleCh.setOnClickListener(this);
		tvTraditional.setOnClickListener(this);

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		String langCode = prefsManager.getPrefs().getString(PARAM_LANG, ConstantData.LANG_TRADITIONALCHINESE);
		if (!TextUtils.isEmpty(langCode)) {
			isFirstTime = true;
			selectLang(langCode);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnenglish:
			Log.e("English", "English");
			selectLang(ConstantData.LANG_ENGLISH);
			break;
		case R.id.btnSimplified:
			Log.e("English", "Simple ch");
			selectLang(ConstantData.LANG_SIMPLECHINSESE);
			break;
		case R.id.btnTraditional:
			Log.e("English", "Traditional ch");
			selectLang(ConstantData.LANG_TRADITIONALCHINESE);

			break;
		case R.id.imgBack:

			getActivity().finish();

			break;
		default:
			break;
		}
	}

	private void selectLang(String langCode) {

		if (langCode.equals(ConstantData.LANG_ENGLISH)) {
			tvEnglish.setEnabled(false);
			tvSimpleCh.setEnabled(true);
			tvTraditional.setEnabled(true);
			changeLocale(ConstantData.LANG_ENGLISH);
			HomeScreen.LANGUAGE = ConstantData.LANG_ENGLISH;
		} else if (langCode.equals(ConstantData.LANG_SIMPLECHINSESE)) {
			tvEnglish.setEnabled(true);
			tvSimpleCh.setEnabled(false);
			tvTraditional.setEnabled(true);
			changeLocale(ConstantData.LANG_SIMPLECHINSESE);
			HomeScreen.LANGUAGE = ConstantData.LANG_SIMPLECHINSESE;

		} else if (langCode.equals(ConstantData.LANG_TRADITIONALCHINESE)) {
			tvEnglish.setEnabled(true);
			tvSimpleCh.setEnabled(true);
			tvTraditional.setEnabled(false);
			changeLocale(ConstantData.LANG_TRADITIONALCHINESE);
			HomeScreen.LANGUAGE = ConstantData.LANG_TRADITIONALCHINESE;
		}

	}

	private void changeLocale(final String lang_code) {
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {

				try {

					String[] str = lang_code.split("_");
					Locale locale = new Locale(str[0], str[1]);
					Log.e("Change Language", lang_code);
					Locale.setDefault(locale);
					Configuration config = new Configuration();
					config.locale = locale;
					getResources().updateConfiguration(config, getResources().getDisplayMetrics());
					prefsManager.getPrefs().edit().putString(PARAM_LANG, lang_code).commit();

				} catch (Exception e) {
					Log.e("Erro in Language", e.toString());
				}

				if (!isFirstTime) {
					getActivity().setResult(Activity.RESULT_OK);
					getActivity().finish();

				} else {
					isFirstTime = false;
				}
			}
		}, 500);
	}
}
