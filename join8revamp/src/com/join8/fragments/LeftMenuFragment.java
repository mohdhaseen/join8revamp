package com.join8.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.listeners.MenuListener;
import com.join8.model.Registration;
import com.join8.utils.CryptoManager;
import com.join8.utils.TypefaceUtils;

public class LeftMenuFragment extends Fragment implements OnClickListener {
	private LinearLayout lnrUserInfo, lnrUser, lnrHome, lnrLogin, lnrRegister, lnrCity, lnrLanaguge, lnrStaff, lnrFeedback, lnrPolicy, lnrLogout, leftMenu;
	private MenuListener listener;
	private TextView tvUser;
	private CryptoManager prefsmanagaer;

	public void setMenuListner(MenuListener listener) {
		this.listener = listener;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		prefsmanagaer = CryptoManager.getInstance(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.leftmenu, container, false);
		Log.e("in left menu", "left menu");
		lnrUserInfo = (LinearLayout) v.findViewById(R.id.lnrUserInfo);
		lnrUser = (LinearLayout) v.findViewById(R.id.lnrUser);
		lnrHome = (LinearLayout) v.findViewById(R.id.lnrHome);
		lnrLogin = (LinearLayout) v.findViewById(R.id.lnrLogin);
		lnrRegister = (LinearLayout) v.findViewById(R.id.lnrRegister);
		lnrCity = (LinearLayout) v.findViewById(R.id.lnrCity);
		lnrLanaguge = (LinearLayout) v.findViewById(R.id.lnrlanguage);
		lnrStaff = (LinearLayout) v.findViewById(R.id.lnrStaff);
		lnrFeedback = (LinearLayout) v.findViewById(R.id.lnrFeedback);
		lnrPolicy = (LinearLayout) v.findViewById(R.id.lnrPolicy);
		lnrLogout = (LinearLayout) v.findViewById(R.id.lnrLogout);
		leftMenu = (LinearLayout) v.findViewById(R.id.leftMenu);
		tvUser = (TextView) v.findViewById(R.id.tvUser);
		leftMenu.setVisibility(View.GONE);
		lnrHome.setOnClickListener(this);
		lnrUser.setOnClickListener(this);
		lnrLogin.setOnClickListener(this);
		lnrRegister.setOnClickListener(this);
		lnrCity.setOnClickListener(this);
		lnrLanaguge.setOnClickListener(this);
		lnrStaff.setOnClickListener(this);
		lnrFeedback.setOnClickListener(this);
		lnrPolicy.setOnClickListener(this);
		lnrLogout.setOnClickListener(this);

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvUser));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvUserProfile));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvHome));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvStaff));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvPolicy));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvFeedback));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvLanguage));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvLogin));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvRegister));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvLogout));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvCity));

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	public void isLoggedIn(boolean isLogin) {

		if (isLogin) {
			Gson gson = new Gson();
			try {
				Registration registration = gson.fromJson(prefsmanagaer.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
				tvUser.setText(registration.getFirstName() + " " + registration.getLastName());
			} catch (Exception e) {
				e.printStackTrace();
			}
			lnrLogin.setVisibility(View.GONE);
			lnrRegister.setVisibility(View.GONE);
			lnrUser.setVisibility(View.VISIBLE);
			lnrHome.setVisibility(View.VISIBLE);
			lnrUserInfo.setVisibility(View.VISIBLE);
			lnrStaff.setVisibility(View.VISIBLE);
			lnrLogout.setVisibility(View.VISIBLE);
		} else {
			lnrUserInfo.setVisibility(View.GONE);
			lnrLogin.setVisibility(View.VISIBLE);
			lnrRegister.setVisibility(View.VISIBLE);
			lnrUser.setVisibility(View.GONE);
			lnrHome.setVisibility(View.VISIBLE);
			lnrStaff.setVisibility(View.GONE);
			lnrLogout.setVisibility(View.GONE);

		}
		leftMenu.setVisibility(View.VISIBLE);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.lnrHome:
			listener.onClick(HomeScreen.MENU_HOME);
			break;
		case R.id.lnrLogin:
			listener.onClick(HomeScreen.MENU_LOGIN);
			break;
		case R.id.lnrRegister:
			listener.onClick(HomeScreen.MENU_REGISTRATION);
			break;
		case R.id.lnrLogout:
			listener.onClick(HomeScreen.MENU_LOGOUT);
			break;

		default:
			break;
		}
	}
}
