package com.join8.fragments;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.join8.HomeScreen;
import com.join8.MyHandler;
import com.join8.R;
import com.join8.Toast;
import com.join8.listeners.Requestlistener;
import com.join8.model.City;
import com.join8.model.ResponseCity;
import com.join8.model.StaffLogin;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.SymmetricEncryption;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.UrlUtil;
import com.join8.utils.Utils;
import com.microsoft.windowsazure.messaging.NotificationHub;
import com.microsoft.windowsazure.notifications.NotificationsManager;

public class LoginFragment extends Fragment implements OnClickListener, Requestlistener {

	private static final String TAG = LoginFragment.class.getSimpleName();

	private EditText edtEmail, edtPassword;
	private TextView btnSigin, tvSelect, tvRegister, tvDontHave;
	private NetworkManager networManager;
	private int signn_request = -1;
	private String access_token;
	private CryptoManager prefsManager;
	private ResponseCity city;

	private NotificationHub hub = null;

	private String deviceID;

	private String deviceType;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		networManager = NetworkManager.getInstance();
		networManager.addListener(this);
		networManager.isProgressVisible(true);
		prefsManager = CryptoManager.getInstance(getActivity());

		try {

			NotificationsManager.handleNotifications(getActivity(), ConstantData.SENDER_ID, MyHandler.class);
			hub = new NotificationHub("hubnotification", ConstantData.HUB_NOTI_CONNECTION_STRING, getActivity());

		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Error in Notification Hub", e.toString());
		}
		deviceID = Secure.getString(this.getActivity().getContentResolver(), Secure.ANDROID_ID);
		deviceType = "Android";
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.login, container, false);
		edtEmail = (EditText) v.findViewById(R.id.edtEmail);
		edtPassword = (EditText) v.findViewById(R.id.edtPassword);
		btnSigin = (TextView) v.findViewById(R.id.btnSignin);
		tvSelect = (TextView) v.findViewById(R.id.tvSelect);
		tvRegister = (TextView) v.findViewById(R.id.tvSignUp);
		tvDontHave = (TextView) v.findViewById(R.id.tvAlready);
		tvRegister.setOnClickListener(this);
		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(tvRegister, Typeface.BOLD);
		mTypefaceUtils.applyTypeface(tvDontHave);
		mTypefaceUtils.applyTypeface(edtEmail);
		mTypefaceUtils.applyTypeface(edtPassword);
		mTypefaceUtils.applyTypeface(btnSigin);
		mTypefaceUtils.applyTypeface(tvSelect);
		btnSigin.setOnClickListener(this);

		edtEmail.requestFocus();

		ScrollView scroview = (ScrollView) v.findViewById(R.id.scroview);
		scroview.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(edtPassword.getWindowToken(), 0);
				}
				return false;
			}
		});

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		StringBuilder cityName = new StringBuilder(getResources().getString(R.string.tvSignInLogin)+" - ");
		city = new Gson().fromJson(prefsManager.getPrefs().getString(SelectCityFragment.PARAM_CITYDATA_NEW, ""), ResponseCity.class);
		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
			cityName.append(city.getNameEN());
		} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_SIMPLECHINSESE)) {
			cityName.append(city.getName());
		} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_TRADITIONALCHINESE)) {
			cityName.append(city.getName());
		}
		if (!getActivity().isFinishing()) {
			((HomeScreen) getActivity()).setCustomTitle(cityName.toString(), Typeface.NORMAL);
			((HomeScreen) getActivity()).changeActionItems(true);
		}

		access_token = prefsManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");

	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.btnSignin) {

			/*
			 * if (!TextUtils.isEmpty(edtEmail.getText().toString()) &&
			 * !TextUtils.isEmpty(edtPassword.getText().toString())) {
			 * 
			 * if (Utils.checkDigit(edtEmail.getText().toString())) { if
			 * (edtEmail.getText().toString().length() < 8 ||
			 * edtEmail.getText().toString().length() > 11) {
			 * Toast.displayText(getActivity(), getString(R.string.enterPhone));
			 * return; } } else if
			 * (!Utils.checkEmail(edtEmail.getText().toString())) {
			 * Toast.displayText(getActivity(),
			 * getString(R.string.emailnovalideerror)); return; }
			 * 
			 * signn_request = networManager.addRequest(
			 * Join8RequestBuilder.getSigninRequest(access_token,
			 * edtEmail.getText().toString(), edtPassword.getText().toString(),
			 * TextUtils.isDigitsOnly(edtEmail.getText().toString()) ?
			 * Join8RequestBuilder.Phone : Join8RequestBuilder.Email,
			 * city.getCountryCode(), city.getPartitionKey()),
			 * RequestMethod.POST, getActivity(),
			 * Join8RequestBuilder.METHOD_SIGNIN);
			 * 
			 * } else { Toast.displayText(getActivity(),
			 * getString(R.string.enterAllValues)); }
			 */

			if (!TextUtils.isEmpty(edtEmail.getText().toString()) && !TextUtils.isEmpty(edtPassword.getText().toString())) {
				try {
					signn_request = networManager.addRequest(new HashMap<String, String>(), getActivity(), true, UrlUtil.getUserLoginUrl(), getRequestData(), ConstantData.method_post);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				Toast.displayText(getActivity(), getString(R.string.enterAllValues));
			}
		} else if (v.getId() == R.id.tvAlready) {

		} else if (v.getId() == R.id.tvSignUp) {
			if (!getActivity().isFinishing())
				((HomeScreen) getActivity()).clearBackStackEntry();
			// RegistrationFragment register = new RegistrationFragment();
			Bundle b = new Bundle();
			b.putString(RegistrationFormFragment.PARAMS_TOKEN, access_token);
			RegistrationFormFragment register = new RegistrationFormFragment();
			register.setArguments(b);
			getActivity().getSupportFragmentManager().beginTransaction().replace(HomeScreen.FRAGMENT_CONTAINER, register).addToBackStack(null).commit();
		}
	}

	private String getRequestData() throws Exception {
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		String userName = edtEmail.getText().toString();
		if (!userName.contains("-"))
			userName = city.getPhoneCode() + "-" + userName;
		StaffLogin req = new StaffLogin(userName, SymmetricEncryption.encrypt(edtPassword.getText().toString(), ConstantData.ENCRPITION_KEY), "", deviceID, city.getCityCode(), deviceType, "Mobile");
		return gson.toJson(req, StaffLogin.class);
	}

	@Override
	public void onSuccess(int id, String response) {

		Log.e(TAG, "Login Response" + response);

		if (id == signn_request) {

			try {

				final JSONObject jObject = new JSONObject(response);

				if (jObject.has("ActivationCode")) {

					if (!jObject.getString("UserStatus").equalsIgnoreCase("active")) {
						showMessage("", getString(R.string.activate_account));
					} else {

						prefsManager.getPrefs().edit().putBoolean(HomeScreen.PREFS_LOGIN, true).commit();
						prefsManager.getPrefs().edit().putString(ConstantData.USER_NAME, jObject.getString("UserName")).commit();
						prefsManager.getPrefs().edit().putString(HomeScreen.PARAMS_USERDATA, jObject.toString()).commit();
						Toast.displayText(getActivity(), getString(R.string.signin_success));

						if (!Utils.isEmpty(jObject.getString("Tag"))) {
							new Thread(new Runnable() {
								@Override
								public void run() {

									try {
										hub.register(ConstantData.RegistrationID, jObject.getString("Tag"));
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}).start();
						}

						if (!getActivity().isFinishing()) {
							((HomeScreen) getActivity()).clearBackStackEntry();
							HomeFragmentNew homeFragment = new HomeFragmentNew();
							getActivity().getSupportFragmentManager().beginTransaction().add(HomeScreen.FRAGMENT_CONTAINER, homeFragment).addToBackStack(null).commit();

						}
					}

				} else {
					Toast.displayText(getActivity(), getString(R.string.loginfail));
					prefsManager.getPrefs().edit().putBoolean(HomeScreen.PREFS_LOGIN, false);
				}
			} catch (JSONException e) {
				e.printStackTrace();
				Toast.displayText(getActivity(), getString(R.string.loginfail));
			}
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		networManager.removeListeners(this);
	}

	private void showMessage(final String title, final String message) {

		AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
		dialog.setTitle(title);
		dialog.setMessage(message);
		dialog.setPositiveButton(getString(R.string.tvOk), new android.content.DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		dialog.show();

	}
}
