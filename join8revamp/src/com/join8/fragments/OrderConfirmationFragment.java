package com.join8.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.listeners.Requestlistener;
import com.join8.model.OrderData;
import com.join8.model.Registration;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.CryptoManager;
import com.join8.utils.KeyboardUtils;
import com.join8.utils.Log;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;

public class OrderConfirmationFragment extends Fragment implements OnClickListener, Requestlistener {
	
	private static final String TAG = OrderConfirmationFragment.class.getSimpleName();
	
	private String menuData, total;
	private TypefaceUtils mTypefaceUtils;
	private TextView btnChangeUserProfile, btnTimePicker;
	private TextView tvUserName, tvAddress, tvPhone, tvTotal, btnPlaceOrder;
	private EditText edtEmail, edtRemark;
	private ToggleButton chkPickUp;
	private int userProfile_request = -1, draft_order = -1, pending_order = -1;
	private NetworkManager networManager;
	private CryptoManager prefManager;
	private String access_token = "";
	private ArrayList<Registration> userProfileList = null;
	private String[] strArrayUserProfile = null;
	private long selectedDate;
	private int selectedUserProfileIndex = -1;
	private int CompanyId;
	private String listEmail = "", mobileNumber = "";
	private int year = -1, month, day, hours, minute;
	private OrderData orderData;
	private Date tempOpenDialogDate;
	private RelativeLayout rltCheckBox;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (savedInstanceState != null) {
			menuData = savedInstanceState.getString("menuData", null);
			total = savedInstanceState.getString("total", null);
			CompanyId = savedInstanceState.getInt("CompanyId", -1);
			if (savedInstanceState.containsKey("orderData")) {
				orderData = (OrderData) savedInstanceState.getSerializable("orderData");
			}
		} else {
			menuData = getArguments().getString("menuData", null);
			total = getArguments().getString("total", "");
			CompanyId = getArguments().getInt("CompanyId", -1);
			if (getArguments().containsKey("orderData")) {
				orderData = (OrderData) getArguments().getSerializable("orderData");
			}
		}
		networManager = NetworkManager.getInstance();
		networManager.addListener(this);
		prefManager = CryptoManager.getInstance(getActivity());
		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {

		super.onSaveInstanceState(outState);
		outState.putSerializable("menuData", menuData);
		outState.putSerializable("total", total);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.order_conformation_fragment, container, false);
		mappingWidgets(mView);
		return mView;
	}

	private void mappingWidgets(View mView) {

		View view = getActivity().getActionBar().getCustomView();
		((ImageButton) view.findViewById(R.id.btnFav)).setVisibility(View.INVISIBLE);
		((TextView) view.findViewById(R.id.txtTitle)).setText(getString(R.string.order_confirmation));
		((TextView) view.findViewById(R.id.txtOpen)).setVisibility(View.GONE);

		mTypefaceUtils = TypefaceUtils.getInstance(getActivity());

		btnChangeUserProfile = (TextView) mView.findViewById(R.id.btnChangeUserProfile);
		btnTimePicker = (TextView) mView.findViewById(R.id.btnTimePicker);

		tvUserName = (TextView) mView.findViewById(R.id.tvUserName);
		tvAddress = (TextView) mView.findViewById(R.id.tvAddress);
		tvPhone = (TextView) mView.findViewById(R.id.tvPhone);
		tvTotal = (TextView) mView.findViewById(R.id.tvTotal);
		btnPlaceOrder = (TextView) mView.findViewById(R.id.btnPlaceOrder);

		edtEmail = (EditText) mView.findViewById(R.id.edtEmail);
		edtRemark = (EditText) mView.findViewById(R.id.edtRemark);
		rltCheckBox = (RelativeLayout) mView.findViewById(R.id.rltCheckBox);

		chkPickUp = (ToggleButton) mView.findViewById(R.id.chkPickUp);

		mTypefaceUtils.applyTypeface(btnChangeUserProfile);
		mTypefaceUtils.applyTypeface(btnTimePicker);
		mTypefaceUtils.applyTypeface(tvUserName);
		mTypefaceUtils.applyTypeface(tvAddress);
		mTypefaceUtils.applyTypeface(tvPhone);
		mTypefaceUtils.applyTypeface(tvTotal);
		mTypefaceUtils.applyTypeface(edtEmail);
		mTypefaceUtils.applyTypeface(edtRemark);
		mTypefaceUtils.applyTypeface(btnPlaceOrder);
		mTypefaceUtils.applyTypeface(((TextView) mView.findViewById(R.id.tvIWillPickup)));

		tvTotal.setText(total);

		if (orderData != null) {
			if (!Utils.isEmpty(orderData.getListEmails())) {
				edtEmail.setText(orderData.getListEmails());
			} else if (!Utils.isEmpty(orderData.getMobileNumber())) {
				String mobileNumber = orderData.getMobileNumber();
				if (mobileNumber.contains("-")) {
					mobileNumber = mobileNumber.substring(mobileNumber.indexOf("-") + 1);
				}
				if (mobileNumber.contains("|")) {
					mobileNumber = mobileNumber.substring(mobileNumber.indexOf("|") + 1);
				}
				edtEmail.setText(mobileNumber);
			}
			if (!Utils.isEmpty(orderData.getRemarks())) {
				edtRemark.setText(orderData.getRemarks());
			}
			chkPickUp.setChecked(orderData.isIsOrderPickUpByUser());
			selectedDate = orderData.getOrderDeadlineDate().getTime();
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(selectedDate);
			year = calendar.get(Calendar.YEAR);
			month = calendar.get(Calendar.MONTH);
			day = calendar.get(Calendar.DAY_OF_MONTH);
			hours = calendar.get(Calendar.HOUR_OF_DAY);
			minute = calendar.get(Calendar.MINUTE);
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd - hh:mm a", Locale.getDefault());
			String dateTime = format.format(orderData.getOrderDeadlineDate());
			btnTimePicker.setText(dateTime);
		}

		edtRemark.setOnFocusChangeListener(new OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {

				if (!hasFocus) {
					KeyboardUtils.hideKeyboard(edtRemark);
				}
			}
		});

		btnChangeUserProfile.setOnClickListener(this);
		btnTimePicker.setOnClickListener(this);
		btnPlaceOrder.setOnClickListener(this);
		tvPhone.setOnClickListener(this);
		rltCheckBox.setOnClickListener(this);
		loadUserProfile();

	}

	@Override
	public void onClick(View v) {
		if (v == btnChangeUserProfile) {
			showUserProfilePopup();
		} else if (v == btnTimePicker) {
			tempOpenDialogDate = new Date();
			showDateTimeDialog(btnTimePicker);
		} else if (v == btnPlaceOrder) {
			try {
				if (btnTimePicker.getText().toString().length() == 0 || btnTimePicker.getText().equals(getString(R.string.select_date))) {
					Toast.displayText(getActivity(), getString(R.string.date_required));
					return;
				} else if (edtEmail.getText().toString().trim().length() > 0) {
					String[] emails = edtEmail.getText().toString().split(",");
					String invalidEmail = "";
					for (int i = 0; i < emails.length; i++) {
						if (!Utils.checkEmail(emails[i].trim())) {
							invalidEmail += emails[i].trim() + "\n";
						}
					}
					if (invalidEmail.length() > 0) {
						Toast.displayText(getActivity(), invalidEmail + getString(R.string.emails_not_valid));
						return;
					} else {
						listEmail = edtEmail.getText().toString();
					}

				}
				if (orderData == null) {
					placedOrder(false, 90, 0);
				} else {
					placedOrder(true, orderData.getOrderStatus(), Integer.parseInt(orderData.getOrderId()));
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else if (v == tvPhone) {
			if (!Utils.isEmpty(userProfileList.get(selectedUserProfileIndex).getPhoneNumber())) {
				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel:" + PhoneNumberUtils.stringFromStringAndTOA(PhoneNumberUtils.stripSeparators(userProfileList.get(selectedUserProfileIndex).getPhoneNumber()), PhoneNumberUtils.TOA_International)));
				startActivity(intent);
			}

		} else if (v == rltCheckBox) {
			if (chkPickUp.isChecked()) {
				chkPickUp.setChecked(false);
			} else {
				chkPickUp.setChecked(true);
			}
		}

	}

	private void placedOrder(boolean DeletePreviousDetails, int OrderStatus, int OrderId) {
		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		networManager.isProgressVisible(true);
		if (OrderStatus == 90) {
			draft_order = networManager.addRequest(Join8RequestBuilder.createUpdateDraftOrder(access_token, registration.getPartitionKey(), OrderStatus, userProfileList.get(selectedUserProfileIndex).getFirstName(), userProfileList.get(selectedUserProfileIndex).getLastName(),
					OrderId, userProfileList.get(selectedUserProfileIndex).getProfileName(), "/Date(" + selectedDate + ")/", CompanyId, chkPickUp.isChecked(), !edtEmail.getText().toString().trim().equals("") ? true : false, DeletePreviousDetails, menuData, listEmail,
					mobileNumber, null, edtRemark.getText().toString()), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_CREATE_UPDATE_DRAFT_ORDER);
		} else {
			if (orderData == null) {
				pending_order = networManager.addRequest(Join8RequestBuilder.createUpdateDraftOrder(access_token, registration.getPartitionKey(), OrderStatus, userProfileList.get(selectedUserProfileIndex).getFirstName(), userProfileList.get(selectedUserProfileIndex)
						.getLastName(), OrderId, userProfileList.get(selectedUserProfileIndex).getProfileName(), "/Date(" + selectedDate + ")/", CompanyId, chkPickUp.isChecked(), !edtEmail.getText().toString().trim().equals("") ? true : false, DeletePreviousDetails, menuData,
						listEmail, mobileNumber, null, edtRemark.getText().toString()), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_CREATE_UPDATE_DRAFT_ORDER);
			} else {
				pending_order = networManager.addRequest(Join8RequestBuilder.createUpdateDraftOrder(access_token, registration.getPartitionKey(), OrderStatus, userProfileList.get(selectedUserProfileIndex).getFirstName(), userProfileList.get(selectedUserProfileIndex)
						.getLastName(), OrderId, userProfileList.get(selectedUserProfileIndex).getProfileName(), "/Date(" + selectedDate + ")/", CompanyId, chkPickUp.isChecked(), !edtEmail.getText().toString().trim().equals("") ? true : false, DeletePreviousDetails, menuData,
						listEmail, mobileNumber, orderData, edtRemark.getText().toString()), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_CREATE_UPDATE_DRAFT_ORDER);
			}

		}

	}

	private void loadUserProfile() {
		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		networManager.isProgressVisible(true);
		userProfile_request = networManager.addRequest(Join8RequestBuilder.getProfileOfUser(access_token, registration.getPartitionKey()), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_GET_PROFILES_OF_USER);
	}

	@Override
	public void onSuccess(int id, String response) {

		if (id == userProfile_request) {
			Log.e("response", response);

			try {
				JSONArray jsonArray = new JSONArray(response);
				if (jsonArray.length() > 0) {
					userProfileList = new ArrayList<Registration>();
					strArrayUserProfile = new String[jsonArray.length()];
					for (int i = 0; i < jsonArray.length(); i++) {
						Registration registration = new Gson().fromJson(jsonArray.getJSONObject(i).toString(), Registration.class);
						userProfileList.add(registration);

						if (registration.getProfileName().contains("-")) {
							strArrayUserProfile[i] = registration.getProfileName().substring(registration.getProfileName().lastIndexOf("-") + 1);
						} else {
							strArrayUserProfile[i] = registration.getProfileName();
						}

						// strArrayUserProfile[i] =
						// registration.getProfileName().substring(registration.getEmail().length()
						// + 1).replace("-", "");

						if (orderData == null) {
							if (registration.isDefaultProfile()) {
								selectedUserProfileIndex = i;
								setUserData(registration);
							}
						} else {
							if (registration.getProfileName().equalsIgnoreCase(orderData.getUserProfile())) {
								selectedUserProfileIndex = i;
								setUserData(registration);
							}
						}
					}

					if (selectedUserProfileIndex == -1) {
						for (int i = 0; i < userProfileList.size(); i++) {
							Registration registration = userProfileList.get(i);
							userProfileList.add(registration);
							strArrayUserProfile[i] = registration.getProfileName().substring(registration.getEmail().length() + 1).replace("-", "");
							if (registration.isDefaultProfile()) {
								selectedUserProfileIndex = i;
								setUserData(registration);
								break;
							}
						}
					}
				}
			} catch (Exception e) {
				Log.e("Error", e.toString());
			}
		} else if (id == draft_order) {
			Log.e("response", response);
			try {
				JSONObject jsonObject = new JSONObject(response);

				placedOrder(true, 10, jsonObject.getInt("LastInsertedValue"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (id == pending_order) {
			try {
				Log.e("response", response);
				if (!response.toLowerCase().contains("Failure".toLowerCase())) {
					OrderDetailFragment.clearOrderList();
					OrderDetailFragment.companyId = -1;

					if (orderData == null) {
						Intent intent = new Intent();
						// intent.putExtra("requestCode", 33);
						getActivity().setResult(Activity.RESULT_OK);
						getActivity().finish();
					} else {
						getActivity().finish();
					}
				} else {
					Toast.displayText(getActivity(), getString(R.string.failed));
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	private void setUserData(Registration registration) {

		String profileName = "";
		if (registration.getProfileName().contains("-")) {
			profileName = registration.getProfileName().substring(registration.getProfileName().lastIndexOf("-") + 1);
		} else {
			profileName = registration.getProfileName();
		}
		btnChangeUserProfile.setText(profileName);

		tvUserName.setText(registration.getFirstName() + " " + registration.getLastName());
		if (!Utils.isEmpty(registration.getAddress1())) {
			tvAddress.setText(registration.getAddress1());
			if (!Utils.isEmpty(registration.getAddress2())) {
				tvAddress.setText(registration.getAddress1() + "\n" + registration.getAddress2());
			}
			if (!Utils.isEmpty(registration.getZIP())) {
				tvAddress.setText(registration.getAddress1() + "\n" + registration.getAddress2() + "\n" + registration.getZIP());
			}

		} else {
			tvAddress.setText(getString(R.string.no_address));
		}
		if (!Utils.isEmpty(registration.getMobileNumber())) {

			String mobileNumber = registration.getMobileNumber();

			if (mobileNumber.contains("|")) {
				mobileNumber = mobileNumber.replaceAll("\\|", "-");
			}

			if (mobileNumber.contains("-")) {

				String[] str = mobileNumber.split("-");
				tvPhone.setText(str[str.length - 1]);

			} else {
				tvPhone.setText(mobileNumber);
			}

			tvPhone.setCompoundDrawablesWithIntrinsicBounds(R.drawable.theme_btn_call, 0, 0, 0);

		} else {
			tvPhone.setText(getString(R.string.no_phono_number));
			tvPhone.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		}

	}

	private void showUserProfilePopup() {

		AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
		ab.setTitle(getString(R.string.change_user_profile));
		ab.setSingleChoiceItems(strArrayUserProfile, selectedUserProfileIndex, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface d, int choice) {
				selectedUserProfileIndex = choice;
				setUserData(userProfileList.get(choice));
				((Dialog) d).dismiss();
			}
		});

		ab.show();

	}

	private void showDateTimeDialog(final TextView t) {
		final Date date = new Date();
		LayoutInflater li = LayoutInflater.from(getActivity());
		final View view = li.inflate(R.layout.datetime_dialog, null);

		Builder dialog = new AlertDialog.Builder(getActivity());
		dialog.setTitle(getString(R.string.select_date_and_time));

		final DatePicker dtPicker = (DatePicker) view.findViewById(R.id.datePicker);
		final TimePicker tmPicker = (TimePicker) view.findViewById(R.id.timePicker);

		dtPicker.setCalendarViewShown(false);

		if (year != -1) {
			// dtPicker.updateDate(year,month,day,null);

			dtPicker.updateDate(year, month, day);
			tmPicker.setCurrentHour(hours);
			tmPicker.setCurrentMinute(minute);
		}
		dialog.setView(view);

		dialog.setPositiveButton(getString(R.string.set), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

				// int day = dtPicker.getDayOfMonth();
				// tempDate = day;
				// int month = (dtPicker.getMonth() + 1);
				// tempMonth = month;
				// int year = dtPicker.getYear();
				// tempYear = year;

				// dateTime = "" + (month > 9 ? "" + month : "0" + month);
				// dateTime += "/" + (day > 9 ? "" + day : "0" + day);
				// dateTime += "/" + year;

				date.setDate(dtPicker.getDayOfMonth());
				date.setMonth(dtPicker.getMonth());
				date.setYear(dtPicker.getYear() - 1900);
				date.setHours(tmPicker.getCurrentHour());
				date.setMinutes(tmPicker.getCurrentMinute());
				date.setSeconds(new Date().getSeconds());

				if (date.getTime() >= tempOpenDialogDate.getTime()) {
					try {
						year = dtPicker.getYear();
						day = dtPicker.getDayOfMonth();
						month = dtPicker.getMonth();
						hours = tmPicker.getCurrentHour();
						minute = tmPicker.getCurrentMinute();

						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd - hh:mm a");
						String dateTime = format.format(date);
						t.setText(dateTime);
						format = new SimpleDateFormat("EEEE yyyy-MM-dd HH:mm:ss");
						getUTCTIME(format.format(date));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					Toast.displayText(getActivity(), getString(R.string.error_past_date));
				}

			}
		});

		dialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

			}
		});
		dialog.show();
	}

	private void getUTCTIME(String date) {
		// String result = "";
		try {

			SimpleDateFormat sourceFormat = new SimpleDateFormat("EEEE yyyy-MM-dd HH:mm:ss");
			sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date parsed = sourceFormat.parse(date); // => Date is in UTC now
			Log.d("CurrentDate", "" + parsed);
			selectedDate = parsed.getTime();

			// TimeZone tUTC = TimeZone.getTimeZone("UTC");
			// SimpleDateFormat destFormat = new
			// SimpleDateFormat("EEEE yyyy-MM-dd HH:mm:ss");
			// destFormat.setTimeZone(tUTC);
			//
			// result = destFormat.format(parsed);
			//
			// selectedDate = destFormat.parse(result).getTime() + 3600 * 1000 *
			// 8;

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
