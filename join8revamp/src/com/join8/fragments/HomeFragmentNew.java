package com.join8.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.adpaters.ImageSlideAdapter;
import com.join8.adpaters.MainCategoryAdapter;
import com.join8.listeners.BannerLoadedListener;
import com.join8.listeners.Requestlistener;
import com.join8.model.ResponseCity;
import com.join8.model.ShopCategory;
import com.join8.model.TokenModel;
import com.join8.network.NetworkManager;
import com.join8.utils.CirclePageIndicator;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.UrlUtil;
import com.join8.utils.Utils;

public class HomeFragmentNew extends Fragment implements Requestlistener, OnItemClickListener, BannerLoadedListener {
	private static final long ANIM_VIEWPAGER_DELAY = 5000;
	private static final long ANIM_VIEWPAGER_DELAY_USER_VIEW = 10000;
	private NetworkManager networManager;
	private CryptoManager prefManager;
	private String tokenCloud;
	private int ShopCategoryRequest;
	private HashMap<String, String> auth;
	private GridView gridview;
	private int tokenRequest = -1;
	private ViewPager mViewPager;
	private CirclePageIndicator mIndicator;
	private ArrayList<ResponseCity> cities;
	private boolean stopSliding = false;
	private Handler handler;
	private Runnable animateViewPager;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		networManager = NetworkManager.getInstance();
		networManager.addListener(this);
		prefManager = CryptoManager.getInstance(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.home_screen_new, container, false);
		((HomeScreen) getActivity()).initActionBar();
		gridview = (GridView) v.findViewById(R.id.gridview);
		gridview.setOnItemClickListener(this);
		mViewPager = (ViewPager) v.findViewById(R.id.view_pager);
		mIndicator = (CirclePageIndicator) v.findViewById(R.id.indicator);
		mViewPager.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				v.getParent().requestDisallowInterceptTouchEvent(true);
				switch (event.getAction()) {

					case MotionEvent.ACTION_CANCEL :
						break;

					case MotionEvent.ACTION_UP :
						// calls when touch release on ViewPager
						if (cities != null && cities.size() != 0) {
							stopSliding = false;
							runnable(cities.size());
							handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY_USER_VIEW);
						}
						break;

					case MotionEvent.ACTION_MOVE :
						// calls when ViewPager touch
						if (handler != null && stopSliding == false) {
							stopSliding = true;
							handler.removeCallbacks(animateViewPager);
						}
						break;
				}
				return false;
			}
		});
		((HomeScreen) getActivity()).setBannerListener(this);
		return v;
	}

	public void runnable(final int size) {
		handler = new Handler();
		animateViewPager = new Runnable() {
			public void run() {
				if (!stopSliding) {
					if (mViewPager.getCurrentItem() == size - 1) {
						mViewPager.setCurrentItem(0);
					} else {
						mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1, true);
					}
					handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);
				}
			}
		};
	}

	@Override
	public void onStart() {
		getFragmentManager().beginTransaction().replace(R.id.fragment_container_child, new PromotionFragmentNew()).commit();
		super.onStart();
	}
	@Override
	public void onResume() {
		super.onResume();
		cities = ((HomeScreen) getActivity()).arrCitiesNew;
		setBanner();

	}
	@Override
	public void onStop() {
		super.onStop();
		if (networManager != null) {
			networManager.removeListeners(this);
		}
	}
	@Override
	public void onPause() {
		if (handler != null) {
			// Remove callback
			handler.removeCallbacks(animateViewPager);
		}
		super.onPause();
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (isAdded()) {

			((HomeScreen) getActivity()).changeActionItems(false);
			loadShopMainCategories();
		}
	}

	private void loadShopMainCategories() {
		// call only if token is not available
		tokenCloud = prefManager.getPrefs().getString(ConstantData.TOKEN_CLOUD, null);
		if (tokenCloud == null) {
			tokenRequest = networManager.addRequest(new HashMap<String, String>(), getActivity(), true, UrlUtil.getTokenUrlForCloud(), getTokenRequest(), ConstantData.method_post);
		} else {
			auth = new HashMap<String, String>();
			auth.put("Authorization", "bearer " + tokenCloud);
			ShopCategoryRequest = networManager.addRequest(auth, getActivity(), true, UrlUtil.getShopCategoryUrl(), "", ConstantData.method_get);
		}
	}
	private String getTokenRequest() {
		return "{\"UserName\":\"join8PosAdmin\",\"Password\":\"j01Np05@dm1n\"}";
	}

	@Override
	public void onSuccess(int id, String response) {

		if (isAdded() && !Utils.isEmpty(response)) {

			if (id == tokenRequest) {
				TokenModel token = new Gson().fromJson(response, TokenModel.class);
				tokenCloud = token.getAccess_token();
				prefManager.getPrefs().edit().putString(ConstantData.TOKEN_CLOUD, tokenCloud).commit();
				auth = new HashMap<String, String>();
				auth.put("Authorization", "bearer " + tokenCloud);
				ShopCategoryRequest = networManager.addRequest(auth, getActivity(), true, UrlUtil.getShopCategoryUrl(), "", ConstantData.method_get);
			} else if (id == ShopCategoryRequest) {
				Gson gson = new Gson();
				ArrayList<ShopCategory> list = gson.fromJson(response, new TypeToken<List<ShopCategory>>() {
				}.getType());
				gridview.setAdapter(new MainCategoryAdapter(getActivity(), list));
			}
		}
	}

	private void setBanner() {
		cities = ((HomeScreen) getActivity()).arrCitiesNew;
		if (null == cities)
			return;
		mViewPager.setAdapter(new ImageSlideAdapter(getActivity(), cities));
		mIndicator.setViewPager(mViewPager);
		runnable(cities.size());
		// Re-run callback
		handler.postDelayed(animateViewPager, ANIM_VIEWPAGER_DELAY);
	}

	@Override
	public void onError(int id, String message) {
		if (isAdded()) {
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Toast.displayTextForShortTime(getActivity(), parent.getItemAtPosition(position).toString());
	}

	@Override
	public void onBAnnerGet() {
		setBanner();

	}

}
