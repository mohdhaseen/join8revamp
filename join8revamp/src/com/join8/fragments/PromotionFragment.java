package com.join8.fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.costum.android.widget.LoadMoreListView;
import com.costum.android.widget.LoadMoreListView.OnLoadMoreListener;
import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.adpaters.PromotionListAdapter;
import com.join8.listeners.Requestlistener;
import com.join8.model.City;
import com.join8.model.CuisineData;
import com.join8.model.Promotions;
import com.join8.model.Registration;
import com.join8.model.ResponseCity;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.KeyboardUtils;
import com.join8.utils.TypefaceUtils;

public class PromotionFragment extends Fragment implements OnClickListener, Requestlistener {

	private static final String TAG = PromotionFragment.class.getSimpleName(); 
	private EditText edtSearchView;
	private Button btnCuisin, btnNewDish, btnFavorite, btnAll;
	public boolean isPromotion = true;
	private int promotion_request = -1;
	private int loadmore_request = -1;
	private int cuisin_request = -1;
	private int dish_request = -1;

	private NetworkManager networManager;
	private int starIndex = 0;
	private int count = 25;
	private CryptoManager prefManager;
	private String access_token = "";
	public ResponseCity city;
	private String searchtext = "";
	private TextView tvSwitchSearch;
	private ArrayList<CuisineData> arrCuisins;
	private ArrayList<Promotions> arrPromotionsData;
	public static final String PARAM_PROMOTION = "promotion";
	private String[] strArrayCuisins;
	private String strCuisinType = "0";
	private String strNewDishType = "0";
	private PromotionListAdapter promotionListAdapter;
	private LoadMoreListView lstSearch;
	private int totalrecords = 0;
	private LinearLayout lnrSearchText, lnrSearch;
	private int selectedCuisin = 0, selectedNewDish = 0;

	private boolean dialogOpenFlag = true;
	private TextView tvresults;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		networManager = NetworkManager.getInstance();
		prefManager = CryptoManager.getInstance(getActivity());
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.promotions, container, false);
		btnFavorite = (Button) v.findViewById(R.id.btnFavorite);
		btnAll = (Button) v.findViewById(R.id.btnAll);
		edtSearchView = (EditText) v.findViewById(R.id.edtSearchView);
		btnCuisin = (Button) v.findViewById(R.id.btnCuisin);
		btnNewDish = (Button) v.findViewById(R.id.btnNewDish);
		lstSearch = (LoadMoreListView) v.findViewById(R.id.lstRestorent);
		lnrSearchText = (LinearLayout) v.findViewById(R.id.lnrSearchText);
		lnrSearch = (LinearLayout) v.findViewById(R.id.lnrSearch);
		tvSwitchSearch = (TextView) v.findViewById(R.id.tvSwitchSearch);
		tvresults = (TextView) v.findViewById(R.id.tvresults);

		lstSearch.setEmptyView(tvresults);

		edtSearchView.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

				if (event != null && event.getAction() != KeyEvent.ACTION_DOWN)
					return false;
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					starIndex = 0;
					KeyboardUtils.hideKeyboard(v);
					doLoadData();

					return true;
				}

				return false;

			}
		});

		lstSearch.setOnLoadMoreListener(new OnLoadMoreListener() {

			@Override
			public void onLoadMore() {
				Log.e("on Load More", "in Load mOrew");
				if (arrPromotionsData.size() <= totalrecords) {

					starIndex = arrPromotionsData.size();
					doLoadMore();
				}

			}
		});
		lstSearch.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				KeyboardUtils.hideKeyboard(view);
				Promotions promotions = (Promotions) parent.getAdapter().getItem(position);
				Bundle b = new Bundle();
				b.putString(PARAM_PROMOTION, new Gson().toJson(promotions));
				PromotionDetails promotiondetaisFragment = new PromotionDetails();
				promotiondetaisFragment.setArguments(b);
				getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(PromotionDetails.class.getName()).add(HomeScreen.FRAGMENT_CONTAINER, promotiondetaisFragment).commit();

			}
		});

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(btnCuisin);
		mTypefaceUtils.applyTypeface(btnNewDish);
		mTypefaceUtils.applyTypeface(btnFavorite);
		mTypefaceUtils.applyTypeface(btnAll);
		mTypefaceUtils.applyTypeface(edtSearchView);
		mTypefaceUtils.applyTypeface(tvSwitchSearch);
		mTypefaceUtils.applyTypeface(tvresults);

		btnCuisin.setOnClickListener(this);
		btnNewDish.setOnClickListener(this);

		btnFavorite.setOnClickListener(this);
		btnAll.setOnClickListener(this);
		lnrSearchText.setOnClickListener(this);

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (!getActivity().isFinishing()) {
			((HomeScreen) getActivity()).changeActionItems(false);
		}
		city = ((HomeScreen) getActivity()).getCityData();
		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
		arrPromotionsData = new ArrayList<Promotions>();
		LoadCuisins();
		// LoadDishTypes();
		btnFavorite.performClick();

	}

	private void LoadCuisins() {
		Log.e("In home Fragment---", "AccessToken-----" + access_token);
		cuisin_request = networManager.addRequest(Join8RequestBuilder.getCityList(access_token), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_CUISIN);
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onClick(View v) {
		KeyboardUtils.hideKeyboard(edtSearchView);
		switch (v.getId()) {
		case R.id.btnFavorite:
			strCuisinType = "0";
			strNewDishType = "0";
			starIndex = 0;
			btnFavorite.setBackgroundDrawable(getResources().getDrawable(R.drawable.restourent_bg_hover));
			btnAll.setBackgroundDrawable(getResources().getDrawable(R.drawable.menu_bg));
			btnFavorite.setTextColor(Color.WHITE);
			btnAll.setTextColor(getResources().getColor(R.color.orangebg));
			btnFavorite.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.favh), null, null, null);
			btnAll.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.restaurantn), null, null, null);
			isPromotion = true;

			doLoadData();
			break;
		case R.id.btnAll:
			starIndex = 0;
			strCuisinType = "0";
			strNewDishType = "0";

			btnFavorite.setBackgroundDrawable(getResources().getDrawable(R.drawable.restourent_bg));
			btnAll.setBackgroundDrawable(getResources().getDrawable(R.drawable.menu_bg_hover));
			btnFavorite.setTextColor(getResources().getColor(R.color.orangebg));
			btnAll.setTextColor(Color.WHITE);
			btnFavorite.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.favf), null, null, null);
			btnAll.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.restauranth), null, null, null);
			isPromotion = false;

			doLoadData();
			break;

		case R.id.btnCuisin:
			if (dialogOpenFlag) {
				dialogOpenFlag = false;
				showCuisinPopup();
			}
			break;
		case R.id.btnNewDish:
			if (dialogOpenFlag) {
				dialogOpenFlag = false;
				showNewDishPopup();
			}
			break;
		case R.id.lnrSearchText:
			KeyboardUtils.hideKeyboard(v);
			if (tvSwitchSearch.getText().toString().equals(getString(R.string.search_promotion))) {
				tvSwitchSearch.setText(getString(R.string.promotionList));
				lnrSearch.setVisibility(View.VISIBLE);
			} else {
				tvSwitchSearch.setText(getString(R.string.search_promotion));
				lnrSearch.setVisibility(View.GONE);
			}
			break;
		}
	}

	public void doLoadData() {
		lstSearch.onLoadMoreComplete();
		arrPromotionsData.clear();
		networManager.isProgressVisible(true);
		searchtext = edtSearchView.getText().toString();
		if (city != null && city.getNameEN().length() > 0) {
			lstSearch.setVisibility(View.GONE);
			Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
			String userId = "";
			if (registration != null)
				userId = registration.getPartitionKey();
			promotion_request = networManager.addRequest(Join8RequestBuilder.getPromotionsRequest(access_token, city != null ? city.getPhoneCode() : "852", searchtext, "" + count, strCuisinType, strNewDishType, "" + starIndex, userId), RequestMethod.POST, getActivity(),
					isPromotion ? Join8RequestBuilder.METHOD_FAVORITEPROMOTIONS : Join8RequestBuilder.METHOD_PROMOTIONS);

		} else {
			Toast.displayText(getActivity(), getString(R.string.selectCity));
		}

	}

	public void doLoadMore() {
		networManager.isProgressVisible(false);
		searchtext = edtSearchView.getText().toString();
		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		String userId = "";
		if (registration != null)
			userId = registration.getPartitionKey();
		loadmore_request = networManager.addRequest(Join8RequestBuilder.getPromotionsRequest(access_token, city != null ? city.getPhoneCode() : "852", searchtext, "" + count, strCuisinType, strNewDishType, "" + starIndex, userId), RequestMethod.POST, getActivity(),
				isPromotion ? Join8RequestBuilder.METHOD_FAVORITEPROMOTIONS : Join8RequestBuilder.METHOD_PROMOTIONS);

	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		networManager.removeListeners(this);
	}

	@Override
	public void onSuccess(int id, String response) {
		if (response != null) {
			if (id == cuisin_request) {
				Log.e(getTag(), "" + response);
				try {
					JSONArray jsonArray = new JSONArray(response);
					arrCuisins = new ArrayList<CuisineData>();
					strArrayCuisins = new String[jsonArray.length() + 1];
					strArrayCuisins[0] = "";
					if (jsonArray != null && jsonArray.length() > 0) {
						for (int i = 0; i < jsonArray.length(); i++) {
							CuisineData cuisin = new Gson().fromJson(jsonArray.getJSONObject(i).toString(), CuisineData.class);
							arrCuisins.add(cuisin);
							if (HomeScreen.LANGUAGE.equals(ConstantData.LANG_ENGLISH))
								strArrayCuisins[i + 1] = cuisin.getTypeValueEN();
							else if (HomeScreen.LANGUAGE.equals(ConstantData.LANG_SIMPLECHINSESE))
								strArrayCuisins[i + 1] = cuisin.getTypeValueSC();
							else if (HomeScreen.LANGUAGE.equals(ConstantData.LANG_TRADITIONALCHINESE))
								strArrayCuisins[i + 1] = cuisin.getTypeValueTC();
						}

					}

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else if (id == dish_request) {
				Log.e(getTag(), "" + response);
			} else if (id == promotion_request) {
				Log.e(getTag(), "" + response);
				if (response.contains("No record found") || response.equals("")) {
					return;
				} else {
					try {

						JSONArray jsonArray = new JSONArray(response);

						if (jsonArray != null && jsonArray.length() > 0) {

							totalrecords = Integer.parseInt(jsonArray.getJSONObject(0).getString("TotalRecords"));
							for (int i = 0; i < jsonArray.length(); i++) {
								Promotions searchData = new Gson().fromJson(jsonArray.getJSONObject(i).toString(), Promotions.class);
								arrPromotionsData.add(searchData);
							}

						}

						if (arrPromotionsData.size() > 0) {
							Log.e(getTag(), "in if");
							promotionListAdapter = new PromotionListAdapter(getActivity(), R.layout.row_promotions, arrPromotionsData);
							lstSearch.setAdapter(promotionListAdapter);
							lstSearch.setVisibility(View.VISIBLE);
						} else {
							Log.e(getTag(), "in else");
							arrPromotionsData.clear();
							promotionListAdapter.notifyDataSetChanged();
							lstSearch.invalidate();

						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						if (arrPromotionsData != null && promotionListAdapter != null) {
							arrPromotionsData.clear();
							promotionListAdapter.notifyDataSetChanged();
							lstSearch.invalidate();
						}

					}

				}
			} else if (id == loadmore_request) {
				Log.e(getTag(), "" + response);
				try {

					JSONArray jsonArray = new JSONArray(response);

					if (jsonArray != null && jsonArray.length() > 0) {

						totalrecords = Integer.parseInt(jsonArray.getJSONObject(0).getString("TotalRecords"));
						for (int i = 0; i < jsonArray.length(); i++) {
							Promotions searchData = new Gson().fromJson(jsonArray.getJSONObject(i).toString(), Promotions.class);
							arrPromotionsData.add(searchData);
						}
					}

					if (arrPromotionsData.size() > 0) {
						Log.e(getTag(), "in if");
						promotionListAdapter.notifyDataSetChanged();
						lstSearch.invalidate();
						lstSearch.onLoadMoreComplete();
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	private void showCuisinPopup() {

		AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
		ab.setTitle(getString(R.string.app_name));
		ab.setSingleChoiceItems(strArrayCuisins, selectedCuisin, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface d, int choice) {
				if (choice == 0) {
					btnCuisin.setText(getString(R.string.cuisin));
					strCuisinType = "0";
					selectedCuisin = 0;
				} else {
					btnCuisin.setText(strArrayCuisins[choice]);
					strCuisinType = arrCuisins.get(choice - 1).getTypeId();
					selectedCuisin = choice;

				}
				dialogOpenFlag = true;
				((Dialog) d).dismiss();
				doLoadData();
			}
		});
		AlertDialog alert = ab.create();

		alert.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {

				dialogOpenFlag = true;
			}
		});
		alert.show();

	}

	private void showNewDishPopup() {
		final String[] arrDishes = getResources().getStringArray(R.array.disharray);
		AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
		ab.setTitle(getString(R.string.app_name));
		ab.setSingleChoiceItems(arrDishes, selectedNewDish, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface d, int choice) {
				btnNewDish.setText("" + arrDishes[choice]);

				switch (choice) {
				case 0:
					btnNewDish.setText(getString(R.string.newpromotion));
					strNewDishType = "0";
					selectedNewDish = 0;
					break;
				case 1:
					strNewDishType = "1";
					selectedNewDish = 1;
					break;
				case 2:
					strNewDishType = "-3";
					selectedNewDish = 2;
					break;
				case 3:
					strNewDishType = "-7";
					selectedNewDish = 3;
					break;
				case 4:
					strNewDishType = "-30";
					selectedNewDish = 4;
					break;

				default:
					break;
				}
				dialogOpenFlag = true;
				((Dialog) d).dismiss();
				doLoadData();
			}
		});

		AlertDialog alert = ab.create();

		alert.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {

				dialogOpenFlag = true;
			}
		});
		alert.show();

	}

}
