package com.join8.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.google.gson.Gson;
import com.join8.BookingActivity;
import com.join8.HomeScreen;
import com.join8.PaymentActivity;
import com.join8.R;
import com.join8.SelectDateTimeActivity;
import com.join8.SelectTableActivity;
import com.join8.Toast;
import com.join8.adpaters.NumberPickerGalleryAdapter;
import com.join8.listeners.Requestlistener;
import com.join8.model.BookingData;
import com.join8.model.OrderData;
import com.join8.model.Registration;
import com.join8.model.TableTypeData;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.Log;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;

public class BookingFragment extends Fragment implements OnClickListener, Requestlistener, OnCheckedChangeListener {

	private static final String TAG = BookingFragment.class.getSimpleName();
	private static final int INTENT_SELECT_DATETIME = 1;
	private static final int INTENT_SELECT_TABLE_TYPE = 2;
	private static final int INTENT_PAYMENT = 3;

	private TextView btnSelectUserProfile, btnSendBooking;
	private EditText edtFirstName, edtLastName, edtPhone, edtEmail, edtRemark;
	private LinearLayout lnrTableType, lnrPrice;
	private RadioGroup rbgBookingType;
	private RadioButton rbtAuto, rbtManual;

	private TypefaceUtils mTypefaceUtils;
	private BookingData bookingData;
	private NetworkManager networManager;
	private CryptoManager prefManager;
	private String access_token = "";
	private int selectedUserProfileIndex = -1, userProfile_request = -1, getBooking_detail = -1, placeBooking = -1;
	private long selectedDate;
	// private int year = -1, month, day, hours, minute;
	private ArrayList<Registration> userProfileList = null;
	private String[] strArrayUserProfile = null;
	// private ArrayList<DressCodeData> dressCodeList = null;
	// private int derssCodeSelected = 0;
	// private Date tempOpenDialogDate;
	private String companyId = "", tableBookingId = "";
	private String phoneCode = "";
	private String restaurantName = "";
	private String tableType = "";

	private Gallery galleryNoOfPeople;
	private ArrayList<Integer> listNumbers;
	private TextView lblNoOfPeople, lblTableType, lblDateTime, lblPrice, lblMyDetails;
	private TextView lastSelectedView, txtTableType, txtDateTime, txtPrice, txtNoOfPeople;
	private int bookingType = 0, selectedTimeSlotId = 0;
	private double deposit = 0;
	private Calendar calendar = null;
	private String companyName = "", companyNameEn = "";
	private boolean isEditable = true;

	private SimpleDateFormat displayFormat = new SimpleDateFormat("EEEE MMM dd hh:mma", Locale.getDefault());

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {

			if (savedInstanceState.containsKey("bookingData")) {
				bookingData = (BookingData) savedInstanceState.getSerializable("bookingData");
				bookingType = bookingData.getBookingType();
			}
			if (savedInstanceState.containsKey("companyId")) {
				companyId = savedInstanceState.getString("companyId");
			}
			if (savedInstanceState.containsKey("tableBookingId")) {
				tableBookingId = getArguments().getString("tableBookingId");
			}

			if (savedInstanceState.containsKey("restaurantName")) {
				restaurantName = getArguments().getString("restaurantName");
			}

			if (savedInstanceState.containsKey("bookingType")) {
				bookingType = getArguments().getInt("bookingType");
			}

			if (savedInstanceState.containsKey("companyName")) {
				companyName = getArguments().getString("companyName");
			}

			if (savedInstanceState.containsKey("companyNameEn")) {
				companyNameEn = getArguments().getString("companyNameEn");
			}

		} else {

			if (getArguments().containsKey("companyId")) {
				companyId = getArguments().getString("companyId");
			}

			if (getArguments().containsKey("tableBookingId")) {
				tableBookingId = getArguments().getString("tableBookingId");
			}

			if (getArguments().containsKey("restaurantName")) {
				restaurantName = getArguments().getString("restaurantName");
			}

			if (getArguments().containsKey("bookingType")) {
				bookingType = getArguments().getInt("bookingType");
			}

			if (getArguments().containsKey("companyName")) {
				companyName = getArguments().getString("companyName");
			}

			if (getArguments().containsKey("companyNameEn")) {
				companyNameEn = getArguments().getString("companyNameEn");
			}
		}

		networManager = NetworkManager.getInstance();
		prefManager = CryptoManager.getInstance(getActivity());
		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");

		calendar = Calendar.getInstance();
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.booking_fragment, container, false);

		mTypefaceUtils = TypefaceUtils.getInstance(getActivity());

		btnSelectUserProfile = (TextView) mView.findViewById(R.id.btnSelectUserProfile);
		btnSendBooking = (TextView) mView.findViewById(R.id.btnSendBooking);
		edtFirstName = (EditText) mView.findViewById(R.id.edtFirstName);
		edtLastName = (EditText) mView.findViewById(R.id.edtLastName);
		edtPhone = (EditText) mView.findViewById(R.id.edtPhone);
		edtEmail = (EditText) mView.findViewById(R.id.edtEmail);
		edtRemark = (EditText) mView.findViewById(R.id.edtRemark);
		// txtDressCode = (TextView) mView.findViewById(R.id.txtDressCode);
		galleryNoOfPeople = (Gallery) mView.findViewById(R.id.galleryNoOfPeople);
		lblNoOfPeople = (TextView) mView.findViewById(R.id.lblNoOfPeople);
		lblTableType = (TextView) mView.findViewById(R.id.lblTableType);
		lblDateTime = (TextView) mView.findViewById(R.id.lblDateTime);
		lblPrice = (TextView) mView.findViewById(R.id.lblPrice);
		lblMyDetails = (TextView) mView.findViewById(R.id.lblMyDetails);
		txtTableType = (TextView) mView.findViewById(R.id.txtTableType);
		txtDateTime = (TextView) mView.findViewById(R.id.txtDateTime);
		txtPrice = (TextView) mView.findViewById(R.id.txtPrice);
		rbgBookingType = (RadioGroup) mView.findViewById(R.id.rbgBookingType);
		rbtAuto = (RadioButton) mView.findViewById(R.id.rbtAuto);
		rbtManual = (RadioButton) mView.findViewById(R.id.rbtManual);

		txtNoOfPeople = (TextView) mView.findViewById(R.id.txtNoOfPeople);
		lnrTableType = (LinearLayout) mView.findViewById(R.id.lnrTableType);
		lnrPrice = (LinearLayout) mView.findViewById(R.id.lnrPrice);

		if (bookingType == 2) {
			// pre-set : 2
			lnrTableType.setVisibility(View.VISIBLE);
			lnrPrice.setVisibility(View.VISIBLE);
			rbtAuto.setChecked(true);
			rbtManual.setEnabled(true);
		} else {
			// free-selectable : 3
			lnrTableType.setVisibility(View.GONE);
			lnrPrice.setVisibility(View.GONE);
			rbtManual.setChecked(true);
			rbtAuto.setEnabled(false);
		}

		mTypefaceUtils.applyTypeface(btnSelectUserProfile);
		mTypefaceUtils.applyTypeface(btnSendBooking);
		mTypefaceUtils.applyTypeface(edtFirstName);
		mTypefaceUtils.applyTypeface(edtLastName);
		mTypefaceUtils.applyTypeface(edtPhone);
		mTypefaceUtils.applyTypeface(edtEmail);
		mTypefaceUtils.applyTypeface(edtRemark);
		mTypefaceUtils.applyTypeface(lblNoOfPeople);
		mTypefaceUtils.applyTypeface(lblTableType);
		mTypefaceUtils.applyTypeface(lblDateTime);
		mTypefaceUtils.applyTypeface(lblPrice);
		mTypefaceUtils.applyTypeface(lblMyDetails);
		mTypefaceUtils.applyTypeface(txtTableType);
		mTypefaceUtils.applyTypeface(txtDateTime);
		mTypefaceUtils.applyTypeface(txtPrice);
		mTypefaceUtils.applyTypeface(rbtAuto);
		mTypefaceUtils.applyTypeface(rbtManual);

		btnSelectUserProfile.setOnClickListener(this);
		btnSendBooking.setOnClickListener(this);
		edtPhone.setOnClickListener(this);
		txtTableType.setOnClickListener(this);
		txtDateTime.setOnClickListener(this);
		rbgBookingType.setOnCheckedChangeListener(this);

		galleryNoOfPeople.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {

				if (arg1 != null) {

					if (lastSelectedView != null) {

						lastSelectedView.setTextColor(Color.BLACK);
						lastSelectedView.setBackgroundColor(Color.WHITE);
					}

					lastSelectedView = (TextView) arg1;

					lastSelectedView.setTextColor(Color.WHITE);
					lastSelectedView.setBackgroundResource(R.drawable.orange_circle);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		return mView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		listNumbers = new ArrayList<Integer>();
		for (int i = 0; i < 99; i++) {
			listNumbers.add(i + 1);
		}
		galleryNoOfPeople.setAdapter(new NumberPickerGalleryAdapter(getActivity(), listNumbers));

		phoneCode = prefManager.getPrefs().getString(SelectCityFragment.PARAM_PHONE_CODE, "");

		if (!Utils.isEmpty(tableBookingId) && !tableBookingId.equals("-1")) {
			isEditable = false;
			getBookingDetail();
		} else {

			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
			calendar.set(Calendar.HOUR_OF_DAY, 19);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);

			selectedDate = calendar.getTimeInMillis();
			// tempOpenDialogDate = calendar.getTime();

			// txtDateTime.setText(displayFormat.format(calendar.getTime()));
			galleryNoOfPeople.setSelection(1);

			// year = calendar.get(Calendar.YEAR);
			// month = calendar.get(Calendar.MONTH);
			// day = calendar.get(Calendar.DAY_OF_MONTH);
			// hours = calendar.get(Calendar.HOUR_OF_DAY);
			// minute = calendar.get(Calendar.MINUTE);

			// txtdate.setText(format.format(calendar.getTime()));
			// format = new SimpleDateFormat("hh:mm a", Locale.getDefault());
			// txtTime.setText(format.format(calendar.getTime()));
			// txtSeats.setText("2");
		}

		loadUserProfile();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == INTENT_SELECT_DATETIME && resultCode == Activity.RESULT_OK) {

			selectedTimeSlotId = data.getIntExtra("selectedTimeSlotId", 0);
			deposit = data.getDoubleExtra("deposit", 0.0);
			txtPrice.setText("$" + deposit);

			calendar.setTimeInMillis(data.getLongExtra("date", Calendar.getInstance().getTimeInMillis()));

			txtDateTime.setText(displayFormat.format(calendar.getTime()));

		} else if (requestCode == INTENT_SELECT_TABLE_TYPE && resultCode == Activity.RESULT_OK) {

			if (!txtTableType.getText().toString().trim().equals("") && !txtTableType.getText().toString().trim().equals(data.getStringExtra("tableType"))) {

				calendar = Calendar.getInstance();
				deposit = 0;
				txtDateTime.setText("");
				txtPrice.setText("");
			}

			txtTableType.setText(data.getStringExtra("tableType"));
			txtPrice.setText("$" + Utils.formatDecimal("" + data.getDoubleExtra("deposit", 0.0)));
			tableType = data.getStringExtra("tableTypeEn");

		} else if (requestCode == INTENT_PAYMENT && resultCode == Activity.RESULT_OK) {

			getActivity().setResult(Activity.RESULT_OK, new Intent());
			getActivity().finish();
		}
	}

	private void loadUserProfile() {
		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		networManager.isProgressVisible(true);
		userProfile_request = networManager.addRequest(Join8RequestBuilder.getProfileOfUser(access_token, registration.getPartitionKey()), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_GET_PROFILES_OF_USER);
	}

	private void getBookingDetail() {
		networManager.isProgressVisible(true);
		getBooking_detail = networManager.addRequest(Join8RequestBuilder.getParticularBookingOfUser(access_token, tableBookingId), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_GET_PARTICULAR_BOOKING_OF_USER);
	}

	// private void getDressCode() {
	// networManager.isProgressVisible(false);
	// getDressCode =
	// networManager.addRequest(Join8RequestBuilder.getCityList(access_token),
	// RequestMethod.POST, getActivity(),
	// Join8RequestBuilder.METHOD_GET_ALL_DRESSCODES);
	// }

	private void placeBooking(String tableBookingId, int status) {

		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		networManager.isProgressVisible(true);

		String phone = edtPhone.getText().toString().trim();
		if (!phone.startsWith(phoneCode)) {
			phone = phoneCode + "-" + edtPhone.getText().toString().trim();
		}

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault());
		long bookingTimeNow = getUTCTIME(dateFormat.format(Calendar.getInstance().getTime()));

		placeBooking = networManager.addRequest(
				Join8RequestBuilder.addBookTable(access_token, tableBookingId, status, registration.getPartitionKey(), edtFirstName.getText().toString(), edtLastName.getText().toString(), userProfileList.get(selectedUserProfileIndex).getProfileName(), "/Date(" + selectedDate
						+ ")/", companyId, phone, edtRemark.getText().toString(), (Integer) galleryNoOfPeople.getSelectedItem(), selectedTimeSlotId, rbtAuto.isChecked() ? 2 : 3, deposit, "/Date(" + bookingTimeNow + ")/", companyName, companyNameEn), RequestMethod.POST,
				getActivity(), Join8RequestBuilder.METHOD_BOOK_TABLE);

	}

	private void setActionBarTitle(String bookingStatus) {

		if (!bookingStatus.equals("")) {
			bookingStatus = "-" + bookingStatus;
		}

		View view = getActivity().getActionBar().getCustomView();
		((TextView) view.findViewById(R.id.txtTitle)).setText(getString(R.string.booking) + bookingStatus);
	}

	@Override
	public void onSuccess(int id, String response) {

		try {

			if (id == userProfile_request) {

				Log.e("response", response);

				try {

					JSONArray jsonArray = new JSONArray(response);

					if (jsonArray.length() > 0) {

						userProfileList = new ArrayList<Registration>();
						strArrayUserProfile = new String[jsonArray.length()];

						for (int i = 0; i < jsonArray.length(); i++) {
							Registration registration = new Gson().fromJson(jsonArray.getJSONObject(i).toString(), Registration.class);
							userProfileList.add(registration);

							if (registration.getProfileName().contains("-")) {
								strArrayUserProfile[i] = registration.getProfileName().substring(registration.getProfileName().lastIndexOf("-") + 1);
							} else {
								strArrayUserProfile[i] = registration.getProfileName();
							}

							if (bookingData == null) {
								if (registration.isDefaultProfile()) {
									selectedUserProfileIndex = i;
									setUserData(registration);
								}
							} else {
								if (registration.getProfileName().equalsIgnoreCase(bookingData.getUserProfile())) {
									selectedUserProfileIndex = i;
									if (edtFirstName.getText().toString().trim().equals("")) {
										setUserData(registration);
									}
								}
							}
						}
						if (selectedUserProfileIndex == -1) {
							for (int i = 0; i < userProfileList.size(); i++) {
								Registration registration = userProfileList.get(i);
								userProfileList.add(registration);

								if (registration.getProfileName().contains("-")) {
									strArrayUserProfile[i] = registration.getProfileName().substring(registration.getProfileName().lastIndexOf("-") + 1);
								} else {
									strArrayUserProfile[i] = registration.getProfileName();
								}

								if (registration.isDefaultProfile()) {
									selectedUserProfileIndex = i;
									if (edtFirstName.getText().toString().trim().equals("")) {
										setUserData(registration);
									}
									break;
								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					Log.e("Error", e.toString());
				}

			} else if (id == getBooking_detail) {

				try {

					if (response.startsWith("no")) {

						Toast.displayText(getActivity(), getString(R.string.norecorderror));
						getActivity().finish();

					} else {

						bookingData = new Gson().fromJson(response, BookingData.class);
						companyId = bookingData.getCompanyId();
						companyName = bookingData.getCompanyName();
						companyNameEn = bookingData.getBookingNameEn();
						bookingType = bookingData.getBookingType();

						setOtherData();

						btnSelectUserProfile.setVisibility(View.GONE);
						edtFirstName.setEnabled(false);
						edtLastName.setEnabled(false);
						edtPhone.setFocusable(false);
						edtPhone.setClickable(true);
						edtPhone.setKeyListener(null);
						edtPhone.setTag("call");
						galleryNoOfPeople.setEnabled(false);
						txtDateTime.setEnabled(false);
						txtNoOfPeople.setVisibility(View.VISIBLE);
						galleryNoOfPeople.setVisibility(View.GONE);
						edtEmail.setEnabled(false);
						edtRemark.setEnabled(false);
						btnSendBooking.setVisibility(View.GONE);
						rbgBookingType.setVisibility(View.GONE);

						setActionBarTitle(OrderData.getOrderStatusString(getActivity(), bookingData.getBookingStatus()));
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			} else if (id == placeBooking) {

				if (!response.equalsIgnoreCase("No Record Found") && !response.equalsIgnoreCase("Failure")) {

					JSONObject jObj = new JSONObject(response);

					if (rbtAuto.isChecked() && deposit > 0) {

						tableBookingId = jObj.getString("TableBookingId");

						String refId = tableBookingId + "_Book_" + Calendar.getInstance().get(Calendar.YEAR);

						Intent intent = new Intent(getActivity(), PaymentActivity.class);
						intent.putExtra("amount", deposit);
						intent.putExtra("refId", refId);
						intent.putExtra("companyId", companyId);
						intent.putExtra("companyName", companyName);
						intent.putExtra("title", ((BookingActivity) getActivity()).getActivityTitle());
						startActivityForResult(intent, INTENT_PAYMENT);

					} else {

						getActivity().setResult(Activity.RESULT_OK, new Intent());
						getActivity().finish();
					}

				} else {
					Toast.displayText(getActivity(), getString(R.string.placing_order_error));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {

		if (checkedId == R.id.rbtAuto) {

			// pre-set
			lnrTableType.setVisibility(View.VISIBLE);
			lnrPrice.setVisibility(View.VISIBLE);
			rbtManual.setEnabled(true);

			selectedTimeSlotId = 0;
			deposit = 0;
			tableType = "";
			calendar = Calendar.getInstance();
			txtPrice.setText("");
			txtDateTime.setText("");
			txtTableType.setText("");
			galleryNoOfPeople.setSelection(1);

		} else if (checkedId == R.id.rbtManual) {

			// free-selectable
			lnrTableType.setVisibility(View.GONE);
			lnrPrice.setVisibility(View.GONE);
			rbtManual.setEnabled(false);

			selectedTimeSlotId = 0;
			deposit = 0;
			tableType = "";
			calendar = Calendar.getInstance();
			txtPrice.setText("");
			txtDateTime.setText("");
			txtTableType.setText("");
			galleryNoOfPeople.setSelection(1);
		}
	}

	@Override
	public void onClick(View v) {

		try {

			if (v == btnSelectUserProfile) {

				showUserProfilePopup();

			} else if (v == btnSendBooking) {

				uploadBooking();

			} else if (v == edtPhone && edtPhone.getTag() != null && edtPhone.getTag().equals("call") && !edtPhone.getText().toString().trim().equals("")) {

				try {

					if (TextUtils.isEmpty(edtPhone.getText().toString().trim())) {

						Toast.displayText(getActivity(), getActivity().getString(R.string.no_phono_number));

					} else {

						Intent intent = new Intent(Intent.ACTION_CALL);
						intent.setData(Uri.parse("tel:" + PhoneNumberUtils.stringFromStringAndTOA(PhoneNumberUtils.stripSeparators(edtPhone.getText().toString().trim()), PhoneNumberUtils.TOA_International)));
						startActivity(intent);
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			} else if (v == txtTableType) {

				if (isEditable) {
					Intent intent = new Intent(getActivity(), SelectTableActivity.class);
					intent.putExtra("restaurantName", restaurantName);
					intent.putExtra("companyId", companyId);
					startActivityForResult(intent, INTENT_SELECT_TABLE_TYPE);
				} else {

					TableTypeData data = new TableTypeData();
					data.setBookingName(bookingData.getBookingName());
					data.setBookingNameEn(bookingData.getBookingNameEn());
					data.setTerms(bookingData.getTerms());
					data.setTermsEn(bookingData.getTermsEn());
					data.setBookingDeposit(bookingData.getDeposit());
					data.setBookingImageLink(bookingData.getBookingImageLink());

					Bundle bundle = new Bundle();
					bundle.putSerializable("data", data);

					BookingTableDetailsFragment fragment = new BookingTableDetailsFragment();
					fragment.setArguments(bundle);

					FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
					ft.addToBackStack(BookingTableDetailsFragment.class.getName());
					ft.add(R.id.fragment_container, fragment);
					ft.commit();
				}

			} else if (v == txtDateTime) {

				Intent intent = new Intent(getActivity(), SelectDateTimeActivity.class);
				intent.putExtra("restaurantName", restaurantName);
				intent.putExtra("companyId", companyId);
				intent.putExtra("bookingType", rbtAuto.isChecked() ? 2 : 3);
				intent.putExtra("tableType", tableType);
				intent.putExtra("selectedTimeSlotId", selectedTimeSlotId);
				if (!Utils.isEmpty(txtDateTime.getText().toString().trim())) {
					intent.putExtra("selectedDate", calendar.getTimeInMillis());
				} else {
					intent.putExtra("selectedDate", Calendar.getInstance().getTimeInMillis());
				}
				startActivityForResult(intent, INTENT_SELECT_DATETIME);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void uploadBooking() {

		if (rbtAuto.isChecked() && Utils.isEmpty(txtTableType.getText().toString().trim())) {
			Toast.displayText(getActivity(), getString(R.string.tabletyperequirederror));
		} else if (Utils.isEmpty(txtDateTime.getText().toString().trim())) {
			Toast.displayText(getActivity(), getString(R.string.datetimerequirederror));
		} else if (Utils.isEmpty(edtFirstName.getText().toString().trim())) {
			Toast.displayText(getActivity(), getString(R.string.firstnamerequirederror));
		} else if (Utils.isEmpty(edtLastName.getText().toString().trim())) {
			Toast.displayText(getActivity(), getString(R.string.lastnamerequirederror));
		} else if (Utils.isEmpty(edtPhone.getText().toString().trim())) {
			Toast.displayText(getActivity(), getString(R.string.phonerequirederror));
		} else if (edtPhone.getText().toString().length() < 8 || edtPhone.getText().toString().length() > 11) {
			Toast.displayText(getActivity(), getString(R.string.enterPhone));
		} else if (Utils.isEmpty(edtEmail.getText().toString().trim())) {
			Toast.displayText(getActivity(), getString(R.string.enterEmail));
		} else if (!Utils.checkEmail(edtEmail.getText().toString().trim())) {
			Toast.displayText(getActivity(), getString(R.string.emailnovalideerror));
		} else {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault());
			selectedDate = getUTCTIME(dateFormat.format(calendar.getTime()));

			int bookingStatus = 10;

			if (rbtAuto.isChecked() && deposit > 0) {
				bookingStatus = 30;
			}

			if (Utils.isEmpty(tableBookingId) || tableBookingId.equals("-1")) {
				placeBooking("0", bookingStatus);
			} else {
				placeBooking(tableBookingId, bookingStatus);
			}
		}
	}

	private void showUserProfilePopup() {

		AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
		ab.setTitle(getString(R.string.change_user_profile));
		ab.setSingleChoiceItems(strArrayUserProfile, selectedUserProfileIndex, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface d, int choice) {
				selectedUserProfileIndex = choice;
				setUserData(userProfileList.get(choice));
				((Dialog) d).dismiss();
			}

		});

		ab.show();

	}

	private void setUserData(Registration registration) {

		String profileName = "";
		if (registration.getProfileName().contains("-")) {
			profileName = registration.getProfileName().substring(registration.getProfileName().lastIndexOf("-") + 1);
		} else {
			profileName = registration.getProfileName();
		}
		btnSelectUserProfile.setText(profileName);

		// if (edtFirstName.getText().toString().trim().equals("")) {
		edtFirstName.setText(registration.getFirstName());
		// }

		// if (edtLastName.getText().toString().trim().equals("")) {
		edtLastName.setText(registration.getLastName());
		// }

		edtEmail.setText(registration.getEmail());

		// if (edtPhone.getText().toString().trim().equals("") &&
		// !Utils.isEmpty(registration.getPhoneNumber())) {
		if (!Utils.isEmpty(registration.getMobileNumber())) {
			String mobileNumber = registration.getMobileNumber();
			mobileNumber = mobileNumber.replaceAll("\\|", "-");
			if (mobileNumber.contains("-")) {
				mobileNumber = mobileNumber.substring(mobileNumber.indexOf("-") + 1);
			}
			edtPhone.setText(mobileNumber);
		}
	}

	private void setOtherData() {

		if (bookingData != null) {

			if (bookingType == 2) {
				// pre-set
				lnrTableType.setVisibility(View.VISIBLE);
				lnrPrice.setVisibility(View.VISIBLE);
			} else {
				// free-selectable
				lnrTableType.setVisibility(View.GONE);
				lnrPrice.setVisibility(View.GONE);
			}

			edtFirstName.setText(bookingData.getFirstName());
			edtLastName.setText(bookingData.getLastName());

			if (!Utils.isEmpty(bookingData.getMobileNumber())) {
				String mobileNumber = bookingData.getMobileNumber();
				if (mobileNumber.contains("-")) {
					mobileNumber = mobileNumber.substring(mobileNumber.indexOf("-") + 1);
				}
				if (mobileNumber.contains("|")) {
					mobileNumber = mobileNumber.substring(mobileNumber.indexOf("|") + 1);
				}
				edtPhone.setText(mobileNumber);
			}

			if (!Utils.isEmpty(bookingData.getUserId())) {
				edtEmail.setText(bookingData.getUserId());
			}

			if (!Utils.isEmpty(bookingData.getRemarks())) {
				edtRemark.setText(bookingData.getRemarks());
			}

			selectedDate = bookingData.getDateToBook().getTime();
			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(selectedDate);

			txtDateTime.setText(displayFormat.format(calendar.getTime()));

			// year = calendar.get(Calendar.YEAR);
			// month = calendar.get(Calendar.MONTH);
			// day = calendar.get(Calendar.DAY_OF_MONTH);
			// hours = calendar.get(Calendar.HOUR_OF_DAY);
			// minute = calendar.get(Calendar.MINUTE);
			// SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd",
			// Locale.getDefault());
			// // txtdate.setText(format.format(bookingData.getDateToBook()));
			// format = new SimpleDateFormat("hh:mm a", Locale.getDefault());
			// // txtTime.setText(format.format(bookingData.getDateToBook()));

			if (bookingData.getSeats() > 0) {
				galleryNoOfPeople.setSelection(bookingData.getSeats() - 1);
			} else {
				galleryNoOfPeople.setSelection(0);
			}

			txtNoOfPeople.setText("" + bookingData.getSeats());

			if (TextUtils.isEmpty(bookingData.getBookingName()) && TextUtils.isEmpty(bookingData.getBookingNameEn())) {

				lnrTableType.setVisibility(View.GONE);
				lnrPrice.setVisibility(View.GONE);

			} else {

				lnrTableType.setVisibility(View.VISIBLE);
				lnrPrice.setVisibility(View.VISIBLE);

				if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
					txtTableType.setText(bookingData.getBookingNameEn());
				} else {
					txtTableType.setText(bookingData.getBookingName());
				}
			}

			txtPrice.setText("$" + Utils.formatDecimal("" + bookingData.getDeposit()));
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (bookingData != null) {
			outState.putSerializable("bookingData", bookingData);
			outState.putString("companyId", companyId);
			outState.putString("tableBookingId", tableBookingId);
			outState.putString("restaurantName", restaurantName);
			outState.putInt("bookingType", bookingType);

		}

	}

	// private void showDateTimeDialog(final TextView t) {
	// final Date date = new Date();
	// LayoutInflater li = LayoutInflater.from(getActivity());
	// final View view = li.inflate(R.layout.datetime_dialog, null);
	//
	// Builder dialog = new AlertDialog.Builder(getActivity());
	//
	// final DatePicker dtPicker = (DatePicker)
	// view.findViewById(R.id.datePicker);
	// final TimePicker tmPicker = (TimePicker)
	// view.findViewById(R.id.timePicker);
	//
	// dtPicker.setCalendarViewShown(false);
	//
	// // if (t == txtdate) {
	// // dialog.setTitle(getString(R.string.select_date));
	// // tmPicker.setVisibility(View.GONE);
	// // dtPicker.setVisibility(View.VISIBLE);
	// // } else {
	// // dialog.setTitle(getString(R.string.select_time));
	// // tmPicker.setVisibility(View.VISIBLE);
	// // dtPicker.setVisibility(View.GONE);
	// // }
	//
	// if (year != -1) {
	//
	// dtPicker.updateDate(year, month, day);
	// tmPicker.setCurrentHour(hours);
	// tmPicker.setCurrentMinute(minute);
	//
	// }
	//
	// dialog.setView(view);
	//
	// dialog.setPositiveButton(getString(R.string.set), new
	// DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog, int whichButton) {
	//
	// // int day = dtPicker.getDayOfMonth();
	// // tempDate = day;
	// // int month = (dtPicker.getMonth() + 1);
	// // tempMonth = month;
	// // int year = dtPicker.getYear();
	// // tempYear = year;
	//
	// // dateTime = "" + (month > 9 ? "" + month : "0" + month);
	// // dateTime += "/" + (day > 9 ? "" + day : "0" + day);
	// // dateTime += "/" + year;
	//
	// date.setDate(dtPicker.getDayOfMonth());
	// date.setMonth(dtPicker.getMonth());
	// date.setYear(dtPicker.getYear() - 1900);
	//
	// date.setHours(tmPicker.getCurrentHour());
	// date.setMinutes(tmPicker.getCurrentMinute());
	// date.setSeconds(new Date().getSeconds());
	//
	// if (date.getTime() >= tempOpenDialogDate.getTime()) {
	// try {
	// year = dtPicker.getYear();
	// day = dtPicker.getDayOfMonth();
	// month = dtPicker.getMonth();
	// hours = tmPicker.getCurrentHour();
	// minute = tmPicker.getCurrentMinute();
	//
	// // if (t == txtdate) {
	// //
	// // SimpleDateFormat format = new
	// // SimpleDateFormat("yyyy-MM-dd");
	// // String dateTime = format.format(date);
	// // t.setText(dateTime);
	// // // format = new
	// // // SimpleDateFormat("EEEE yyyy-MM-dd HH:mm:ss");
	// // // getUTCTIME(format.format(date));
	// // } else {
	// //
	// // SimpleDateFormat format = new
	// // SimpleDateFormat("hh:mm a");
	// // String dateTime = format.format(date);
	// // t.setText(dateTime);
	// // }
	//
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// } else {
	// Toast.makeText(getActivity(), getString(R.string.error_past_date));
	// }
	//
	// }
	// });
	//
	// dialog.setNegativeButton(getString(R.string.cancel), new
	// DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog, int whichButton) {
	//
	// }
	// });
	// dialog.show();
	// }

	private long getUTCTIME(String date) {
		// String result = "";
		try {

			SimpleDateFormat sourceFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault());
			sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date parsed = sourceFormat.parse(date); // => Date is in UTC now
			Log.d("CurrentDate", "" + parsed);
			return parsed.getTime();

			// TimeZone tUTC = TimeZone.getTimeZone("UTC");
			// SimpleDateFormat destFormat = new
			// SimpleDateFormat("yyyy-MM-dd hh:mm a");
			// destFormat.setTimeZone(tUTC);
			//
			// result = destFormat.format(parsed);
			//
			// selectedDate = destFormat.parse(result).getTime() + 3600 * 1000 *
			// 8;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}
}
