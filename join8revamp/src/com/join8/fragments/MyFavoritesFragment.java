package com.join8.fragments;

import java.util.ArrayList;

import org.json.JSONArray;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.RestaurantActivity;
import com.join8.Toast;
import com.join8.adpaters.MyFavoritesAdapter;
import com.join8.adpaters.MyFavoritesAdapter.OnRestaurantDeleteListner;
import com.join8.listeners.Requestlistener;
import com.join8.model.Registration;
import com.join8.model.RestaurantData;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.CryptoManager;
import com.join8.utils.Log;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;

public class MyFavoritesFragment extends Fragment implements OnClickListener, Requestlistener {

	private static final String TAG = MyFavoritesFragment.class.getSimpleName();
	
	private int myFavoritesRequestId = -1, deleteFavoritesRequestId = -1;

	private ListView listView = null;
	private TextView txtEdit = null;

	private NetworkManager networManager = null;
	private CryptoManager prefManager = null;
	private String access_token = "";

	private ArrayList<RestaurantData> arrRestaurantList = null;
	private MyFavoritesAdapter adapterMyFavorites = null;

	private int deletedPosition = -1;
	private TextView tvresults;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		networManager = NetworkManager.getInstance();
		networManager.addListener(this);
		networManager.isProgressVisible(true);
		prefManager = CryptoManager.getInstance(getActivity());
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);

		loadMyFavorites();
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.my_favorites_fragment, container, false);
		mappingWidgets(mView);
		addListeners();
		init();
		return mView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
		if (!getActivity().isFinishing()) {
			((HomeScreen) getActivity()).setCustomTitle(getString(R.string.my_fav), Typeface.NORMAL);
			((HomeScreen) getActivity()).changeActionItems(true);
		}
	}

	private void mappingWidgets(View mView) {

		listView = (ListView) mView.findViewById(R.id.listView);

		txtEdit = (TextView) mView.findViewById(R.id.txtEdit);
		tvresults = (TextView) mView.findViewById(R.id.tvresults);
		listView.setEmptyView(tvresults);

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(txtEdit);
		mTypefaceUtils.applyTypeface(tvresults);
		mTypefaceUtils.applyTypeface((TextView) mView.findViewById(R.id.txtMyFavoritesTitle));

	}

	private void addListeners() {

		txtEdit.setOnClickListener(this);

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				Intent intent = new Intent(getActivity(), RestaurantActivity.class);
				intent.putExtra("restaurantData", arrRestaurantList.get(position));
				getActivity().startActivityForResult(intent, 33);
			}

		});
	}

	private void init() {

		arrRestaurantList = new ArrayList<RestaurantData>();
	}

	private void loadMyFavorites() {

		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		String userId = registration.getPartitionKey();

		networManager.isProgressVisible(true);
		myFavoritesRequestId = networManager.addRequest(Join8RequestBuilder.getMyFavoriteListRequest(access_token, userId), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_MY_FAVORITES_LIST);
	}

	public void deleteRestaurant(String watchListId) {

		networManager.isProgressVisible(true);
		deleteFavoritesRequestId = networManager.addRequest(Join8RequestBuilder.removeRestaurantFromWatchRequest(access_token, watchListId), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_REMOVE_RESTAURANT_FROM_WATCH);

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.txtEdit:

			if (txtEdit.getTag().equals("edit")) {
				txtEdit.setText(getString(R.string.done));
				txtEdit.setTag("done");
				adapterMyFavorites.setDelete(true);
				adapterMyFavorites.notifyDataSetChanged();

			} else if (txtEdit.getTag().equals("done")) {
				txtEdit.setText(getString(R.string.edit));
				txtEdit.setTag("edit");
				adapterMyFavorites.setDelete(false);
				adapterMyFavorites.notifyDataSetChanged();

			}

			break;
		}
	}

	OnRestaurantDeleteListner onRestaurantDeleteListner = new OnRestaurantDeleteListner() {

		@Override
		public void onRestaurantDeleted(int position, final String watchListId) {

			deletedPosition = position;

			if (!Utils.isEmpty(watchListId)) {

				AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
				dialog.setMessage(getString(R.string.delete_restaurant));
				dialog.setPositiveButton(getString(R.string.yes), new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						deleteRestaurant(watchListId);
					}
				});
				dialog.setNegativeButton(getString(R.string.no), new android.content.DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				});
				dialog.show();
			}
		}
	};

	@Override
	public void onSuccess(int id, String response) {

		try {

			Log.i(getTag(), "Result => " + response);

			if (response != null && !response.equals("")) {

				if (id == myFavoritesRequestId) {

					arrRestaurantList = new ArrayList<RestaurantData>();

					if (response.toLowerCase().startsWith("no record found")) {

						// Toast.makeText(getActivity(),
						// getString(R.string.norecorderror));

						txtEdit.setVisibility(View.GONE);
						listView.setVisibility(View.GONE);
						adapterMyFavorites = new MyFavoritesAdapter(getActivity(), arrRestaurantList, onRestaurantDeleteListner);
						listView.setAdapter(adapterMyFavorites);

					} else {

						JSONArray jArray = new JSONArray(response);

						if (jArray != null && jArray.length() > 0) {

							for (int i = 0; i < jArray.length(); i++) {
								RestaurantData data = new Gson().fromJson(jArray.getString(i).toString(), RestaurantData.class);
								arrRestaurantList.add(data);
							}

							listView.setVisibility(View.VISIBLE);
							adapterMyFavorites = new MyFavoritesAdapter(getActivity(), arrRestaurantList, onRestaurantDeleteListner);
							if (txtEdit.getTag().equals("done")) {
								adapterMyFavorites.setDelete(true);
							}
							listView.setAdapter(adapterMyFavorites);
						}
					}

				} else if (id == deleteFavoritesRequestId) {

					if (response.toLowerCase().startsWith("success")) {

						arrRestaurantList.remove(deletedPosition);

						adapterMyFavorites.notifyDataSetChanged();

					} else if (response.toLowerCase().startsWith("failed")) {

						Toast.displayText(getActivity(), getString(R.string.failed));
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}
}
