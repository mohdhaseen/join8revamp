package com.join8.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.FragmentTransaction;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.costum.android.widget.LoadMoreListView;
import com.google.gson.Gson;
import com.join8.BookingActivity;
import com.join8.FoodOptionActivity;
import com.join8.HomeScreen;
import com.join8.ImagePreviewScreen;
import com.join8.MapScreen;
import com.join8.R;
import com.join8.RestaurantActivity;
import com.join8.Toast;
import com.join8.adpaters.OrderDetailAdapter.OnItemDeleteListener;
import com.join8.adpaters.RestaurantMenuAdapter;
import com.join8.adpaters.RestaurantMenuItemListAdapter;
import com.join8.adpaters.RestaurantServiceHoursAdapter;
import com.join8.layout.MyProgressDialog;
import com.join8.listeners.Requestlistener;
import com.join8.model.Registration;
import com.join8.model.RestaurantData;
import com.join8.model.RestaurantMenuData;
import com.join8.model.RestaurantMenuItemData;
import com.join8.model.RestaurantServiceHoursData;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class RestaurantDetailsFragment extends Fragment implements Requestlistener, OnClickListener, OnTouchListener, OnItemDeleteListener {

	private static final String TAG = RestaurantDetailsFragment.class.getSimpleName();
	public static final int INTENT_BOOKING = 11;
	private LoadMoreListView listView = null;
	private ImageButton btnFav = null;
	private TextView txtTitle = null, txtOpen = null, txtAddress = null, txtDescription = null, txtMap = null, txtCall = null, txtBook = null, txtPromo = null;
	private ImageView imgPhoto = null, imvBook = null, imvPromo = null;
	private Gallery galleryMenu = null, galleryServiceHours = null;
	private Button btnMenu = null, btnServiceHours = null;
	private RelativeLayout relBook = null, relPromotions = null;

	private NetworkManager networManager = null;
	private CryptoManager prefManager = null;
	private int menuRequestId = -1, menuItemsRequestId1 = -1, serviceHoursRequestId = -1, isWatchListExistRequestId = -1, addToWatchListRequestId = -1, removeFromWatchListRequestId = -1;
	private String access_token = "";

	private ImageLoader imageLoader = null;
	private DisplayImageOptions options = null;

	private RestaurantData restaurantData = null;

	private ArrayList<RestaurantMenuData> listMenu = null;
	private String[] arrayServiceHours = new String[] { "", "", "", "", "", "", "" };
	private ArrayList<RestaurantMenuItemData> listRestaurantMenuItems = null;

	private ArrayList<String> listServiceHours = null;
	private RestaurantServiceHoursAdapter restaurantServiceHoursAdapter = null;

	private int prePosition = -1;
	private RestaurantMenuItemListAdapter restaurantMenuItemListAdapter = null;
	private TextView btnViewOrderSummary;
	private View footerView = null;

	private MyProgressDialog progressDialog = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e("Create", "Create");
		restaurantData = (RestaurantData) getArguments().getSerializable("restaurantData");

		networManager = NetworkManager.getInstance();
		networManager.isProgressVisible(true);
		prefManager = CryptoManager.getInstance(getActivity());

		View view = getActivity().getActionBar().getCustomView();
		btnFav = (ImageButton) view.findViewById(R.id.btnFav);
		txtTitle = (TextView) view.findViewById(R.id.txtTitle);
		txtOpen = (TextView) view.findViewById(R.id.txtOpen);

		btnFav.setOnClickListener(this);
		setFavOnOff(false);

		imageLoader = ImageLoader.getInstance();
		DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();
		int roundSize = (int) (dm.density * getResources().getDimension(R.dimen.image_height));
		options = new DisplayImageOptions.Builder().displayer(new RoundedBitmapDisplayer(roundSize)).showStubImage(R.drawable.default_image_round).showImageForEmptyUri(R.drawable.default_image_round).showImageOnFail(R.drawable.default_image_round).cacheInMemory(true)
				.cacheOnDisc(true).build();

	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);

		try {
			if (listView != null && listView.getAdapter() != null && restaurantMenuItemListAdapter != null) {
				restaurantMenuItemListAdapter.notifyDataSetChanged();
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.e("Create view", "Create view");
		View view = inflater.inflate(R.layout.restaurant_details, container, false);

		listView = (LoadMoreListView) view.findViewById(R.id.lstRestaurant);

		View headerView = getActivity().getLayoutInflater().inflate(R.layout.restaurant_details_header, null);
		listView.addHeaderView(headerView);

		footerView = getActivity().getLayoutInflater().inflate(R.layout.restaurant_menu_details_footer, null);
		// listView.addFooterView(footerView);

		listView.setSelector(R.drawable.trans);
		listView.setAdapter(null);

		btnViewOrderSummary = (TextView) footerView.findViewById(R.id.btnViewOrderSummary);
		imgPhoto = (ImageView) view.findViewById(R.id.imgPhoto);
		imvBook = (ImageView) view.findViewById(R.id.imvBook);
		imvPromo = (ImageView) view.findViewById(R.id.imvPromo);
		txtBook = (TextView) view.findViewById(R.id.txtBook);
		txtPromo = (TextView) view.findViewById(R.id.txtPromo);

		btnMenu = (Button) view.findViewById(R.id.btnMenu);
		btnServiceHours = (Button) view.findViewById(R.id.btnServiceHours);
		txtAddress = (TextView) view.findViewById(R.id.txtAddress);
		txtDescription = (TextView) view.findViewById(R.id.txtDescription);
		txtMap = (TextView) view.findViewById(R.id.txtMap);
		txtCall = (TextView) view.findViewById(R.id.txtCall);
		galleryMenu = (Gallery) view.findViewById(R.id.galleryMenu);
		galleryServiceHours = (Gallery) view.findViewById(R.id.galleryServiceHours);
		relBook = (RelativeLayout) view.findViewById(R.id.relBook);
		relPromotions = (RelativeLayout) view.findViewById(R.id.relPromotions);
		galleryMenu.setVisibility(View.GONE);
		galleryServiceHours.setVisibility(View.GONE);

		relBook.setOnTouchListener(this);
		relPromotions.setOnTouchListener(this);

		txtMap.setOnClickListener(this);
		txtCall.setOnClickListener(this);
		btnMenu.setOnClickListener(this);
		btnServiceHours.setOnClickListener(this);
		relBook.setOnClickListener(this);
		relPromotions.setOnClickListener(this);
		btnViewOrderSummary.setOnClickListener(this);

		btnMenu.setSelected(false);
		btnServiceHours.setSelected(true);

		if (restaurantData.getAvailability().equalsIgnoreCase("red")) {
			txtOpen.setText(getString(R.string.closed_today));
			txtOpen.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.redbullet), null, null, null);
		} else if (restaurantData.getAvailability().equalsIgnoreCase("orange")) {
			txtOpen.setText(getString(R.string.closed_now));
			txtOpen.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.yellowbullet), null, null, null);
		} else if (restaurantData.getAvailability().equalsIgnoreCase("green")) {
			txtOpen.setText(getString(R.string.open_now));
			txtOpen.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.greenbullet), null, null, null);
		}

		if (restaurantData.getPhoto().size() > 0) {
			imageLoader.displayImage(restaurantData.getPhoto().get(0), imgPhoto, options);
		}

		imgPhoto.setOnClickListener(this);

		listRestaurantMenuItems = new ArrayList<RestaurantMenuItemData>();
		restaurantMenuItemListAdapter = new RestaurantMenuItemListAdapter(getActivity(), listRestaurantMenuItems);
		listView.setAdapter(restaurantMenuItemListAdapter);

		listServiceHours = new ArrayList<String>();
		restaurantServiceHoursAdapter = new RestaurantServiceHoursAdapter(getActivity(), restaurantData.getCompanyId(), listServiceHours);
		listView.setAdapter(restaurantServiceHoursAdapter);

		String address = "";
		if (!Utils.isEmpty(restaurantData.getAddress1())) {
			address = restaurantData.getAddress1();
		}

		if (!Utils.isEmpty(restaurantData.getAddress2())) {
			address += address.equals("") ? restaurantData.getAddress2() : "\n" + restaurantData.getAddress2();
		}
		txtAddress.setText(address);

		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
			if (!Utils.isEmpty(restaurantData.getDescription_EN())) {
				txtDescription.setText(restaurantData.getDescription_EN());
			}
		} else {
			if (!Utils.isEmpty(restaurantData.getDescription())) {
				txtDescription.setText(restaurantData.getDescription());
			}
		}

		if (!restaurantData.isBookable()) {

			relBook.setEnabled(false);
			txtBook.setTextColor(getResources().getColor(R.color.orange_disable));
			imvBook.setImageResource(R.drawable.book_dis);
		}
				

		galleryMenu.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					prePosition = galleryMenu.getSelectedItemPosition();
				} else if (event.getAction() == MotionEvent.ACTION_UP) {

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {

							try {

								if (prePosition != galleryMenu.getSelectedItemPosition()) {

									// restaurantMenuItemListAdapter = new
									// RestaurantMenuItemListAdapter(getActivity(),
									// listMenu.get(galleryMenu.getSelectedItemPosition()).getTblMenuItem());
									// listView.setAdapter(restaurantMenuItemListAdapter);

									listRestaurantMenuItems.clear();
									listRestaurantMenuItems.addAll(listMenu.get(galleryMenu.getSelectedItemPosition()).getTblMenuItem());
									restaurantMenuItemListAdapter.notifyDataSetChanged();

									// loadMenuItems();
								}

							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}, 500);
				}
				return false;
			}
		});

		galleryServiceHours.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					prePosition = galleryServiceHours.getSelectedItemPosition();
				} else if (event.getAction() == MotionEvent.ACTION_UP) {

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {

							try {

								if (prePosition != galleryServiceHours.getSelectedItemPosition()) {

									listServiceHours.clear();
									listServiceHours.add(arrayServiceHours[galleryServiceHours.getSelectedItemPosition()]);
									restaurantServiceHoursAdapter.notifyDataSetChanged();

									// listView.setAdapter(new
									// RestaurantServiceHoursAdapter(getActivity(),
									// restaurantData.getCompanyId(),
									// arrayServiceHours[galleryServiceHours.getSelectedItemPosition()]));
								}

							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}, 500);
				}
				return false;
			}
		});
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				// if (btnMenu.isSelected() &&
				// listRestaurantMenuItems.get(position - 1).isOrderable()) {
				if (btnMenu.isSelected() && listMenu.get(galleryMenu.getSelectedItemPosition()).getTblMenuItem().get(position - 1).isOrderable()) {

					Intent intent = new Intent(getActivity(), FoodOptionActivity.class);
					// intent.putExtra("menuItemId",
					// listRestaurantMenuItems.get(position -
					// 1).getMenuItemId());
					intent.putExtra("menuItemId", listMenu.get(galleryMenu.getSelectedItemPosition()).getTblMenuItem().get(position - 1).getMenuItemId());

					startActivity(intent);

					// Bundle bundle = new Bundle();
					// bundle.putSerializable("obj",
					// listRestaurantMenuItems.get(position - 1));
					//
					// PromotionDetailsFromList obj = new
					// PromotionDetailsFromList(RestaurantDetailsFragment.this);
					// obj.setArguments(bundle);
					//
					// FragmentTransaction ft =
					// getActivity().getSupportFragmentManager().beginTransaction();
					// ft.addToBackStack(PromotionDetailsFromList.class.getName());
					// ft.add(RestaurantActivity.restaurant_fragment_container,
					// obj);
					// ft.commit();
				}
			}
		});

		getFragmentManager().addOnBackStackChangedListener(new OnBackStackChangedListener() {
			@Override
			public void onBackStackChanged() {

				try {
					if (((RestaurantActivity) getActivity()).getActiveFragment().equals(RestaurantDetailsFragment.class.getName())) {

						if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
							if (!Utils.isEmpty(restaurantData.getCompanyNameEN())) {
								txtTitle.setText(restaurantData.getCompanyNameEN());
							}
						} else {
							if (!Utils.isEmpty(restaurantData.getCompanyName())) {
								txtTitle.setText(restaurantData.getCompanyName());
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(txtAddress);
		mTypefaceUtils.applyTypeface(txtMap);
		mTypefaceUtils.applyTypeface(txtCall);
		mTypefaceUtils.applyTypeface(txtBook);
		mTypefaceUtils.applyTypeface(txtPromo);
		mTypefaceUtils.applyTypeface(txtDescription);
		mTypefaceUtils.applyTypeface(btnMenu);
		mTypefaceUtils.applyTypeface(btnServiceHours);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
		
		loadServiceHours();
		
		if (prefManager.getPrefs().getBoolean(HomeScreen.PREFS_LOGIN, false)) {
			checkIsRestaurantExistInWatchList();
		}
				
	}

	private void setFavOnOff(boolean flag) {

		if (flag) {
			btnFav.setImageResource(R.drawable.fav_on);
			btnFav.setTag(true);
		} else {
			btnFav.setImageResource(R.drawable.fav_off);
			btnFav.setTag(false);
		}
	}

	private boolean isFav() {
		return (Boolean) btnFav.getTag();
	}

	private void loadMenu() {

		networManager.isProgressVisible(true);
		menuRequestId = networManager.addRequest(Join8RequestBuilder.getRestaurantMenuRequest(access_token, "" + restaurantData.getCompanyId()), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_RESTAURANT_MENU);
	}

	private void loadServiceHours() {

		networManager.isProgressVisible(true);
		serviceHoursRequestId = networManager.addRequest(Join8RequestBuilder.getRestaurantServiceHoursRequest(access_token, "" + restaurantData.getCompanyId()), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_RESTAURANT_SERVICE_HOURS);
	}

	// private void loadMenuItems() {
	//
	// networManager.isProgressVisible(true);
	// menuItemsRequestId =
	// networManager.addRequest(Join8RequestBuilder.getRestaurantMenuItemsRequest(access_token,
	// listMenu.get(galleryMenu.getSelectedItemPosition()).getMenuId()),
	// RequestMethod.POST, getActivity(),
	// Join8RequestBuilder.METHOD_RESTAURANT_MENU_ITEMS);
	// }

	private void checkIsRestaurantExistInWatchList() {

		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		String userId = registration.getPartitionKey();

		networManager.isProgressVisible(false);
		isWatchListExistRequestId = networManager.addRequest(Join8RequestBuilder.isRestaurantExistInWatchRequest(access_token, "" + restaurantData.getCompanyId(), userId), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_IS_RESTAURANT_EXIST_IN_WATCH);
	}

	private void addRestaurantToWatchList() {

		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		String userId = registration.getPartitionKey();

		networManager.isProgressVisible(true);
		addToWatchListRequestId = networManager.addRequest(Join8RequestBuilder.addRestaurantToWatchRequest(access_token, "" + restaurantData.getCompanyId(), userId, restaurantData.getWatchListId()), RequestMethod.POST, getActivity(),
				Join8RequestBuilder.METHOD_ADD_RESTAURANT_TO_WATCH);
	}

	private void removeRestaurantFromWatchList() {

		networManager.isProgressVisible(true);
		removeFromWatchListRequestId = networManager.addRequest(Join8RequestBuilder.removeRestaurantFromWatchRequest(access_token, "" + restaurantData.getWatchListId()), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_REMOVE_RESTAURANT_FROM_WATCH);
	}

	@Override
	public void onSuccess(int id, String response) {

		try {

			Log.i(getTag(), "Result => " + response);

			if (response != null && !response.equals("")) {

				if (id == menuRequestId) {

					if (progressDialog != null && progressDialog.isShowing()) {
						progressDialog.dismiss();
					}

					JSONArray jArray = new JSONArray(response);
					listMenu = new ArrayList<RestaurantMenuData>();
					// listRestaurantMenuItems = new
					// ArrayList<RestaurantMenuItemData>();
					// galleryServiceHours.setVisibility(View.GONE);

					if (jArray != null && jArray.length() > 0) {

						for (int i = 0; i < jArray.length(); i++) {
							RestaurantMenuData menuData = new Gson().fromJson(jArray.getString(i).toString(), RestaurantMenuData.class);
							listMenu.add(menuData);
						}
					} else {

						// galleryMenu.setVisibility(View.GONE);
					}

					if (listMenu != null) {

						galleryMenu.setAdapter(new RestaurantMenuAdapter(getActivity(), listMenu));

						if (btnMenu.isSelected()) {
							galleryMenu.setVisibility(View.VISIBLE);
							// restaurantMenuItemListAdapter = new
							// RestaurantMenuItemListAdapter(getActivity(),
							// listMenu.get(galleryMenu.getSelectedItemPosition()).getTblMenuItem());
							// listView.setAdapter(restaurantMenuItemListAdapter);
							listRestaurantMenuItems.clear();
							listRestaurantMenuItems.addAll(listMenu.get(galleryMenu.getSelectedItemPosition()).getTblMenuItem());
							restaurantMenuItemListAdapter.notifyDataSetChanged();
						}
					}

					// if (listMenu != null && listMenu.size() > 0) {
					//
					// restaurantMenuItemListAdapter = new
					// RestaurantMenuItemListAdapter(getActivity(),
					// listMenu.get(galleryMenu.getSelectedItemPosition()).getTblMenuItem());
					// listView.setAdapter(restaurantMenuItemListAdapter);
					//
					// // loadMenuItems();
					// }
			

				} else if (id == serviceHoursRequestId) {

					JSONArray jArray = new JSONArray(response);
					galleryServiceHours.setVisibility(View.VISIBLE);

					galleryServiceHours.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.row_restaurant_gallery_service_hours, getResources().getStringArray(R.array.weekday)));

					if (jArray != null && jArray.length() > 0) {

						galleryMenu.setVisibility(View.GONE);

						for (int i = 0; i < jArray.length(); i++) {

							RestaurantServiceHoursData serviceHoursData = new Gson().fromJson(jArray.getString(i).toString(), RestaurantServiceHoursData.class);

							if (serviceHoursData != null && serviceHoursData.getOpenTime() != null && !serviceHoursData.getOpenTime().equals("")) {
								String str = arrayServiceHours[serviceHoursData.getWeekday()];
								str += serviceHoursData.getOpenTime() + " " + serviceHoursData.getCloseTime() + "\n";
								arrayServiceHours[serviceHoursData.getWeekday()] = str;
							}
						}
					}

					SimpleDateFormat sdf = new SimpleDateFormat("EEEE", Locale.getDefault());
					Date d = new Date();
					String dayOfTheWeek = sdf.format(d);
					Log.e("dayoftheweek", dayOfTheWeek);

					galleryServiceHours.setSelection(d.getDay());

					listView.removeFooterView(footerView);
					listServiceHours.clear();
					listServiceHours.add(arrayServiceHours[galleryServiceHours.getSelectedItemPosition()]);
					restaurantServiceHoursAdapter.notifyDataSetChanged();
					// listView.setAdapter(new
					// RestaurantServiceHoursAdapter(getActivity(),
					// restaurantData.getCompanyId(),
					// arrayServiceHours[galleryServiceHours.getSelectedItemPosition()]));

					// } else if (id == menuItemsRequestId) {
					//
					// JSONArray jsonArray = new JSONArray(response);
					// // listRestaurantMenuItems = new
					// ArrayList<RestaurantMenuItemData>();
					//
					// if (jsonArray != null && jsonArray.length() > 0) {
					//
					// for (int i = 0; i < jsonArray.length(); i++) {
					// RestaurantMenuItemData restaurantMenuItemsData = new
					// Gson().fromJson(jsonArray.getString(i).toString(),
					// RestaurantMenuItemData.class);
					// // listRestaurantMenuItems.add(restaurantMenuItemsData);
					// }
					// }
					//
					// restaurantMenuItemListAdapter = new
					// RestaurantMenuItemListAdapter(getActivity(),
					// listMenu.get(galleryMenu.getSelectedItemPosition()).getTblMenuItem());
					// // restaurantMenuItemListAdapter = new
					// RestaurantMenuItemListAdapter(getActivity(),
					// listRestaurantMenuItems);
					// listView.setAdapter(restaurantMenuItemListAdapter);
					// // if (listRestaurantMenuItems.size() > 1) {
					// // listView.setSelection(1);
					// // }

				} else if (id == isWatchListExistRequestId) {

					if (response.trim().equalsIgnoreCase("false")) {
						setFavOnOff(false);
					} else {
						setFavOnOff(true);
						restaurantData.setWatchListId(new JSONObject(response).getString("WatchListId"));
					}

				} else if (id == addToWatchListRequestId) {

					setFavOnOff(true);
					checkIsRestaurantExistInWatchList();

				} else if (id == removeFromWatchListRequestId) {

					setFavOnOff(false);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	@Override
	public void onClick(View v) {

		if (v == btnFav) {

			if (!prefManager.getPrefs().getBoolean(HomeScreen.PREFS_LOGIN, false)) {
				Toast.displayText(getActivity(), getString(R.string.no_login_msg));
			} else {

				if (!isFav()) {
					addRestaurantToWatchList();
				} else {
					removeRestaurantFromWatchList();
				}
			}

		} else if (v == txtMap) {

			if (restaurantData.getLatitude() == 0 && restaurantData.getLongitude() == 0) {

				Toast.displayText(getActivity(), getActivity().getString(R.string.no_lat_long));

			} else {

				if (v.getContext() != null) {
					Intent intent = new Intent(v.getContext(), MapScreen.class);
					if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
						intent.putExtra("title", restaurantData.getCompanyNameEN());
					} else {
						intent.putExtra("title", restaurantData.getCompanyName());
					}
					intent.putExtra("lat", restaurantData.getLatitude());
					intent.putExtra("lon", restaurantData.getLongitude());
					startActivity(intent);
				}
			}

		} else if (v == txtCall) {

			try {

				if (TextUtils.isEmpty(restaurantData.getPhone())) {

					Toast.displayText(getActivity(), getActivity().getString(R.string.no_phono_number));

				} else {

					Intent intent = new Intent(Intent.ACTION_CALL);
					intent.setData(Uri.parse("tel:" + PhoneNumberUtils.stringFromStringAndTOA(PhoneNumberUtils.stripSeparators(restaurantData.getPhone()), PhoneNumberUtils.TOA_International)));
					startActivity(intent);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (v == btnMenu && !btnMenu.isSelected()) {

			btnMenu.setSelected(true);
			btnServiceHours.setSelected(false);

			galleryServiceHours.setVisibility(View.GONE);

			if (listView.getFooterViewsCount() > 0) {
				listView.removeFooterView(footerView);
			}
			listView.addFooterView(footerView);

			if (listMenu != null && listMenu.size() > 0) {
				galleryMenu.setVisibility(View.VISIBLE);
				galleryMenu.setSelection(0);
				//int index = galleryMenu.getSelectedItemPosition() == -1 ? 0 : galleryMenu.getSelectedItemPosition();
				// restaurantMenuItemListAdapter = new
				// RestaurantMenuItemListAdapter(getActivity(),
				// listMenu.get(index).getTblMenuItem());
				// listView.setAdapter(restaurantMenuItemListAdapter);
				listRestaurantMenuItems.clear();
				listRestaurantMenuItems.addAll(listMenu.get(galleryMenu.getSelectedItemPosition()).getTblMenuItem());
				listView.setAdapter(restaurantMenuItemListAdapter);
			} else {
				// progressDialog = new MyProgressDialog(getActivity());
				// restaurantMenuItemListAdapter = new
				// RestaurantMenuItemListAdapter(getActivity(), new
				// ArrayList<RestaurantMenuItemData>());
				// listView.setAdapter(restaurantMenuItemListAdapter);
				listRestaurantMenuItems.clear();
				listView.setAdapter(restaurantMenuItemListAdapter);
				loadMenu();
			}

		} else if (v == btnServiceHours && !btnServiceHours.isSelected()) {

			btnMenu.setSelected(false);
			btnServiceHours.setSelected(true);

			galleryMenu.setVisibility(View.GONE);
			galleryServiceHours.setVisibility(View.VISIBLE);
			SimpleDateFormat sdf = new SimpleDateFormat("EEEE", Locale.getDefault());
			Date d = new Date();
			String dayOfTheWeek = sdf.format(d);
			Log.e("dayoftheweek", dayOfTheWeek);

			galleryServiceHours.setSelection(d.getDay());

			listView.removeFooterView(footerView);

			listServiceHours.clear();
			listServiceHours.add(arrayServiceHours[galleryServiceHours.getSelectedItemPosition()]);
			// restaurantServiceHoursAdapter.notifyDataSetChanged();

			restaurantServiceHoursAdapter = new RestaurantServiceHoursAdapter(getActivity(), restaurantData.getCompanyId(), listServiceHours);
			listView.setAdapter(restaurantServiceHoursAdapter);

		} else if (v == relPromotions) {

			String name = "";
			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				name = restaurantData.getCompanyNameEN();
			} else {
				name = restaurantData.getCompanyName();
			}
			Bundle bundle = new Bundle();
			bundle.putSerializable("name", name);

			PromotionFromListFragment obj = new PromotionFromListFragment();
			obj.setArguments(bundle);

			FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
			ft.addToBackStack(PromotionFromListFragment.class.getName());
			ft.add(RestaurantActivity.restaurant_fragment_container, obj);
			ft.commit();

		} else if (v == relBook) {

			Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);

			if (registration == null) {
				Toast.displayText(getActivity(), getString(R.string.please_login));
			} else {
				Intent intent = new Intent(getActivity(), BookingActivity.class);
				intent.putExtra("companyId", "" + restaurantData.getCompanyId());
				intent.putExtra("tableBookingId", "-1");
				intent.putExtra("restaurantName", txtTitle.getText().toString().trim());
				intent.putExtra("bookingType", restaurantData.getBookingType());
				intent.putExtra("companyName", restaurantData.getCompanyName());
				intent.putExtra("companyNameEn", restaurantData.getCompanyNameEN());
				startActivityForResult(intent, INTENT_BOOKING);
			}
		} else if (v == imgPhoto) {
			if (restaurantData.getPhoto().size() <= 0) {
				Toast.displayText(getActivity(), getString(R.string.no_image));
			} else {
				Intent intent = new Intent(getActivity(), ImagePreviewScreen.class);
				if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
					intent.putExtra("title", restaurantData.getCompanyNameEN());
				} else {
					intent.putExtra("title", restaurantData.getCompanyName());
				}
				intent.putStringArrayListExtra("images", restaurantData.getPhoto());
				startActivity(intent);
			}
		} else if (v == btnViewOrderSummary) {

			Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);

			if (registration == null) {
				Toast.displayText(getActivity(), getString(R.string.please_login));
			} else {

				Bundle bundle = new Bundle();
				bundle.putSerializable("restaurantData", restaurantData);
				OrderDetailFragment orderDetailFragment = new OrderDetailFragment();
				orderDetailFragment.setOnItemDeleteListener(RestaurantDetailsFragment.this);
				orderDetailFragment.setArguments(bundle);
				getFragmentManager().beginTransaction().addToBackStack(OrderDetailFragment.class.getName()).add(RestaurantActivity.restaurant_fragment_container, orderDetailFragment).commit();
			}
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {

		if (event.getAction() == MotionEvent.ACTION_DOWN) {

			if (v == relBook) {
				imvBook.setImageResource(R.drawable.book_white);
				txtBook.setTextColor(Color.WHITE);
			} else if (v == relPromotions) {
				imvPromo.setImageResource(R.drawable.promo_white);
				txtPromo.setTextColor(Color.WHITE);

			}

		} else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_OUTSIDE) {

			if (v == relBook) {
				imvBook.setImageResource(R.drawable.book);
				txtBook.setTextColor(getResources().getColor(R.color.orangebg));
			} else if (v == relPromotions) {
				imvPromo.setImageResource(R.drawable.promo);
				txtPromo.setTextColor(getResources().getColor(R.color.orangebg));
			}
		}

		return false;
	}

	@Override
	public void itemDeleted() {
		try {
			if (listView != null && listView.getAdapter() != null) {
				restaurantMenuItemListAdapter.notifyDataSetChanged();
			}
		} catch (Exception e) {
		}
	}
}
