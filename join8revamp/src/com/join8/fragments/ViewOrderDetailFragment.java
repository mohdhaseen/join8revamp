package com.join8.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.adpaters.OrderDetailAdapter;
import com.join8.adpaters.OrderDetailAdapter.ChangeCart;
import com.join8.listeners.Requestlistener;
import com.join8.model.FoodOptionItemData;
import com.join8.model.OrderData;
import com.join8.model.Registration;
import com.join8.model.RestaurantMenuItemData;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.Log;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class ViewOrderDetailFragment extends Fragment implements ChangeCart, Requestlistener, OnClickListener {

	private static final String TAG = ViewOrderDetailFragment.class.getSimpleName();

	private ArrayList<RestaurantMenuItemData> listItems = null;

	private ListView mListView;

	private TextView txtDeliveryCharge, txtTotal;
	private ImageView imvUser;
	private TextView txtOrderId = null, txtName, txtDateToBook, txtMobilePhone, txtAddress, txtRemarks;

	private NetworkManager networManager;
	private CryptoManager prefManager;
	private String access_token = "";
	private int get_Order = -1, userProfile_request = -1;
	private String orderId = "";

	private ImageLoader imageLoader = null;
	private DisplayImageOptions options = null;

	private ArrayList<Registration> userProfileList = null;
	private String[] strArrayUserProfile = null;
	private int selectedUserProfileIndex = -1;
	private OrderData orderData = null;
	private long selectedDate;
	
	private double total = 0, deliveryFee = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		networManager = NetworkManager.getInstance();
		prefManager = CryptoManager.getInstance(getActivity());
		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");

		if (savedInstanceState != null) {
			if (savedInstanceState.containsKey("orderId")) {
				orderId = savedInstanceState.getString("orderId");
			}
		} else {
			if (getArguments() != null) {
				if (getArguments().containsKey("orderId")) {
					orderId = getArguments().getString("orderId");
				}
			}
		}

		imageLoader = ImageLoader.getInstance();
		DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();
		int roundSize = (int) (dm.density * getResources().getDimension(R.dimen.image_height));
		options = new DisplayImageOptions.Builder().displayer(new RoundedBitmapDisplayer(roundSize)).showStubImage(R.drawable.default_user).showImageForEmptyUri(R.drawable.default_user).showImageOnFail(R.drawable.default_user).cacheInMemory(true).cacheOnDisc(true).build();
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (!Utils.isEmpty(orderId)) {
			outState.putSerializable("orderId", orderId);
		}

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.order_details_fragment, container, false);
		mappingWidgets(mView);
		return mView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (isAdded() && !Utils.isEmpty(orderId)) {
			loadOrder(orderId);
		}
	}

	private void mappingWidgets(View mView) {

		mListView = (ListView) mView.findViewById(android.R.id.list);

		View mHeaderView = LayoutInflater.from(getActivity()).inflate(R.layout.view_order_details_header, null);
		mListView.addHeaderView(mHeaderView);

		txtOrderId = (TextView) mHeaderView.findViewById(R.id.txtOrderId);
		txtName = (TextView) mHeaderView.findViewById(R.id.txtName);
		txtDateToBook = (TextView) mHeaderView.findViewById(R.id.txtDateToBook);
		txtMobilePhone = (TextView) mHeaderView.findViewById(R.id.txtMobilePhone);
		txtAddress = (TextView) mHeaderView.findViewById(R.id.txtAddress);
		txtRemarks = (TextView) mHeaderView.findViewById(R.id.txtRemarks);
		imvUser = (ImageView) mHeaderView.findViewById(R.id.imvUser);

		View mFooterView = LayoutInflater.from(getActivity()).inflate(R.layout.order_details_footer, null);
		mListView.addFooterView(mFooterView);

		txtDeliveryCharge = (TextView) mFooterView.findViewById(R.id.txtDeliveryCharge);
		txtTotal = (TextView) mFooterView.findViewById(R.id.txtTotal);

		mFooterView.findViewById(R.id.txtPlaceOrder).setVisibility(View.GONE);
		mFooterView.findViewById(R.id.rbgPaymentType).setVisibility(View.GONE);

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface((TextView) mFooterView.findViewById(R.id.lblDeliveryCharge));
		mTypefaceUtils.applyTypeface((TextView) mFooterView.findViewById(R.id.lblTotal));
		mTypefaceUtils.applyTypeface(txtOrderId);
		mTypefaceUtils.applyTypeface(txtName);
		mTypefaceUtils.applyTypeface(txtDateToBook);
		mTypefaceUtils.applyTypeface(txtAddress);
		mTypefaceUtils.applyTypeface(txtRemarks);
		mTypefaceUtils.applyTypeface(txtMobilePhone);
		mTypefaceUtils.applyTypeface(txtTotal);

		txtMobilePhone.setOnClickListener(this);

	}

	private void setTitle(String title) {

		View view = getActivity().getActionBar().getCustomView();
		((ImageButton) view.findViewById(R.id.btnFav)).setVisibility(View.INVISIBLE);
		if (Utils.isEmpty(title)) {
			((TextView) view.findViewById(R.id.txtTitle)).setText(getString(R.string.order_details));
		} else {
			((TextView) view.findViewById(R.id.txtTitle)).setText(getString(R.string.order_details) + "-" + title);
		}
		((TextView) view.findViewById(R.id.txtOpen)).setVisibility(View.GONE);
	}

	@Override
	public void changeTotal() {
		try {
			total = 0.0;
			if (listItems != null) {

				for (int i = 0; i < listItems.size(); i++) {

					total += (listItems.get(i).getQuantity() * listItems.get(i).getPrice());

					for (int j = 0; j < listItems.get(i).getFoodOptions().size(); j++) {

						total += listItems.get(i).getQuantity() * listItems.get(i).getFoodOptions().get(j).getExtraPrice();
					}
				}
			}

			deliveryFee = 0;

			if (!orderData.isIsOrderPickUpByUser()) {

				if (orderData.getDeliveryOption() == 1) {

					deliveryFee = orderData.getDeliveryFee();

				} else if (orderData.getDeliveryOption() == 2) {

					deliveryFee = total * orderData.getDeliveryFee() / 100;
				}
			}

			txtDeliveryCharge.setText("$" + Utils.formatDecimal(String.valueOf(deliveryFee)));
			txtTotal.setText("$" + Utils.formatDecimal(String.valueOf(deliveryFee + total)));

		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	}

	private void setUserData(Registration registration) {

		txtName.setText(registration.getFirstName() + " " + registration.getLastName());

		if (!Utils.isEmpty(registration.getMobileNumber())) {

			String mobileNumber = registration.getMobileNumber();

			if (mobileNumber.contains("|")) {
				mobileNumber = mobileNumber.replaceAll("\\|", "-");
			}

			if (mobileNumber.contains("-")) {
				String[] str = mobileNumber.split("-");
				txtMobilePhone.setText(str[str.length - 1]);
			} else {
				txtMobilePhone.setText(mobileNumber);
			}

			txtMobilePhone.setCompoundDrawablesWithIntrinsicBounds(R.drawable.theme_btn_call, 0, 0, 0);

		} else {
			txtMobilePhone.setText(getString(R.string.no_phono_number));
			txtMobilePhone.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		}
	}

	private void loadOrder(String orderId) {
		networManager.isProgressVisible(true);
		get_Order = networManager.addRequest(Join8RequestBuilder.getParticularOrder(access_token, orderId), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_GET_PARTICULAR_ORDER);
	}

	@Override
	public void onSuccess(int id, String response) {

		if (isAdded() && !TextUtils.isEmpty(response)) {

			if (id == get_Order) {

				try {

					if (response.toLowerCase(Locale.getDefault()).startsWith("failure")) {

						Toast.displayText(getActivity(), getString(R.string.norecorderror));

					} else {

						JSONObject jObj = new JSONObject(response);
						listItems = new ArrayList<RestaurantMenuItemData>();

						orderData = new Gson().fromJson(jObj.toString(), OrderData.class);

						if (orderData.getMenuItem().size() > 0) {

							for (int i = 0; i < orderData.getMenuItem().size(); i++) {

								RestaurantMenuItemData data = new RestaurantMenuItemData();

								data.setCompanyId(orderData.getCompanyId());
								data.setMenuItemId(orderData.getMenuItem().get(i).getMenuItemId());
								data.setItemName(orderData.getMenuItem().get(i).getItemName());
								data.setItemName_EN(orderData.getMenuItem().get(i).getItemName_EN());
								data.setPrice(orderData.getMenuItem().get(i).getPrice());
								data.setQuantity(orderData.getMenuItem().get(i).getQuantity());
								data.setHasFoodOption(orderData.isHasFoodOption());

								for (int j = 0; j < orderData.getMenuItem().get(i).getFoodOption().size(); j++) {

									double basePrice = orderData.getMenuItem().get(i).getFoodOption().get(j).getBasePrice();

									for (int k = 0; k < orderData.getMenuItem().get(i).getFoodOption().get(j).getFoodOptionItem().size(); k++) {

										FoodOptionItemData foodOptionItemData = orderData.getMenuItem().get(i).getFoodOption().get(j).getFoodOptionItem().get(k);
										foodOptionItemData.setExtraPrice(foodOptionItemData.getExtraPrice() + basePrice);
										basePrice = 0;
										data.getFoodOptions().add(foodOptionItemData);
									}
								}

								listItems.add(data);
							}
						}

						mListView.setAdapter(new OrderDetailAdapter(getActivity(), listItems, this, false));

						if (orderData.getOrderDeadlineDate() != null) {
							try {
								selectedDate = orderData.getOrderDeadlineDate().getTime();
								Calendar calendar = Calendar.getInstance();
								calendar.setTimeInMillis(selectedDate);								
								SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd - hh:mm a", Locale.getDefault());
								String dateTime = format.format(orderData.getOrderDeadlineDate());
								txtDateToBook.setText(dateTime);
							} catch (Exception e) {
							}
						}

						try {

							setTitle(OrderData.getOrderStatusString(getActivity(), orderData.getOrderStatus()));

							String orderId = orderData.getOrderId() + "--";

							if (orderData.isIsOrderPickUpByUser()) {
								orderId += " (" + getString(R.string.pickup) + ")";
							} else {
								orderId += " (" + getString(R.string.deliver) + ")";
							}
							txtOrderId.setText(orderId);

							if (!Utils.isEmpty(orderData.getRestaurnatPhoto())) {
								imageLoader.displayImage(orderData.getRestaurnatPhoto(), imvUser, options);
							}

							if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
								txtName.setText(orderData.getCompanyNameEN());
							} else {
								txtName.setText(orderData.getCompanyName());
							}

							if (!Utils.isEmpty(orderData.getMobileNumber())) {
								txtMobilePhone.setText(orderData.getMobileNumber());
							}

							if (!Utils.isEmpty(orderData.getDeliverAddress())) {
								txtAddress.setText(orderData.getDeliverAddress());
							}

							if (!Utils.isEmpty(orderData.getRemarks())) {
								txtRemarks.setText(orderData.getRemarks());
							}

							changeTotal();

						} catch (Exception e) {
							e.printStackTrace();
						}

						// setTitle(OrderData.getOrderStatusString(getActivity(),
						// orderData.getOrderStatus()));

						changeTotal();

						OrderDetailAdapter mAdapter = new OrderDetailAdapter(getActivity(), listItems, this, false);
						mListView.setAdapter(mAdapter);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			} else if (id == userProfile_request) {
				
				Log.e("response", response);

				try {
					JSONArray jsonArray = new JSONArray(response);
					if (jsonArray.length() > 0) {
						userProfileList = new ArrayList<Registration>();
						strArrayUserProfile = new String[jsonArray.length()];
						for (int i = 0; i < jsonArray.length(); i++) {
							Registration registration = new Gson().fromJson(jsonArray.getJSONObject(i).toString(), Registration.class);
							userProfileList.add(registration);

							if (registration.getProfileName().contains("-")) {
								strArrayUserProfile[i] = registration.getProfileName().substring(registration.getProfileName().lastIndexOf("-") + 1);
							} else {
								strArrayUserProfile[i] = registration.getProfileName();
							}

							// strArrayUserProfile[i] =
							// registration.getProfileName().substring(registration.getEmail().length()
							// + 1).replace("-", "");

							if (orderData == null) {
								if (registration.isDefaultProfile()) {
									selectedUserProfileIndex = i;
									setUserData(registration);
								}
							} else {
								if (registration.getProfileName().equalsIgnoreCase(orderData.getUserProfile())) {
									selectedUserProfileIndex = i;
									setUserData(registration);
								}
							}
						}

						if (selectedUserProfileIndex == -1) {
							for (int i = 0; i < userProfileList.size(); i++) {
								Registration registration = userProfileList.get(i);
								userProfileList.add(registration);
								strArrayUserProfile[i] = registration.getProfileName().substring(registration.getEmail().length() + 1).replace("-", "");
								if (registration.isDefaultProfile()) {
									selectedUserProfileIndex = i;
									setUserData(registration);
									break;
								}
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	@Override
	public void onClick(View v) {

		if (v == txtMobilePhone) {
			if (!Utils.isEmpty(txtMobilePhone.getText().toString().trim())) {
				Intent intent = new Intent(Intent.ACTION_CALL);
				intent.setData(Uri.parse("tel:" + PhoneNumberUtils.stringFromStringAndTOA(PhoneNumberUtils.stripSeparators(txtMobilePhone.getText().toString().trim()), PhoneNumberUtils.TOA_International)));
				startActivity(intent);
			}
		}
	}
}
