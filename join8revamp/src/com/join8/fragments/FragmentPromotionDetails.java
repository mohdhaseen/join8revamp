package com.join8.fragments;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.ImagePreviewScreen;
import com.join8.MapScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.model.Promotion;
import com.join8.model.Promotions;
import com.join8.utils.ConstantData;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class FragmentPromotionDetails extends Fragment implements OnClickListener {
	private Promotion promotions = null;
	private TextView tvRestauratnName, tvPromotionName, tvDesc, tvStartdate, tvEndDate, tvPromotionCode;
	private ImageView imgPhoto;
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private TypefaceUtils mTypefaceUtils;
	private TextView tvMap, tvCall;
	private boolean fromFlag;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (savedInstanceState != null) {
			if (savedInstanceState.containsKey(PromotionFragmentNew.PARAM_PROMOTION)) {
				promotions = new Gson().fromJson(savedInstanceState.getString(PromotionFragmentNew.PARAM_PROMOTION), Promotion.class);
			}

		} else {
			if (getArguments() != null) {
				if (getArguments().containsKey(PromotionFragmentNew.PARAM_PROMOTION)) {

					promotions = new Gson().fromJson(getArguments().getString(PromotionFragmentNew.PARAM_PROMOTION), Promotion.class);
					fromFlag = getArguments().getBoolean("flag", false);
				}
			}
		}

		imageLoader = ImageLoader.getInstance();
		mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();
		int roundSize = (int) (dm.density * getResources().getDimension(R.dimen.image_height));
		options = new DisplayImageOptions.Builder().displayer(new RoundedBitmapDisplayer(roundSize)).showStubImage(R.drawable.trans).showImageForEmptyUri(R.drawable.trans).showImageOnFail(R.drawable.trans).cacheInMemory(true).cacheOnDisc(true).build();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.promotion_detail, container, false);
		tvRestauratnName = (TextView) v.findViewById(R.id.tvResaurantName);
		tvPromotionName = (TextView) v.findViewById(R.id.tvPromotionName);
		tvDesc = (TextView) v.findViewById(R.id.tvDesc);
		tvStartdate = (TextView) v.findViewById(R.id.tvStartTime);
		tvEndDate = (TextView) v.findViewById(R.id.tvEndTime);
		tvPromotionCode = (TextView) v.findViewById(R.id.tvPromotionCode);
		imgPhoto = (ImageView) v.findViewById(R.id.imgPhoto);

		tvMap = (TextView) v.findViewById(R.id.tvMap);
		tvCall = (TextView) v.findViewById(R.id.tvCall);

		tvMap.setOnClickListener(this);
		tvCall.setOnClickListener(this);
		imgPhoto.setOnClickListener(this);

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (promotions != null) {
			if (fromFlag == true) {
				View view = getActivity().getActionBar().getCustomView();
				((ImageButton) view.findViewById(R.id.btnFav)).setVisibility(View.INVISIBLE);
				((TextView) view.findViewById(R.id.txtTitle)).setText(promotions.getPromotionName());
				((TextView) view.findViewById(R.id.txtOpen)).setVisibility(View.GONE);
			} else {
				if (!getActivity().isFinishing()) {
					((HomeScreen) getActivity()).setCustomTitle(promotions.getPromotionName(), Typeface.NORMAL);
					((HomeScreen) getActivity()).changeActionItems(true);
				}
			}

			tvPromotionName.setText(promotions.getPromotionName());
			try {
				tvStartdate.setText(getDateFromat(promotions.getStartDate()));
				tvEndDate.setText(getDateFromat(promotions.getEndDate()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			imageLoader.displayImage(promotions.getIcon(), imgPhoto, options);
			tvPromotionCode.setText(promotions.getPromotionCode());
			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				tvDesc.setText(promotions.getDescriptionEn());
				tvRestauratnName.setText(promotions.getShopNameEN());
			} else {
				tvDesc.setText(promotions.getDescription());
				tvRestauratnName.setText(promotions.getShopName());
			}

			mTypefaceUtils.applyTypeface(tvDesc);
			mTypefaceUtils.applyTypeface(tvStartdate);
			mTypefaceUtils.applyTypeface(tvEndDate);
			mTypefaceUtils.applyTypeface(tvRestauratnName);
			mTypefaceUtils.applyTypeface(tvPromotionName);
			mTypefaceUtils.applyTypeface(tvPromotionCode);

		}

	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		outState.putString(PromotionFragmentNew.PARAM_PROMOTION, new Gson().toJson(promotions));

	}

	private String getDateFromat(String date) {

		try {
			date = date.replace("T", " ");
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
			Date formatted = null;
			formatted = formatter.parse(date);

			formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
			// formatter.format(formatted);
			return formatter.format(formatted);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";

	}

	@Override
	public void onClick(View v) {
		if (v == tvMap) {
			Intent intent = new Intent(getActivity(), MapScreen.class);
			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
				intent.putExtra("title", promotions.getShopNameEN());
			} else {
				intent.putExtra("title", promotions.getShopName());
			}
			intent.putExtra("lat", promotions.getLatitude());
			intent.putExtra("lon", promotions.getLongitude());
			startActivity(intent);

		} else if (v == tvCall) {
			try {
				if (TextUtils.isEmpty(promotions.getPhone())) {
					Toast.displayText(getActivity(), getActivity().getString(R.string.nophoneerror));
				} else {
					Intent intent = new Intent(Intent.ACTION_CALL);
					intent.setData(Uri.parse("tel:" + PhoneNumberUtils.stringFromStringAndTOA(PhoneNumberUtils.stripSeparators(promotions.getPhone()), PhoneNumberUtils.TOA_International)));
					startActivity(intent);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (v == imgPhoto) {
			if (Utils.isEmpty(promotions.getIcon())) {
				Toast.displayText(getActivity(), getString(R.string.no_image));
			} else {

				ArrayList<String> listImages = new ArrayList<String>();
				listImages.add(promotions.getIcon());

				Intent intent = new Intent(getActivity(), ImagePreviewScreen.class);
				intent.putExtra("title", promotions.getPromotionName());
				intent.putExtra("images", listImages);
				startActivity(intent);
			}
		}
	}
}
