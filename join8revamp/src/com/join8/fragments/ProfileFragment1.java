package com.join8.fragments;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.layout.MyProgressDialog;
import com.join8.listeners.Requestlistener;
import com.join8.model.Area;
import com.join8.model.Registration;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.network.RestClient;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.Log;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.UrlUtil;
import com.join8.utils.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class ProfileFragment1 extends Fragment implements Requestlistener, OnClickListener, TextWatcher {

	private static final String TAG = ProfileFragment.class.getSimpleName();

	public static final int INTENT_CAMERA = 111;
	public static final int INTENT_GALLERY = 112;

	private TextView txtSlideToSeeProfile = null, txtName = null, txtRegisteredIn = null, txtDeleteThisProfile = null, txtLanguage = null, txtPhoneNoAndAddresses = null, btnCancel = null, btnSave = null;
	private EditText edtFirstName = null, edtLastName = null, edtProfileName = null, edtEmail = null, edtPassword = null, edtRepeatPassword = null, edtCountryCode = null, edtMobilePhone = null, edtCountryCodeBusiness = null, edtCityCodeBusiness = null, edtBusinessPhone = null,
			edtCompany = null, edtJobTitle = null, edtAddress1 = null, edtAddress2 = null, edtRemark = null;
	private ImageView imvProfile = null;
	private LinearLayout lnrPageIndicator = null;

	private int index = -1, size = 0;
	private Registration userData = null;

	private NetworkManager networManager = null;
	private CryptoManager prefManager = null;
	private int deleteProfileRequestId = -1, editProfileRequestId = 111, addProfileRequestId = 112, isProfileExists = -1;
	private String access_token = "";
	private String phoneCode = "";
	private MyProgressDialog progressDialog = null;

	private ImageLoader imageLoader = null;
	private DisplayImageOptions options = null;
	private int roundPixels = 0;
	private int mReqAreaList = -1;
	private OnProfileDeleteListner onProfileDeleteListner = null;

	public boolean isEdited = false, isTextChangeIgnore = true;
	private boolean isProfileNameAlreadyExist = false, lostFocus = false, fromFlag = false;
	private HashMap<String, String> auth = null;
	private String tokenCloud = null;
	private byte[] byteData = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		networManager = NetworkManager.getInstance();
		networManager.isProgressVisible(true);
		prefManager = CryptoManager.getInstance(getActivity());

		index = getArguments().getInt("index");
		size = getArguments().getInt("size");

		if (index >= 0 && !UserProfileFragment.isNewProfile) {
			userData = (Registration) getArguments().getSerializable("data");
		}

		imageLoader = ImageLoader.getInstance();
		DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();
		roundPixels = (int) (dm.density * getResources().getDimension(R.dimen.image_height));
		options = new DisplayImageOptions.Builder().displayer(new RoundedBitmapDisplayer(roundPixels)).showStubImage(R.drawable.trans).showImageForEmptyUri(R.drawable.trans).showImageOnFail(R.drawable.trans).cacheInMemory(true).cacheOnDisc(true).build();
	}

	@Override
	public void onStart() {
		super.onStart();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.profile_view1, container, false);
		mappingWidgets(rootView);
		addListeners();
		init();
		return rootView;
	}

	private void mappingWidgets(View mView) {

		try {

			txtSlideToSeeProfile = (TextView) mView.findViewById(R.id.txtSlideToSeeProfile);
			txtName = (TextView) mView.findViewById(R.id.txtName);
			txtRegisteredIn = (TextView) mView.findViewById(R.id.txtRegisteredIn);
			txtDeleteThisProfile = (TextView) mView.findViewById(R.id.txtDeleteThisProfile);
			txtLanguage = (TextView) mView.findViewById(R.id.txtLanguage);
			txtPhoneNoAndAddresses = (TextView) mView.findViewById(R.id.txtPhoneNoAndAddresses);
			btnCancel = (TextView) mView.findViewById(R.id.txtCancel);
			btnSave = (TextView) mView.findViewById(R.id.txtSave);

			edtFirstName = (EditText) mView.findViewById(R.id.edtFirstName);
			edtLastName = (EditText) mView.findViewById(R.id.edtLastName);
			edtProfileName = (EditText) mView.findViewById(R.id.edtProfileName);
			edtEmail = (EditText) mView.findViewById(R.id.edtEmail);
			edtPassword = (EditText) mView.findViewById(R.id.edtPassword);
			edtRepeatPassword = (EditText) mView.findViewById(R.id.edtRepeatPassword);
			edtCountryCode = (EditText) mView.findViewById(R.id.edtCountryCode);
			edtMobilePhone = (EditText) mView.findViewById(R.id.edtMobilePhone);
			edtCountryCodeBusiness = (EditText) mView.findViewById(R.id.edtCountryCodeBusiness);
			edtCityCodeBusiness = (EditText) mView.findViewById(R.id.edtCityCodeBusiness);
			edtBusinessPhone = (EditText) mView.findViewById(R.id.edtBusinessPhone);
			edtCompany = (EditText) mView.findViewById(R.id.edtCompany);
			edtJobTitle = (EditText) mView.findViewById(R.id.edtJobTitle);
			edtAddress1 = (EditText) mView.findViewById(R.id.edtAddress1);
			edtAddress2 = (EditText) mView.findViewById(R.id.edtAddress2);
			edtRemark = (EditText) mView.findViewById(R.id.edtRemark);

			imvProfile = (ImageView) mView.findViewById(R.id.imvProfile);

			lnrPageIndicator = (LinearLayout) mView.findViewById(R.id.lnrPageIndicator);

			TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
			mTypefaceUtils.applyTypeface(txtSlideToSeeProfile);
			mTypefaceUtils.applyTypeface(txtName);
			mTypefaceUtils.applyTypeface(txtRegisteredIn);
			mTypefaceUtils.applyTypeface(txtDeleteThisProfile);
			mTypefaceUtils.applyTypeface(txtLanguage);
			mTypefaceUtils.applyTypeface(txtPhoneNoAndAddresses);
			mTypefaceUtils.applyTypeface(btnCancel);
			mTypefaceUtils.applyTypeface(btnSave);

			mTypefaceUtils.applyTypeface(edtFirstName);
			mTypefaceUtils.applyTypeface(edtLastName);
			mTypefaceUtils.applyTypeface(edtProfileName);
			mTypefaceUtils.applyTypeface(edtEmail);
			mTypefaceUtils.applyTypeface(edtPassword);
			mTypefaceUtils.applyTypeface(edtRepeatPassword);
			mTypefaceUtils.applyTypeface(edtCountryCode);
			mTypefaceUtils.applyTypeface(edtMobilePhone);
			mTypefaceUtils.applyTypeface(edtCountryCodeBusiness);
			mTypefaceUtils.applyTypeface(edtCityCodeBusiness);
			mTypefaceUtils.applyTypeface(edtBusinessPhone);
			mTypefaceUtils.applyTypeface(edtCompany);
			mTypefaceUtils.applyTypeface(edtJobTitle);
			mTypefaceUtils.applyTypeface(edtAddress1);
			mTypefaceUtils.applyTypeface(edtAddress2);
			mTypefaceUtils.applyTypeface(edtRemark);

		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Error in mappingWidgets", e.toString());
		}
	}

	private void addListeners() {

		btnSave.setOnClickListener(this);
		btnCancel.setOnClickListener(this);

		edtFirstName.addTextChangedListener(this);
		edtLastName.addTextChangedListener(this);
		edtProfileName.addTextChangedListener(this);
		edtEmail.addTextChangedListener(this);
		edtPassword.addTextChangedListener(this);
		edtRepeatPassword.addTextChangedListener(this);
		edtCountryCode.addTextChangedListener(this);
		edtMobilePhone.addTextChangedListener(this);
		edtCountryCodeBusiness.addTextChangedListener(this);
		edtCityCodeBusiness.addTextChangedListener(this);
		edtBusinessPhone.addTextChangedListener(this);
		edtCompany.addTextChangedListener(this);
		edtJobTitle.addTextChangedListener(this);
		edtAddress1.addTextChangedListener(this);
		edtAddress2.addTextChangedListener(this);
		edtRemark.addTextChangedListener(this);

		txtLanguage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				tokenCloud = prefManager.getPrefs().getString(ConstantData.TOKEN_CLOUD, "");
				auth = new HashMap<String, String>();
				auth.put("Authorization", "bearer " + tokenCloud);
				mReqAreaList = networManager.addRequest(auth, getActivity(), true, UrlUtil.getAreaListUrl(), "", ConstantData.method_get);

			}
		});

		txtDeleteThisProfile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (userData != null && !Utils.isEmpty(userData.getProfileName())) {

					AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
					dialog.setMessage(getString(R.string.delete_profile));
					dialog.setPositiveButton(getString(R.string.yes), new android.content.DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							deleteProfileRequest(Join8RequestBuilder.deleteUserProfileRequest(access_token, userData.getProfileName()));

							// deleteProfile();
						}
					});
					dialog.setNegativeButton(getString(R.string.no), new android.content.DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

						}
					});
					dialog.show();
				}
			}
		});

		// imvLeft.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		//
		// if (onProfileDeleteListner != null && index > 0) {
		// onProfileDeleteListner.onSwipeLeft(index - 1);
		// }
		// }
		// });
		//
		// imvRight.setOnClickListener(new OnClickListener() {
		// @Override
		// public void onClick(View v) {
		//
		// if (onProfileDeleteListner != null && index <
		// UserProfileFragment.jArrayUserProfile.length() - 1) {
		// onProfileDeleteListner.onSwipeLeft(index + 1);
		// }
		// }
		// });

		edtProfileName.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {

				if (!hasFocus && UserProfileFragment.isNewProfile) {

					if (edtProfileName.getText().toString().trim().length() > 0) {
						fromFlag = false;
						networManager.isProgressVisible(false);
						isProfileExist();
					}

				} else {

					isProfileNameAlreadyExist = false;
					lostFocus = false;
				}
			}

		});
		edtProfileName.addTextChangedListener(new TextWatcher() {

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
			}

			@Override
			public void afterTextChanged(Editable s) {
				isProfileNameAlreadyExist = false;
				lostFocus = false;
			}
		});

		imvProfile.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				showAlertToSelectCameraOrGellary();
			}
		});
	}

	private void init() {

		try {

			if (UserProfileFragment.isNewProfile) {
				edtProfileName.setEnabled(true);
				edtProfileName.setFocusable(true);
				isEdited = true;
			} else {
				edtProfileName.setEnabled(false);
				edtProfileName.setFocusable(false);
				isEdited = false;
			}

			displayPageDot(index);

		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Error in init", e.toString());
		}
	}

	private void showAlertToSelectCameraOrGellary() {

		final Dialog dialog = new Dialog(getActivity());

		dialog.setTitle(getString(R.string.choose_photo_from));
		dialog.setContentView(R.layout.dialog_layout);

		Button btnCamera = (Button) dialog.findViewById(R.id.btnCameraFromDialog);
		Button btnGallery = (Button) dialog.findViewById(R.id.btnGalleryFromDialog);

		btnCamera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				byteData = null;

				Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(cameraIntent, INTENT_CAMERA);

				dialog.dismiss();
			}
		});

		btnGallery.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				byteData = null;

				Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
				startActivityForResult(galleryIntent, INTENT_GALLERY);

				dialog.dismiss();
			}
		});

		dialog.show();
	}

	public void setOnProfileDeleteListner(OnProfileDeleteListner onProfileDeleteListner) {
		this.onProfileDeleteListner = onProfileDeleteListner;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");

		fillData(userData);
	}

	// @Override
	// public void onResume() {
	// super.onResume();
	//
	// fillData(userData);
	// Log.e("onResume", "onResume - " + index);
	// }

	private void editProfile() {

		String language = "";
		if (txtLanguage.getText().toString().trim().equals(getString(R.string.tvEnglish))) {
			language = "EN";
		} else if (txtLanguage.getText().toString().trim().equals(getString(R.string.chinese1))) {
			language = "TC";
		} else if (txtLanguage.getText().toString().trim().equals(getString(R.string.chinese2))) {
			language = "SC";
		}

		String mobileNo = "";
		if (!edtMobilePhone.getText().toString().trim().equals("")) {
			mobileNo = edtCountryCode.getText().toString().trim() + "|" + edtMobilePhone.getText().toString().trim();
			mobileNo = mobileNo.replaceAll("-", "|");
		}

		String bussinessPhone = "";
		if (!edtBusinessPhone.getText().toString().trim().equals("")) {
			bussinessPhone = edtCountryCode.getText().toString().trim();
			if (!edtCityCodeBusiness.getText().toString().trim().equals("")) {
				bussinessPhone += "-" + edtCityCodeBusiness.getText().toString().trim();
			}
			bussinessPhone += "-" + edtBusinessPhone.getText().toString().trim();
		}

		addEditProfileRequest(Join8RequestBuilder.editProfile(access_token, edtFirstName.getText().toString().trim(), edtLastName.getText().toString().trim(), edtProfileName.getText().toString().trim(), edtEmail.getText().toString().trim(), edtPassword.getText().toString()
				.trim(), language, mobileNo, bussinessPhone, edtCompany.getText().toString().trim(), edtJobTitle.getText().toString().trim(), edtAddress1.getText().toString().trim(), edtAddress2.getText().toString().trim(), edtRemark.getText().toString().trim(),
				userData.isDefaultProfile(), userData.getProvince(), userData.getRowKey(), userData.getLoginPreference(), userData.getGender(), userData.getRecordStatus(), userData.getPhoto(), userData.getCity(), userData.getPartitionKey()), editProfileRequestId,
				Join8RequestBuilder.METHOD_UPDATE_USER_PROFILE);
	}

	private void addProfile() {

		String language = "";
		if (txtLanguage.getText().toString().trim().equals(getString(R.string.tvEnglish))) {
			language = "EN";
		} else if (txtLanguage.getText().toString().trim().equals(getString(R.string.chinese1))) {
			language = "SC";
		} else if (txtLanguage.getText().toString().trim().equals(getString(R.string.chinese2))) {
			language = "TC";
		}

		String mobileNo = "";
		if (!edtMobilePhone.getText().toString().trim().equals("")) {
			mobileNo = edtCountryCode.getText().toString().trim() + "-" + edtMobilePhone.getText().toString().trim();
			mobileNo = mobileNo.replaceAll("-", "|");
		}

		String bussinessPhone = "";
		if (!edtBusinessPhone.getText().toString().trim().equals("")) {
			bussinessPhone = edtCountryCode.getText().toString().trim();
			if (!edtCityCodeBusiness.getText().toString().trim().equals("")) {
				bussinessPhone += "-" + edtCityCodeBusiness.getText().toString().trim();
			}
			bussinessPhone += "-" + edtBusinessPhone.getText().toString().trim();
		}

		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);

		addEditProfileRequest(Join8RequestBuilder.addProfile(access_token, edtFirstName.getText().toString().trim(), edtLastName.getText().toString().trim(), edtProfileName.getText().toString().trim(), edtEmail.getText().toString().trim(),
				edtPassword.getText().toString().trim(), language, mobileNo, bussinessPhone, edtCompany.getText().toString().trim(), edtJobTitle.getText().toString().trim(), edtAddress1.getText().toString().trim(), edtAddress2.getText().toString().trim(), edtRemark.getText()
						.toString().trim(), false, registration.getProvince(), registration.getRowKey(), registration.getLoginPreference(), registration.getGender(), 99, "", registration.getCity(), registration.getPartitionKey()), addProfileRequestId,
				Join8RequestBuilder.METHOD_ADD_USER_PROFILE);
	}

	private void isProfileExist() {
		Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);
		isProfileExists = networManager.addRequest(Join8RequestBuilder.isProfileExits(access_token, registration.getPartitionKey(), registration.getPartitionKey() + "-" + edtProfileName.getText().toString()), RequestMethod.POST, getActivity(),
				Join8RequestBuilder.METHOD_IS_USER_PROFILE_EXIST);
	}

	private void displayPageDot(int selectedIndex) {

		try {

			lnrPageIndicator.removeAllViews();
			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			params.setMargins(5, 5, 5, 5);

			for (int i = 0; i < size; i++) {

				ImageView imageView = new ImageView(getActivity());

				if (i == selectedIndex) {
					imageView.setBackgroundResource(R.drawable.pager_on);
				} else {
					imageView.setBackgroundResource(R.drawable.pager_off);
				}
				lnrPageIndicator.addView(imageView, params);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Error in displayPageDot", e.toString());
		}
	}

	private void fillData(Registration data) {

		isTextChangeIgnore = true;
		Log.e("fillData", "" + isTextChangeIgnore);

		try {

			if (data == null) {

				edtPassword.setEnabled(false);
				edtRepeatPassword.setEnabled(false);
				imvProfile.setEnabled(false);
				txtLanguage.setEnabled(false);
				txtLanguage.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);

				imvProfile.setImageResource(R.drawable.default_user);

				txtDeleteThisProfile.setVisibility(View.GONE);

				txtName.setText("");
				txtRegisteredIn.setText("");
				edtFirstName.setText("");
				edtLastName.setText("");
				edtPassword.setText("");
				edtRepeatPassword.setText("");
				edtProfileName.setText("");
				edtEmail.setText("");

				if (phoneCode.contains("-")) {
					edtCountryCode.setText(phoneCode.substring(0, phoneCode.indexOf("-")));
					edtCountryCodeBusiness.setText(phoneCode.substring(0, phoneCode.indexOf("-")));
					edtCityCodeBusiness.setText(phoneCode.substring(phoneCode.indexOf("-") + 1));
				} else {
					edtCountryCode.setText(phoneCode);
					edtCountryCodeBusiness.setText(phoneCode);
				}

				edtCompany.setText("");
				edtJobTitle.setText("");
				edtAddress1.setText("");
				edtAddress2.setText("");
				edtRemark.setText("");

				if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
					txtRegisteredIn.setText(String.format(getString(R.string.you_registered_in), ((HomeScreen) getActivity()).getCityData().getNameEN()));
					txtLanguage.setText(getString(R.string.tvEnglish));
				} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_SIMPLECHINSESE)) {
					txtRegisteredIn.setText(String.format(getString(R.string.you_registered_in), ((HomeScreen) getActivity()).getCityData().getName()));
					txtLanguage.setText(getString(R.string.chinese2));
				} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_TRADITIONALCHINESE)) {
					txtRegisteredIn.setText(String.format(getString(R.string.you_registered_in), ((HomeScreen) getActivity()).getCityData().getName()));
					txtLanguage.setText(getString(R.string.chinese1));
				}

			} else {

				if (userData.getCountry() != -1) {
					phoneCode = "" + userData.getCountry();
				}

				if (!userData.isDefaultProfile()) {
					edtPassword.setEnabled(false);
					edtRepeatPassword.setEnabled(false);
					imvProfile.setEnabled(false);
					txtLanguage.setEnabled(false);
					txtLanguage.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
					txtDeleteThisProfile.setVisibility(View.VISIBLE);
				} else {

					imvProfile.setEnabled(true);
					txtLanguage.setEnabled(true);
					txtDeleteThisProfile.setVisibility(View.GONE);

					if (userData.getLoginPreference().equals("3") || userData.getLoginPreference().equals("4") || userData.getLoginPreference().equals("5")) {
						edtPassword.setEnabled(false);
						edtRepeatPassword.setEnabled(false);
					} else {
						edtPassword.setEnabled(true);
						edtRepeatPassword.setEnabled(true);
					}
				}

				if (!Utils.isEmpty(data.getPhoto())) {
					imageLoader.displayImage(data.getPhoto(), imvProfile, options);
				}

				txtName.setText((data.getFirstName() + " " + data.getLastName()).trim());
				String country = UserProfileFragment.mapCityList.get(userData.getCity());
				txtRegisteredIn.setText(String.format(getString(R.string.you_registered_in), country));
				edtFirstName.setText(data.getFirstName());
				edtLastName.setText(data.getLastName());

				if (data.getUserPreferLanguage().equals("TC")) {
					txtLanguage.setText(getString(R.string.tvAreaEnglish));
				} else if (data.getUserPreferLanguage().equals("SC")) {
					txtLanguage.setText(getString(R.string.tvAreaEnglish));
				} else if (data.getUserPreferLanguage().equals("EN")) {
					txtLanguage.setText(getString(R.string.tvAreaEnglish));
				}

				if (userData.isDefaultProfile()) {
					edtPassword.setText(data.getLoginPassword());
					edtRepeatPassword.setText(data.getLoginPassword());
				} else {
					edtPassword.setText("");
					edtRepeatPassword.setText("");
				}

				String profileName = data.getProfileName();
				if (!Utils.isEmpty(profileName)) {

					if (profileName.contains("-")) {
						profileName = profileName.substring(profileName.lastIndexOf("-") + 1);
					}
					edtProfileName.setText(profileName);
				}
				edtEmail.setText(data.getEmail());

				String phone = data.getMobileNumber();

				if (phoneCode.contains("-")) {
					edtCountryCode.setText(phoneCode.substring(0, phoneCode.indexOf("-")));
				} else {
					edtCountryCode.setText(phoneCode);
				}

				if (!Utils.isEmpty(phone)) {

					if (phone.contains("|")) {
						phone = phone.replaceAll("\\|", "-");
					}

					if (phone.contains("-")) {

						String[] str = phone.split("-");
						edtCountryCode.setText(str[0]);
						edtMobilePhone.setText(str[str.length - 1]);

					} else {
						edtMobilePhone.setText(phone);
					}
				}

				if (phoneCode.contains("-")) {
					edtCountryCodeBusiness.setText(phoneCode.substring(0, phoneCode.indexOf("-")));
					edtCityCodeBusiness.setText(phoneCode.substring(phoneCode.indexOf("-") + 1));
				} else {
					edtCountryCodeBusiness.setText(phoneCode);
				}

				phone = data.getBusinessPhone();

				if (!Utils.isEmpty(phone)) {

					if (phone.contains("|")) {
						phone = phone.replaceAll("\\|", "-");
					}

					if (phone.contains("-")) {

						String[] str = phone.split("-");
						edtCountryCodeBusiness.setText(str[0]);
						if (str.length > 2) {
							edtCityCodeBusiness.setText(str[1]);
						}
						edtBusinessPhone.setText(str[str.length - 1]);

					} else {
						edtBusinessPhone.setText(phone);
					}
				}

				edtCompany.setText(data.getCompanyName());
				edtJobTitle.setText(data.getJobTitle());
				edtAddress1.setText(data.getAddress1());
				edtAddress2.setText(data.getAddress2());
				edtRemark.setText(data.getRemarks());
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Error in fillData", e.toString());
		}

		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {

				isTextChangeIgnore = false;
				Log.e("fillData", "" + isTextChangeIgnore);
			}
		}, 1000);

	}

	private void showLanguagePopup(final String[] strLang) {

		// final String[] strLang = new String[]{getString(R.string.tvEnglish),
		// getString(R.string.chinese1), getString(R.string.chinese2)};

		// int selection = Arrays.binarySearch(strLang,
		// txtLanguage.getText().toString());

		AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
		ab.setTitle(getString(R.string.app_name));
		ab.setSingleChoiceItems(strLang, 0, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface d, int choice) {
				txtLanguage.setText(strLang[choice]);
				((Dialog) d).dismiss();
			}
		});
		ab.show();
	}

	public void pageChanged(boolean save) {

		if (!save) {

			fillData(userData);

		} else {

			Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);

			if (TextUtils.isEmpty(edtFirstName.getText().toString().trim())) {
				Toast.displayText(getActivity(), getString(R.string.firstnamerequirederror));
			} else if (Utils.isEmpty(edtLastName.getText().toString().trim())) {
				Toast.displayText(getActivity(), getString(R.string.lastnamerequirederror));
			} else if (Utils.isEmpty(edtProfileName.getText().toString().trim())) {
				Toast.displayText(getActivity(), getString(R.string.profilenameerror));
			} else if (!registration.getLoginPreference().equals("6") && Utils.isEmpty(edtEmail.getText().toString().trim())) {
				Toast.displayText(getActivity(), getString(R.string.emailrequirederror));
			} else if (registration.getLoginPreference().equals("6") && Utils.isEmpty(edtMobilePhone.getText().toString().trim())) {
				Toast.displayText(getActivity(), getString(R.string.phonerequirederror));
			} else if (!Utils.isEmpty(edtEmail.getText().toString().trim()) && !Utils.checkEmail(edtEmail.getText().toString().trim())) {
				Toast.displayText(getActivity(), getString(R.string.emailnovalideerror));
			} else if (userData != null && userData.isDefaultProfile() && edtPassword.isEnabled() && Utils.isEmpty(edtPassword.getText().toString())) {
				Toast.displayText(getActivity(), getString(R.string.passwordrequirederror));
			} else if (userData != null && userData.isDefaultProfile() && edtRepeatPassword.isEnabled() && Utils.isEmpty(edtRepeatPassword.getText().toString())) {
				Toast.displayText(getActivity(), getString(R.string.repeatpasswordrequirederror));
			} else if (userData != null && userData.isDefaultProfile() && edtPassword.isEnabled() && !edtPassword.getText().toString().equals(edtRepeatPassword.getText().toString())) {
				Toast.displayText(getActivity(), getString(R.string.samepassword));
			} else if (!edtMobilePhone.getText().toString().trim().equals("") && (edtMobilePhone.getText().toString().length() < 8 || edtMobilePhone.getText().toString().length() > 11)) {
				Toast.displayText(getActivity(), getString(R.string.enterPhone));
			} else {

				if (UserProfileFragment.isNewProfile) {

					if (!isProfileNameAlreadyExist && !lostFocus) {

						fromFlag = true;
						networManager.isProgressVisible(true);
						isProfileExist();

					} else if (!isProfileNameAlreadyExist) {

						addProfile();

					} else if (!isProfileNameAlreadyExist) {

						Toast.displayText(getActivity(), getString(R.string.profilenameexisterror));
					}

				} else {
					editProfile();
				}
			}
		}
	}

	public interface OnProfileDeleteListner {

		public void onProfileDeleted(int index);

		public void onSwipeLeft(int index);

		public void onSwipeRight(int index);
	}

	@Override
	public void onSuccess(int id, String response) {

		try {

			Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);

			Log.i(getTag(), "Result => " + response);

			if (!Utils.isEmpty(response)) {

				if (id == deleteProfileRequestId) {

					if (response.toLowerCase().startsWith("success")) {

						if (onProfileDeleteListner != null) {
							onProfileDeleteListner.onProfileDeleted(index);
						}
					} else {
						Toast.displayText(getActivity(), getString(R.string.failed));
					}

				} else if (id == editProfileRequestId) {

					isEdited = false;

					final JSONObject jObject = new JSONObject(response);

					if (jObject.has("ActivationCode")) {

						if (userData.isDefaultProfile()) {

							prefManager.getPrefs().edit().putString(HomeScreen.PARAMS_USERDATA, jObject.toString()).commit();

						}
					}

					Toast.displayText(getActivity(), getString(R.string.profile_updated));
					((HomeScreen) getActivity()).clearBackStackEntry();
					HomeFragment homeFragment = new HomeFragment();
					getActivity().getSupportFragmentManager().beginTransaction().add(HomeScreen.FRAGMENT_CONTAINER, homeFragment).addToBackStack(null).commit();

				} else if (id == addProfileRequestId) {

					isEdited = false;

					if (response.startsWith("0")) {

						if (registration.getLoginPreference().equals("6")) {
							Toast.displayText(getActivity(), getString(R.string.mobilealreadyexist));
						} else {
							Toast.displayText(getActivity(), getString(R.string.emailalreadyexist));
						}

					} else if (response.startsWith("1")) {

						Toast.displayText(getActivity(), getString(R.string.profile_added));

						((HomeScreen) getActivity()).clearBackStackEntry();
						HomeFragment homeFragment = new HomeFragment();
						getActivity().getSupportFragmentManager().beginTransaction().add(HomeScreen.FRAGMENT_CONTAINER, homeFragment).addToBackStack(null).commit();

					} else if (response.startsWith("2")) {

						if (registration.getLoginPreference().equals("6")) {
							Toast.displayText(getActivity(), getString(R.string.enter_correct_phone));
						} else {
							Toast.displayText(getActivity(), getString(R.string.emailnovalideerror));
						}
					}

				} else if (id == isProfileExists) {

					lostFocus = true;

					if (response.toLowerCase().startsWith("false")) {

						isProfileNameAlreadyExist = false;

					} else {

						Toast.displayText(getActivity(), getString(R.string.profilenameexisterror));
						isProfileNameAlreadyExist = true;
					}

					if (fromFlag && !isProfileNameAlreadyExist) {
						addProfile();
					}
				} else if (id == mReqAreaList) {
					Gson gson = new Gson();
					Area[] arr = gson.fromJson(response, Area[].class);
					String[] cities = new String[arr.length];
					for (int i = 0; i < arr.length; i++) {
						cities[i] = arr[i].toString();
					}
					showLanguagePopup(cities);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			if (getActivity() != null && !getActivity().isFinishing() && isVisible()) {
				Toast.displayText(getActivity(), getString(R.string.failed));
			}

		}
	}
	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	private void addEditProfileRequest(final HashMap<String, String> request, final int requestId, final String method) {

		showProgressDialog();

		new Thread(new Runnable() {
			@Override
			public void run() {

				try {

					HttpClient httpclient = new DefaultHttpClient();

					HttpPost httppost = new HttpPost(ConstantData.WEBSERVICE_URL + method);
					httppost.setHeader("Content-Type", "application/json");

					if (request.size() > 0) {
						for (Map.Entry<String, String> entry : request.entrySet()) {
							httppost.addHeader(entry.getKey(), entry.getValue());
						}
					}

					if (byteData != null) {
						JSONObject jObjParam = new JSONObject();
						jObjParam.put("FileName", System.currentTimeMillis() + ".png");
						jObjParam.put("Email", request.get(Join8RequestBuilder.TAG_PARTITION_KEY));
						jObjParam.put("ContentType", "image/png");
						jObjParam.put("FileContent", new String(new Base64().encode(byteData)));
						httppost.setEntity(new StringEntity(jObjParam.toString()));
					}

					HttpResponse responce = httpclient.execute(httppost);

					HttpEntity entity = responce.getEntity();

					final String responseString = convertStreamToString(entity.getContent());

					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							dismissProgressDialog();
							ProfileFragment1.this.onSuccess(requestId, responseString);
						}
					});

				} catch (final Exception e) {
					e.printStackTrace();
					Log.e("Error in deleteProfileRequest", e.toString());

					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							dismissProgressDialog();
							ProfileFragment1.this.onError(requestId, e.toString());
						}
					});
				}
			}
		}).start();
	}

	private static String convertStreamToString(InputStream is) {

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			StringBuilder sb = new StringBuilder();

			String line = null;
			try {
				while ((line = reader.readLine()) != null) {
					sb.append(line + "\n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return sb.toString();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void deleteProfileRequest(final HashMap<String, String> request) {

		showProgressDialog();

		new Thread(new Runnable() {
			@Override
			public void run() {

				try {

					RestClient client = new RestClient(getActivity(), ConstantData.WEBSERVICE_URL + Join8RequestBuilder.METHOD_DELETE_USER_PROFILE, ProfileFragment1.this, deleteProfileRequestId);
					client.AddParam("Content-Type", ConstantData.WEBSERVICE_CONTENTTYPE);
					if (request != null) {
						for (Map.Entry<String, String> entry : request.entrySet()) {
							client.AddHeader(entry.getKey(), entry.getValue());
						}
					}
					client.execute(RequestMethod.POST);
					final String response = client.getResponse();
					Log.d("delete profile", "Response= " + client.getResponse());

					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							dismissProgressDialog();
							ProfileFragment1.this.onSuccess(deleteProfileRequestId, response);
						}
					});

				} catch (final Exception e) {
					e.printStackTrace();
					Log.e("Error in deleteProfileRequest", e.toString());

					getActivity().runOnUiThread(new Runnable() {
						@Override
						public void run() {
							dismissProgressDialog();
							ProfileFragment1.this.onError(deleteProfileRequestId, e.toString());
						}
					});
				}
			}
		}).start();
	}

	public void showProgressDialog() {

		progressDialog = new com.join8.layout.MyProgressDialog(getActivity());

	}

	/**
	 * Dismiss custom progress dialog
	 */
	public void dismissProgressDialog() {

		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	@Override
	public void onClick(View v) {

		if (v.equals(btnSave)) {

			Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);

			if (TextUtils.isEmpty(edtFirstName.getText().toString().trim())) {
				Toast.displayText(getActivity(), getString(R.string.firstnamerequirederror));
			} else if (Utils.isEmpty(edtLastName.getText().toString().trim())) {
				Toast.displayText(getActivity(), getString(R.string.lastnamerequirederror));
			} else if (Utils.isEmpty(edtProfileName.getText().toString().trim())) {
				Toast.displayText(getActivity(), getString(R.string.profilenameerror));
			} else if (registration.getLoginPreference().equals("6") && Utils.isEmpty(edtMobilePhone.getText().toString().trim())) {
				Toast.displayText(getActivity(), getString(R.string.phonerequirederror));
			} else if (!registration.getLoginPreference().equals("6") && Utils.isEmpty(edtEmail.getText().toString().trim())) {
				Toast.displayText(getActivity(), getString(R.string.emailrequirederror));
			} else if (!Utils.isEmpty(edtEmail.getText().toString().trim()) && !Utils.checkEmail(edtEmail.getText().toString().trim())) {
				Toast.displayText(getActivity(), getString(R.string.emailnovalideerror));
			} else if (userData != null && userData.isDefaultProfile() && edtPassword.isEnabled() && Utils.isEmpty(edtPassword.getText().toString())) {
				Toast.displayText(getActivity(), getString(R.string.passwordrequirederror));
			} else if (userData != null && userData.isDefaultProfile() && edtRepeatPassword.isEnabled() && Utils.isEmpty(edtRepeatPassword.getText().toString())) {
				Toast.displayText(getActivity(), getString(R.string.repeatpasswordrequirederror));
			} else if (userData != null && userData.isDefaultProfile() && edtPassword.isEnabled() && !edtPassword.getText().toString().equals(edtRepeatPassword.getText().toString())) {
				Toast.displayText(getActivity(), getString(R.string.samepassword));
			} else if (!edtMobilePhone.getText().toString().trim().equals("") && (edtMobilePhone.getText().toString().length() < 8 || edtMobilePhone.getText().toString().length() > 11)) {
				Toast.displayText(getActivity(), getString(R.string.enterPhone));
			} else {

				if (UserProfileFragment.isNewProfile) {

					if (!isProfileNameAlreadyExist && !lostFocus) {

						fromFlag = true;
						networManager.isProgressVisible(true);
						isProfileExist();

					} else if (!isProfileNameAlreadyExist) {

						addProfile();

					} else if (isProfileNameAlreadyExist) {

						Toast.displayText(getActivity(), getString(R.string.profilenameexisterror));
					}

				} else {
					editProfile();
				}
			}

		} else if (v.equals(btnCancel)) {

			AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
			dialog.setMessage(getString(R.string.profile_cancel));
			dialog.setPositiveButton(getString(R.string.yes), new android.content.DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

					((HomeScreen) getActivity()).clearBackStackEntry();
					HomeFragment homeFragment = new HomeFragment();
					getActivity().getSupportFragmentManager().beginTransaction().add(HomeScreen.FRAGMENT_CONTAINER, homeFragment).addToBackStack(null).commit();

					// if (UserProfileFragment.isNewProfile) {
					// UserProfileFragment.isNewProfile = false;
					//
					// if (onProfileDeleteListner != null) {
					// onProfileDeleteListner.onProfileDeleted(index);
					// }
					//
					// } else {
					//
					// fillData(userData);
					// }
				}
			});
			dialog.setNegativeButton(getString(R.string.no), new android.content.DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			});
			dialog.show();
		}
	}

	@Override
	public void afterTextChanged(Editable s) {
		if (!isTextChangeIgnore && !UserProfileFragment.isNewProfile) {
			isEdited = true;
			Log.e("afterTextChanged - " + index, "text changed = " + isEdited);
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == INTENT_CAMERA && resultCode == Activity.RESULT_OK) {

			Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
			if (thumbnail.getWidth() > 300) {
				thumbnail = Utils.resizeBitmapWithRatio(thumbnail, 600);
			}

			imvProfile.setScaleType(ScaleType.CENTER_CROP);
			imvProfile.setImageBitmap(RoundedBitmapDisplayer.roundCorners(thumbnail, imvProfile, roundPixels));

			ByteArrayOutputStream stream = new ByteArrayOutputStream();

			thumbnail = resizeBMP(thumbnail);

			thumbnail.compress(Bitmap.CompressFormat.JPEG /* FileType */, 100 /* Ratio */, stream);

			byteData = stream.toByteArray();

			Log.e("lengthhhh", byteData.length + "");

		} else if (requestCode == INTENT_GALLERY && resultCode == Activity.RESULT_OK) {

			Uri selectedImage = data.getData();
			String[] filePathColumn = {MediaStore.Images.Media.DATA};

			Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
			cursor.moveToFirst();

			int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
			String filePath = cursor.getString(columnIndex);
			cursor.close();

			Bitmap thumbnail = BitmapFactory.decodeFile(filePath);
			if (thumbnail.getWidth() > 300) {
				thumbnail = Utils.resizeBitmapWithRatio(thumbnail, 600);
			}

			imvProfile.setScaleType(ScaleType.CENTER_CROP);
			imvProfile.setImageBitmap(RoundedBitmapDisplayer.roundCorners(thumbnail, imvProfile, roundPixels));

			ByteArrayOutputStream stream = new ByteArrayOutputStream();

			thumbnail.compress(Bitmap.CompressFormat.JPEG /* FileType */, 100 /* Ratio */, stream);

			byteData = stream.toByteArray();

			Log.e("lengthhhh", byteData.length + "");

		}
	}

	private static Bitmap resizeBMP(Bitmap bmp) {
		int width;
		int height;
		float scaleWidth;
		float scaleHeight;

		float newHeight;
		float newWidth = 600;

		Matrix matrix;

		Bitmap resizedBitmap;

		width = bmp.getWidth();
		height = bmp.getHeight();

		newHeight = (float) (((float) ((float) height / (float) width)) * 200);

		scaleWidth = ((float) newWidth) / width;
		scaleHeight = ((float) newHeight) / height;

		matrix = new Matrix();

		matrix.postScale(scaleWidth, scaleHeight);

		resizedBitmap = Bitmap.createBitmap(bmp, 0, 0, width, height, matrix, false);

		return resizedBitmap;

	}
}