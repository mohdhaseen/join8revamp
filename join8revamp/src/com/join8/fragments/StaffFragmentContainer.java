package com.join8.fragments;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.join8.R;
import com.join8.slidingmenu.SlidingActivity;
import com.join8.utils.KeyboardUtils;
import com.join8.utils.TypefaceUtils;

public class StaffFragmentContainer extends Fragment implements OnClickListener, OnTouchListener {

	private static final String Tag = StaffFragmentContainer.class.getSimpleName();
	private static final int FRAGMENT_CONTAINER = R.id.fragment_container;
	private ImageView imgHome;
	private TextView txtActionTitle;
	private RadioButton rBooking;
	private RadioButton rOrder;
	private RadioButton rPrint;
	private RadioGroup radioHome;
	private boolean isTouched = false;
	private boolean isFromPush = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments() != null) {

			isFromPush = true;

			if (getArguments().getString("redirectTo").equalsIgnoreCase("StaffOrderDetail")) {

				Bundle bundle = new Bundle();
				bundle.putString("redirectTo", "StaffOrderDetail");
				bundle.putString("reference", getArguments().getString("reference"));

				StaffOrderListFragment fragment = new StaffOrderListFragment();
				fragment.setArguments(bundle);
				getChildFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, fragment).commit();

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						rOrder.setChecked(true);
					}
				}, 100);

			} else if (getArguments().getString("redirectTo").equalsIgnoreCase("StaffBookingDetail")) {

				Bundle bundle = new Bundle();
				bundle.putString("redirectTo", "StaffBookingDetail");
				bundle.putString("reference", getArguments().getString("reference"));

				StaffBookingListFragment fragment = new StaffBookingListFragment();
				fragment.setArguments(bundle);
				getChildFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, fragment).commit();

				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						rBooking.setChecked(true);
					}
				}, 100);
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.homelayout, container, false);
		initActionBar((ViewGroup) mView.findViewById(R.id.drawer_layout));
		return mView;
	}

	private void initActionBar(ViewGroup mViewGroup) {

		ActionBar mActionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();

		mActionBar.setHomeButtonEnabled(false);
		mActionBar.setDisplayHomeAsUpEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		mActionBar.setDisplayUseLogoEnabled(false);
		mActionBar.setDisplayShowCustomEnabled(true);
		mActionBar.setDisplayShowHomeEnabled(false);

		View mActionbarView = getActivity().getLayoutInflater().inflate(R.layout.staff_actionbar, mViewGroup, false);
		mActionbarView.setBackgroundColor(getResources().getColor(R.color.staff_actionbar_bg_color));
		mActionBar.setCustomView(mActionbarView);

		imgHome = (ImageView) mActionbarView.findViewById(R.id.imgHome);
		txtActionTitle = (TextView) mActionbarView.findViewById(R.id.txtTitle);
		txtActionTitle.setText("");

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(txtActionTitle);

		radioHome = (RadioGroup) mActionbarView.findViewById(R.id.radioHome);
		rBooking = (RadioButton) mActionbarView.findViewById(R.id.booking);
		rPrint = (RadioButton) mActionbarView.findViewById(R.id.print);
		rOrder = (RadioButton) mActionbarView.findViewById(R.id.order);
		imgHome.setOnClickListener(this);

		rBooking.setOnTouchListener(this);
		rOrder.setOnTouchListener(this);
		rPrint.setOnTouchListener(this);
		
		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter == null) {
			rPrint.setVisibility(View.GONE);
		}
		radioHome.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(RadioGroup group, int checkedId) {

				if (isTouched) {

					isTouched = false;

					switch (checkedId) {

					case R.id.order:
						getChildFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, new StaffOrderListFragment()).commit();
						break;

					case R.id.booking:
						getChildFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, new StaffBookingListFragment()).commit();

						break;
					case R.id.print:
						getChildFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, new PrinterSettingFragment()).commit();

						break;
					}
				}
			}
		});

		if (!isFromPush) {
			isTouched = true;
			rBooking.setChecked(true);
		}
	}

	public void setActionBarTitle(String title) {
		txtActionTitle.setText(title);
		txtActionTitle.setVisibility(View.VISIBLE);
		imgHome.setImageResource(R.drawable.back_blue);
		radioHome.setVisibility(View.GONE);
	}

	public void showTabMenu() {
		txtActionTitle.setVisibility(View.GONE);
		imgHome.setImageResource(R.drawable.staff_home_icon);
		radioHome.setVisibility(View.VISIBLE);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.imgHome:

			try {
				KeyboardUtils.hideKeyboard(v);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (getChildFragmentManager().getBackStackEntryCount() > 0) {
				getChildFragmentManager().popBackStack();
				showTabMenu();
			} else {
				((SlidingActivity) getActivity()).getSlidingMenu().toggle();
			}
			break;
		}
	}

	public void addFragment(Fragment f, boolean isAddToBackStack) {
		Log.d(Tag, "addFragment");
		if (isAddToBackStack) {
			getChildFragmentManager().beginTransaction().add(FRAGMENT_CONTAINER, f).addToBackStack(null).commit();
		} else {
			getChildFragmentManager().beginTransaction().add(FRAGMENT_CONTAINER, f).commit();
		}
	}

	public void replaceFragment(Fragment f, boolean isAddToBackStack) {
		Log.d(Tag, "replaceFragment");
		if (isAddToBackStack) {
			getChildFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, f).addToBackStack(null).commit();
		} else {
			getChildFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, f).commit();
		}

	}

	public void popBackStack() {
		getChildFragmentManager().popBackStack();
		showTabMenu();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		isTouched = true;
		return false;
	}

	public interface SendBack {
		public void onBack();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d(Tag, "onActivityResult");
		Fragment f = getChildFragmentManager().findFragmentById(FRAGMENT_CONTAINER);
		if(f != null && f instanceof StaffOrderDetailFragment) {
			f.onActivityResult(requestCode, resultCode, data);
		}else if(f != null && f instanceof PrinterSettingFragment) {
			f.onActivityResult(requestCode, resultCode, data);
		}
	}
}
