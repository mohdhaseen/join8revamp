package com.join8.fragments;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.costum.android.widget.LoadMoreListView;
import com.costum.android.widget.LoadMoreListView.OnLoadMoreListener;
import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.RestaurantActivity;
import com.join8.Toast;
import com.join8.adpaters.CuisineListAdapter;
import com.join8.adpaters.CuisineListAdapter.OnCuisineSelectListener;
import com.join8.adpaters.HorizontalGalleryAdapter;
import com.join8.adpaters.SearchMenuAdapter;
import com.join8.adpaters.SearchRestaurantAdapter;
import com.join8.listeners.Requestlistener;
import com.join8.model.City;
import com.join8.model.CuisineCategoryData;
import com.join8.model.RegionData;
import com.join8.model.ResponseCity;
import com.join8.model.RestaurantData;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.KeyboardUtils;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

public class HomeFragment extends Fragment implements OnClickListener, Requestlistener {

	private static final int COUNT_PER_PAGE = 25;

	private EditText edtSearch;
	private Button btnRestaurant, btnMenuItem;
	private ImageView imvBanner;
	private Gallery mGallery = null;
	public boolean isSearchRestaurant = true, isCusineList = true;

	private NetworkManager networManager;
	private CryptoManager prefManager;
	private int allCusinesRequestId = -1, restaurentByCuisineRequestId = -1, searchRequestId = -1, regionRequestId = -1;

	private String accessToken = "";
	public ResponseCity city;
	private TextView txtSwitchSearch, txtRegion, txtCuisineName;
	private ArrayList<RestaurantData> listRestaurant;
	private int prePosition = -1;

	private TextView txtResults;
	private SearchRestaurantAdapter searchRestaurantAdapter;
	private SearchMenuAdapter searchMenuAdapter;
	private LoadMoreListView listView;
	private int totalRecords = 0;

	private ArrayList<CuisineCategoryData> listCuisineCategory = null;
	private ArrayList<RegionData> listRegion = null;
	public int selectedRegionId = 0;
	private int cuisineTypeId = 0;
	private String noOfDays = "0";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		networManager = NetworkManager.getInstance();
		networManager.addListener(this);
		prefManager = CryptoManager.getInstance(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.homescreen, container, false);
		btnRestaurant = (Button) v.findViewById(R.id.btnRestaurant);
		btnMenuItem = (Button) v.findViewById(R.id.btnMenu);
		edtSearch = (EditText) v.findViewById(R.id.edtSearchView);
		txtResults = (TextView) v.findViewById(R.id.txtResults);
		listView = (LoadMoreListView) v.findViewById(R.id.lstSearch);
		txtSwitchSearch = (TextView) v.findViewById(R.id.txtSwitchSearch);
		txtRegion = (TextView) v.findViewById(R.id.txtRegion);
		txtCuisineName = (TextView) v.findViewById(R.id.txtCuisineName);
		imvBanner = (ImageView) v.findViewById(R.id.imvBanner);
		mGallery = (Gallery) v.findViewById(R.id.gallery);
		mGallery.setVisibility(View.GONE);
		txtResults.setVisibility(View.GONE);
		txtCuisineName.setVisibility(View.GONE);

	//	((HomeScreen) getActivity()).initActionBar();
		txtSwitchSearch.setOnClickListener(this);
		txtRegion.setOnClickListener(this);
		btnRestaurant.setOnClickListener(this);
		btnMenuItem.setOnClickListener(this);

		txtResults.setText(getString(R.string.norecorderror));

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(txtResults);
		mTypefaceUtils.applyTypeface(txtSwitchSearch);
		mTypefaceUtils.applyTypeface(txtRegion);
		mTypefaceUtils.applyTypeface(btnRestaurant);
		mTypefaceUtils.applyTypeface(btnMenuItem);
		mTypefaceUtils.applyTypeface(edtSearch);

		mGallery.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					prePosition = mGallery.getSelectedItemPosition();
				} else if (event.getAction() == MotionEvent.ACTION_UP) {

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {

							try {

								if (prePosition != mGallery.getSelectedItemPosition()) {

									switch (mGallery.getSelectedItemPosition()) {

										case 0 :
											noOfDays = "-1";
											break;
										case 1 :
											noOfDays = "-3";
											break;
										case 2 :
											noOfDays = "-7";
											break;
										case 3 :
											noOfDays = "-30";
											break;
									}

									if (edtSearch.getVisibility() != View.VISIBLE) {
										edtSearch.setText("");
										txtSwitchSearch.setSelected(false);
									}

									isSearchRestaurant = false;
									searchData("", "0", edtSearch.getText().toString().trim(), noOfDays, selectedRegionId);
								}

							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}, 500);
				}
				return false;
			}
		});

		edtSearch.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

				if (event != null && event.getAction() != KeyEvent.ACTION_DOWN) {
					return false;
				}

				if (actionId == EditorInfo.IME_ACTION_SEARCH) {

					if (!Utils.isEmpty(edtSearch.getText().toString().trim())) {

						txtCuisineName.setVisibility(View.GONE);
						txtRegion.setSelected(false);
						txtSwitchSearch.setSelected(true);
						txtResults.setVisibility(View.VISIBLE);
						KeyboardUtils.hideKeyboard(v);

						if (isSearchRestaurant) {
							searchData(edtSearch.getText().toString().trim(), "0", "", "0", selectedRegionId);
						} else {
							searchData("", "0", edtSearch.getText().toString().trim(), "0", selectedRegionId);
							mGallery.setVisibility(View.GONE);
							noOfDays = "0";
						}
					}
					return true;
				}

				return false;
			}
		});

		listView.setOnLoadMoreListener(new OnLoadMoreListener() {
			@Override
			public void onLoadMore() {

				if (totalRecords > 0 && listRestaurant.size() <= totalRecords) {

					if (isSearchRestaurant) {
						searchMoreData("", "0", "", "0", listRestaurant.size(), selectedRegionId);
					} else {
						searchMoreData("", "0", "", "2", listRestaurant.size(), selectedRegionId);
					}
				}
			}
		});

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				if (!isCusineList) {

					if (isSearchRestaurant) {
						KeyboardUtils.hideKeyboard(view);
						Intent intent = new Intent(getActivity(), RestaurantActivity.class);
						intent.putExtra("restaurantData", listRestaurant.get(position));
						getActivity().startActivityForResult(intent, 33);

					} else {

						Intent intent = new Intent(getActivity(), RestaurantActivity.class);
						intent.putExtra("restaurantData", listRestaurant.get(position));
						intent.putExtra("fromFlag", true);
						getActivity().startActivityForResult(intent, 33);
					}
				}
			}
		});

		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);

		try {
			if (listView != null && listView.getAdapter() != null && !isSearchRestaurant) {
				searchMenuAdapter.notifyDataSetChanged();
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if (networManager != null) {
			networManager.removeListeners(this);
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (isAdded()) {

			city = ((HomeScreen) getActivity()).getCityData();

			mGallery.setAdapter(new HorizontalGalleryAdapter(getActivity(), getResources().getStringArray(R.array.disharray)));
			mGallery.setSelection(0);

			accessToken = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
			listRestaurant = new ArrayList<RestaurantData>();

			loadRegion();
			loadAllCusines(selectedRegionId);
		}
	}

	private void displayBanner(String url, final RestaurantData restaurantData) {

		try {

			imvBanner.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {

					Intent intent = new Intent(getActivity(), RestaurantActivity.class);
					intent.putExtra("restaurantData", restaurantData);
					getActivity().startActivityForResult(intent, 33);
				}
			});

			ImageLoader.getInstance().displayImage(url, imvBanner, new ImageLoadingListener() {

				@Override
				public void onLoadingStarted(String imageUri, View view) {
					imvBanner.setVisibility(View.GONE);
				}

				@Override
				public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
					imvBanner.setVisibility(View.GONE);
				}

				@Override
				public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
					imvBanner.setVisibility(View.VISIBLE);
				}

				@Override
				public void onLoadingCancelled(String imageUri, View view) {
					imvBanner.setVisibility(View.GONE);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showRegionPopup() {

		String[] items = new String[listRegion.size()];
		int selectedIndex = 0;

		for (int i = 0; i < listRegion.size(); i++) {

			if (listRegion.get(i).getAreaId() == selectedRegionId) {
				selectedIndex = i;
			}

			if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_SIMPLECHINSESE)) {
				items[i] = listRegion.get(i).getAreaNameSC();
			} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_TRADITIONALCHINESE)) {
				items[i] = listRegion.get(i).getAreaNameTC();
			} else {
				items[i] = listRegion.get(i).getAreaNameEN();
			}
		}

		AlertDialog.Builder ab = new AlertDialog.Builder(getActivity());
		ab.setTitle(getString(R.string.region));
		ab.setSingleChoiceItems(items, selectedIndex, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface d, int choice) {

				selectedRegionId = listRegion.get(choice).getAreaId();
				listCuisineCategory.clear();

				if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_SIMPLECHINSESE)) {
					txtRegion.setText(listRegion.get(choice).getAreaNameSC());
				} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_TRADITIONALCHINESE)) {
					txtRegion.setText(listRegion.get(choice).getAreaNameTC());
				} else {
					txtRegion.setText(listRegion.get(choice).getAreaNameEN());
				}

				if (isSearchRestaurant) {
					searchData(edtSearch.getText().toString().trim(), "0", "", "0", selectedRegionId);
				} else {
					searchData("", "0", edtSearch.getText().toString().trim(), noOfDays, selectedRegionId);
				}

				((Dialog) d).dismiss();
			}
		});

		ab.show();
	}

	private void loadRegion() {

		networManager.isProgressVisible(true);
	//	regionRequestId = networManager.addRequest(Join8RequestBuilder.getRegionRequest(accessToken, city != null ? city.getCountryCode() : "852"), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_GET_AREA);
	}

	public void loadAllCusines(int areaId) {

		/*isCusineList = true;
		totalRecords = 0;
		isSearchRestaurant = true;
		txtResults.setText("");

		if (listCuisineCategory != null && listCuisineCategory.size() > 0) {
			listView.setAdapter(new CuisineListAdapter(getActivity(), listCuisineCategory, onCuisineSelectListener));
			txtResults.setVisibility(View.GONE);
		} else {

			if (city != null && !Utils.isEmpty(city.getCountryCode())) {
				networManager.isProgressVisible(true);
				allCusinesRequestId = networManager.addRequest(Join8RequestBuilder.getAllCusinesRequest(accessToken, city != null ? city.getCountryCode() : "852", areaId), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_GET_CUISINE_BY_CITY);
			} else {
				Toast.displayText(getActivity(), getString(R.string.selectCity));
			}
		}*/
	}

	private void loadRestaurantByCusine(int id) {

		isCusineList = false;
		totalRecords = 0;
		isSearchRestaurant = true;
		txtResults.setText("");

		if (listRestaurant != null) {
			listRestaurant.clear();
		}
		listView.setAdapter(new SearchRestaurantAdapter(getActivity(), listRestaurant));

		networManager.isProgressVisible(true);
	//	restaurentByCuisineRequestId = networManager.addRequest(Join8RequestBuilder.getRestByCusineRequest(accessToken, city != null ? city.getCountryCode() : "852", id, selectedRegionId), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_GET_REST_BY_CUISINE);
	}

	private void searchData(String companyName, String cuisineType, String itemName, String noOfDays, int areaId) {

	/*	isCusineList = false;
		totalRecords = 0;
		txtResults.setText("");

		if (listRestaurant != null) {
			listRestaurant.clear();
		}
		listView.setAdapter(new SearchRestaurantAdapter(getActivity(), listRestaurant));

		networManager.isProgressVisible(true);
		if (city != null && !Utils.isEmpty(city.getCountryCode())) {
			searchRequestId = networManager.addRequest(Join8RequestBuilder.getSearchRequest(accessToken, city != null ? city.getCountryCode() : "852", companyName, "" + COUNT_PER_PAGE, cuisineType, itemName, noOfDays, 0, areaId), RequestMethod.POST, getActivity(),
					Join8RequestBuilder.METHOD_SEARCH);
		} else {
			Toast.displayText(getActivity(), getString(R.string.selectCity));
		}*/
	}

	private void searchMoreData(String companyName, String cuisineType, String itemName, String noOfDays, int startIndex, int areaId) {

		/*if (listRestaurant == null) {
			listRestaurant = new ArrayList<RestaurantData>();
		}

		networManager.isProgressVisible(false);
		if (city != null && !Utils.isEmpty(city.getCityNameEN())) {
			searchRequestId = networManager.addRequest(Join8RequestBuilder.getSearchRequest(accessToken, city != null ? city.getCountryCode() : "852", companyName, "" + COUNT_PER_PAGE, cuisineType, itemName, noOfDays, startIndex, areaId), RequestMethod.POST, getActivity(),
					Join8RequestBuilder.METHOD_SEARCH);
		}*/
	}

	@Override
	public void onSuccess(int id, String response) {

		if (isAdded() && !Utils.isEmpty(response)) {

			if (id == regionRequestId) {

				try {

					JSONArray jArrayResult = new JSONArray(response);
					listRegion = new ArrayList<RegionData>();

					for (int i = 0; i < jArrayResult.length(); i++) {

						RegionData data = new Gson().fromJson(jArrayResult.getString(i), RegionData.class);
						listRegion.add(data);
					}

					if (listRegion != null && listRegion.size() > 0) {
						if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_SIMPLECHINSESE)) {
							txtRegion.setText(listRegion.get(0).getAreaNameSC());
						} else if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_TRADITIONALCHINESE)) {
							txtRegion.setText(listRegion.get(0).getAreaNameTC());
						} else {
							txtRegion.setText(listRegion.get(0).getAreaNameEN());
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			} else if (id == allCusinesRequestId) {

				try {

					listCuisineCategory = new ArrayList<CuisineCategoryData>();

					JSONObject jObjResult = new JSONObject(response);
					JSONArray jArray = jObjResult.getJSONArray("CategoriesList");

					if (jObjResult.has("BannerImage")) {

						RestaurantData restaurantData = new Gson().fromJson(jObjResult.getString("CompanyDetails"), RestaurantData.class);
						displayBanner(jObjResult.getString("BannerImage"), restaurantData);
					}

					for (int i = 0; i < jArray.length(); i++) {

						CuisineCategoryData data = new Gson().fromJson(jArray.getJSONObject(i).toString(), CuisineCategoryData.class);
						listCuisineCategory.add(data);
					}

					listView.setAdapter(new CuisineListAdapter(getActivity(), listCuisineCategory, onCuisineSelectListener));

					if (listCuisineCategory.size() > 0) {
						txtResults.setVisibility(View.GONE);
					} else {
						txtResults.setVisibility(View.VISIBLE);
						txtResults.setText(getString(R.string.norecorderror));
					}

				} catch (Exception e) {
					e.printStackTrace();
					txtResults.setVisibility(View.VISIBLE);
					txtResults.setText(getString(R.string.norecorderror));
				}

			} else if (id == restaurentByCuisineRequestId) {

				try {

					listRestaurant = new ArrayList<RestaurantData>();

					if (response.startsWith("Record Not Found")) {
						txtResults.setVisibility(View.VISIBLE);
						txtResults.setText(getString(R.string.norecorderror));
					} else {

						JSONArray jArray = new JSONArray(response);

						for (int i = 0; i < jArray.length(); i++) {

							RestaurantData data = new Gson().fromJson(jArray.getJSONObject(i).toString(), RestaurantData.class);
							listRestaurant.add(data);
						}

						listView.setAdapter(new SearchRestaurantAdapter(getActivity(), listRestaurant));

						txtResults.setVisibility(View.GONE);
						// txtResults.setText(listRestaurant.size() + " " +
						// getString(R.string.result_for) + " \"" +
						// selectedCategory + "\"");

					}

				} catch (Exception e) {
					e.printStackTrace();
					txtResults.setVisibility(View.VISIBLE);
					txtResults.setText(getString(R.string.norecorderror));
				}

			} else if (id == searchRequestId) {

				try {

					JSONArray jsonArray = new JSONArray(response);

					if (jsonArray != null && jsonArray.length() > 0) {

						totalRecords = jsonArray.getJSONObject(0).getInt("TotalRecords");

						for (int i = 0; i < jsonArray.length(); i++) {
							RestaurantData searchData = new Gson().fromJson(jsonArray.getJSONObject(i).toString(), RestaurantData.class);
							listRestaurant.add(searchData);
						}

						if (isSearchRestaurant) {
							searchRestaurantAdapter = new SearchRestaurantAdapter(getActivity(), listRestaurant);
							listView.setAdapter(searchRestaurantAdapter);
						} else {
							searchMenuAdapter = new SearchMenuAdapter(getActivity(), listRestaurant);
							listView.setAdapter(searchMenuAdapter);
						}

						if (edtSearch.getVisibility() == View.VISIBLE && !Utils.isEmpty(edtSearch.getText().toString().trim())) {
							txtResults.setVisibility(View.VISIBLE);
							txtResults.setText(totalRecords + " " + getString(R.string.result_for) + " \"" + edtSearch.getText().toString() + "\"");
						} else {
							txtResults.setVisibility(View.GONE);
						}

					} else {

						listRestaurant.clear();
						listView.setAdapter(null);
						txtResults.setVisibility(View.VISIBLE);
						txtResults.setText(getString(R.string.norecorderror));
					}

				} catch (JSONException e) {

					if (listRestaurant != null) {
						listRestaurant.clear();
						listView.setAdapter(null);
					}
					txtResults.setVisibility(View.VISIBLE);
					txtResults.setText(getString(R.string.norecorderror));
				}
			}
		}
	}

	@Override
	public void onError(int id, String message) {
		if (isAdded()) {
			txtResults.setText(getString(R.string.no_internet));
		}
	}

	@Override
	public void onClick(View v) {

		KeyboardUtils.hideKeyboard(v);

		switch (v.getId()) {

			case R.id.btnRestaurant :

				txtRegion.setSelected(false);
				txtSwitchSearch.setSelected(false);
				isSearchRestaurant = true;
				txtCuisineName.setVisibility(View.GONE);
				mGallery.setVisibility(View.GONE);
				noOfDays = "0";
				txtResults.setVisibility(View.GONE);

				// txtRegion.setText(getString(R.string.show_all_rest));
				listRestaurant.clear();
				searchRestaurantAdapter = new SearchRestaurantAdapter(getActivity(), listRestaurant);
				listView.setAdapter(searchRestaurantAdapter);
				txtResults.setText(getString(R.string.norecorderror));

				edtSearch.setHint(getString(R.string.restaurant));
				btnRestaurant.setBackgroundDrawable(getResources().getDrawable(R.drawable.restourent_bg_hover));
				btnMenuItem.setBackgroundDrawable(getResources().getDrawable(R.drawable.menu_bg));
				btnRestaurant.setTextColor(Color.WHITE);
				btnMenuItem.setTextColor(getResources().getColor(R.color.orangebg));
				btnRestaurant.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.restauranth), null, null, null);
				btnMenuItem.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.menuhomen), null, null, null);
				isSearchRestaurant = true;

				edtSearch.setText("");

				loadAllCusines(selectedRegionId);

				break;

			case R.id.btnMenu :

				txtRegion.setSelected(false);
				txtSwitchSearch.setSelected(false);
				isSearchRestaurant = false;
				txtCuisineName.setVisibility(View.GONE);
				mGallery.setVisibility(View.VISIBLE);
				txtResults.setVisibility(View.GONE);

				// txtRegion.setText(getString(R.string.show_all_menu_items));
				listRestaurant.clear();
				searchRestaurantAdapter = new SearchRestaurantAdapter(getActivity(), listRestaurant);
				listView.setAdapter(searchRestaurantAdapter);
				txtResults.setText(getString(R.string.norecorderror));

				edtSearch.setHint(getString(R.string.menuitem));
				btnRestaurant.setBackgroundDrawable(getResources().getDrawable(R.drawable.restourent_bg));
				btnMenuItem.setBackgroundDrawable(getResources().getDrawable(R.drawable.menu_bg_hover));
				btnRestaurant.setTextColor(getResources().getColor(R.color.orangebg));
				btnMenuItem.setTextColor(Color.WHITE);
				btnRestaurant.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.restaurantn), null, null, null);
				btnMenuItem.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.menuhome), null, null, null);
				isSearchRestaurant = false;

				edtSearch.setText("");

				mGallery.setSelection(0);

				searchData("", "0", "", "-1", selectedRegionId);

				break;

			case R.id.txtSwitchSearch :

				txtRegion.setSelected(false);

				if (edtSearch.getVisibility() != View.VISIBLE) {
					txtSwitchSearch.setText(getString(R.string.close_search));
					edtSearch.setVisibility(View.VISIBLE);
					if (isSearchRestaurant) {
						edtSearch.setHint(getString(R.string.restaurant));
					} else {
						edtSearch.setHint(getString(R.string.menuitem));
						mGallery.setVisibility(View.VISIBLE);
					}
				} else {
					txtSwitchSearch.setText(getString(R.string.search));
					edtSearch.setText("");
					edtSearch.setVisibility(View.GONE);
				}
				break;

			case R.id.txtRegion :

				/*if (city != null && !Utils.isEmpty(city.getCountryCode())) {
					txtRegion.setSelected(true);
					txtSwitchSearch.setSelected(false);
					txtCuisineName.setVisibility(View.GONE);
					txtResults.setVisibility(View.GONE);
					showRegionPopup();
				} else {
					Toast.displayText(getActivity(), getString(R.string.selectCity));
				}*/

				break;

		// case R.id.txtShowAllRestaurant:
		//
		// txtRegion.setSelected(true);
		// txtSwitchSearch.setSelected(false);
		// edtSearch.setText("");
		// txtCuisineName.setVisibility(View.GONE);
		// txtResults.setVisibility(View.GONE);
		//
		// if (isSearchRestaurant) {
		// searchData("", "0", "", "0");
		// } else {
		// mGallery.setVisibility(View.GONE);
		// searchData("", "0", "", "2");
		// }
		// break;
		}
	}

	OnCuisineSelectListener onCuisineSelectListener = new OnCuisineSelectListener() {

		@Override
		public void onCuisineSelected(int id, String category) {
			cuisineTypeId = id;
			loadRestaurantByCusine(cuisineTypeId);
			txtCuisineName.setText(category);
			txtCuisineName.setVisibility(View.VISIBLE);
		}
	};
}
