package com.join8.fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.gson.Gson;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.Toast;
import com.join8.calendar.CalendarView;
import com.join8.calendar.CalendarView.OnMonthChangedListener;
import com.join8.listeners.Requestlistener;
import com.join8.model.HolidayData;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.CryptoManager;

public class ViewHolidaysFragment extends Fragment implements Requestlistener {

	private static final String TAG = ViewHolidaysFragment.class.getSimpleName();
	
	private ArrayList<HolidayData> arrHolidays = null;
	private CalendarView calendarView = null;

	private ImageButton btnFav = null;
	private TextView txtTitle = null, txtOpen = null;

	private CryptoManager prefsManager;
	private NetworkManager networManager;
	private int holidayRequestId = -1;

	private int companyId = -1;
	private String access_token = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		prefsManager = CryptoManager.getInstance(getActivity());
		networManager = NetworkManager.getInstance();
		networManager.isProgressVisible(true);

		View view = getActivity().getActionBar().getCustomView();
		btnFav = (ImageButton) view.findViewById(R.id.btnFav);
		txtTitle = (TextView) view.findViewById(R.id.txtTitle);
		txtOpen = (TextView) view.findViewById(R.id.txtOpen);
		txtOpen.setVisibility(View.GONE);
		btnFav.setVisibility(View.INVISIBLE);

		companyId = getArguments().getInt("companyId");

		arrHolidays = new ArrayList<HolidayData>();
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.view_holiday_calendar, container, false);

		calendarView = (CalendarView) v.findViewById(R.id.calendar_view);

		calendarView.setOnMonthChangedListener(new OnMonthChangedListener() {
			@Override
			public void onMonthChanged(CalendarView view) {

				calendarView.refreshDayCells(getHolidaysOfCurrentMonth(calendarView.getCalendar().getMonth(), calendarView.getCalendar().getYear()));
			}
		});

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		access_token = prefsManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");

		loadHolidayData();
	}

	private void loadHolidayData() {

		networManager.isProgressVisible(true);
		holidayRequestId = networManager.addRequest(Join8RequestBuilder.getHolidayList(access_token, "" + companyId), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_HOLIDAY_LIST);
	}

	private ArrayList<Integer> getHolidaysOfCurrentMonth(int month, int year) {

		ArrayList<Integer> listDays = new ArrayList<Integer>();

		if (arrHolidays != null && arrHolidays.size() > 0) {

			for (int i = 0; i < arrHolidays.size(); i++) {
				if (arrHolidays.get(i).getMonth() == month && arrHolidays.get(i).getYear() == year) {
					listDays.add(Integer.valueOf(arrHolidays.get(i).getDay()));
				}
			}
		}
		return listDays;
	}

	@Override
	public void onSuccess(int id, String response) {

		Log.e(getTag(), "Response" + response);

		if (response != null && !response.equals("")) {

			if (id == holidayRequestId) {

				try {

					JSONArray jArray = new JSONArray(response);
					arrHolidays = new ArrayList<HolidayData>();

					if (jArray != null && jArray.length() > 0) {

						for (int i = 0; i < jArray.length(); i++) {

							HolidayData data = new Gson().fromJson(jArray.getJSONObject(i).toString(), HolidayData.class);
							arrHolidays.add(data);
						}

						calendarView.refreshDayCells(getHolidaysOfCurrentMonth(calendarView.getCalendar().getMonth(), calendarView.getCalendar().getYear()));
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}
}
