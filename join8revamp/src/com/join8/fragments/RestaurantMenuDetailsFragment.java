package com.join8.fragments;

import java.util.ArrayList;

import org.json.JSONArray;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;
import android.widget.TextView;

import com.costum.android.widget.LoadMoreListView;
import com.google.gson.Gson;
import com.join8.FoodOptionActivity;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.RestaurantActivity;
import com.join8.Toast;
import com.join8.adpaters.OrderDetailAdapter.OnItemDeleteListener;
import com.join8.adpaters.RestaurantMenuAdapter;
import com.join8.adpaters.RestaurantMenuItemListAdapter;
import com.join8.listeners.Requestlistener;
import com.join8.model.Registration;
import com.join8.model.RestaurantData;
import com.join8.model.RestaurantMenuData;
import com.join8.model.RestaurantMenuItemData;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;

public class RestaurantMenuDetailsFragment extends Fragment implements Requestlistener, OnItemDeleteListener {

	private static final String TAG = RestaurantMenuDetailsFragment.class.getSimpleName();

	private LoadMoreListView listView = null;

	private Gallery galleryMenu = null;

	private NetworkManager networManager = null;
	private CryptoManager prefManager = null;
	private int menuRequestId = -1;
	private String access_token = "";
	private RestaurantData restaurantData = null;
	private ArrayList<RestaurantMenuData> listMenu = null;
	// private ArrayList<RestaurantMenuItemData> listRestaurantMenuItems = null;
	private int prePosition = -1;
	private TextView txtTitle, btnViewOrderSummary;
	private RestaurantMenuItemListAdapter restaurantMenuItemListAdapter = null;
	private ArrayList<RestaurantMenuItemData> listRestaurantMenuItems = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e("Create", "Create");
		restaurantData = (RestaurantData) getArguments().getSerializable("restaurantData");

		networManager = NetworkManager.getInstance();
		networManager.addListener(this);
		networManager.isProgressVisible(true);
		prefManager = CryptoManager.getInstance(getActivity());

		View view = getActivity().getActionBar().getCustomView();
		txtTitle = (TextView) view.findViewById(R.id.txtTitle);
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);

		try {
			if (listView != null && listView.getAdapter() != null && restaurantMenuItemListAdapter != null) {
				restaurantMenuItemListAdapter.notifyDataSetChanged();
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.e("Create view", "Create view");
		View view = inflater.inflate(R.layout.restaurant_details, container, false);

		listView = (LoadMoreListView) view.findViewById(R.id.lstRestaurant);

		View headerView = getActivity().getLayoutInflater().inflate(R.layout.restaurant_menu_details_header, null);
		listView.addHeaderView(headerView);

		View footerView = getActivity().getLayoutInflater().inflate(R.layout.restaurant_menu_details_footer, null);
		listView.addFooterView(footerView);

		listView.setSelector(R.drawable.trans);
		listView.setAdapter(null);

		galleryMenu = (Gallery) view.findViewById(R.id.galleryMenu);		
		btnViewOrderSummary = (TextView) view.findViewById(R.id.btnViewOrderSummary);

		galleryMenu.setCallbackDuringFling(false);
		galleryMenu.setVisibility(View.GONE);

		listRestaurantMenuItems = new ArrayList<RestaurantMenuItemData>();
		restaurantMenuItemListAdapter = new RestaurantMenuItemListAdapter(getActivity(), listRestaurantMenuItems);
		listView.setAdapter(restaurantMenuItemListAdapter);

		btnViewOrderSummary.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				Registration registration = new Gson().fromJson(prefManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);

				if (registration == null) {
					Toast.displayText(getActivity(), getString(R.string.please_login));
				} else {

					Bundle bundle = new Bundle();
					bundle.putSerializable("restaurantData", restaurantData);
					OrderDetailFragment orderDetailFragment = new OrderDetailFragment();
					orderDetailFragment.setOnItemDeleteListener(RestaurantMenuDetailsFragment.this);
					orderDetailFragment.setArguments(bundle);
					getFragmentManager().beginTransaction().addToBackStack(OrderDetailFragment.class.getName()).add(RestaurantActivity.restaurant_fragment_container, orderDetailFragment).commit();
				}
			}
		});

		galleryMenu.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					prePosition = galleryMenu.getSelectedItemPosition();
				} else if (event.getAction() == MotionEvent.ACTION_UP) {

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {

							try {

								if (prePosition != galleryMenu.getSelectedItemPosition()) {

									// restaurantMenuItemListAdapter = new
									// RestaurantMenuItemListAdapter(getActivity(),
									// listMenu.get(galleryMenu.getSelectedItemPosition()).getTblMenuItem());
									// listView.setAdapter(restaurantMenuItemListAdapter);

									listRestaurantMenuItems.clear();
									listRestaurantMenuItems.addAll(listMenu.get(galleryMenu.getSelectedItemPosition()).getTblMenuItem());
									restaurantMenuItemListAdapter.notifyDataSetChanged();
								}

							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}, 500);
				}
				return false;
			}
		});

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				if (listMenu.get(galleryMenu.getSelectedItemPosition()).getTblMenuItem().get(position - 1).isOrderable()) {

					Intent intent = new Intent(getActivity(), FoodOptionActivity.class);
					intent.putExtra("menuItemId", listMenu.get(galleryMenu.getSelectedItemPosition()).getTblMenuItem().get(position - 1).getMenuItemId());
					// intent.putExtra("menuItemId",
					// listRestaurantMenuItems.get(position -
					// 1).getMenuItemId());
					getActivity().startActivityForResult(intent, 33);

					// Bundle bundle = new Bundle();
					// bundle.putSerializable("obj",
					// listRestaurantMenuItems.get(position - 1));
					//
					// PromotionDetailsFromList obj = new
					// PromotionDetailsFromList(RestaurantMenuDetailsFragment.this);
					// obj.setArguments(bundle);
					//
					// FragmentTransaction ft =
					// getActivity().getSupportFragmentManager().beginTransaction();
					// ft.addToBackStack(PromotionDetailsFromList.class.getName());
					// ft.add(RestaurantActivity.restaurant_fragment_container,
					// obj);
					// ft.commit();
				}
			}
		});

		getFragmentManager().addOnBackStackChangedListener(new OnBackStackChangedListener() {
			@Override
			public void onBackStackChanged() {

				if (((RestaurantActivity) getActivity()).getActiveFragment().equals(RestaurantMenuDetailsFragment.class.getName())) {

					if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
						if (!Utils.isEmpty(restaurantData.getCompanyNameEN())) {
							txtTitle.setText(restaurantData.getCompanyNameEN());
						}
					} else {
						if (!Utils.isEmpty(restaurantData.getCompanyName())) {
							txtTitle.setText(restaurantData.getCompanyName());
						}
					}
				}
			}
		});

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(getActivity());
		mTypefaceUtils.applyTypeface(txtTitle);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");

		loadMenu();

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void loadMenu() {

		networManager.isProgressVisible(true);
		menuRequestId = networManager.addRequest(Join8RequestBuilder.getRestaurantMenuRequest(access_token, "" + restaurantData.getCompanyId()), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_RESTAURANT_MENU);
	}

	// private void loadMenuItems() {
	//
	// networManager.isProgressVisible(true);
	// menuItemsRequestId =
	// networManager.addRequest(Join8RequestBuilder.getRestaurantMenuItemsRequest(access_token,
	// listMenu.get(galleryMenu.getSelectedItemPosition()).getMenuId()),
	// RequestMethod.POST, getActivity(),
	// Join8RequestBuilder.METHOD_RESTAURANT_MENU_ITEMS);
	// }

	@Override
	public void onSuccess(int id, String response) {

		try {

			if (response != null && !response.equals("")) {

				if (id == menuRequestId) {

					JSONArray jArray = new JSONArray(response);
					listMenu = new ArrayList<RestaurantMenuData>();
					// listRestaurantMenuItems = new
					// ArrayList<RestaurantMenuItemData>();

					int selectedPos = 0;

					if (jArray != null && jArray.length() > 0) {

						galleryMenu.setVisibility(View.VISIBLE);

						for (int i = 0; i < jArray.length(); i++) {
							RestaurantMenuData menuData = new Gson().fromJson(jArray.getString(i).toString(), RestaurantMenuData.class);
							listMenu.add(menuData);
							if (menuData.getMenuId().equals("" + restaurantData.getMenuId())) {
								selectedPos = i;								
							}
						}
					} else {
						galleryMenu.setVisibility(View.GONE);
					}

					if (listMenu != null) {

						galleryMenu.setAdapter(new RestaurantMenuAdapter(getActivity(), listMenu));
						galleryMenu.setSelection(selectedPos);

						// restaurantMenuItemListAdapter = new
						// RestaurantMenuItemListAdapter(getActivity(),
						// listMenu.get(galleryMenu.getSelectedItemPosition()).getTblMenuItem());
						// listView.setAdapter(restaurantMenuItemListAdapter);
						// loadMenuItems();

						listRestaurantMenuItems.clear();
						listRestaurantMenuItems.addAll(listMenu.get(galleryMenu.getSelectedItemPosition()).getTblMenuItem());
						restaurantMenuItemListAdapter.notifyDataSetChanged();
					}

					// } else if (id == menuItemsRequestId) {
					//
					// JSONArray jsonArray = new JSONArray(response);
					// listRestaurantMenuItems = new
					// ArrayList<RestaurantMenuItemData>();
					//
					// if (jsonArray != null && jsonArray.length() > 0) {
					//
					// for (int i = 0; i < jsonArray.length(); i++) {
					// RestaurantMenuItemData restaurantMenuItemsData = new
					// Gson().fromJson(jsonArray.getString(i).toString(),
					// RestaurantMenuItemData.class);
					// listRestaurantMenuItems.add(restaurantMenuItemsData);
					// }
					// }
					//
					// restaurantMenuItemListAdapter = new
					// RestaurantMenuItemListAdapter(getActivity(),
					// listRestaurantMenuItems);
					// listView.setAdapter(restaurantMenuItemListAdapter);

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

	@Override
	public void itemDeleted() {
		try {
			if (listView != null && listView.getAdapter() != null) {
				restaurantMenuItemListAdapter.notifyDataSetChanged();
			}
		} catch (Exception e) {
		}
	}
}
