package com.join8.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.Location;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.facebook.android.FacebookError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.join8.HomeScreen;
import com.join8.R;
import com.join8.adpaters.ImageAdapter;
import com.join8.controller.ImageController;
import com.join8.listeners.Requestlistener;
import com.join8.model.Area;
import com.join8.model.City;
import com.join8.model.Registration;
import com.join8.model.RequestAddShop;
import com.join8.model.ShopCategory;
import com.join8.model.SubCategory;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.runnable.ConvertImageRunnable;
import com.join8.utils.BackgroundExecutor;
import com.join8.utils.CameraUtil;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.Log;
import com.join8.utils.UrlUtil;
import com.join8.views.HorizontalListView;
import com.join8.views.MyEditText;
import com.join8.views.MyTextView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by craterzone on 8/1/16.
 */
public class FragmentSearch extends Fragment implements View.OnClickListener, Requestlistener {

	private final String TAG = this.getClass().getName();
	private Spinner spnShopCategory, spnArea;
	private NetworkManager mNetworkManager;
	private int mReqSHopCAtegory = -1;
	private int mReqAreaList = -1;
	private CryptoManager prefsManager;
	private int mReqAddShop = -1;
	private String tokenCloud = null;
	private HashMap<String, String> auth = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mNetworkManager = NetworkManager.getInstance();
		prefsManager = CryptoManager.getInstance(getActivity());
		mNetworkManager.isProgressVisible(true);
		tokenCloud = prefsManager.getPrefs().getString(ConstantData.TOKEN_CLOUD, null);
		auth = new HashMap<String, String>();
		auth.put("Authorization", "bearer " + tokenCloud);
		mReqSHopCAtegory = mNetworkManager.addRequest(auth, getActivity(), true, UrlUtil.getShopAllSubCategoryUrl(), "", ConstantData.method_get);
		mReqAreaList = mNetworkManager.addRequest(auth, getActivity(), true, UrlUtil.getAreaListUrl(), "", ConstantData.method_get);
	}

	@Override
	public void onStart() {
		super.onStart();
		mNetworkManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		mNetworkManager.removeListeners(this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mNetworkManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		@SuppressLint("InflateParams")
		View view = inflater.inflate(R.layout.fragment_search, null);
		initView(view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	private void initView(View view) {
		spnShopCategory = (Spinner) view.findViewById(R.id.spn_shop_category);
		spnArea = (Spinner) view.findViewById(R.id.spn_area);

	}

	@Override
	public void onClick(View v) {

	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onSuccess(int id, String response) {
		Gson gson = new Gson();
		if (isAdded() && !TextUtils.isEmpty(response)) {

			if (id == mReqSHopCAtegory) {
				ArrayList<SubCategory> list = gson.fromJson(response, new TypeToken<List<SubCategory>>() {
				}.getType());
				SubCategory shopcat = new SubCategory();
				shopcat.setName("Category");
				shopcat.setNameEn("Category");
				list.add(0, shopcat);
				ArrayAdapter<SubCategory> adapter = new ArrayAdapter<SubCategory>(getActivity(), R.layout.item_spinner, list);
				spnShopCategory.setAdapter(adapter);
			} else if (id == mReqAreaList) {
				ArrayList<Area> list = gson.fromJson(response, new TypeToken<List<Area>>() {
				}.getType());
				Area area = new Area();
				area.setAreaNameEn("Area");
				area.setAreaName("Area");
				list.add(0, area);
				ArrayAdapter<Area> adapter = new ArrayAdapter<Area>(getActivity(), R.layout.item_spinner, list);
				spnArea.setAdapter(adapter);
			}
		}
	}
	@Override
	public void onError(int id, String message) {
		// TODO Auto-generated method stub

	}

}
