package com.join8.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.join8.HomeScreen;
import com.join8.R;
import com.join8.adpaters.TableDetailsViewPagerAdapter;
import com.join8.model.TableTypeData;
import com.join8.utils.ConstantData;
import com.join8.utils.Log;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;

public class BookingTableDetailsFragment extends Fragment {

	private TextView lblTableType = null, txtTableType = null, lblPrice = null, txtPrice = null, lblDesc = null, txtDesc = null;
	private TableTypeData data = null;
	private ViewPager viewPager = null;
	private LinearLayout lnrPagerIndicator = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		data = (TableTypeData) getArguments().getSerializable("data");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mView = inflater.inflate(R.layout.booking_table_details_fragment, container, false);

		lblTableType = (TextView) mView.findViewById(R.id.lblTableType);
		txtTableType = (TextView) mView.findViewById(R.id.txtTableType);
		lblPrice = (TextView) mView.findViewById(R.id.lblPrice);
		txtPrice = (TextView) mView.findViewById(R.id.txtPrice);
		lblDesc = (TextView) mView.findViewById(R.id.lblDesc);
		txtDesc = (TextView) mView.findViewById(R.id.txtDesc);
		viewPager = (ViewPager) mView.findViewById(R.id.viewPager);
		lnrPagerIndicator = (LinearLayout) mView.findViewById(R.id.lnrPagerIndicator);

		TypefaceUtils typefaceUtils = TypefaceUtils.getInstance(getActivity());
		typefaceUtils.applyTypeface(lblTableType);
		typefaceUtils.applyTypeface(txtTableType);
		typefaceUtils.applyTypeface(lblPrice);
		typefaceUtils.applyTypeface(txtPrice);
		typefaceUtils.applyTypeface(lblDesc);
		typefaceUtils.applyTypeface(txtDesc);

		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int index) {

				displayPageDot(index);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});

		return mView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH)) {
			txtTableType.setText(data.getBookingNameEn());
			if (!Utils.isEmpty(data.getTermsEn())) {
				txtDesc.setText(data.getTermsEn());
			}
		} else {
			txtTableType.setText(data.getBookingName());
			if (!Utils.isEmpty(data.getTerms())) {
				txtDesc.setText(data.getTerms());
			}
		}

		txtPrice.setText(getString(R.string.deposit) + " $" + data.getBookingDeposit());

		if (data.getBookingImageLink() != null && data.getBookingImageLink().size() > 0) {
			viewPager.setAdapter(new TableDetailsViewPagerAdapter(getActivity(), data.getBookingImageLink(), ((TextView) getActivity().getActionBar().getCustomView().findViewById(R.id.txtTitle)).getText().toString()));
			displayPageDot(0);
		} else {
			viewPager.setBackgroundDrawable(getResources().getDrawable(R.drawable.joinlogo));
		}
	}

	private void displayPageDot(int selectedIndex) {

		try {

			lnrPagerIndicator.removeAllViews();
			int margin = getResources().getDimensionPixelOffset(R.dimen.margin_2);

			for (int i = 0; i < data.getBookingImageLink().size(); i++) {

				LinearLayout lnr = new LinearLayout(getActivity());
				ImageView imageView = new ImageView(getActivity());
				lnr.setPadding(margin, margin, margin, margin);
				lnr.addView(imageView);
				if (i == selectedIndex) {
					imageView.setBackgroundResource(R.drawable.dot_sel);
				} else {
					imageView.setBackgroundResource(R.drawable.dot);
				}
				lnrPagerIndicator.addView(lnr);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Error in displayPageDot", e.toString());
		}
	}
}
