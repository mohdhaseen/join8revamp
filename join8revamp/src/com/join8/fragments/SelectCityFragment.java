package com.join8.fragments;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.join8.HomeScreen;
import com.join8.PreferenceActivty;
import com.join8.R;
import com.join8.Toast;
import com.join8.adpaters.CityListAdapter;
import com.join8.adpaters.CityListAdapterNew;
import com.join8.listeners.Requestlistener;
import com.join8.model.City;
import com.join8.model.ResponseCity;
import com.join8.model.TokenModel;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.UrlUtil;

public class SelectCityFragment extends Fragment implements Requestlistener, OnClickListener {

	private static final String TAG = SelectCityFragment.class.getSimpleName();
//here
	//public static String PARAM_CITYDATA = "citydata";
	public static String PARAM_CITYDATA_NEW = "citydatanew";
	public static String PARAM_PHONE_CODE = "phone_code";
	private ListView lstCity;
	private CityListAdapter cityListAdapter;
	private CityListAdapterNew cityListAdapterNew;
	private CryptoManager prefsManager;
	private NetworkManager networManager;
	private String access_token = "";
	private ArrayList<City> arrCities;
	private ArrayList<ResponseCity> arrCitiesNew;
	int index = -1;
	private int city_request = -1;
	private int request_city_new = -1;
	private boolean isFirstTime = false;
	private ImageView imgBack;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		prefsManager = CryptoManager.getInstance(getActivity());
		networManager = NetworkManager.getInstance();
		networManager.isProgressVisible(true);
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View v = inflater.inflate(R.layout.selectcity, container, false);
		lstCity = (ListView) v.findViewById(R.id.lstCity);
		lstCity.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		imgBack = (ImageView) v.findViewById(R.id.imgBack);
		imgBack.setOnClickListener(this);

		lstCity.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> view, View arg1, int arg2, long arg3) {

				ResponseCity city = (ResponseCity) view.getAdapter().getItem(arg2);
				Gson gson = new Gson();
				String json = gson.toJson(city);

				String phoneCode = "";
/*
				if (city.getCountryCode().equals(city.getPartitionKey())) {
					phoneCode = city.getCountryCode();
				} else {
					phoneCode = city.getCountryCode() + "-" + city.getPartitionKey();
				}
*/
				Editor editor = prefsManager.getPrefs().edit();
				editor.putString(PARAM_CITYDATA_NEW, json);
				editor.putString(PARAM_PHONE_CODE, city.getPhoneCode());
				editor.commit();
				

				if (!isFirstTime) {
					// lstCity.setEnabled(false);
					// new Handler().postDelayed(new Runnable() {
					//
					// @Override
					// public void run() {

					getActivity().setResult(Activity.RESULT_OK);
					getActivity().finish();
					// }
					// }, 500);

				} else {
					isFirstTime = false;
				}
			}
		});
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	/*	access_token = prefsManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
		arrCities = ((PreferenceActivty) getActivity()).cities;
		if (arrCities != null && arrCities.size() > 0) {
			loadCityData();
		} else {
			access_token = prefsManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
			city_request = networManager.addRequest(Join8RequestBuilder.getCityList(prefsManager.getPrefs().getString(access_token, "")), RequestMethod.POST, getActivity(), Join8RequestBuilder.METHOD_GETCITY);
		}*/
		String tokenCloud = prefsManager.getPrefs().getString(ConstantData.TOKEN_CLOUD, null);
		HashMap<String, String> auth = new HashMap<String, String>();
		auth.put("Authorization", "bearer " + tokenCloud);
		arrCitiesNew = ((PreferenceActivty) getActivity()).citiesNew;
		if (arrCitiesNew != null && arrCitiesNew.size() > 0) {
			loadCityDataNew();
		} /*else {
			request_city_new = networManager.addRequest(auth, getActivity(), true, UrlUtil.getCityListUrl(), "", ConstantData.method_get);
		}*/
	}

	/*public void getCityData() {

		Gson gson = new Gson();
		String json = prefsManager.getPrefs().getString(PARAM_CITYDATA, "");
		City obj = gson.fromJson(json, City.class);

		if (obj != null) {
			for (int i = 0; i < arrCities.size(); i++) {
				if (arrCities.get(i).getCityNameEN().equals(obj.getCityNameEN())) {
					index = i;
					break;
				}
			}
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					isFirstTime = true;
					lstCity.performItemClick(lstCity.getAdapter().getView(index, null, null), index, lstCity.getAdapter().getItemId(index));
				}
			}, 200);
		}
	}*/

	public void getCityDataNew() {

		Gson gson = new Gson();
		String json = prefsManager.getPrefs().getString(PARAM_CITYDATA_NEW, "");
		/*ArrayList<ResponseCity> arr = new Gson().fromJson(json, new TypeToken<ArrayList<ResponseCity>>() {
		}.getType());*/
		ResponseCity obj = gson.fromJson(json, ResponseCity.class);


		if (obj != null) {
			for (int i = 0; i < arrCitiesNew.size(); i++) {
				if ((arrCitiesNew.get(i).getNameEN().equals(obj.getNameEN()) || (arrCitiesNew.get(i).getName().equals(obj.getName())))) {
					index = i;
					break;
				}
			}
			isFirstTime = true;
			lstCity.performItemClick(lstCity.getAdapter().getView(index, null, null), index, lstCity.getAdapter().getItemId(index));
		}
			/*new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {*/
					/*isFirstTime = true;
					lstCity.performItemClick(lstCity.getAdapter().getView(index, null, null), index, lstCity.getAdapter().getItemId(index));*/
				/*}
			}, 0);/*
		}
	}

	@Override
	public void onSuccess(int id, String response) {
		/*if (response != null) {
			if (id == request_city_new) {
				try {
					ArrayList<ResponseCity> cityList = new Gson().fromJson(response, new TypeToken<ArrayList<ResponseCity>>() {
					}.getType());
				} catch (JsonSyntaxException e) {
					Toast.displayText(getActivity(), e.toString());
				}

			}
		}*/
		/*if (response != null && id == city_request) {
			Log.e(getTag(), "Response" + response);
			try {
				JSONArray jCity = new JSONArray(response);
				arrCities = new ArrayList<City>();
				if (jCity != null && jCity.length() > 0) {
					for (int i = 0; i < jCity.length(); i++) {

						City city = new Gson().fromJson(jCity.getJSONObject(i).toString(), City.class);
						arrCities.add(city);

					}
					loadCityData();
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

		}*/
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(getActivity(), message);
	}

/*	private void loadCityData() {
		cityListAdapter = new CityListAdapter(getActivity(), R.layout.row_city, arrCities);
		lstCity.setAdapter(cityListAdapter);
		//getCityData();

	}*/
	private void loadCityDataNew() {
		cityListAdapterNew = new CityListAdapterNew(getActivity(), R.layout.row_city, arrCitiesNew);
		lstCity.setAdapter(cityListAdapterNew);
		//lstCity.performItemClick(lstCity.getAdapter().getView(index, null, null), index, lstCity.getAdapter().getItemId(index));

		getCityDataNew();

	}
	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.imgBack) {
			getActivity().finish();

		}
	}

	@Override
	public void onSuccess(int id, String response) {
		// TODO Auto-generated method stub
		
	}

}
