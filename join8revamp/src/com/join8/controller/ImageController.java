package com.join8.controller;

import com.join8.model.ShopImageData;

import java.util.ArrayList;

/**
 * Created by craterzone on 28/1/16.
 */
public class ImageController {
	private static ImageController mInstance;
	private ArrayList<ShopImageData> images;

	private ImageController() {
		images = new ArrayList<ShopImageData>();
	}

	public static ImageController getInstance() {
		if (mInstance == null) {
			mInstance = new ImageController();
		}
		return mInstance;
	}
	public ArrayList<ShopImageData> getImages() {
		return this.images;
	}
	public void clearDataP() {
		images.clear();
	}
	public void setShopId(String fkShopId) {
		for (ShopImageData img : images) {
			img.setFk_ShopId(fkShopId);
		}
	}
}
