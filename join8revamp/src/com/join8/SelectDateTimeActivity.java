package com.join8;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.join8.calendar.CalendarGalleryView;
import com.join8.calendar.CalendarGalleryView.OnDayChangeListner;
import com.join8.calendar.CalendarGalleryView.OnMonthChangeListner;
import com.join8.layout.TimeView;
import com.join8.listeners.OnStateChnageListner;
import com.join8.listeners.Requestlistener;
import com.join8.model.TimeSlotData;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.CryptoManager;
import com.join8.utils.DialogHelper;
import com.join8.utils.Log;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.Utils;

public class SelectDateTimeActivity extends ActionBarActivity implements OnStateChnageListner, OnCheckedChangeListener, Requestlistener {

	private static final String TAG = SelectDateTimeActivity.class.getSimpleName();
	private ImageButton btnBack = null;
	private TextView txtTitle = null, txtSelect = null;
	private CalendarGalleryView calendarView = null;
	private LinearLayout lnrTimeSlotContainer = null, lnrTableStateContainer = null, lnrTimePickerContainer = null;
	private TimeView lastSelectedTimeView = null;
	private RadioGroup rbgSlot = null;
	private TimePicker timePicker = null;

	private NetworkManager networManager = null;
	private CryptoManager prefManager;
	private int timeSlotRequestId = -1;

	private ArrayList<TimeSlotData> listMorning = null;
	private ArrayList<TimeSlotData> listAfterNoon = null;
	private ArrayList<TimeSlotData> listEvening = null;

	private String companyId = "", tableType = "";
	private int selectedTimeSlotId = -1, bookingType = 0;
	private double deposit = 0.0;
	private String selectedTime = "";
	private boolean isRestClosed = false;

	private Calendar calendar = null;

	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
	SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm", Locale.getDefault());

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_select_date_time);

		getSupportActionBar().setHomeButtonEnabled(false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		getSupportActionBar().setDisplayUseLogoEnabled(false);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);

		View view = getLayoutInflater().inflate(R.layout.actionviewdetail, null);
		getSupportActionBar().setCustomView(view);

		btnBack = (ImageButton) view.findViewById(R.id.btnBack);
		view.findViewById(R.id.btnFav).setVisibility(View.INVISIBLE);
		txtTitle = (TextView) view.findViewById(R.id.txtTitle);
		view.findViewById(R.id.txtOpen).setVisibility(View.GONE);

		lnrTableStateContainer = (LinearLayout) findViewById(R.id.lnrTableStateContainer);
		lnrTimePickerContainer = (LinearLayout) findViewById(R.id.lnrTimePickerContainer);
		timePicker = (TimePicker) findViewById(R.id.timePicker);
		calendarView = (CalendarGalleryView) findViewById(R.id.calendar_view);
		lnrTimeSlotContainer = (LinearLayout) findViewById(R.id.lnrTimeSlotContainer);
		txtSelect = (TextView) findViewById(R.id.txtSelect);
		rbgSlot = (RadioGroup) findViewById(R.id.rbg);
		rbgSlot.setOnCheckedChangeListener(this);

		txtTitle.setText(getIntent().getStringExtra("restaurantName"));
		companyId = getIntent().getStringExtra("companyId");
		tableType = getIntent().getStringExtra("tableType");
		selectedTimeSlotId = getIntent().getIntExtra("selectedTimeSlotId", -1);
		bookingType = getIntent().getIntExtra("bookingType", 0);

		if (bookingType == 2) {
			lnrTableStateContainer.setVisibility(View.VISIBLE);
			lnrTimePickerContainer.setVisibility(View.GONE);
		} else {
			lnrTableStateContainer.setVisibility(View.GONE);
			lnrTimePickerContainer.setVisibility(View.VISIBLE);
		}

		networManager = NetworkManager.getInstance();
		prefManager = CryptoManager.getInstance(SelectDateTimeActivity.this);

		calendar = Calendar.getInstance();
		calendar.setTimeInMillis(getIntent().getLongExtra("selectedDate", Calendar.getInstance().getTimeInMillis()));
		calendarView.setDate(calendar);

		if (bookingType == 2) {
			selectedTimeSlotId = getIntent().getIntExtra("selectedTimeSlotId", 0);
		} else {
			timePicker.setCurrentHour(calendar.get(Calendar.HOUR_OF_DAY));
			timePicker.setCurrentMinute(calendar.get(Calendar.MINUTE));
		}

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(this);
		mTypefaceUtils.applyTypeface(txtTitle);
		mTypefaceUtils.applyTypeface(txtSelect);
		mTypefaceUtils.applyTypeface((RadioButton) findViewById(R.id.rbtMorning));
		mTypefaceUtils.applyTypeface((RadioButton) findViewById(R.id.rbtAfterNoon));
		mTypefaceUtils.applyTypeface((RadioButton) findViewById(R.id.rbtEvening));
		mTypefaceUtils.applyTypeface((TextView) findViewById(R.id.txtAvailable));
		mTypefaceUtils.applyTypeface((TextView) findViewById(R.id.txtNotAvailable));
		mTypefaceUtils.applyTypeface((TextView) findViewById(R.id.txtFewAvailable));
		mTypefaceUtils.applyTypeface((TextView) findViewById(R.id.txtSelected));
		mTypefaceUtils.applyTypeface((TextView) findViewById(R.id.txtConfAvailability));

		getTimeSlot();

		calendarView.setOnDayChangeListner(new OnDayChangeListner() {
			@Override
			public void onDayChanged(Calendar cal) {
				calendar = cal;
				selectedTimeSlotId = 0;
				selectedTime = "";
				deposit = 0;
				getTimeSlot();
			}
		});

		calendarView.setOnMonthChangeListner(new OnMonthChangeListner() {
			@Override
			public void onMonthChanged(Calendar cal) {
				calendar = cal;
				selectedTimeSlotId = 0;
				selectedTime = "";
				deposit = 0;
				getTimeSlot();
			}
		});

		txtSelect.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (isRestClosed) {
					DialogHelper.popupMessage(SelectDateTimeActivity.this, null, getString(R.string.rest_closed), getString(R.string.tvOk), null, null, null, true);
					return;
				}

				if (bookingType == 2) {

					if (selectedTimeSlotId != 0) {

						if (!Utils.isEmpty(selectedTime)) {
							String time[] = selectedTime.split(":");
							calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
							calendar.set(Calendar.MINUTE, Integer.parseInt(time[1]));
						}

						if (calendar.getTimeInMillis() < Calendar.getInstance().getTimeInMillis()) {
							Toast.displayText(SelectDateTimeActivity.this, getString(R.string.error_past_date));
							return;
						}

						Intent intent = new Intent();
						intent.putExtra("selectedTimeSlotId", selectedTimeSlotId);
						intent.putExtra("date", calendar.getTimeInMillis());
						intent.putExtra("deposit", deposit);
						setResult(RESULT_OK, intent);
						finish();

					} else {
						Toast.displayText(SelectDateTimeActivity.this, getString(R.string.select_time_slot));
					}

				} else {

					calendar.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
					calendar.set(Calendar.MINUTE, timePicker.getCurrentMinute());

					if (calendar.getTimeInMillis() < Calendar.getInstance().getTimeInMillis()) {
						Toast.displayText(SelectDateTimeActivity.this, getString(R.string.error_past_date));
						return;
					}

					Intent intent = new Intent();
					intent.putExtra("selectedTimeSlotId", selectedTimeSlotId);
					intent.putExtra("date", calendar.getTimeInMillis());
					intent.putExtra("deposit", 0.0);
					setResult(RESULT_OK, intent);
					finish();
				}
			}
		});

		btnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				finish();
			}
		});
	}

	@Override
	public void onResume() {
		super.onResume();
		networManager.addListener(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		networManager.removeListeners(this);
	}

	private void displayTimeSlot(ArrayList<TimeSlotData> list) {

		try {

			lnrTimeSlotContainer.removeAllViews();
			int margin = getResources().getDimensionPixelOffset(R.dimen.margin_5);

			LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			layoutParams.setMargins(margin, margin, margin, margin);

			LinearLayout lnrRow = new LinearLayout(SelectDateTimeActivity.this);
			lnrRow.setLayoutParams(layoutParams);
			lnrRow.setWeightSum(3f);

			for (int i = 0; i < list.size(); i++) {

				TimeView timeView = new TimeView(SelectDateTimeActivity.this);
				timeView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

				if (!Utils.isEmpty(list.get(i).getBookingTime()) && list.get(i).getBookingTime().contains(":")) {
					String time[] = list.get(i).getBookingTime().split(":");
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time[0]));
					cal.set(Calendar.MINUTE, Integer.parseInt(time[1]));
					timeView.setText(new SimpleDateFormat("hh:mma", Locale.getDefault()).format(cal.getTime()));
				} else {
					timeView.setText(list.get(i).getBookingTime());
				}
				timeView.setTempletId(list.get(i).getBookingTemplateId());
				timeView.setDeposit(list.get(i).getBookingDeposit());
				timeView.setState(list.get(i).getStatus());
				timeView.setId(i + 1);
				timeView.setTime(list.get(i).getBookingTime());

				if (selectedTimeSlotId != -1 && selectedTimeSlotId == list.get(i).getBookingTemplateId()) {
					timeView.setState(TimeView.STATE_SELECTED);
					lastSelectedTimeView = timeView;
					selectedTimeSlotId = list.get(i).getBookingTemplateId();
					selectedTime = list.get(i).getBookingTime();
					deposit = list.get(i).getBookingDeposit();
				}

				timeView.setOnStateChnageListner(this);

				LinearLayout lnrChild = new LinearLayout(SelectDateTimeActivity.this);
				LayoutParams layoutParams1 = new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1);
				lnrChild.setLayoutParams(layoutParams1);
				lnrChild.setPadding(margin, margin, margin, margin);
				lnrChild.addView(timeView);

				lnrRow.addView(lnrChild);

				if ((i + 1) % 3 == 0) {
					lnrTimeSlotContainer.addView(lnrRow);
					lnrRow = new LinearLayout(SelectDateTimeActivity.this);
					lnrRow.setLayoutParams(layoutParams);
					lnrRow.setWeightSum(3f);
				}
			}

			if (list.size() % 3 != 0) {
				lnrTimeSlotContainer.addView(lnrRow);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, e.toString());
		}
	}

	@Override
	public void onStateChange(TimeView view, int state) {

		if (lastSelectedTimeView != null && lastSelectedTimeView != view) {
			lastSelectedTimeView.resetState();
		}

		lastSelectedTimeView = view;

		if (state == TimeView.STATE_SELECTED) {
			selectedTimeSlotId = view.getTempletId();
			selectedTime = view.getTime().trim();
			deposit = view.getDeposit();
		} else {
			selectedTimeSlotId = 0;
			selectedTime = "";
			deposit = 0;
		}
	}

	private long getUTCTIME(String stringDate) {

		long date = Calendar.getInstance().getTimeInMillis();

		try {

			SimpleDateFormat sourceFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
			sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date parsed = sourceFormat.parse(stringDate); // => Date is in UTC
			Log.d("CurrentDate", "" + parsed);
			date = parsed.getTime();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return date;
	}

	private void getTimeSlot() {

		String access_token = prefManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");

		networManager.isProgressVisible(true);
		timeSlotRequestId = networManager.addRequest(Join8RequestBuilder.getTimeSlot(access_token, companyId, tableType, "/Date(" + getUTCTIME(dateFormat.format(calendar.getTime())) + ")/"), RequestMethod.POST, SelectDateTimeActivity.this,
				Join8RequestBuilder.METHOD_GET_TIME_SLOT);
	}

	@Override
	public void onSuccess(int id, String response) {

		if (id == timeSlotRequestId) {

			try {

				listMorning = new ArrayList<TimeSlotData>();
				listAfterNoon = new ArrayList<TimeSlotData>();
				listEvening = new ArrayList<TimeSlotData>();

				JSONObject jObj = new JSONObject(response);

				if (jObj.getInt("IsOpen") == 0) {
					isRestClosed = true;
					DialogHelper.popupMessage(SelectDateTimeActivity.this, null, getString(R.string.rest_closed), getString(R.string.tvOk), null, null, null, true);
				} else {
					isRestClosed = false;
				}

				if (bookingType == 2) {

					JSONArray jArray = jObj.getJSONArray("MorningSlot");

					for (int i = 0; i < jArray.length(); i++) {

						TimeSlotData data = new Gson().fromJson(jArray.getString(i), TimeSlotData.class);
						listMorning.add(data);
					}

					jArray = jObj.getJSONArray("AfterNoonSlot");

					for (int i = 0; i < jArray.length(); i++) {

						TimeSlotData data = new Gson().fromJson(jArray.getString(i), TimeSlotData.class);
						listAfterNoon.add(data);
					}

					jArray = jObj.getJSONArray("EveningSlot");

					for (int i = 0; i < jArray.length(); i++) {

						TimeSlotData data = new Gson().fromJson(jArray.getString(i), TimeSlotData.class);
						listEvening.add(data);
					}

					// preSelectTime = true;
					Calendar cal = Calendar.getInstance();
					rbgSlot.clearCheck();

					if (cal.get(Calendar.HOUR_OF_DAY) >= 18) {
						rbgSlot.check(R.id.rbtEvening);
					} else if (cal.get(Calendar.HOUR_OF_DAY) >= 12) {
						rbgSlot.check(R.id.rbtAfterNoon);
					} else {
						rbgSlot.check(R.id.rbtMorning);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(SelectDateTimeActivity.this, message);
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {

		switch (checkedId) {

		case R.id.rbtMorning:
			displayTimeSlot(listMorning);
			break;

		case R.id.rbtAfterNoon:
			displayTimeSlot(listAfterNoon);
			break;

		case R.id.rbtEvening:
			displayTimeSlot(listEvening);
			break;
		}
	}
}
