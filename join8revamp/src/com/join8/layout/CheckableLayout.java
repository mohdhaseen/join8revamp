package com.join8.layout;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.join8.R;

public class CheckableLayout extends RelativeLayout implements Checkable {

	public CheckableLayout(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public CheckableLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CheckableLayout(Context context, int checkableId) {
		super(context);
	}

	private boolean checked = false;

	private ImageView img_arrow;

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		img_arrow = (ImageView) findViewById(R.id.imgRight);
	}

	@Override
	public boolean isChecked() {
		return checked;
	}

	@Override
	public void setChecked(boolean checked) {
		this.checked = checked;

		if (checked) {
			img_arrow.setImageDrawable(getResources().getDrawable(R.drawable.arrowright));
		} else {
			img_arrow.setImageDrawable(getResources().getDrawable(R.drawable.arrowhover));
		}
	}

	@Override
	public void toggle() {
		setChecked(!checked);
	}

}