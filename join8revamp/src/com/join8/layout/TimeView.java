package com.join8.layout;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.join8.R;
import com.join8.listeners.OnStateChnageListner;
import com.join8.utils.TypefaceUtils;

public class TimeView extends TextView implements OnClickListener {

	public static final int STATE_AVAILABLE = 2, STATE_NOT_AVAILABLE = 3, STATE_FEW_LEFT = 1, STATE_SELECTED = 4;
	private int margin = 5;
	private int preState = STATE_FEW_LEFT;
	private int templetId = 0;
	private double deposit = 0;
	private String time = "";

	private OnStateChnageListner listner = null;

	public TimeView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init();
	}

	public TimeView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public TimeView(Context context) {
		super(context);
		init();
	}

	private void init() {

		margin = getResources().getDimensionPixelOffset(R.dimen.margin_5);

		TypefaceUtils.getInstance(getContext()).applyTypeface(this);
		setGravity(Gravity.CENTER);
		setState(STATE_AVAILABLE);
		setOnClickListener(this);
	}

	public void setState(int state) {

		switch (state) {

		case STATE_AVAILABLE:
			setBackgroundResource(R.drawable.box_green_border);
			setTag(STATE_AVAILABLE);
			setTextColor(getResources().getColor(R.color.green));
			setPadding(margin, margin, margin, margin);
			setEnabled(true);
			break;

		case STATE_NOT_AVAILABLE:
			setBackgroundResource(R.drawable.box_gray_round);
			setTag(STATE_NOT_AVAILABLE);
			setTextColor(getResources().getColor(R.color.dropdown_gray_text));
			setPadding(margin, margin, margin, margin);
			setEnabled(false);
			break;

		case STATE_FEW_LEFT:
			setBackgroundResource(R.drawable.box_orange_border);
			setTag(STATE_FEW_LEFT);
			setTextColor(getResources().getColor(R.color.orange));
			setPadding(margin, margin, margin, margin);
			setEnabled(true);
			break;

		case STATE_SELECTED:
			setBackgroundResource(R.drawable.box_orange_round);
			setTag(STATE_SELECTED);
			setTextColor(getResources().getColor(R.color.white));
			setPadding(margin, margin, margin, margin);
			setEnabled(true);
			break;

		default:
			setBackgroundResource(R.drawable.box_green_border);
			setTag(STATE_AVAILABLE);
			setTextColor(getResources().getColor(R.color.green));
			setPadding(margin, margin, margin, margin);
			setEnabled(true);
			break;
		}

		if (listner != null) {
			listner.onStateChange(this, state);
		}
	}

	public int getState() {
		return (Integer) getTag();
	}

	public void resetState() {
		setState(preState);
	}

	public int getTempletId() {
		return templetId;
	}

	public void setTempletId(int templetId) {
		this.templetId = templetId;
	}

	public double getDeposit() {
		return deposit;
	}

	public void setDeposit(double deposit) {
		this.deposit = deposit;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@Override
	public void onClick(View v) {

		if (((Integer) v.getTag()) == STATE_AVAILABLE || ((Integer) v.getTag()) == STATE_FEW_LEFT) {
			preState = (Integer) v.getTag();
			setState(STATE_SELECTED);
		} else if (((Integer) v.getTag()) == STATE_SELECTED) {
			setState(preState);
		}
	}

	public void setOnStateChnageListner(OnStateChnageListner listner) {
		this.listner = listner;
	}
}
