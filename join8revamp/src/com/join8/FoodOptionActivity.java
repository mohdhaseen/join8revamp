package com.join8;

import android.os.Bundle;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;

import com.join8.fragments.FoodOptionFragment;
import com.join8.fragments.RestaurantDetailsFragment;
import com.join8.utils.TypefaceUtils;

public class FoodOptionActivity extends ActionBarActivity {

	private ImageButton btnBack = null, btnFav = null;
	private TextView txtTitle = null, txtOpen = null;
	public static int fragment_container = R.id.fragment_container;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_restaurant);

		getSupportActionBar().setHomeButtonEnabled(false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		getSupportActionBar().setDisplayUseLogoEnabled(false);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);

		View view = getLayoutInflater().inflate(R.layout.actionviewdetail, null);
		getSupportActionBar().setCustomView(view);

		btnBack = (ImageButton) view.findViewById(R.id.btnBack);
		btnFav = (ImageButton) view.findViewById(R.id.btnFav);
		txtTitle = (TextView) view.findViewById(R.id.txtTitle);
		txtOpen = (TextView) view.findViewById(R.id.txtOpen);
		txtOpen.setVisibility(View.GONE);
		btnFav.setVisibility(View.INVISIBLE);

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(this);
		mTypefaceUtils.applyTypeface(txtTitle);

		Bundle bundle = new Bundle();
		bundle.putInt("menuItemId", getIntent().getIntExtra("menuItemId", 0));

		FoodOptionFragment foodOptionFragment = new FoodOptionFragment();
		foodOptionFragment.setArguments(bundle);

		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.replace(fragment_container, foodOptionFragment);
		// ft.addToBackStack(OrderDetailFragment.class.getName());
		ft.commit();

		btnBack.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
					finish();
				} else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
					getSupportFragmentManager().popBackStack();
				}
			}
		});

		getSupportFragmentManager().addOnBackStackChangedListener(new OnBackStackChangedListener() {
			@Override
			public void onBackStackChanged() {

				if (getActiveFragment().equals(RestaurantDetailsFragment.class.getName())) {

					btnFav.setVisibility(View.VISIBLE);
					txtTitle.setVisibility(View.VISIBLE);
					txtOpen.setVisibility(View.VISIBLE);
				}
			}
		});
	}

	public String getActiveFragment() {

		if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
			return "";
		}
		return getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
	}

	@Override
	public void onBackPressed() {

		if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
			finish();
		} else {
			super.onBackPressed();
		}
	}

	public String getActivityTitle() {
		if (txtTitle != null) {
			return txtTitle.getText().toString();
		} else {
			return "";
		}
	}
}
