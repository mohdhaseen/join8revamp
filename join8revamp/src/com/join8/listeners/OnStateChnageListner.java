package com.join8.listeners;

import com.join8.layout.TimeView;

public interface OnStateChnageListner {
	public void onStateChange(TimeView view, int state);
}
