package com.join8.model;

import java.util.ArrayList;

public class MenuItemData {

	private int MenuItemId = 0;
	private int Quantity = 0;
	private double Amount = 0;
	private double Price = 0;
	private String ItemName = "";
	private String ItemName_EN = "";
	private ArrayList<FoodOptionData> FoodOption = new ArrayList<FoodOptionData>();

	public int getMenuItemId() {
		return MenuItemId;
	}

	public void setMenuItemId(int menuItemId) {
		MenuItemId = menuItemId;
	}

	public int getQuantity() {
		return Quantity;
	}

	public void setQuantity(int quantity) {
		Quantity = quantity;
	}

	public double getAmount() {
		return Amount;
	}

	public void setAmount(double amount) {
		Amount = amount;
	}

	public double getPrice() {
		return Price;
	}

	public void setPrice(double price) {
		Price = price;
	}

	public String getItemName() {
		return ItemName;
	}

	public void setItemName(String itemName) {
		ItemName = itemName;
	}

	public String getItemName_EN() {
		return ItemName_EN;
	}

	public void setItemName_EN(String itemName_EN) {
		ItemName_EN = itemName_EN;
	}

	public ArrayList<FoodOptionData> getFoodOption() {
		return FoodOption;
	}

	public void setFoodOption(ArrayList<FoodOptionData> foodOption) {
		FoodOption = foodOption;
	}

}
