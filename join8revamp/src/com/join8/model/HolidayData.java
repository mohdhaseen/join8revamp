package com.join8.model;

import java.text.SimpleDateFormat;
import java.util.Locale;

import com.join8.utils.Utils;

public class HolidayData {

	private String HolidayDate = "";
	private String Remarks = "";

	private SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.getDefault());

	public String getHolidayDate() {
		return HolidayDate;
	}

	public void setHolidayDate(String holidayDate) {
		HolidayDate = holidayDate;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}

	public int getMonth() {

		try {
			if (!Utils.isEmpty(HolidayDate)) {
				return format.parse(HolidayDate).getMonth();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public int getDay() {

		try {
			if (!Utils.isEmpty(HolidayDate)) {
				return format.parse(HolidayDate).getDate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public int getYear() {

		try {
			if (!Utils.isEmpty(HolidayDate)) {
				return 1900 + format.parse(HolidayDate).getYear();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}
}
