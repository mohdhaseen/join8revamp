package com.join8.model;

import com.join8.utils.Utils;

public class RestaurantServiceHoursData {

	private int Weekday = 0;
	private String OpenTime = "";
	private String CloseTime = "";
	private String OpenHourId = "";
	private String SundayHours = "";
	private String MondayHours = "";
	private String TuesdayHours = "";
	private String WednesdayHours = "";
	private String ThursdayHours = "";
	private String FridayHours = "";
	private String SaturdayHours = "";

	public int getWeekday() {
		return Weekday;
	}

	public void setWeekday(int weekday) {
		Weekday = weekday;
	}

	public String getOpenTime() {
		return OpenTime;
	}

	public void setOpenTime(String openTime) {

		if (!Utils.isEmpty(openTime)) {
			OpenTime = openTime;
		}
	}

	public String getCloseTime() {
		return CloseTime;
	}

	public void setCloseTime(String closeTime) {

		if (!Utils.isEmpty(closeTime)) {
			CloseTime = closeTime;
		}
	}

	public String getOpenHourId() {
		return OpenHourId;
	}

	public void setOpenHourId(String openHourId) {

		if (!Utils.isEmpty(openHourId)) {
			OpenHourId = openHourId;
		}
	}

	public String getSundayHours() {
		return SundayHours;
	}

	public void setSundayHours(String sundayHours) {

		if (!Utils.isEmpty(sundayHours)) {
			SundayHours = sundayHours;
		}
	}

	public String getMondayHours() {
		return MondayHours;
	}

	public void setMondayHours(String mondayHours) {

		if (!Utils.isEmpty(mondayHours)) {
			MondayHours = mondayHours;
		}
	}

	public String getTuesdayHours() {
		return TuesdayHours;
	}

	public void setTuesdayHours(String tuesdayHours) {

		if (!Utils.isEmpty(tuesdayHours)) {
			TuesdayHours = tuesdayHours;
		}
	}

	public String getWednesdayHours() {
		return WednesdayHours;
	}

	public void setWednesdayHours(String wednesdayHours) {

		if (!Utils.isEmpty(wednesdayHours)) {
			WednesdayHours = wednesdayHours;
		}
	}

	public String getThursdayHours() {
		return ThursdayHours;
	}

	public void setThursdayHours(String thursdayHours) {

		if (!Utils.isEmpty(thursdayHours)) {
			ThursdayHours = thursdayHours;
		}
	}

	public String getFridayHours() {
		return FridayHours;
	}

	public void setFridayHours(String fridayHours) {

		if (!Utils.isEmpty(fridayHours)) {
			FridayHours = fridayHours;
		}
	}

	public String getSaturdayHours() {
		return SaturdayHours;
	}

	public void setSaturdayHours(String saturdayHours) {

		if (!Utils.isEmpty(saturdayHours)) {
			SaturdayHours = saturdayHours;
		}
	}

}
