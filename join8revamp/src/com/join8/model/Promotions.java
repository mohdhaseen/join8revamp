package com.join8.model;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class Promotions implements Serializable {

	private String TotalRecords = "";
	private String PromotionId = "";
	private String PromotionCode = "";
	private String PromotionName = "";
	private String CompanyId = "";
	private String StartDate = "";
	private String EndDate = "";
	private double OriginalAmount = 0.0;
	private double PromotionalAmount = 0.0;
	private String Currency = "";
	private String IsWatchList = "";
	private boolean IsEnabled = true;
	private String UserId = "";
	private String IsDeleted = "";
	private String Added_On = "";
	private ArrayList<String> CompanyPhoto = new ArrayList<String>();
	private double Latitude = 0.0;
	private double Longitude = 0.0;
	private String Updated_On = "";
	private String Deleted_On = "";
	private String Description = "";
	private String DescriptionEN = "";
	private String Photo = "";
	private String PromotionStatus = "";
	private String LastNotification = "";
	private String PromotionCount = "";
	private String CompanyName = "";
	private String CompanyNameEN = "";
	private String Phone = "";

	public double getLatitude() {
		return Latitude;
	}

	public double getLongitude() {
		return Longitude;
	}

	public void setLatitude(double latitude) {
		Latitude = latitude;
	}

	public void setLongitude(double longitude) {
		Longitude = longitude;
	}

	public ArrayList<String> getCompanyPhoto() {
		return CompanyPhoto;
	}

	public void setCompanyPhoto(ArrayList<String> companyPhoto) {
		CompanyPhoto = companyPhoto;
	}

	public String getTotalRecords() {
		return TotalRecords;
	}

	public void setTotalRecords(String totalRecords) {
		TotalRecords = totalRecords.trim();
	}

	public String getPromotionId() {
		return PromotionId;
	}

	public void setPromotionId(String promotionId) {
		PromotionId = promotionId.trim();
	}

	public String getPromotionCode() {
		return PromotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		PromotionCode = promotionCode.trim();
	}

	public String getPromotionName() {
		return PromotionName;
	}

	public void setPromotionName(String promotionName) {
		PromotionName = promotionName.trim();
	}

	public String getCompanyId() {
		return CompanyId;
	}

	public void setCompanyId(String companyId) {
		CompanyId = companyId.trim();
	}

	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate.trim();
	}

	public String getEndDate() {
		return EndDate;
	}

	public void setEndDate(String endDate) {
		EndDate = endDate.trim();
	}

	public double getOriginalAmount() {
		return OriginalAmount;
	}

	public void setOriginalAmount(double originalAmount) {
		OriginalAmount = originalAmount;
	}

	public double getPromotionalAmount() {
		return PromotionalAmount;
	}

	public void setPromotionalAmount(double promotionalAmount) {
		DecimalFormat df = new DecimalFormat("#.##");
		PromotionalAmount = Double.parseDouble(df.format(promotionalAmount));
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		Currency = currency.trim();
	}

	public String getIsWatchList() {
		return IsWatchList;
	}

	public void setIsWatchList(String isWatchList) {
		IsWatchList = isWatchList.trim();
	}

	public boolean isIsEnabled() {
		return IsEnabled;
	}

	public void setIsEnabled(boolean isEnabled) {
		IsEnabled = isEnabled;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId.trim();
	}

	public String getIsDeleted() {
		return IsDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		IsDeleted = isDeleted.trim();
	}

	public String getAdded_On() {
		return Added_On;
	}

	public void setAdded_On(String added_On) {
		Added_On = added_On.trim();
	}

	public String getUpdated_On() {
		return Updated_On;
	}

	public void setUpdated_On(String updated_On) {
		Updated_On = updated_On.trim();
	}

	public String getDeleted_On() {
		return Deleted_On;
	}

	public void setDeleted_On(String deleted_On) {
		Deleted_On = deleted_On.trim();
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description.trim();
	}

	public String getDescriptionEN() {
		return DescriptionEN;
	}

	public void setDescriptionEN(String descriptionEN) {
		DescriptionEN = descriptionEN.trim();
	}

	public String getPhoto() {
		return Photo;
	}

	public void setPhoto(String photo) {
		Photo = photo.trim();
	}

	public String getPromotionStatus() {
		return PromotionStatus;
	}

	public void setPromotionStatus(String promotionStatus) {
		PromotionStatus = promotionStatus.trim();
	}

	public String getLastNotification() {
		return LastNotification;
	}

	public void setLastNotification(String lastNotification) {
		LastNotification = lastNotification.trim();
	}

	public String getPromotionCount() {
		return PromotionCount;
	}

	public void setPromotionCount(String promotionCount) {
		PromotionCount = promotionCount.trim();
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName.trim();
	}

	public String getCompanyNameEN() {
		return CompanyNameEN;
	}

	public void setCompanyNameEN(String companyNameEN) {
		CompanyNameEN = companyNameEN.trim();
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		Phone = phone.trim();
	}
}
