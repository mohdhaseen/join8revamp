package com.join8.model;

public class Banner {
	private String SettingType;

	private String AddedOn;

	private String CitySettingId;

	private String SettingValueEN;

	private String Fk_CityId;

	private String SettingValue;

	private String Remarks;

	public String getSettingType() {
		return SettingType;
	}

	public void setSettingType(String SettingType) {
		this.SettingType = SettingType;
	}

	public String getAddedOn() {
		return AddedOn;
	}

	public void setAddedOn(String AddedOn) {
		this.AddedOn = AddedOn;
	}

	public String getCitySettingId() {
		return CitySettingId;
	}

	public void setCitySettingId(String CitySettingId) {
		this.CitySettingId = CitySettingId;
	}

	public String getSettingValueEN() {
		return SettingValueEN;
	}

	public void setSettingValueEN(String SettingValueEN) {
		this.SettingValueEN = SettingValueEN;
	}

	public String getFk_CityId() {
		return Fk_CityId;
	}

	public void setFk_CityId(String Fk_CityId) {
		this.Fk_CityId = Fk_CityId;
	}

	public String getSettingValue() {
		return SettingValue;
	}

	public void setSettingValue(String SettingValue) {
		this.SettingValue = SettingValue;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String Remarks) {
		this.Remarks = Remarks;
	}
}
