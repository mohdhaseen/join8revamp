package com.join8.model;
import android.os.Parcel;
import android.os.Parcelable;

public class ResponseCity implements Parcelable {
	private String SubscriptionPlan1;

	private String Name;

	private String SubscriptionPlan3Renew;

	private String PhoneCode;

	private String NameEN;

	private String SubscriptionPlan3;

	private String SubscriptionPlan2;

	private String SubscriptionPlan1Renew;

	private String UpdatedOn;

	private boolean IsEnable;

	private String CityCode;

	private String BannerImage;

	private String SubscriptionPlan2Renew;

	private String BannerShopID;

	public String getSubscriptionPlan1() {
		return SubscriptionPlan1;
	}

	public void setSubscriptionPlan1(String SubscriptionPlan1) {
		this.SubscriptionPlan1 = SubscriptionPlan1;
	}

	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}

	public String getSubscriptionPlan3Renew() {
		return SubscriptionPlan3Renew;
	}

	public void setSubscriptionPlan3Renew(String SubscriptionPlan3Renew) {
		this.SubscriptionPlan3Renew = SubscriptionPlan3Renew;
	}

	public String getPhoneCode() {
		return PhoneCode;
	}

	public void setPhoneCode(String PhoneCode) {
		this.PhoneCode = PhoneCode;
	}

	public String getNameEN() {
		return NameEN;
	}

	public void setNameEN(String NameEN) {
		this.NameEN = NameEN;
	}

	public String getSubscriptionPlan3() {
		return SubscriptionPlan3;
	}

	public void setSubscriptionPlan3(String SubscriptionPlan3) {
		this.SubscriptionPlan3 = SubscriptionPlan3;
	}

	public String getSubscriptionPlan2() {
		return SubscriptionPlan2;
	}

	public void setSubscriptionPlan2(String SubscriptionPlan2) {
		this.SubscriptionPlan2 = SubscriptionPlan2;
	}

	public String getSubscriptionPlan1Renew() {
		return SubscriptionPlan1Renew;
	}

	public void setSubscriptionPlan1Renew(String SubscriptionPlan1Renew) {
		this.SubscriptionPlan1Renew = SubscriptionPlan1Renew;
	}

	public String getUpdatedOn() {
		return UpdatedOn;
	}

	public void setUpdatedOn(String UpdatedOn) {
		this.UpdatedOn = UpdatedOn;
	}

	public boolean getIsEnable() {
		return IsEnable;
	}

	public void setIsEnable(boolean IsEnable) {
		this.IsEnable = IsEnable;
	}

	public String getCityCode() {
		return CityCode;
	}

	public void setCityCode(String CityCode) {
		this.CityCode = CityCode;
	}

	public String getBannerImage() {
		return BannerImage;
	}

	public void setBannerImage(String BannerImage) {
		this.BannerImage = BannerImage;
	}

	public String getSubscriptionPlan2Renew() {
		return SubscriptionPlan2Renew;
	}

	public void setSubscriptionPlan2Renew(String SubscriptionPlan2Renew) {
		this.SubscriptionPlan2Renew = SubscriptionPlan2Renew;
	}

	public String getBannerShopID() {
		return BannerShopID;
	}

	public void setBannerShopID(String BannerShopID) {
		this.BannerShopID = BannerShopID;
	}

	protected ResponseCity(Parcel in) {
		SubscriptionPlan1 = in.readString();
		Name = in.readString();
		SubscriptionPlan3Renew = in.readString();
		PhoneCode = in.readString();
		NameEN = in.readString();
		SubscriptionPlan3 = in.readString();
		SubscriptionPlan2 = in.readString();
		SubscriptionPlan1Renew = in.readString();
		UpdatedOn = in.readString();
		IsEnable = in.readByte() != 0x00;
		CityCode = in.readString();
		BannerImage = in.readString();
		SubscriptionPlan2Renew = in.readString();
		BannerShopID = in.readString();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(SubscriptionPlan1);
		dest.writeString(Name);
		dest.writeString(SubscriptionPlan3Renew);
		dest.writeString(PhoneCode);
		dest.writeString(NameEN);
		dest.writeString(SubscriptionPlan3);
		dest.writeString(SubscriptionPlan2);
		dest.writeString(SubscriptionPlan1Renew);
		dest.writeString(UpdatedOn);
		dest.writeByte((byte) (IsEnable ? 0x01 : 0x00));
		dest.writeString(CityCode);
		dest.writeString(BannerImage);
		dest.writeString(SubscriptionPlan2Renew);
		dest.writeString(BannerShopID);
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<ResponseCity> CREATOR = new Parcelable.Creator<ResponseCity>() {
		@Override
		public ResponseCity createFromParcel(Parcel in) {
			return new ResponseCity(in);
		}

		@Override
		public ResponseCity[] newArray(int size) {
			return new ResponseCity[size];
		}
	};
}