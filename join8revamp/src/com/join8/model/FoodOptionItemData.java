package com.join8.model;

import java.io.Serializable;

public class FoodOptionItemData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int FoodOptionItemId = 0;
	private String FoodOptionItemName = "";
	private String FoodOptionItemNameEn = "";
	private double ExtraPrice = 0;
	// custom
	private boolean added = false;
	private int IsFoodOption = 0;

	public int getFoodOptionItemId() {
		return FoodOptionItemId;
	}

	public void setFoodOptionItemId(int foodOptionItemId) {
		FoodOptionItemId = foodOptionItemId;
	}

	public String getFoodOptionItemName() {
		return FoodOptionItemName;
	}

	public void setFoodOptionItemName(String foodOptionItemName) {
		FoodOptionItemName = foodOptionItemName;
	}

	public String getFoodOptionItemNameEn() {
		return FoodOptionItemNameEn;
	}

	public void setFoodOptionItemNameEn(String foodOptionItemNameEn) {
		FoodOptionItemNameEn = foodOptionItemNameEn;
	}

	public double getExtraPrice() {
		return ExtraPrice;
	}

	public void setExtraPrice(double extraPrice) {
		ExtraPrice = extraPrice;
	}

	public boolean isAdded() {
		return added;
	}

	public void setAdded(boolean added) {
		this.added = added;
	}

	public int getIsFoodOption() {
		return IsFoodOption;
	}

	public void setIsFoodOption(int isFoodOption) {
		IsFoodOption = isFoodOption;
	}

}
