package com.join8.model;

import java.io.Serializable;

public class Registration implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String UserName = "";
	private String Email = "";
	private String LoginPassword = "";
	private String UserPreferLanguage = "";
	private String LoginPreference = "";   // System = 1, Live = 2, Facebook = 3, Google = 4, Yahoo = 5, Phone = 6
	private String LastLoginTime = "";
	private boolean DefaultProfile = false;
	private String JobTitle = "";
	private String FirstName = "";
	private String LastName = "";
	private String MobileNumber = "";
	private String Photo = "";
	private String ActivationCode = "";
	private boolean IsRestaurantStaff = false;
	private String Address1 = "";
	private String Address2 = "";
	private String ZIP = "";
	private String PhoneNumber = "";
	private String ProfileName = "";
	private String Gender = "";
	private int Country = -1;
	private String Province = "";
	private int City = -1;
	private String CompanyName = "";
	private int RecordStatus = -1;
	private String RecordUpdatedBy = "";
	private String RecordCreatedTime = "";
	private String ForgotPasswordCode = "";
	private String ForgotPasswordTime = "";
	private String BusinessPhone = "";
	private String Remarks = "";
	private String Roles = "";
	private String Timestamp = "";
	private String PartitionKey = "";
	private String RowKey = "";

	public String getProfileName() {
		return ProfileName;
	}

	public void setProfileName(String profileName) {
		ProfileName = profileName;
	}

	public String getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}

	public String getAddress1() {
		return Address1;
	}

	public void setAddress1(String address1) {
		Address1 = address1;
	}

	public String getAddress2() {
		return Address2;
	}

	public void setAddress2(String address2) {
		Address2 = address2;
	}

	public String getZIP() {
		return ZIP;
	}

	public void setZIP(String zIP) {
		ZIP = zIP;
	}

	public boolean isIsRestaurantStaff() {
		return IsRestaurantStaff;
	}

	public void setIsRestaurantStaff(boolean isRestaurantStaff) {
		IsRestaurantStaff = isRestaurantStaff;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String userName) {
		UserName = userName;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public String getLoginPassword() {
		return LoginPassword;
	}

	public void setLoginPassword(String loginPassword) {
		LoginPassword = loginPassword;
	}

	public String getUserPreferLanguage() {
		return UserPreferLanguage;
	}

	public void setUserPreferLanguage(String userPreferLanguage) {
		UserPreferLanguage = userPreferLanguage;
	}

	public String getLoginPreference() {
		return LoginPreference;
	}

	public void setLoginPreference(String loginPreference) {
		LoginPreference = loginPreference;
	}

	public String getLastLoginTime() {
		return LastLoginTime;
	}

	public void setLastLoginTime(String lastLoginTime) {
		LastLoginTime = lastLoginTime;
	}

	public void setDefaultProfile(boolean defaultProfile) {
		DefaultProfile = defaultProfile;
	}

	public boolean isDefaultProfile() {
		return DefaultProfile;
	}

	public String getJobTitle() {
		return JobTitle;
	}

	public void setJobTitle(String jobTitle) {
		JobTitle = jobTitle;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}

	public String getPhoto() {
		return Photo;
	}

	public void setPhoto(String photo) {
		Photo = photo;
	}

	public String getActivationCode() {
		return ActivationCode;
	}

	public void setActivationCode(String activationCode) {
		ActivationCode = activationCode;
	}

	public String getGender() {
		return Gender;
	}

	public void setGender(String gender) {
		Gender = gender;
	}

	public int getCountry() {
		return Country;
	}

	public void setCountry(int country) {
		Country = country;
	}

	public String getProvince() {
		return Province;
	}

	public void setProvince(String province) {
		Province = province;
	}

	public int getCity() {
		return City;
	}

	public void setCity(int city) {
		City = city;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	public int getRecordStatus() {
		return RecordStatus;
	}

	public void setRecordStatus(int recordStatus) {
		RecordStatus = recordStatus;
	}

	public String getRecordUpdatedBy() {
		return RecordUpdatedBy;
	}

	public void setRecordUpdatedBy(String recordUpdatedBy) {
		RecordUpdatedBy = recordUpdatedBy;
	}

	public String getRecordCreatedTime() {
		return RecordCreatedTime;
	}

	public void setRecordCreatedTime(String recordCreatedTime) {
		RecordCreatedTime = recordCreatedTime;
	}

	public String getForgotPasswordCode() {
		return ForgotPasswordCode;
	}

	public void setForgotPasswordCode(String forgotPasswordCode) {
		ForgotPasswordCode = forgotPasswordCode;
	}

	public String getForgotPasswordTime() {
		return ForgotPasswordTime;
	}

	public void setForgotPasswordTime(String forgotPasswordTime) {
		ForgotPasswordTime = forgotPasswordTime;
	}

	public String getBusinessPhone() {
		return BusinessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		BusinessPhone = businessPhone;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}

	public String getRoles() {
		return Roles;
	}

	public void setRoles(String roles) {
		Roles = roles;
	}

	public String getTimestamp() {
		return Timestamp;
	}

	public void setTimestamp(String timestamp) {
		Timestamp = timestamp;
	}

	public String getPartitionKey() {
		return PartitionKey;
	}

	public void setPartitionKey(String partitionKey) {
		PartitionKey = partitionKey;
	}

	public String getRowKey() {
		return RowKey;
	}

	public void setRowKey(String rowKey) {
		RowKey = rowKey;
	}

}
