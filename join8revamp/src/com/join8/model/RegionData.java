package com.join8.model;

public class RegionData {

	private int AreaId = 0;
	private String AreaNameEN = "";
	private String AreaNameSC ="";
	private String AreaNameTC = "";
	private boolean checked = false; 
	
	public int getAreaId() {
		return AreaId;
	}
	public void setAreaId(int areaId) {
		AreaId = areaId;
	}
	public String getAreaNameEN() {
		return AreaNameEN;
	}
	public void setAreaNameEN(String areaNameEN) {
		AreaNameEN = areaNameEN;
	}
	public String getAreaNameSC() {
		return AreaNameSC;
	}
	public void setAreaNameSC(String areaNameSC) {
		AreaNameSC = areaNameSC;
	}
	public String getAreaNameTC() {
		return AreaNameTC;
	}
	public void setAreaNameTC(String areaNameTC) {
		AreaNameTC = areaNameTC;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
		

}
