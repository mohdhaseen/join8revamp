package com.join8.model;

import java.io.Serializable;
import java.util.ArrayList;

public class TableTypeData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String BookingName = "";
	private String BookingNameEn = "";
	private String Terms = "";
	private String TermsEn = "";
	private double BookingDeposit = 0;
	private ArrayList<String> BookingImageLink = new ArrayList<String>();

	public String getBookingName() {
		return BookingName;
	}

	public void setBookingName(String bookingName) {
		BookingName = bookingName;
	}

	public String getBookingNameEn() {
		return BookingNameEn;
	}

	public void setBookingNameEn(String bookingNameEn) {
		BookingNameEn = bookingNameEn;
	}

	public String getTerms() {
		return Terms;
	}

	public void setTerms(String terms) {
		Terms = terms;
	}

	public String getTermsEn() {
		return TermsEn;
	}

	public void setTermsEn(String termsEn) {
		TermsEn = termsEn;
	}

	public double getBookingDeposit() {
		return BookingDeposit;
	}

	public void setBookingDeposit(double bookingDeposit) {
		BookingDeposit = bookingDeposit;
	}

	public ArrayList<String> getBookingImageLink() {
		return BookingImageLink;
	}

	public void setBookingImageLink(ArrayList<String> bookingImageLink) {
		BookingImageLink = bookingImageLink;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	// public static final Parcelable.Creator<TableTypeData> CREATOR = new
	// Creator<TableTypeData>() {
	//
	// public TableTypeData createFromParcel(Parcel source) {
	//
	// TableTypeData data = new TableTypeData();
	//
	// data.BookingName = source.readString();
	// data.BookingNameEN = source.readString();
	// data.Terms = source.readString();
	// data.TermsEN = source.readString();
	// data.BookingDeposit = source.readDouble();
	// data.BookingImageLink = source.readString();
	//
	// return data;
	// }
	//
	// public TableTypeData[] newArray(int size) {
	// return new TableTypeData[size];
	// }
	// };
	//
	// public int describeContents() {
	// return 0;
	// }
	//
	// public void writeToParcel(Parcel parcel, int flags) {
	//
	// parcel.writeString(BookingName);
	// parcel.writeString(BookingNameEN);
	// parcel.writeString(Terms);
	// parcel.writeString(TermsEN);
	// parcel.writeDouble(BookingDeposit);
	// parcel.writeString(BookingImageLink);
	// }
}
