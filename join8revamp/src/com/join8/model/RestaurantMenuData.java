package com.join8.model;

import java.util.ArrayList;

import com.join8.utils.Utils;

public class RestaurantMenuData {

	private String MenuId = "";
	private String MenuName = "";
	private String MenuNameEn = "";
	private ArrayList<RestaurantMenuItemData> tblMenuItem = new ArrayList<RestaurantMenuItemData>();

	public String getMenuId() {
		return MenuId;
	}

	public void setMenuId(String menuId) {
		MenuId = menuId.trim();
	}

	public String getMenuName() {
		return MenuName;
	}

	public void setMenuName(String menuName) {

		if (!Utils.isEmpty(menuName)) {
			MenuName = menuName.trim();
		}
	}

	public String getMenuNameEn() {
		return MenuNameEn;
	}

	public void setMenuNameEn(String menuNameEn) {

		if (!Utils.isEmpty(menuNameEn)) {
			MenuNameEn = menuNameEn.trim();
		}
	}

	public ArrayList<RestaurantMenuItemData> getTblMenuItem() {
		return tblMenuItem;
	}

	public void setTblMenuItem(ArrayList<RestaurantMenuItemData> tblMenuItem) {
		this.tblMenuItem = tblMenuItem;
	}

}
