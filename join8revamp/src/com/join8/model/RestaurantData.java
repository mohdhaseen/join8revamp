package com.join8.model;

import java.io.Serializable;
import java.util.ArrayList;

import com.join8.utils.Utils;

public class RestaurantData implements Serializable {

	private static final long serialVersionUID = 1L;
	private int CompanyId = -1;
	private int DishType = -1;
	private int CuisineType = -1;
	private String CompanyNameEN = "";
	private String CompanyName = "";
	private String Address1 = "";
	private String Address2 = "";
	private String Description = "";
	private String Description_EN = "";
	private String Phone = "";
	private double Longitude = 0.0;
	private double Latitude = 0.0;
	private String Availability = "";
	private ArrayList<String> Photo = new ArrayList<String>();
	private String ItemName_EN = "";
	private String ItemName = "";
	private String Price = "0";
	private boolean FoodRecommended = false;
	private int MenuItemId = -1;
	private int MenuId = -1;
	private String Currency = "";
	private boolean FoodSpicy = false;
	private boolean TaxRequired = false;
	private boolean FoodVegtarian = false;
	private boolean FoodTakeOut = false;
	private String Added_On = "";
	private String Updated_On = "";
	private String Deleted_On = "";
	private boolean IsEnable = false;
	private boolean IsDeleted = false;
	private boolean IsBookable = true;
	private boolean IsOrderable = true;
	private String RecordUpdatedBy = "";
	private String UserId = "";
	private String TypeValueEN = "";
	private String POSMenuId = "";
	private String WatchListId = "";
	// new
	private boolean HasFoodOption = false;
	private ArrayList<FoodOptionData> FoodOption = new ArrayList<FoodOptionData>();
	private int BookingType = 0;
	private int TakeOutPayOptionOrder = 0;
	private int DeliveryOption = 0;
	private double DeliveryFee = 0;

	public String getPOSMenuId() {
		return POSMenuId;
	}

	public void setPOSMenuId(String pOSMenuId) {
		if (!Utils.isEmpty(pOSMenuId)) {
			POSMenuId = pOSMenuId;
		}
	}

	public int getDishType() {
		return DishType;
	}

	public void setDishType(int dishType) {
		DishType = dishType;
	}

	public int getCuisineType() {
		return CuisineType;
	}

	public void setCuisineType(int cuisineType) {
		CuisineType = cuisineType;
	}

	public boolean isFoodRecommended() {
		return FoodRecommended;
	}

	public void setFoodRecommended(boolean foodRecommended) {
		FoodRecommended = foodRecommended;
	}

	public int getMenuItemId() {
		return MenuItemId;
	}

	public void setMenuItemId(int menuItemId) {
		MenuItemId = menuItemId;
	}

	public int getMenuId() {
		return MenuId;
	}

	public void setMenuId(int menuId) {
		MenuId = menuId;
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		if (!Utils.isEmpty(currency)) {
			Currency = currency;
		}
	}

	public boolean isFoodSpicy() {
		return FoodSpicy;
	}

	public void setFoodSpicy(boolean foodSpicy) {
		FoodSpicy = foodSpicy;
	}

	public boolean isTaxRequired() {
		return TaxRequired;
	}

	public void setTaxRequired(boolean taxRequired) {
		TaxRequired = taxRequired;
	}

	public boolean isFoodVegtarian() {
		return FoodVegtarian;
	}

	public void setFoodVegtarian(boolean foodVegtarian) {
		FoodVegtarian = foodVegtarian;
	}

	public boolean isFoodTakeOut() {
		return FoodTakeOut;
	}

	public void setFoodTakeOut(boolean foodTakeOut) {
		FoodTakeOut = foodTakeOut;
	}

	public String getAdded_On() {
		return Added_On;
	}

	public void setAdded_On(String added_On) {
		if (!Utils.isEmpty(added_On)) {
			Added_On = added_On;
		}
	}

	public String getUpdated_On() {
		return Updated_On;
	}

	public void setUpdated_On(String updated_On) {
		if (!Utils.isEmpty(updated_On)) {
			Updated_On = updated_On;
		}
	}

	public String getDeleted_On() {
		return Deleted_On;
	}

	public void setDeleted_On(String deleted_On) {
		if (!Utils.isEmpty(deleted_On)) {
			Deleted_On = deleted_On;
		}
	}

	public boolean isIsEnable() {
		return IsEnable;
	}

	public void setIsEnable(boolean isEnable) {
		IsEnable = isEnable;
	}

	public boolean isIsDeleted() {
		return IsDeleted;
	}

	public void setIsDeleted(boolean isDeleted) {
		IsDeleted = isDeleted;
	}

	public String getRecordUpdatedBy() {
		return RecordUpdatedBy;
	}

	public void setRecordUpdatedBy(String recordUpdatedBy) {
		if (!Utils.isEmpty(recordUpdatedBy)) {
			RecordUpdatedBy = recordUpdatedBy;
		}
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		if (!Utils.isEmpty(userId)) {
			UserId = userId;
		}
	}

	public String getTypeValueEN() {
		return TypeValueEN;
	}

	public void setTypeValueEN(String typeValueEN) {
		if (!Utils.isEmpty(typeValueEN)) {
			TypeValueEN = typeValueEN;
		}
	}

	public int getCompanyId() {
		return CompanyId;
	}

	public void setCompanyId(int companyId) {
		CompanyId = companyId;

	}

	public String getItemName_EN() {
		return ItemName_EN;
	}

	public void setItemName_EN(String itemName_EN) {
		if (!Utils.isEmpty(itemName_EN)) {
			ItemName_EN = itemName_EN;
		}
	}

	public String getItemName() {
		return ItemName;
	}

	public void setItemName(String itemName) {
		if (!Utils.isEmpty(itemName)) {
			ItemName = itemName;
		}
	}

	public String getAddress1() {
		return Address1;
	}

	public void setAddress1(String address1) {
		if (!Utils.isEmpty(address1)) {
			Address1 = address1;
		}
	}

	public String getAddress2() {
		return Address2;
	}

	public void setAddress2(String address2) {
		if (!Utils.isEmpty(address2)) {
			Address2 = address2;
		}
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		if (!Utils.isEmpty(description)) {
			Description = description;
		}
	}

	public String getDescription_EN() {
		return Description_EN;
	}

	public void setDescription_EN(String description_EN) {
		Description_EN = description_EN;
	}

	public String getPrice() {
		return Price.equals("") ? "0" : Price;
	}

	public void setPrice(String price) {
		if (!Utils.isEmpty(price)) {
			Price = price;
		}
	}

	public String getCompanyNameEN() {
		return CompanyNameEN;
	}

	public void setCompanyNameEN(String companyNameEN) {
		if (!Utils.isEmpty(companyNameEN)) {
			CompanyNameEN = companyNameEN;
		}
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		if (!Utils.isEmpty(companyName)) {
			CompanyName = companyName;
		}
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		if (!Utils.isEmpty(phone)) {
			Phone = phone;
		}
	}

	public String getAvailability() {
		if (Utils.isEmpty(Availability)) {
			return "";
		} else {
			return Availability;
		}
	}

	public void setAvailability(String availability) {
		if (!Utils.isEmpty(availability)) {
			Availability = availability;
		}
	}

	public ArrayList<String> getPhoto() {
		return Photo;
	}

	public void setPhoto(ArrayList<String> photo) {
		Photo = photo;
	}

	public double getLongitude() {
		return Longitude;
	}

	public void setLongitude(double longitude) {
		Longitude = longitude;
	}

	public double getLatitude() {
		return Latitude;
	}

	public void setLatitude(double latitude) {
		Latitude = latitude;
	}

	public String getWatchListId() {
		return WatchListId;
	}

	public void setWatchListId(String watchListId) {
		WatchListId = watchListId;
	}

	public boolean isBookable() {
		return IsBookable;
	}

	public void setIsBookable(boolean isBookable) {
		IsBookable = isBookable;
	}

	public boolean isOrderable() {
		return IsOrderable;
	}

	public void setIsOrderable(boolean isOrderable) {
		IsOrderable = isOrderable;
	}

	public boolean isHasFoodOption() {
		return HasFoodOption;
	}

	public void setHasFoodOption(boolean hasFoodOption) {
		HasFoodOption = hasFoodOption;
	}

	public ArrayList<FoodOptionData> getFoodOption() {
		return FoodOption;
	}

	public void setFoodOption(ArrayList<FoodOptionData> foodOption) {
		FoodOption = foodOption;
	}

	public int getBookingType() {
		return BookingType;
	}

	public void setBookingType(int bookingType) {
		BookingType = bookingType;
	}

	public int getTakeOutPayOptionOrder() {
		return TakeOutPayOptionOrder;
	}

	public void setTakeOutPayOptionOrder(int takeOutPayOptionOrder) {
		TakeOutPayOptionOrder = takeOutPayOptionOrder;
	}

	public int getDeliveryOption() {
		return DeliveryOption;
	}

	public void setDeliveryOption(int deliveryOption) {
		DeliveryOption = deliveryOption;
	}

	public double getDeliveryFee() {
		return DeliveryFee;
	}

	public void setDeliveryFee(double deliveryFee) {
		DeliveryFee = deliveryFee;
	}

}
