package com.join8.model;

public class DressCodeData {
	private int TypeId = -1;
	private String Type = "";
	private String TypeValueEN = "";
	private String TypeValueTC = "";
	private String TypeValueSC = "";
	public int getTypeId() {
		return TypeId;
	}
	public void setTypeId(int typeId) {
		TypeId = typeId;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getTypeValueEN() {
		return TypeValueEN;
	}
	public void setTypeValueEN(String typeValueEN) {
		TypeValueEN = typeValueEN;
	}
	public String getTypeValueTC() {
		return TypeValueTC;
	}
	public void setTypeValueTC(String typeValueTC) {
		TypeValueTC = typeValueTC;
	}
	public String getTypeValueSC() {
		return TypeValueSC;
	}
	public void setTypeValueSC(String typeValueSC) {
		TypeValueSC = typeValueSC;
	}
	
	
}
