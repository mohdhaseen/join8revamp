package com.join8.model;

import com.join8.HomeScreen;
import com.join8.utils.ConstantData;

/**
 * Created by craterzone on 27/1/16.
 */
public class Area {
	private String AreaId;

	private String AreaName;

	private String AreaNameEn;

	private String fk_CityID;

	public String getAreaId() {
		return AreaId;
	}

	public void setAreaId(String AreaId) {
		this.AreaId = AreaId;
	}

	public String getAreaName() {
		return AreaName;
	}

	public void setAreaName(String AreaName) {
		this.AreaName = AreaName;
	}

	public String getAreaNameEn() {
		return AreaNameEn;
	}

	public void setAreaNameEn(String AreaNameEn) {
		this.AreaNameEn = AreaNameEn;
	}

	public String getFk_CityID() {
		return fk_CityID;
	}

	public void setFk_CityID(String fk_CityID) {
		this.fk_CityID = fk_CityID;
	}

	@Override
	public String toString() {
		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH))
			return AreaNameEn;
		return AreaName;
	}
}
