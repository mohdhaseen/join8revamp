package com.join8.model;

import java.util.ArrayList;

public class CuisineCategoryData {

	private String CategoryEN = "";
	private String CategorySC = "";
	private String CategoryTC = "";
	private String photo = "";
	private ArrayList<CuisineData> CuisineTypeList = new ArrayList<CuisineData>();

	public String getCategoryEN() {
		return CategoryEN;
	}

	public void setCategoryEN(String categoryEN) {
		CategoryEN = categoryEN;
	}

	public String getCategorySC() {
		return CategorySC;
	}

	public void setCategorySC(String categorySC) {
		CategorySC = categorySC;
	}

	public String getCategoryTC() {
		return CategoryTC;
	}

	public void setCategoryTC(String categoryTC) {
		CategoryTC = categoryTC;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public ArrayList<CuisineData> getCuisineTypeList() {
		return CuisineTypeList;
	}

	public void setCuisineTypeList(ArrayList<CuisineData> cuisineTypeList) {
		CuisineTypeList = cuisineTypeList;
	}

}
