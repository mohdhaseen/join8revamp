package com.join8.model;

import java.io.Serializable;

public class City implements Serializable {

	private String CityNameEN = "";
	private String CityNameSC = "";
	private String CityNameTC = "";
	private String CountryCode = "";
	private boolean IsEnabled = false;
	private String PhoneCode = "";
	private String ProvinceCode = "";
	private String ShowForSearch = "";
	private String PartitionKey = "";

	public String getCityNameEN() {
		return CityNameEN;
	}

	public void setCityNameEN(String cityNameEN) {
		CityNameEN = cityNameEN;
	}

	public String getCityNameSC() {
		return CityNameSC;
	}

	public void setCityNameSC(String cityNameSC) {
		CityNameSC = cityNameSC;
	}

	public String getCityNameTC() {
		return CityNameTC;
	}

	public void setCityNameTC(String cityNameTC) {
		CityNameTC = cityNameTC;
	}

	public String getCountryCode() {
		return CountryCode;
	}

	public void setCountryCode(String countryCode) {
		CountryCode = countryCode;
	}

	public boolean isIsEnabled() {
		return IsEnabled;
	}

	public void setIsEnabled(boolean isEnabled) {
		IsEnabled = isEnabled;
	}

	public String getPhoneCode() {
		return PhoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		PhoneCode = phoneCode;
	}

	public String getProvinceCode() {
		return ProvinceCode;
	}

	public void setProvinceCode(String provinceCode) {
		ProvinceCode = provinceCode;
	}

	public String getShowForSearch() {
		return ShowForSearch;
	}

	public void setShowForSearch(String showForSearch) {
		ShowForSearch = showForSearch;
	}

	public String getPartitionKey() {
		return PartitionKey;
	}

	public void setPartitionKey(String partitionKey) {
		PartitionKey = partitionKey;
	}

}
