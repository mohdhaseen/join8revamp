package com.join8.model;

import com.join8.HomeScreen;
import com.join8.utils.ConstantData;

public class SubCategory {
	private String Name;

	private String AddedOn;

	private String ShopSubCategoryID;

	private String NameEn;

	private String UpdatedOn;

	private String PhotoUrl;

	private String IsEnabled;

	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}

	public String getAddedOn() {
		return AddedOn;
	}

	public void setAddedOn(String AddedOn) {
		this.AddedOn = AddedOn;
	}

	public String getShopSubCategoryID() {
		return ShopSubCategoryID;
	}

	public void setShopSubCategoryID(String ShopSubCategoryID) {
		this.ShopSubCategoryID = ShopSubCategoryID;
	}

	public String getNameEn() {
		return NameEn;
	}

	public void setNameEn(String NameEn) {
		this.NameEn = NameEn;
	}

	public String getUpdatedOn() {
		return UpdatedOn;
	}

	public void setUpdatedOn(String UpdatedOn) {
		this.UpdatedOn = UpdatedOn;
	}

	public String getPhotoUrl() {
		return PhotoUrl;
	}

	public void setPhotoUrl(String PhotoUrl) {
		this.PhotoUrl = PhotoUrl;
	}

	public String getIsEnabled() {
		return IsEnabled;
	}

	public void setIsEnabled(String IsEnabled) {
		this.IsEnabled = IsEnabled;
	}

	@Override
	public String toString() {
		if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH))
			return NameEn;
		return Name;
	}
}
