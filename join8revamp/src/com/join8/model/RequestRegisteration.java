package com.join8.model;

public class RequestRegisteration {
	private String MobilePhoneNumber;

	private String Fk_CityCode;

	private String PreferLanguage;

	private String ActivationCode;

	private String Password;

	private String FirstName;

	private String LastName;

	public String getMobilePhoneNumber() {
		return MobilePhoneNumber;
	}

	public void setMobilePhoneNumber(String MobilePhoneNumber) {
		this.MobilePhoneNumber = MobilePhoneNumber;
	}

	public String getFk_CityCode() {
		return Fk_CityCode;
	}

	public void setFk_CityCode(String Fk_CityCode) {
		this.Fk_CityCode = Fk_CityCode;
	}

	public String getPreferLanguage() {
		return PreferLanguage;
	}

	public void setPreferLanguage(String PreferLanguage) {
		this.PreferLanguage = PreferLanguage;
	}

	public String getActivationCode() {
		return ActivationCode;
	}

	public void setActivationCode(String ActivationCode) {
		this.ActivationCode = ActivationCode;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String Password) {
		this.Password = Password;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String FirstName) {
		this.FirstName = FirstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String LastName) {
		this.LastName = LastName;
	}

}
