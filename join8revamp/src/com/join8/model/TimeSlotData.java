package com.join8.model;

public class TimeSlotData {

	private int BookingTemplateId = 0;
	private String BookingTime = "";
	private double BookingDeposit = 0;
	private int status = 0;

	public int getBookingTemplateId() {
		return BookingTemplateId;
	}

	public void setBookingTemplateId(int bookingTemplateId) {
		BookingTemplateId = bookingTemplateId;
	}

	public String getBookingTime() {
		return BookingTime;
	}

	public void setBookingTime(String bookingTime) {
		BookingTime = bookingTime;
	}

	public double getBookingDeposit() {
		return BookingDeposit;
	}

	public void setBookingDeposit(double bookingDeposit) {
		BookingDeposit = bookingDeposit;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}
