package com.join8.model;

import java.util.ArrayList;

/**
 * Created by craterzone on 29/1/16.
 */
public class RequestAddShop {
    private String Description;

    private String Phone;

    private String Area;

    private String ShopName;

    private String AddressEN;

    private String ShopNameEN;

    private String DescriptionEN;

    private ArrayList<ShopImageData> ShopImageData;

    private String Fk_AddedBy;

    private String Email;

    private String Web;

    private String Address;

    private String Latitude;

    private String Longitude;

    private String Fk_City;

    private String Fk_ShopCategoryID;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String Area) {
        this.Area = Area;
    }

    public String getShopName() {
        return ShopName;
    }

    public void setShopName(String ShopName) {
        this.ShopName = ShopName;
    }

    public String getAddressEN() {
        return AddressEN;
    }

    public void setAddressEN(String AddressEN) {
        this.AddressEN = AddressEN;
    }

    public String getShopNameEN() {
        return ShopNameEN;
    }

    public void setShopNameEN(String ShopNameEN) {
        this.ShopNameEN = ShopNameEN;
    }

    public String getDescriptionEN() {
        return DescriptionEN;
    }

    public void setDescriptionEN(String DescriptionEN) {
        this.DescriptionEN = DescriptionEN;
    }

    public ArrayList<ShopImageData> getShopImageData() {
        return ShopImageData;
    }

    public void setShopImageData(ArrayList<com.join8.model.ShopImageData> ShopImageData) {
        this.ShopImageData = ShopImageData;
    }

    public String getFk_AddedBy() {
        return Fk_AddedBy;
    }

    public void setFk_AddedBy(String Fk_AddedBy) {
        this.Fk_AddedBy = Fk_AddedBy;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getWeb() {
        return Web;
    }

    public void setWeb(String Web) {
        this.Web = Web;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String Latitude) {
        this.Latitude = Latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String Longitude) {
        this.Longitude = Longitude;
    }

    public String getFk_City() {
        return Fk_City;
    }

    public void setFk_City(String Fk_City) {
        this.Fk_City = Fk_City;
    }

    public String getFk_ShopCategoryID() {
        return Fk_ShopCategoryID;
    }

    public void setFk_ShopCategoryID(String Fk_ShopCategoryID) {
        this.Fk_ShopCategoryID = Fk_ShopCategoryID;
    }

}
