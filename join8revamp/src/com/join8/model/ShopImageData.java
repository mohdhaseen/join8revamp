package com.join8.model;

/**
 * Created by craterzone on 29/1/16.
 */
public class ShopImageData {
	private String PhotoNameEN;

	private String PhotoName;
	private String FileName;

	private String FileContent;

	private String ContentType;
	private String Fk_ShopId;

	public String getFk_ShopId() {
		return Fk_ShopId;
	}

	public void setFk_ShopId(String fk_ShopId) {
		Fk_ShopId = fk_ShopId;
	}

	public ShopImageData(String photoNameEN, String photoName, String fileName, String fileContent, String contentType) {
		PhotoNameEN = photoNameEN;
		PhotoName = photoName;
		FileName = fileName;
		FileContent = fileContent;
		ContentType = contentType;
	}

	public ShopImageData() {

	}

	public String getPhotoNameEN() {
		return PhotoNameEN;
	}

	public void setPhotoNameEN(String PhotoNameEN) {
		this.PhotoNameEN = PhotoNameEN;
	}

	public String getPhotoName() {
		return PhotoName;
	}

	public void setPhotoName(String PhotoName) {
		this.PhotoName = PhotoName;
	}

	public String getFileName() {
		return FileName;
	}

	public void setFileName(String FileName) {
		this.FileName = FileName;
	}

	public String getFileContent() {
		return FileContent;
	}

	public void setFileContent(String FileContent) {
		this.FileContent = FileContent;
	}

	public String getContentType() {
		return ContentType;
	}

	public void setContentType(String ContentType) {
		this.ContentType = ContentType;
	}

}
