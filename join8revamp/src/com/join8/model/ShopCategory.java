package com.join8.model;

import com.join8.HomeScreen;
import com.join8.utils.ConstantData;


/**
 * Created by craterzone on 27/1/16.
 */
public class ShopCategory {
    private String ShopTypeEn;

    private String ShopCategoryId;

    private String ShopType;

    public String getShopTypeEn() {
        return ShopTypeEn;
    }

    public void setShopTypeEn(String ShopTypeEn) {
        this.ShopTypeEn = ShopTypeEn;
    }

    public String getShopCategoryId() {
        return ShopCategoryId;
    }

    public void setShopCategoryId(String ShopCategoryId) {
        this.ShopCategoryId = ShopCategoryId;
    }

    public String getShopType() {
        return ShopType;
    }

    public void setShopType(String ShopType) {
        this.ShopType = ShopType;
    }

    @Override
    public String toString() {
    	if (HomeScreen.LANGUAGE.equalsIgnoreCase(ConstantData.LANG_ENGLISH))
            return ShopTypeEn;
        return ShopType;
    }
}

