package com.join8.model;

import java.io.Serializable;
import java.util.ArrayList;

public class FoodOptionData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int FoodOptionId = 0;
	private String FoodOptionName = "";
	private String FoodOptionNameEN = "";
	private int MaxSelection = 0;
	private int Mandatory = 0;
	private double BasePrice = 0;
	private ArrayList<FoodOptionItemData> FoodOptionItem = new ArrayList<FoodOptionItemData>();
	// custom
	private int TotalSelection = 0;

	public int getFoodOptionId() {
		return FoodOptionId;
	}

	public void setFoodOptionId(int foodOptionId) {
		FoodOptionId = foodOptionId;
	}

	public String getFoodOptionName() {
		return FoodOptionName;
	}

	public void setFoodOptionName(String foodOptionName) {
		FoodOptionName = foodOptionName;
	}

	public String getFoodOptionNameEN() {
		return FoodOptionNameEN;
	}

	public void setFoodOptionNameEN(String foodOptionNameEN) {
		FoodOptionNameEN = foodOptionNameEN;
	}

	public int getMaxSelection() {
		return MaxSelection;
	}

	public void setMaxSelection(int maxSelection) {
		MaxSelection = maxSelection;
	}

	public int getMandatory() {
		return Mandatory;
	}

	public void setMandatory(int mandatory) {
		Mandatory = mandatory;
	}

	public double getBasePrice() {
		return BasePrice;
	}

	public void setBasePrice(double basePrice) {
		BasePrice = basePrice;
	}

	public ArrayList<FoodOptionItemData> getFoodOptionItem() {
		return FoodOptionItem;
	}

	public void setFoodOptionItem(ArrayList<FoodOptionItemData> foodOptionItem) {
		FoodOptionItem = foodOptionItem;
	}

	public int getTotalSelection() {
		return TotalSelection;
	}

	public void setTotalSelection(int totalSelection) {
		TotalSelection = totalSelection;
	}

}
