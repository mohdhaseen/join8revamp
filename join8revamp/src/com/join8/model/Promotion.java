package com.join8.model;

public class Promotion {
	 private String PromotionId;

	    private String isEnable;

	    private String Phone;

	    private String EffectiveStartDate;

	    private String UpdatedOn;

	    private int TotalRecords;

	    private String Quota;

	    private String StartDate;

	    private String Fk_ShopId;

	    private String PromotionStatus;

	    private String Latitude;

	    private String Photo4;

	    private String Fk_UserId;

	    private String Photo2;

	    private String PromotionName;

	    private String Photo3;

	    private String Description;

	    private String Photo1;

	    private String ShopName;

	    private String ShopNameEN;

	    private String DescriptionEn;

	    private String AddedOn;

	    private String EndDate;

	    private String EffectiveEndDate;

	    private String PromotionAmount;

	    private String PromotionNameEn;

	    private String Longitude;

	    private String DeletedOn;

	    private String PromotionCode;

	    private String IsPromotionPaid;

	    private String Banner;

	    private String Icon;

	    public String getPromotionId ()
	    {
	        return PromotionId;
	    }

	    public void setPromotionId (String PromotionId)
	    {
	        this.PromotionId = PromotionId;
	    }

	    public String getIsEnable ()
	    {
	        return isEnable;
	    }

	    public void setIsEnable (String isEnable)
	    {
	        this.isEnable = isEnable;
	    }

	    public String getPhone ()
	    {
	        return Phone;
	    }

	    public void setPhone (String Phone)
	    {
	        this.Phone = Phone;
	    }

	    public String getEffectiveStartDate ()
	    {
	        return EffectiveStartDate;
	    }

	    public void setEffectiveStartDate (String EffectiveStartDate)
	    {
	        this.EffectiveStartDate = EffectiveStartDate;
	    }

	    public String getUpdatedOn ()
	    {
	        return UpdatedOn;
	    }

	    public void setUpdatedOn (String UpdatedOn)
	    {
	        this.UpdatedOn = UpdatedOn;
	    }

	    public int getTotalRecords ()
	    {
	        return TotalRecords;
	    }

	    public void setTotalRecords (int TotalRecords)
	    {
	        this.TotalRecords = TotalRecords;
	    }

	    public String getQuota ()
	    {
	        return Quota;
	    }

	    public void setQuota (String Quota)
	    {
	        this.Quota = Quota;
	    }

	    public String getStartDate ()
	    {
	        return StartDate;
	    }

	    public void setStartDate (String StartDate)
	    {
	        this.StartDate = StartDate;
	    }

	    public String getFk_ShopId ()
	    {
	        return Fk_ShopId;
	    }

	    public void setFk_ShopId (String Fk_ShopId)
	    {
	        this.Fk_ShopId = Fk_ShopId;
	    }

	    public String getPromotionStatus ()
	    {
	        return PromotionStatus;
	    }

	    public void setPromotionStatus (String PromotionStatus)
	    {
	        this.PromotionStatus = PromotionStatus;
	    }

	    public String getLatitude ()
	    {
	        return Latitude;
	    }

	    public void setLatitude (String Latitude)
	    {
	        this.Latitude = Latitude;
	    }

	    public String getPhoto4 ()
	    {
	        return Photo4;
	    }

	    public void setPhoto4 (String Photo4)
	    {
	        this.Photo4 = Photo4;
	    }

	    public String getFk_UserId ()
	    {
	        return Fk_UserId;
	    }

	    public void setFk_UserId (String Fk_UserId)
	    {
	        this.Fk_UserId = Fk_UserId;
	    }

	    public String getPhoto2 ()
	    {
	        return Photo2;
	    }

	    public void setPhoto2 (String Photo2)
	    {
	        this.Photo2 = Photo2;
	    }

	    public String getPromotionName ()
	    {
	        return PromotionName;
	    }

	    public void setPromotionName (String PromotionName)
	    {
	        this.PromotionName = PromotionName;
	    }

	    public String getPhoto3 ()
	    {
	        return Photo3;
	    }

	    public void setPhoto3 (String Photo3)
	    {
	        this.Photo3 = Photo3;
	    }

	    public String getDescription ()
	    {
	        return Description;
	    }

	    public void setDescription (String Description)
	    {
	        this.Description = Description;
	    }

	    public String getPhoto1 ()
	    {
	        return Photo1;
	    }

	    public void setPhoto1 (String Photo1)
	    {
	        this.Photo1 = Photo1;
	    }

	    public String getShopName ()
	    {
	        return ShopName;
	    }

	    public void setShopName (String ShopName)
	    {
	        this.ShopName = ShopName;
	    }

	    public String getShopNameEN ()
	    {
	        return ShopNameEN;
	    }

	    public void setShopNameEN (String ShopNameEN)
	    {
	        this.ShopNameEN = ShopNameEN;
	    }

	    public String getDescriptionEn ()
	    {
	        return DescriptionEn;
	    }

	    public void setDescriptionEn (String DescriptionEn)
	    {
	        this.DescriptionEn = DescriptionEn;
	    }

	    public String getAddedOn ()
	    {
	        return AddedOn;
	    }

	    public void setAddedOn (String AddedOn)
	    {
	        this.AddedOn = AddedOn;
	    }

	    public String getEndDate ()
	    {
	        return EndDate;
	    }

	    public void setEndDate (String EndDate)
	    {
	        this.EndDate = EndDate;
	    }

	    public String getEffectiveEndDate ()
	    {
	        return EffectiveEndDate;
	    }

	    public void setEffectiveEndDate (String EffectiveEndDate)
	    {
	        this.EffectiveEndDate = EffectiveEndDate;
	    }

	    public String getPromotionAmount ()
	    {
	        return PromotionAmount;
	    }

	    public void setPromotionAmount (String PromotionAmount)
	    {
	        this.PromotionAmount = PromotionAmount;
	    }

	    public String getPromotionNameEn ()
	    {
	        return PromotionNameEn;
	    }

	    public void setPromotionNameEn (String PromotionNameEn)
	    {
	        this.PromotionNameEn = PromotionNameEn;
	    }

	    public String getLongitude ()
	    {
	        return Longitude;
	    }

	    public void setLongitude (String Longitude)
	    {
	        this.Longitude = Longitude;
	    }

	    public String getDeletedOn ()
	    {
	        return DeletedOn;
	    }

	public void setDeletedOn(String DeletedOn)
	    {
	        this.DeletedOn = DeletedOn;
	    }

	    public String getPromotionCode ()
	    {
	        return PromotionCode;
	    }

	    public void setPromotionCode (String PromotionCode)
	    {
	        this.PromotionCode = PromotionCode;
	    }

	    public String getIsPromotionPaid ()
	    {
	        return IsPromotionPaid;
	    }

	    public void setIsPromotionPaid (String IsPromotionPaid)
	    {
	        this.IsPromotionPaid = IsPromotionPaid;
	    }

	    public String getBanner ()
	    {
	        return Banner;
	    }

	    public void setBanner (String Banner)
	    {
	        this.Banner = Banner;
	    }

	    public String getIcon ()
	    {
	        return Icon;
	    }

	    public void setIcon (String Icon)
	    {
	        this.Icon = Icon;
	    }
}
