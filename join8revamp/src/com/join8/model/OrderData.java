package com.join8.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.content.Context;

import com.join8.R;
import com.join8.utils.Utils;

public class OrderData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String OrderId = "";
	private String OrderName = "";
	private String FirstName = "";
	private String LastName = "";
	private int OrderStatus = -1;
	private int CompanyId = 0;
	private String Currency = "";
	private String OrderDatetime = "";
	private double TotalAmount = 0;
	private String UserId = "";
	private String MobileNumber = "";
	private String UserProfile = "";
	private boolean IsOrderShared = false;
	private boolean IsTemplate = false;
	private String CompanyName = "";
	private String CompanyNameEN = "";

	private String OrderDeadline = "";
	private String Remarks = "";
	private String LastUpdatedTime = "";
	private String ItemName = "";
	private String ItemName_EN = "";
	private String listEmails = "";
	private boolean IsOrderDelivered;
	private boolean IsOrderPickUpByUser;
	private boolean IsOrderCancelled;
	private int HandeledByUserId = -1;
	private int LastInsertedValue;
	private int MenuItemId = -1;
	private int Quantity = -1;
	private double Price = 0;
	private double Amount = 0;
	private String Photo = "";
	private String RestaurnatPhoto = "";
	private String DeliverAddress = "";
	private int OrderPayOption = 0;
	private int AutoPrintOrder = 0;
	private int AutoPrintOrderCopies = 0;
	private int AutoPrintTableCopies = 0;
	private int AutoPrintTableOrder = 0;
	private int TakeOutPayOptionOrder = 0;
	private int DeliveryOption = 0;
	private double DeliveryFee = 0;
	private ArrayList<MenuItemData> MenuItem = new ArrayList<MenuItemData>();
	private boolean HasFoodOption = false;
	private int IsOrderPaid = 0;

	public String getOrderDeadline() {
		return OrderDeadline;
	}

	public void setOrderDeadline(String orderDeadline) {
		OrderDeadline = orderDeadline;
	}

	public Date getOrderDeadlineDate() {

		try {
			if (!Utils.isEmpty(OrderDeadline)) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.getDefault());
				return format.parse(OrderDeadline);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}

	public String getLastUpdatedTime() {
		return LastUpdatedTime;
	}

	public void setLastUpdatedTime(String lastUpdatedTime) {
		LastUpdatedTime = lastUpdatedTime;
	}

	public String getItemName() {
		return ItemName;
	}

	public void setItemName(String itemName) {
		ItemName = itemName;
	}

	public String getItemName_EN() {
		return ItemName_EN;
	}

	public void setItemName_EN(String itemName_EN) {
		ItemName_EN = itemName_EN;
	}

	public String getListEmails() {
		return listEmails;
	}

	public void setListEmails(String listEmails) {
		this.listEmails = listEmails;
	}

	public boolean isIsOrderDelivered() {
		return IsOrderDelivered;
	}

	public void setIsOrderDelivered(boolean isOrderDelivered) {
		IsOrderDelivered = isOrderDelivered;
	}

	public boolean isIsOrderPickUpByUser() {
		return IsOrderPickUpByUser;
	}

	public void setIsOrderPickUpByUser(boolean isOrderPickUpByUser) {
		IsOrderPickUpByUser = isOrderPickUpByUser;
	}

	public boolean isIsOrderCancelled() {
		return IsOrderCancelled;
	}

	public void setIsOrderCancelled(boolean isOrderCancelled) {
		IsOrderCancelled = isOrderCancelled;
	}

	public int getHandeledByUserId() {
		return HandeledByUserId;
	}

	public void setHandeledByUserId(int handeledByUserId) {
		HandeledByUserId = handeledByUserId;
	}

	public int getLastInsertedValue() {
		return LastInsertedValue;
	}

	public void setLastInsertedValue(int lastInsertedValue) {
		LastInsertedValue = lastInsertedValue;
	}

	public int getMenuItemId() {
		return MenuItemId;
	}

	public void setMenuItemId(int menuItemId) {
		MenuItemId = menuItemId;
	}

	public int getQuantity() {
		return Quantity;
	}

	public void setQuantity(int quantity) {
		Quantity = quantity;
	}

	public double getAmount() {
		return Amount;
	}

	public void setAmount(double amount) {
		Amount = amount;
	}

	public double getPrice() {
		return Price;
	}

	public void setPrice(double price) {
		Price = price;
	}

	public String getOrderId() {
		return OrderId;
	}

	public void setOrderId(String orderId) {
		OrderId = orderId;
	}

	public String getOrderName() {
		return OrderName;
	}

	public void setOrderName(String orderName) {
		OrderName = orderName;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public int getOrderStatus() {
		return OrderStatus;
	}

	public void setOrderStatus(int orderStatus) {
		OrderStatus = orderStatus;
	}

	public int getCompanyId() {
		return CompanyId;
	}

	public void setCompanyId(int companyId) {
		CompanyId = companyId;
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {
		Currency = currency;
	}

	public void setOrderDatetime(String orderDatetime) {
		OrderDatetime = orderDatetime;
	}

	public double getTotalAmount() {
		return TotalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		TotalAmount = totalAmount;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}

	public String getUserProfile() {
		return UserProfile;
	}

	public void setUserProfile(String userProfile) {
		UserProfile = userProfile;
	}

	public boolean isIsOrderShared() {
		return IsOrderShared;
	}

	public void setIsOrderShared(boolean isOrderShared) {
		IsOrderShared = isOrderShared;
	}

	public boolean isIsTemplate() {
		return IsTemplate;
	}

	public void setIsTemplate(boolean isTemplate) {
		IsTemplate = isTemplate;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	public String getCompanyNameEN() {
		return CompanyNameEN;
	}

	public void setCompanyNameEN(String companyNameEN) {
		CompanyNameEN = companyNameEN;
	}

	public String getPhoto() {
		return Photo;
	}

	public void setPhoto(String photo) {
		Photo = photo;
	}	
	
	public String getRestaurnatPhoto() {
		return RestaurnatPhoto;
	}

	public void setRestaurnatPhoto(String restaurnatPhoto) {
		RestaurnatPhoto = restaurnatPhoto;
	}

	public String getDeliverAddress() {
		return DeliverAddress;
	}

	public void setDeliverAddress(String deliverAddress) {
		DeliverAddress = deliverAddress;
	}

	public int getOrderPayOption() {
		return OrderPayOption;
	}

	public void setOrderPayOption(int orderPayOption) {
		OrderPayOption = orderPayOption;
	}

	public int getAutoPrintOrder() {
		return AutoPrintOrder;
	}

	public void setAutoPrintOrder(int autoPrintOrder) {
		AutoPrintOrder = autoPrintOrder;
	}

	public int getAutoPrintOrderCopies() {
		return AutoPrintOrderCopies;
	}

	public void setAutoPrintOrderCopies(int autoPrintOrderCopies) {
		AutoPrintOrderCopies = autoPrintOrderCopies;
	}

	public int getAutoPrintTableCopies() {
		return AutoPrintTableCopies;
	}

	public void setAutoPrintTableCopies(int autoPrintTableCopies) {
		AutoPrintTableCopies = autoPrintTableCopies;
	}

	public int getAutoPrintTableOrder() {
		return AutoPrintTableOrder;
	}

	public void setAutoPrintTableOrder(int autoPrintTableOrder) {
		AutoPrintTableOrder = autoPrintTableOrder;
	}

	public int getTakeOutPayOptionOrder() {
		return TakeOutPayOptionOrder;
	}

	public void setTakeOutPayOptionOrder(int takeOutPayOptionOrder) {
		TakeOutPayOptionOrder = takeOutPayOptionOrder;
	}

	public int getDeliveryOption() {
		return DeliveryOption;
	}

	public void setDeliveryOption(int deliveryOption) {
		DeliveryOption = deliveryOption;
	}

	public double getDeliveryFee() {
		return DeliveryFee;
	}

	public void setDeliveryFee(double deliveryFee) {
		DeliveryFee = deliveryFee;
	}

	public ArrayList<MenuItemData> getMenuItem() {
		return MenuItem;
	}

	public void setMenuItem(ArrayList<MenuItemData> menuItem) {
		MenuItem = menuItem;
	}

	public String getOrderDatetime() {
		return OrderDatetime;
	}

	public boolean isHasFoodOption() {
		return HasFoodOption;
	}

	public void setHasFoodOption(boolean hasFoodOption) {
		HasFoodOption = hasFoodOption;
	}

	public boolean isOrderPaid() {
		return IsOrderPaid == 0 ? false : true;
	}

	public void setIsOrderPaid(int isOrderPaid) {
		IsOrderPaid = isOrderPaid;
	}

	public static String getOrderStatusString(Context mContext, int orderStatus) {

		String status = "";

		switch (orderStatus) {

		case 10:
			status = mContext.getString(R.string.pending);
			break;
		case 20:
			status = mContext.getString(R.string.confirmed);
			break;
		case 30:
			status = mContext.getString(R.string.cancelled);
			break;
		case 40:
			status = mContext.getString(R.string.shared);
			break;
		case 50:
			status = mContext.getString(R.string.rejected);
			break;
		case 60:
			status = mContext.getString(R.string.processing);
			break;
		case 70:
			status = mContext.getString(R.string.pickuped);
			break;
		case 80:
			status = mContext.getString(R.string.delivered);
			break;
		case 90:
			status = mContext.getString(R.string.draft);
			break;
		case 100:
			status = mContext.getString(R.string.template);
			break;
		}
		return status;
	}
}
