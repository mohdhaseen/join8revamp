package com.join8.model;

public class ResponseUserLogin {
	private String LoginOption;

	private String Fk_CityCode;

	private String ActivationCode;

	private String Updated_On;

	private String Area;

	private String Password;

	private String UserStatus;

	private String Deleted_On;

	private String AddressEN;

	private String UserName;

	private String ActivationCodeExpire_On;

	private String UserId;

	private String LastName;

	private String Photo;

	private String LastLogin_On;

	private String MobilePhoneNumber;

	private String IsStaff;

	private String Email;

	private String Tag;

	private String PreferLanguage;

	private String Address;

	private TokenModel TokenModel;

	private String Added_On;

	private String FirstName;

	public String getLoginOption() {
		return LoginOption;
	}

	public void setLoginOption(String LoginOption) {
		this.LoginOption = LoginOption;
	}

	public String getFk_CityCode() {
		return Fk_CityCode;
	}

	public void setFk_CityCode(String Fk_CityCode) {
		this.Fk_CityCode = Fk_CityCode;
	}

	public String getActivationCode() {
		return ActivationCode;
	}

	public void setActivationCode(String ActivationCode) {
		this.ActivationCode = ActivationCode;
	}

	public String getUpdated_On() {
		return Updated_On;
	}

	public void setUpdated_On(String Updated_On) {
		this.Updated_On = Updated_On;
	}

	public String getArea() {
		return Area;
	}

	public void setArea(String Area) {
		this.Area = Area;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String Password) {
		this.Password = Password;
	}

	public String getUserStatus() {
		return UserStatus;
	}

	public void setUserStatus(String UserStatus) {
		this.UserStatus = UserStatus;
	}

	public String getDeleted_On() {
		return Deleted_On;
	}

	public void setDeleted_On(String Deleted_On) {
		this.Deleted_On = Deleted_On;
	}

	public String getAddressEN() {
		return AddressEN;
	}

	public void setAddressEN(String AddressEN) {
		this.AddressEN = AddressEN;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String UserName) {
		this.UserName = UserName;
	}

	public String getActivationCodeExpire_On() {
		return ActivationCodeExpire_On;
	}

	public void setActivationCodeExpire_On(String ActivationCodeExpire_On) {
		this.ActivationCodeExpire_On = ActivationCodeExpire_On;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String UserId) {
		this.UserId = UserId;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String LastName) {
		this.LastName = LastName;
	}

	public String getPhoto() {
		return Photo;
	}

	public void setPhoto(String Photo) {
		this.Photo = Photo;
	}

	public String getLastLogin_On() {
		return LastLogin_On;
	}

	public void setLastLogin_On(String LastLogin_On) {
		this.LastLogin_On = LastLogin_On;
	}

	public String getMobilePhoneNumber() {
		return MobilePhoneNumber;
	}

	public void setMobilePhoneNumber(String MobilePhoneNumber) {
		this.MobilePhoneNumber = MobilePhoneNumber;
	}

	public String getIsStaff() {
		return IsStaff;
	}

	public void setIsStaff(String IsStaff) {
		this.IsStaff = IsStaff;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String Email) {
		this.Email = Email;
	}

	public String getTag() {
		return Tag;
	}

	public void setTag(String Tag) {
		this.Tag = Tag;
	}

	public String getPreferLanguage() {
		return PreferLanguage;
	}

	public void setPreferLanguage(String PreferLanguage) {
		this.PreferLanguage = PreferLanguage;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String Address) {
		this.Address = Address;
	}

	public TokenModel getTokenModel() {
		return TokenModel;
	}

	public void setTokenModel(TokenModel TokenModel) {
		this.TokenModel = TokenModel;
	}

	public String getAdded_On() {
		return Added_On;
	}

	public void setAdded_On(String Added_On) {
		this.Added_On = Added_On;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String FirstName) {
		this.FirstName = FirstName;
	}

}
