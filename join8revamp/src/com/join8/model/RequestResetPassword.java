package com.join8.model;

public class RequestResetPassword {
	private String ActivationCode;

	private String Password;

	private String UserName;

	public String getActivationCode() {
		return ActivationCode;
	}

	public void setActivationCode(String ActivationCode) {
		this.ActivationCode = ActivationCode;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String Password) {
		this.Password = Password;
	}

	public String getUserName() {
		return UserName;
	}

	public void setUserName(String UserName) {
		this.UserName = UserName;
	}

}
