package com.join8.model;

import java.util.ArrayList;

public class ResponsePromotions
{
    private ArrayList<Promotion> PromotionList;

    private Banner[] BannerList;

    public ArrayList<Promotion> getPromotionList ()
    {
        return PromotionList;
    }

    public void setPromotionList (ArrayList<Promotion> PromotionList)
    {
        this.PromotionList = PromotionList;
    }

    public Banner[] getBannerList ()
    {
        return BannerList;
    }

    public void setBannerList (Banner[] BannerList)
    {
        this.BannerList = BannerList;
    }

   
}
			
			
