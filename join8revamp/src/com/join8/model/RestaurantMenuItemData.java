package com.join8.model;

import java.io.Serializable;
import java.util.ArrayList;

import com.join8.utils.Utils;

public class RestaurantMenuItemData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int MenuItemId = -1;
	private int MenuId = -1;
	private String ItemName = "";
	private String ItemName_EN = "";
	private String Description = "";
	private String Description_EN = "";
	private String Currency = "";
	private double Price = 0;
	private String Photo = "";
	private boolean FoodRecommended = false;
	private boolean FoodSpicy = false;
	private boolean TaxRequired = false;
	private boolean FoodVegtarian = false;
	private boolean FoodTakeOut = false;
	private String Added_On = "";
	private String Updated_On = "";
	private String Deleted_On = "";
	private boolean IsEnable = false;
	private String RecordUpdatedBy = "";
	private int CompanyId = -1;
	private boolean IsDeleted = false;
	private boolean IsOrderable = false;
	private String UserId = "";
	private int DishType = -1;
	private String POSMenuId = "";
	private int CuisineType = -1;
	private String CompanyName = "";
	private String CompanyNameEN = "";
	private String CompanyPhoto = "";
	private String address = "";
	private String phone = "";

	private int quantity = 1;
	private double totalPrice = 0;
	// new
	private ArrayList<FoodOptionItemData> FoodOptions = new ArrayList<FoodOptionItemData>();
	private boolean HasFoodOption = false;

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public int getMenuItemId() {
		return MenuItemId;
	}

	public void setMenuItemId(int menuItemId) {
		MenuItemId = menuItemId;
	}

	public int getMenuId() {
		return MenuId;
	}

	public void setMenuId(int menuId) {
		MenuId = menuId;
	}

	public String getItemName() {
		return ItemName;
	}

	public void setItemName(String itemName) {

		if (!Utils.isEmpty(itemName)) {
			ItemName = itemName.trim();
		}
	}

	public String getItemName_EN() {
		return ItemName_EN;
	}

	public void setItemName_EN(String itemName_EN) {

		if (!Utils.isEmpty(itemName_EN)) {
			ItemName_EN = itemName_EN.trim();
		}
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {

		if (!Utils.isEmpty(description)) {
			Description = description.trim();
		}
	}

	public String getDescription_EN() {
		return Description_EN;
	}

	public void setDescription_EN(String description_EN) {

		if (!Utils.isEmpty(description_EN)) {
			Description_EN = description_EN.trim();
		}
	}

	public String getCurrency() {
		return Currency;
	}

	public void setCurrency(String currency) {

		if (currency != null && !Utils.isEmpty(currency)) {
			Currency = currency.trim();
		}
	}

	public double getPrice() {
		return Price;
	}

	public void setPrice(double price) {
		Price = price;
	}

	public String getPhoto() {
		return Photo;
	}

	public void setPhoto(String photo) {

		if (!Utils.isEmpty(photo)) {
			Photo = photo.trim();
		}
	}

	public boolean isFoodRecommended() {
		return FoodRecommended;
	}

	public void setFoodRecommended(boolean foodRecommended) {
		FoodRecommended = foodRecommended;
	}

	public boolean isFoodSpicy() {
		return FoodSpicy;
	}

	public void setFoodSpicy(boolean foodSpicy) {
		FoodSpicy = foodSpicy;
	}

	public boolean isTaxRequired() {
		return TaxRequired;
	}

	public void setTaxRequired(boolean taxRequired) {

		TaxRequired = taxRequired;
	}

	public boolean isFoodVegtarian() {
		return FoodVegtarian;
	}

	public void setFoodVegtarian(boolean foodVegtarian) {
		FoodVegtarian = foodVegtarian;
	}

	public boolean isFoodTakeOut() {
		return FoodTakeOut;
	}

	public void setFoodTakeOut(boolean foodTakeOut) {
		FoodTakeOut = foodTakeOut;
	}

	public String getAdded_On() {
		return Added_On;
	}

	public void setAdded_On(String added_On) {

		if (!Utils.isEmpty(added_On)) {
			Added_On = added_On.trim();
		}

	}

	public String getUpdated_On() {
		return Updated_On;
	}

	public void setUpdated_On(String updated_On) {

		if (!Utils.isEmpty(updated_On)) {
			Updated_On = updated_On.trim();
		}

	}

	public String getDeleted_On() {
		return Deleted_On;
	}

	public void setDeleted_On(String deleted_On) {

		if (!Utils.isEmpty(deleted_On)) {
			Deleted_On = deleted_On.trim();
		}

	}

	public boolean isIsEnable() {
		return IsEnable;
	}

	public void setIsEnable(boolean isEnable) {
		IsEnable = isEnable;
	}

	public String getRecordUpdatedBy() {
		return RecordUpdatedBy;
	}

	public void setRecordUpdatedBy(String recordUpdatedBy) {

		if (!Utils.isEmpty(recordUpdatedBy)) {
			RecordUpdatedBy = recordUpdatedBy.trim();
		}

	}

	public int getCompanyId() {
		return CompanyId;
	}

	public void setCompanyId(int companyId) {
		CompanyId = companyId;
	}

	public boolean isIsDeleted() {
		return IsDeleted;
	}

	public void setIsDeleted(boolean isDeleted) {
		IsDeleted = isDeleted;
	}

	public boolean isOrderable() {
		return IsOrderable;
	}

	public void setIsOrderable(boolean isOrderable) {
		IsOrderable = isOrderable;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {

		if (!Utils.isEmpty(userId)) {
			UserId = userId.trim();
		}
	}

	public int getDishType() {
		return DishType;
	}

	public void setDishType(int dishType) {
		DishType = dishType;
	}

	public String getPOSMenuId() {
		return POSMenuId;
	}

	public void setPOSMenuId(String pOSMenuId) {

		if (!Utils.isEmpty(pOSMenuId)) {
			POSMenuId = pOSMenuId.trim();
		}
	}

	public int getCuisineType() {
		return CuisineType;
	}

	public void setCuisineType(int cuisineType) {
		CuisineType = cuisineType;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {

		if (!Utils.isEmpty(companyName)) {
			CompanyName = companyName.trim();
		}
	}

	public String getCompanyNameEN() {
		return CompanyNameEN;
	}

	public void setCompanyNameEN(String companyNameEN) {

		if (!Utils.isEmpty(companyNameEN)) {
			CompanyNameEN = companyNameEN.trim();
		}
	}

	public String getCompanyPhoto() {
		return CompanyPhoto;
	}

	public void setCompanyPhoto(String companyPhoto) {
		CompanyPhoto = companyPhoto.trim();
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public ArrayList<FoodOptionItemData> getFoodOptions() {
		return FoodOptions;
	}

	public void setFoodOptions(ArrayList<FoodOptionItemData> foodOptions) {
		FoodOptions = foodOptions;
	}

	public boolean isHasFoodOption() {
		return HasFoodOption;
	}

	public void setHasFoodOption(boolean hasFoodOption) {
		HasFoodOption = hasFoodOption;
	}

	public static RestaurantMenuItemData copy(RestaurantMenuItemData data) {

		RestaurantMenuItemData obj = new RestaurantMenuItemData();

		obj.setMenuItemId(data.getMenuItemId());
		obj.setMenuId(data.getMenuId());
		obj.setItemName(data.getItemName());
		obj.setItemName_EN(data.getItemName_EN());
		obj.setDescription(data.getDescription());
		obj.setDescription_EN(data.getDescription_EN());
		obj.setCurrency(data.getCurrency());
		obj.setPrice(data.getPrice());
		obj.setPhoto(data.getPhoto());
		obj.setFoodRecommended(data.isFoodRecommended());
		obj.setFoodSpicy(data.isFoodSpicy());
		obj.setTaxRequired(data.isTaxRequired());
		obj.setFoodVegtarian(data.isFoodVegtarian());
		obj.setFoodTakeOut(data.isFoodTakeOut());
		obj.setAdded_On(data.getAdded_On());
		obj.setUpdated_On(data.getUpdated_On());
		obj.setDeleted_On(data.getDeleted_On());
		obj.setIsEnable(data.isIsEnable());
		obj.setRecordUpdatedBy(data.getRecordUpdatedBy());
		obj.setCompanyId(data.getCompanyId());
		obj.setIsDeleted(data.isIsDeleted());
		obj.setIsOrderable(data.isOrderable());
		obj.setUserId(data.getUserId());
		obj.setDishType(data.getDishType());
		obj.setPOSMenuId(data.getPOSMenuId());
		obj.setCuisineType(data.getCuisineType());
		obj.setCompanyName(data.getCompanyName());
		obj.setCompanyNameEN(data.getCompanyNameEN());
		obj.setCompanyPhoto(data.getCompanyPhoto());
		obj.setAddress(data.getAddress());
		obj.setPhone(data.getPhone());
		obj.setQuantity(data.getQuantity());
		obj.setTotalPrice(data.getTotalPrice());
		obj.setFoodOptions(data.getFoodOptions());
		obj.setHasFoodOption(data.isHasFoodOption());

		return obj;
	}

}
