package com.join8.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import com.join8.utils.Utils;

public class BookingData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String TableBookingId = "";
	private int Seats = 0;
	private boolean IsShared = false;
	private String DateToBook = "";
	private int BookingStatus = -1;
	private String CompanyName = "";
	private String CompanyNameEN = "";
	private String MobileNumber = "";
	private String FirstName = "";
	private String LastName = "";
	private String UserId = "";
	private String UserProfile = "";
	private boolean IsCancelled = false;
	private String HomePhone = "";
	private String BookingConfirmationCode = "";
	private String HandeledByUserId = "";
	private String LastUpdateTime = "";
	private String Remarks = "";
	private String DressCodeId = "";
	private String CompanyId = "";
	private String Email = "";
	private int BookingType = 0;
	private String BookingName = "";
	private String BookingNameEn = "";
	private double Deposit = 0;
	private String Terms = "";
	private String TermsEn = "";
	private ArrayList<String> BookingImageLink = new ArrayList<String>();
	private String Photo = "";

	public String getTableBookingId() {
		return TableBookingId;
	}

	public void setTableBookingId(String tableBookingId) {
		TableBookingId = tableBookingId;
	}

	public int getSeats() {
		return Seats;
	}

	public void setSeats(int seats) {
		Seats = seats;
	}

	public boolean isIsShared() {
		return IsShared;
	}

	public void setIsShared(boolean isShared) {
		IsShared = isShared;
	}

	public Date getDateToBook() {

		try {
			if (!Utils.isEmpty(DateToBook)) {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.getDefault());
				return format.parse(DateToBook);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setDateToBook(String dateToBook) {
		DateToBook = dateToBook;
	}

	public int getBookingStatus() {
		return BookingStatus;
	}

	public void setBookingStatus(int bookingStatus) {
		BookingStatus = bookingStatus;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	public String getCompanyNameEN() {
		return CompanyNameEN;
	}

	public void setCompanyNameEN(String companyNameEN) {
		CompanyNameEN = companyNameEN;
	}

	public String getMobileNumber() {
		return MobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		LastName = lastName;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	public String getUserProfile() {
		return UserProfile;
	}

	public void setUserProfile(String userProfile) {
		UserProfile = userProfile;
	}

	public boolean isIsCancelled() {
		return IsCancelled;
	}

	public void setIsCancelled(boolean isCancelled) {
		IsCancelled = isCancelled;
	}

	public String getHomePhone() {
		return HomePhone;
	}

	public void setHomePhone(String homePhone) {
		HomePhone = homePhone;
	}

	public String getBookingConfirmationCode() {
		return BookingConfirmationCode;
	}

	public void setBookingConfirmationCode(String bookingConfirmationCode) {
		BookingConfirmationCode = bookingConfirmationCode;
	}

	public String getHandeledByUserId() {
		return HandeledByUserId;
	}

	public void setHandeledByUserId(String handeledByUserId) {
		HandeledByUserId = handeledByUserId;
	}

	public String getLastUpdateTime() {
		return LastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		LastUpdateTime = lastUpdateTime;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}

	public String getDressCodeId() {
		return DressCodeId;
	}

	public void setDressCodeId(String dressCodeId) {
		DressCodeId = dressCodeId;
	}

	public String getCompanyId() {
		return CompanyId;
	}

	public void setCompanyId(String companyId) {
		CompanyId = companyId;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public int getBookingType() {
		return BookingType;
	}

	public void setBookingType(int bookingType) {
		BookingType = bookingType;
	}

	public String getBookingName() {
		return BookingName;
	}

	public void setBookingName(String bookingName) {
		BookingName = bookingName;
	}

	public String getBookingNameEn() {
		return BookingNameEn;
	}

	public void setBookingNameEn(String bookingNameEn) {
		BookingNameEn = bookingNameEn;
	}

	public double getDeposit() {
		return Deposit;
	}

	public void setDeposit(double deposit) {
		Deposit = deposit;
	}

	public String getTerms() {
		return Terms;
	}

	public void setTerms(String terms) {
		Terms = terms;
	}

	public String getTermsEn() {
		return TermsEn;
	}

	public void setTermsEn(String termsEn) {
		TermsEn = termsEn;
	}

	public ArrayList<String> getBookingImageLink() {
		return BookingImageLink;
	}

	public void setBookingImageLink(ArrayList<String> bookingImageLink) {
		BookingImageLink = bookingImageLink;
	}

	public String getPhoto() {
		return Photo;
	}

	public void setPhoto(String photo) {
		Photo = photo;
	}

}
