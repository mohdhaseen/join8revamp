package com.join8.model;

public class StaffLogin {
	private String UserName;
	private String Password;
	private String Token;
	private String DeviceId;
	private String Fk_CityCode;
	private String DeviceType;
	private String LoginWith;

	public StaffLogin(String userName, String password, String token, String deviceId, String fk_CityCode, String deviceType, String loginWith) {
		UserName = userName;
		Password = password;
		Token = token;
		DeviceId = deviceId;
		Fk_CityCode = fk_CityCode;
		DeviceType = deviceType;
		LoginWith = loginWith;
	}

}
