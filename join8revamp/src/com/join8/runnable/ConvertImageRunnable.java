package com.join8.runnable;

import android.graphics.Bitmap;

import com.join8.controller.ImageController;
import com.join8.model.ShopImageData;
import com.join8.utils.ImageUtil;

/**
 * Created by craterzone on 29/1/16.
 */
public class ConvertImageRunnable implements Runnable {
	private Bitmap bitmap;

	public ConvertImageRunnable(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

	@Override
	public void run() {
		ImageController.getInstance().getImages().add(new ShopImageData(null, null, null, ImageUtil.getBase64(bitmap), "image/jpeg"));
	}
}
