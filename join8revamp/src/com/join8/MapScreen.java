package com.join8;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapScreen extends ActionBarActivity implements OnClickListener{

	private String title;
	private double latitude, longitude;
	private GoogleMap mapView;
	private ImageView imgBack;
	private TextView txtActionTitle;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mapscreen);
		
		/*
		 * if (savedInstanceState != null) { title =
		 * savedInstanceState.getString("title"); latitude =
		 * savedInstanceState.getDouble("lat"); longitude =
		 * savedInstanceState.getDouble("lon");
		 * 
		 * } else { if (getArguments() != null) { title =
		 * getArguments().getString("title"); latitude =
		 * getArguments().getDouble("lat"); longitude =
		 * getArguments().getDouble("lon"); } }
		 */

		if (getIntent() != null && getIntent().getExtras() != null) {
			title = getIntent().getExtras().getString("title");
			latitude = getIntent().getExtras().getDouble("lat");
			longitude = getIntent().getExtras().getDouble("lon");
		}
		initActionBar();
		mapView = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.mapview)).getMap();
		LatLng latLng = new LatLng(latitude, longitude);
		Bitmap overlay = BitmapFactory.decodeResource(getResources(), R.drawable.marker);
		mapView.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(overlay)).anchor(0.5f, 1).snippet(""));
		CameraPosition cameraPosition = new CameraPosition.Builder()
		.target(latLng).zoom(18) // Sets the zoom
		 // Sets the orientation of the camera to east
		 // Sets the tilt of the camera to 30 degrees
		
		
		.build();
		mapView.animateCamera(CameraUpdateFactory
				.newCameraPosition(cameraPosition));

	}

	/*@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.mapscreen, container, false);
		mapView = ((SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.mapview)).getMap();
		return v;
	}*/

	/*@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (!getActivity().isFinishing()) {
			((HomeScreen) getActivity()).setCustomTitle(title, Typeface.NORMAL);
			((HomeScreen) getActivity()).changeActionItems(true);
		}
		LatLng latLng = new LatLng(latitude, longitude);
		Bitmap overlay = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.marker);
		mapView.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(overlay)).anchor(0.5f, 1).snippet(""));
	}*/
	
	private void initActionBar() {
		getSupportActionBar().setHomeButtonEnabled(false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		getSupportActionBar().setDisplayUseLogoEnabled(false);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		
		View mActionbarView = getLayoutInflater().inflate(R.layout.actionview,
				(ViewGroup) findViewById(R.id.rltMain), false);
		imgBack = (ImageButton) mActionbarView.findViewById(R.id.btn_back);
		imgBack.setOnClickListener(this);
		txtActionTitle = (TextView) mActionbarView
				.findViewById(R.id.txtTitle);
		txtActionTitle.setText(title);
		getSupportActionBar().setCustomView(mActionbarView);
	}

	@Override
	public void onClick(View v) {
		if (v == imgBack) {
			finish();
		}

	}

	public void setCustomTitle(String title) {
		txtActionTitle.setText(title);

	}

}
