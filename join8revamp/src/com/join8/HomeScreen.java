package com.join8;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.join8.fragments.AddShopFragment;
import com.join8.fragments.BookingListFragment;
import com.join8.fragments.FeedbackFragment;
import com.join8.fragments.FragmentAddEditAddress;
import com.join8.fragments.FragmentChangePassword;
import com.join8.fragments.FragmentForgotPassword;
import com.join8.fragments.FragmentSearch;
import com.join8.fragments.HomeFragment;
import com.join8.fragments.HomeFragmentNew;
import com.join8.fragments.LoginFragment;
import com.join8.fragments.MyFavoritesFragment;
import com.join8.fragments.OrderDetailFragment;
import com.join8.fragments.OrderListFragment;
import com.join8.fragments.PolicyFragment;
import com.join8.fragments.ProfileFragment;
import com.join8.fragments.PromotionFragment;
import com.join8.fragments.RegistrationFormFragment;
import com.join8.fragments.RestaurantDetailsFragment;
import com.join8.fragments.SelectCityFragment;
import com.join8.fragments.StaffFragmentContainer;
import com.join8.fragments.UserProfileFragment;
import com.join8.listeners.BannerLoadedListener;
import com.join8.listeners.Requestlistener;
import com.join8.model.City;
import com.join8.model.Registration;
import com.join8.model.ResponseCity;
import com.join8.model.TokenModel;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.request.Join8RequestBuilder;
import com.join8.slidingmenu.SlidingActivity;
import com.join8.slidingmenu.SlidingMenu.OnCloseListener;
import com.join8.slidingmenu.SlidingMenu.OnOpenListener;
import com.join8.utils.ConstantData;
import com.join8.utils.CryptoManager;
import com.join8.utils.DialogHelper;
import com.join8.utils.KeyboardUtils;
import com.join8.utils.TypefaceUtils;
import com.join8.utils.UrlUtil;
import com.join8.utils.Utils;
import com.microsoft.windowsazure.messaging.NotificationHub;
import com.microsoft.windowsazure.notifications.NotificationsManager;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

public class HomeScreen extends SlidingActivity implements OnClickListener, OnTouchListener, Requestlistener {

	public static String LANGUAGE = ConstantData.LANG_TRADITIONALCHINESE;
	public static final int REQUEST_CITY = 1;
	public static final int REQUEST_LANGUAGE = 2;
	public static int signout_request = -1;
	public final String TAG = HomeScreen.this.getClass().getSimpleName();

	public static final int FRAGMENT_CONTAINER = R.id.fragment_container;
	public static final String PREFS_LOGIN = "islogin";
	public static final String PARAM_TOKEN = "token";
	public static final String PARAM_TOKEN_EXPIRY_TIME = "token_expiry_time";
	public static final String PARAM_TOKEN_RECEIVED_TIME = "token_received_time";
	public static final String PARAMS_USERDATA = "userdata";

	public static final int MENU_HOME = 0;
	public static final int MENU_LOGIN = 1;
	public static final int MENU_REGISTRATION = 2;
	public static final int MENU_USERPROFILE = 3;
	public static final int MENU_CITY = 4;
	public static final int MENU_STAFF = 5;
	public static final int MENU_FEEDBACK = 6;
	public static final int MENU_POLICY = 7;
	public static final int MENU_LOGOUT = 8;
	public static final int MENU_LANGUAGE = 9;
	public static final int MENU_CHANGE_PASSWORD = 10;
	public static final int MENU_MY_FAVOURITE = 11;
	public static final int MENU_EDIT_MY_INFO = 12;
	public static final int MENU_MY_BOOKING = 13;
	public static final int MENU_MY_ORDER = 14;
	public static final int MENU_ADD_EDIT_ADDRESS = 15;
	public static final int MENU_ADD_SHOP = 16;
	public static final int MENU_FORGOT_PASSWORD = 17;

	public ImageView imgHome;
	private CryptoManager prefsManager;
	private RadioGroup radioHome;
	private TextView txtActionTitle;
	private ImageButton btnAdd;
	private NetworkManager networManager;
	private LinearLayout lnrUserInfo, lnrUser, lnrSubMenu, lnrHome, lnrLogin, lnrRegister, lnrForgotPassword, lnrAddShop, lnrCity, lnrLanaguge, lnrStaff, lnrFeedback, lnrLogout, leftMenu;
	private RelativeLayout rlNewActionBar, rlActionSearch;
	private TextView tvUser;
	private int login_request = -1;
	private RadioButton rFav, rBooking, rOrder, rSearch, rPromo;
	private int city_request = -1;
	private int signn_request = -1;
	private ArrayList<City> arrCities;
	private String access_token = "";
	private ImageView imgUser;
	private ImageLoader imageLoader = null;
	private DisplayImageOptions options = null;
	private boolean isTouched = false;
	private String tokenCloud;
	private NotificationHub hub = null;
	private int tokenRequest = -1;
	private int request_city_new = -1;
	public ArrayList<ResponseCity> arrCitiesNew;
	BannerLoadedListener mBannerLoadedListener;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.homelayout);
		imageLoader = ImageLoader.getInstance();
		DisplayMetrics dm = getResources().getDisplayMetrics();
		int roundSize = (int) (dm.density * getResources().getDimension(R.dimen.image_height));
		options = new DisplayImageOptions.Builder().displayer(new RoundedBitmapDisplayer(roundSize)).showStubImage(R.drawable.trans).showImageForEmptyUri(R.drawable.trans).showImageOnFail(R.drawable.trans).cacheInMemory(true).cacheOnDisc(true).build();

		networManager = NetworkManager.getInstance();
		networManager.addListener(this);
		prefsManager = CryptoManager.getInstance(this);
		LANGUAGE = prefsManager.getPrefs().getString("language", ConstantData.LANG_TRADITIONALCHINESE);
		initDrawerlayout();
		init();
		getSupportFragmentManager().addOnBackStackChangedListener(new OnBackStackChangedListener() {
			@Override
			public void onBackStackChanged() {

				try {
					String str = getActiveFragment();
					if (str != null && str.equals(PromotionFragment.class.getName())) {
						changeActionItems(false);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		// call only if token is not available
		/*
		 * if
		 * (TextUtils.isEmpty(CryptoManager.getInstance(HomeScreen.this).getPrefs
		 * ().getString(HomeScreen.PARAM_TOKEN, ""))) { getToken(); } else {
		 * access_token =
		 * prefsManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
		 * city_request =
		 * networManager.addRequest(Join8RequestBuilder.getCityList
		 * (access_token), RequestMethod.POST, this,
		 * Join8RequestBuilder.METHOD_GETCITY); }
		 */

		// call only if tokenCloud is not available

		if (TextUtils.isEmpty(CryptoManager.getInstance(HomeScreen.this).getPrefs().getString(ConstantData.TOKEN_CLOUD, ""))) {
			getTokenCloud();
		} else {

			request_city_new = networManager.addRequest(null, this, true, UrlUtil.getCityListUrl(), "", ConstantData.method_get);
		}
		openFragmentFromPush();

		try {

			NotificationsManager.handleNotifications(this, ConstantData.SENDER_ID, MyHandler.class);
			hub = new NotificationHub("hubnotification", ConstantData.HUB_NOTI_CONNECTION_STRING, this);

		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Error in Notification Hub", e.toString());
		}

	}
	private void getTokenCloud() {
		networManager.isProgressVisible(true);
		tokenRequest = networManager.addRequest(new HashMap<String, String>(), this, true, UrlUtil.getTokenUrlForCloud(), getTokenRequest(), ConstantData.method_post);
	}

	private String getTokenRequest() {
		return "{\"UserName\":\"join8PosAdmin\",\"Password\":\"j01Np05@dm1n\"}";
	}
	public String getActiveFragment() {

		if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
			return "";
		}
		return getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
	}

	private void openFragmentFromPush() {
		try {
			if (getIntent().getExtras() != null) {

				if (getIntent().getStringExtra("redirectTo").equalsIgnoreCase("ConfirmBookingDetail")) {

					clearBackStackEntry();

					Bundle bundle = new Bundle();
					bundle.putString("redirectTo", "BookingDetail");
					bundle.putString("reference", getIntent().getStringExtra("reference"));

					BookingListFragment bookingListFragment = new BookingListFragment();
					bookingListFragment.setArguments(bundle);
					getSupportFragmentManager().beginTransaction().addToBackStack(BookingListFragment.class.getName()).replace(HomeScreen.FRAGMENT_CONTAINER, bookingListFragment).commit();

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							rBooking.setChecked(true);
						}
					}, 100);

				} else if (getIntent().getStringExtra("redirectTo").equalsIgnoreCase("ConfirmOrderDetail")) {

					clearBackStackEntry();

					Bundle bundle = new Bundle();
					bundle.putString("redirectTo", "OrderDetail");
					bundle.putString("reference", getIntent().getStringExtra("reference"));

					OrderListFragment fragment = new OrderListFragment();
					fragment.setArguments(bundle);
					getSupportFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, fragment).addToBackStack(OrderListFragment.class.getName()).commit();

					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							rOrder.setChecked(true);
						}
					}, 100);

				} else if (getIntent().getStringExtra("redirectTo").equalsIgnoreCase("StaffOrderDetail")) {

					Bundle bundle = new Bundle();
					bundle.putString("redirectTo", "StaffOrderDetail");
					bundle.putString("reference", getIntent().getStringExtra("reference"));

					StaffFragmentContainer mStaffFragmentContainer = new StaffFragmentContainer();
					mStaffFragmentContainer.setArguments(bundle);
					getSupportFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, mStaffFragmentContainer).addToBackStack(StaffFragmentContainer.class.getName()).commit();

				} else if (getIntent().getStringExtra("redirectTo").equalsIgnoreCase("StaffBookingDetail")) {

					Bundle bundle = new Bundle();
					bundle.putString("redirectTo", "StaffBookingDetail");
					bundle.putString("reference", getIntent().getStringExtra("reference"));

					StaffFragmentContainer mStaffFragmentContainer = new StaffFragmentContainer();
					mStaffFragmentContainer.setArguments(bundle);
					getSupportFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, mStaffFragmentContainer).addToBackStack(StaffFragmentContainer.class.getName()).commit();

				} else if (getIntent().getStringExtra("redirectTo").equalsIgnoreCase("PromotionDetail")) {

				}

				getIntent().putExtra("redirectTo", "");
				getIntent().putExtra("reference", "");
				getIntent().getExtras().clear();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void getToken() {
		networManager.isProgressVisible(true);
		login_request = networManager.addRequest(Join8RequestBuilder.getLoginRequest(ConstantData.USERNAME_FOR_TOKEN, ConstantData.PASSWORD_FOR_TOKEN), RequestMethod.POST, this, Join8RequestBuilder.METHOD_LOGIN);
	}

	private void init() {

		getSlidingMenu().setBehindOffsetRes(R.dimen.sliding_menu_offset);
		getSlidingMenu().setShadowWidthRes(R.dimen.sliding_menu_shadow_width);
		getSlidingMenu().setOnOpenListener(new OnOpenListener() {

			@Override
			public void onOpen() {
				Log.e(TAG, "Drawer Open");
				if (!imgHome.getTag().toString().equals("1"))
					imgHome.setImageDrawable(getResources().getDrawable(

					R.drawable.manuh));
				// if (getWindow().getCurrentFocus() != null) {
				// View view = getWindow().getCurrentFocus();
				// if (view instanceof EditText) {
				// InputMethodManager imm = (InputMethodManager)
				// getSystemService(Context.INPUT_METHOD_SERVICE);
				// imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
				// }
				// }
				if (prefsManager.getPrefs().getBoolean(PREFS_LOGIN, false)) {
					isLoggedIn(true);
				} else {
					isLoggedIn(false);
				}
			}
		});
		getSlidingMenu().setOnCloseListener(new OnCloseListener() {

			@Override
			public void onClose() {
				if (!imgHome.getTag().toString().equals("1"))
					imgHome.setImageDrawable(getResources().getDrawable(R.drawable.menu));
			}
		});

		// getSupportFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER,
		// new HomeFragment()).addToBackStack(null).commit();
		getSupportFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, new HomeFragmentNew()).addToBackStack(null).commit();
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				getSlidingMenu().toggle();
				// getSlidingMenu().toggle();
			}
		}, 20);

	}

	public void initDrawerlayout() {

		View mView = getLayoutInflater().inflate(R.layout.leftmenu, null);
		InitMenuControls(mView);
		setBehindContentView(mView);

		initActionBar();
	}

	private void InitMenuControls(View v) {
		lnrUserInfo = (LinearLayout) v.findViewById(R.id.lnrUserInfo);
		lnrUser = (LinearLayout) v.findViewById(R.id.lnrUser);
		lnrSubMenu = (LinearLayout) v.findViewById(R.id.sub_menu);
		setSubMenuListener(lnrSubMenu);
		lnrHome = (LinearLayout) v.findViewById(R.id.lnrHome);
		lnrForgotPassword = (LinearLayout) v.findViewById(R.id.lnrForgotPassword);
		lnrAddShop = (LinearLayout) v.findViewById(R.id.lnrAddShop);
		lnrLogin = (LinearLayout) v.findViewById(R.id.lnrLogin);
		lnrRegister = (LinearLayout) v.findViewById(R.id.lnrRegister);
		lnrCity = (LinearLayout) v.findViewById(R.id.lnrCity);
		lnrLanaguge = (LinearLayout) v.findViewById(R.id.lnrlanguage);
		lnrStaff = (LinearLayout) v.findViewById(R.id.lnrStaff);
		lnrFeedback = (LinearLayout) v.findViewById(R.id.lnrFeedback);
		lnrLogout = (LinearLayout) v.findViewById(R.id.lnrLogout);
		leftMenu = (LinearLayout) v.findViewById(R.id.leftMenu);
		tvUser = (TextView) v.findViewById(R.id.tvUser);
		imgUser = (ImageView) v.findViewById(R.id.imgUser);
		lnrForgotPassword.setOnClickListener(this);
		lnrAddShop.setOnClickListener(this);
		imgUser.setOnClickListener(this);
		leftMenu.setVisibility(View.GONE);
		lnrHome.setOnClickListener(this);
		lnrUser.setOnClickListener(this);
		lnrLogin.setOnClickListener(this);
		lnrRegister.setOnClickListener(this);
		lnrCity.setOnClickListener(this);
		lnrLanaguge.setOnClickListener(this);
		lnrStaff.setOnClickListener(this);
		lnrFeedback.setOnClickListener(this);
		// lnrPolicy.setOnClickListener(this);
		lnrLogout.setOnClickListener(this);

		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(this);
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvUser));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvUserProfile));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvHome));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvStaff));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvPolicy));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvFeedback));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvForgotPassword));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvLanguage));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvLogin));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvRegister));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvLogout));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvAddShop));
		mTypefaceUtils.applyTypeface((TextView) v.findViewById(R.id.tvCity));
		mTypefaceUtils.applyTypeface((TextView) lnrSubMenu.findViewById(R.id.txtChangePassword));
		mTypefaceUtils.applyTypeface((TextView) lnrSubMenu.findViewById(R.id.txtEditMyInfo));
		mTypefaceUtils.applyTypeface((TextView) lnrSubMenu.findViewById(R.id.txtEditMyAddress));
		mTypefaceUtils.applyTypeface((TextView) lnrSubMenu.findViewById(R.id.txtMyBooking));
		mTypefaceUtils.applyTypeface((TextView) lnrSubMenu.findViewById(R.id.txtMyOrder));
		mTypefaceUtils.applyTypeface((TextView) lnrSubMenu.findViewById(R.id.txtMyFavorite));
	}

	private void setSubMenuListener(LinearLayout lnrSubMenu2) {
		lnrSubMenu2.findViewById(R.id.txtChangePassword).setOnClickListener(this);
		lnrSubMenu2.findViewById(R.id.txtMyFavorite).setOnClickListener(this);
		lnrSubMenu2.findViewById(R.id.txtEditMyInfo).setOnClickListener(this);
		lnrSubMenu2.findViewById(R.id.txtEditMyAddress).setOnClickListener(this);
		lnrSubMenu2.findViewById(R.id.txtMyBooking).setOnClickListener(this);
		lnrSubMenu2.findViewById(R.id.txtMyOrder).setOnClickListener(this);
	}
	public void initActionBar() {
		getSupportActionBar().setHomeButtonEnabled(false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		getSupportActionBar().setDisplayUseLogoEnabled(false);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);

		View mActionbarView = getLayoutInflater().inflate(R.layout.actionbar_home, (ViewGroup) findViewById(R.id.drawer_layout), false);
		imgHome = (ImageView) mActionbarView.findViewById(R.id.imgHome);
		txtActionTitle = (TextView) mActionbarView.findViewById(R.id.txtTitle);
		txtActionTitle.setText(getString(R.string.tvregister));
		btnAdd = (ImageButton) mActionbarView.findViewById(R.id.btnAdd);
		radioHome = (RadioGroup) mActionbarView.findViewById(R.id.radioHome);
		rlNewActionBar = (RelativeLayout) mActionbarView.findViewById(R.id.new_action_bar);
		rlActionSearch = (RelativeLayout) mActionbarView.findViewById(R.id.new_action_search);
		initNewActionBar();
		initActionBarSearch();
		rFav = (RadioButton) mActionbarView.findViewById(R.id.favorite);
		rSearch = (RadioButton) mActionbarView.findViewById(R.id.search);
		rPromo = (RadioButton) mActionbarView.findViewById(R.id.gift);
		rBooking = (RadioButton) mActionbarView.findViewById(R.id.booking);
		rOrder = (RadioButton) mActionbarView.findViewById(R.id.order);
		imgHome.setOnClickListener(this);

		rFav.setOnTouchListener(this);
		rSearch.setOnTouchListener(this);
		rPromo.setOnTouchListener(this);
		rBooking.setOnTouchListener(this);
		rOrder.setOnTouchListener(this);

		radioHome.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			public void onCheckedChanged(RadioGroup group, int checkedId) {

				if (isTouched) {

					isTouched = false;

					switch (checkedId) {

						case R.id.search :

							clearBackStackEntry();
							HomeFragment homeFragment = new HomeFragment();
							getSupportFragmentManager().beginTransaction().add(FRAGMENT_CONTAINER, homeFragment).addToBackStack(null).commit();
							break;

						case R.id.gift :

							clearBackStackEntry();
							getSupportFragmentManager().beginTransaction().addToBackStack(PromotionFragment.class.getName()).replace(HomeScreen.FRAGMENT_CONTAINER, new PromotionFragment()).commit();
							break;

						case R.id.favorite :

							clearBackStackEntry();
							getSupportFragmentManager().beginTransaction().addToBackStack(MyFavoritesFragment.class.getName()).replace(HomeScreen.FRAGMENT_CONTAINER, new MyFavoritesFragment()).commit();
							break;

						case R.id.booking :
							clearBackStackEntry();
							getSupportFragmentManager().beginTransaction().addToBackStack(BookingListFragment.class.getName()).replace(HomeScreen.FRAGMENT_CONTAINER, new BookingListFragment()).commit();
							break;

						case R.id.order :

							clearBackStackEntry();
							getSupportFragmentManager().beginTransaction().addToBackStack(OrderListFragment.class.getName()).replace(HomeScreen.FRAGMENT_CONTAINER, new OrderListFragment()).commit();
							break;
					}
				}
			}
		});
		getSupportActionBar().setCustomView(mActionbarView);

	}

	private void initActionBarSearch() {
		((EditText) rlActionSearch.findViewById(R.id.etSearch)).setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					performSearch();
					return true;
				}
				return false;
			}

			public void performSearch() {
				Toast.displayText(getApplicationContext(), "Search");
			}
		});
	}

	private void initNewActionBar() {
		rlNewActionBar.findViewById(R.id.txtSearch).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				imgHome.setImageResource(R.drawable.back);
				txtActionTitle.setVisibility(View.GONE);
				getSupportFragmentManager().beginTransaction().add(FRAGMENT_CONTAINER, new FragmentSearch()).addToBackStack(null).commit();
				changeSearchActionBar(true);
			}

		});
	}
	public void changeSearchActionBar(boolean b) {
		if (b) {
			rlNewActionBar.setVisibility(View.GONE);
			rlActionSearch.setVisibility(View.VISIBLE);
		} else {
			rlNewActionBar.setVisibility(View.VISIBLE);
			rlActionSearch.setVisibility(View.GONE);
		}

	}
	private void MenuChanger(int type) {

		switch (type) {
			case MENU_HOME :

				getSlidingMenu().toggle();
				changeActionItems(false);
				clearBackStackEntry();
				// HomeFragment homeFragment = new HomeFragment();
				HomeFragmentNew homeFragment = new HomeFragmentNew();
				getSupportFragmentManager().beginTransaction().add(FRAGMENT_CONTAINER, homeFragment).addToBackStack(null).commit();
				break;
			case MENU_LOGIN :
				getSlidingMenu().toggle();
				hideNewActionBar();
				changeActionItems(true);
				clearBackStackEntry();
				LoginFragment login = new LoginFragment();
				getSupportFragmentManager().beginTransaction().replace(HomeScreen.FRAGMENT_CONTAINER, login).addToBackStack(null).commit();
				break;
			case MENU_REGISTRATION :
				getSlidingMenu().toggle();
				hideNewActionBar();
				changeActionItems(true);
				clearBackStackEntry();
				Bundle b = new Bundle();
				b.putString(RegistrationFormFragment.PARAMS_TOKEN, access_token);
				RegistrationFormFragment register = new RegistrationFormFragment();
				register.setArguments(b);
				getSupportFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, register).addToBackStack(null).commit();
				break;

			case MENU_ADD_SHOP :
				getSlidingMenu().toggle();
				hideNewActionBar();
				changeActionItems(true);
				clearBackStackEntry();
				AddShopFragment addHop = new AddShopFragment();
				getSupportFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, addHop).addToBackStack(null).commit();
				break;
			case MENU_CHANGE_PASSWORD :
				getSlidingMenu().toggle();
				hideNewActionBar();
				changeActionItems(true);
				clearBackStackEntry();
				getSupportFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, new FragmentChangePassword()).addToBackStack(null).commit();
				break;
			case MENU_USERPROFILE :

				break;

			case MENU_EDIT_MY_INFO :
				getSlidingMenu().toggle();
				hideNewActionBar();
				changeActionItems(true);
				clearBackStackEntry();
				UserProfileFragment profile = new UserProfileFragment();
				getSupportFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, profile).addToBackStack(null).commit();
				break;

			case MENU_ADD_EDIT_ADDRESS :
				getSlidingMenu().toggle();
				hideNewActionBar();
				changeActionItems(true);
				clearBackStackEntry();
				FragmentAddEditAddress fragmentEditAddress = new FragmentAddEditAddress();
				getSupportFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, fragmentEditAddress).addToBackStack(null).commit();
				break;
			case MENU_STAFF :
				getSlidingMenu().toggle();
				changeActionItems(true);
				clearBackStackEntry();
				StaffFragmentContainer mStaffFragmentContainer = new StaffFragmentContainer();
				getSupportFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, mStaffFragmentContainer).addToBackStack(null).commit();
				break;
			case MENU_CITY :
				// startActivityForResult(new Intent(this,
				// PreferenceActivty.class).putExtra(PreferenceActivty.PARAM_PREF,
				// PreferenceActivty.PARAM_CITY).putExtra(PreferenceActivty.PARAM_CITIES,
				// arrCities), REQUEST_CITY);
				Intent intentCity = new Intent(this, PreferenceActivty.class);
				intentCity.putExtra(PreferenceActivty.PARAM_PREF, PreferenceActivty.PARAM_CITY);
				intentCity.putExtra(PreferenceActivty.PARAM_CITIES, arrCities);
				intentCity.putParcelableArrayListExtra(PreferenceActivty.PARAM_CITIES_NEW, arrCitiesNew);
				startActivityForResult(intentCity, REQUEST_CITY);
				break;
			case MENU_FEEDBACK :
				getSlidingMenu().toggle();
				changeActionItems(true);
				hideNewActionBar();
				clearBackStackEntry();
				FeedbackFragment feedbackFragment = new FeedbackFragment();
				getSupportFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, feedbackFragment).addToBackStack(null).commit();
				break;
			case MENU_FORGOT_PASSWORD :
				getSlidingMenu().toggle();
				changeActionItems(true);
				hideNewActionBar();
				clearBackStackEntry();
				FragmentForgotPassword forgotPassword = new FragmentForgotPassword();
				getSupportFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, forgotPassword).addToBackStack(null).commit();
				break;
			case MENU_MY_FAVOURITE :
				getSlidingMenu().toggle();
				changeActionItems(true);
				hideNewActionBar();
				clearBackStackEntry();
				getSupportFragmentManager().beginTransaction().addToBackStack(MyFavoritesFragment.class.getName()).replace(HomeScreen.FRAGMENT_CONTAINER, new MyFavoritesFragment()).commit();
				break;
			case MENU_MY_BOOKING :
				getSlidingMenu().toggle();
				changeActionItems(true);
				hideNewActionBar();
				clearBackStackEntry();
				getSupportFragmentManager().beginTransaction().addToBackStack(BookingListFragment.class.getName()).replace(HomeScreen.FRAGMENT_CONTAINER, new BookingListFragment()).commit();
				break;
			case MENU_MY_ORDER :
				getSlidingMenu().toggle();
				changeActionItems(true);
				hideNewActionBar();
				clearBackStackEntry();
				getSupportFragmentManager().beginTransaction().addToBackStack(OrderListFragment.class.getName()).replace(HomeScreen.FRAGMENT_CONTAINER, new OrderListFragment()).commit();
				break;
			case MENU_POLICY :
				hideNewActionBar();
				getSlidingMenu().toggle();
				changeActionItems(true);
				clearBackStackEntry();
				PolicyFragment policyFragment = new PolicyFragment();
				getSupportFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, policyFragment).addToBackStack(null).commit();
				break;
			case MENU_LANGUAGE :
				startActivityForResult(new Intent(this, PreferenceActivty.class).putExtra(PreferenceActivty.PARAM_PREF, PreferenceActivty.PARAM_LANG), REQUEST_LANGUAGE);
				break;
			case MENU_LOGOUT :
				getSlidingMenu().toggle();
				showLogoutDialog();
				break;

			default :
				break;
		}

	}
	public void hideNewActionBar() {
		rlNewActionBar.setVisibility(View.GONE);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.imgHome :
				try {
					KeyboardUtils.hideKeyboard(v);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (getSupportFragmentManager().getBackStackEntryCount() == 2) {
					getSupportFragmentManager().popBackStack();
					changeSearchActionBar(false);
					imgHome.setImageResource(R.drawable.menu);
					return;
				}
				if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
					getSupportFragmentManager().popBackStack();
				} else {
					getSlidingMenu().toggle();
				}

				break;
			case R.id.lnrHome :
				MenuChanger(HomeScreen.MENU_HOME);
				break;
			case R.id.txtChangePassword :
				MenuChanger(HomeScreen.MENU_CHANGE_PASSWORD);
				break;
			case R.id.txtMyFavorite :
				MenuChanger(HomeScreen.MENU_MY_FAVOURITE);
				break;
			case R.id.txtMyBooking :
				MenuChanger(HomeScreen.MENU_MY_BOOKING);
				break;
			case R.id.txtMyOrder :
				MenuChanger(HomeScreen.MENU_MY_ORDER);
				break;
			case R.id.lnrUser :
				MenuChanger(HomeScreen.MENU_USERPROFILE);
				break;
			case R.id.txtEditMyInfo :
				MenuChanger(HomeScreen.MENU_EDIT_MY_INFO);
				break;
			case R.id.txtEditMyAddress :
				MenuChanger(HomeScreen.MENU_ADD_EDIT_ADDRESS);
				break;
			case R.id.lnrForgotPassword :
				MenuChanger(HomeScreen.MENU_FORGOT_PASSWORD);
				break;
			case R.id.lnrAddShop :
				MenuChanger(HomeScreen.MENU_ADD_SHOP);
				break;
			case R.id.lnrLogin :
				if (!TextUtils.isEmpty(prefsManager.getPrefs().getString(SelectCityFragment.PARAM_CITYDATA_NEW, ""))) {
					MenuChanger(HomeScreen.MENU_LOGIN);
				} else {
					Toast.displayText(this, getString(R.string.selectCity));
				}

				break;
			case R.id.lnrRegister :
				if (!TextUtils.isEmpty(prefsManager.getPrefs().getString(SelectCityFragment.PARAM_CITYDATA_NEW, ""))) {
					MenuChanger(HomeScreen.MENU_REGISTRATION);
				} else {
					Toast.displayText(this, getString(R.string.selectCity));
				}
				break;
			case R.id.lnrLogout :
				MenuChanger(HomeScreen.MENU_LOGOUT);
				break;
			case R.id.lnrlanguage :
				MenuChanger(HomeScreen.MENU_LANGUAGE);
				break;
			case R.id.lnrCity :
				MenuChanger(HomeScreen.MENU_CITY);
				break;

			case R.id.lnrStaff :
				MenuChanger(MENU_STAFF);
				break;
			case R.id.lnrFeedback :
				MenuChanger(MENU_FEEDBACK);
				break;
			case R.id.lnrPolicy :
				MenuChanger(MENU_POLICY);
				break;
			case R.id.imgUser :
				if (lnrUser.getVisibility() == View.VISIBLE) {
					getSlidingMenu().toggle();
					changeActionItems(true);
					clearBackStackEntry();
					UserProfileFragment profile = new UserProfileFragment();
					getSupportFragmentManager().beginTransaction().replace(FRAGMENT_CONTAINER, profile).addToBackStack(null).commit();
				}
				break;
		}

	}
	public void clearBackStackEntry() {
		try {
			int count = getSupportFragmentManager().getBackStackEntryCount();
			if (count > 0) {
				getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void changeActionItems(boolean isLogin) {
		Log.e(TAG, "show ActionBAr");
		if (getSupportActionBar() != null && !getSupportActionBar().isShowing())
			getSupportActionBar().show();

		if (isLogin) {
			imgHome.setTag("1");
			imgHome.setImageResource(R.drawable.back);
			radioHome.setVisibility(View.GONE);
			txtActionTitle.setVisibility(View.VISIBLE);
			btnAdd.setVisibility(View.GONE);

		} else {
			imgHome.setTag("0");
			imgHome.setImageResource(R.drawable.menu);
			// radioHome.setVisibility(View.VISIBLE);
			txtActionTitle.setVisibility(View.GONE);
			btnAdd.setVisibility(View.GONE);
		}

		// showHideMenus();
	}

	public ImageButton getAddMenu() {
		return btnAdd;
	}

	public void showHideMenus() {
		if (prefsManager.getPrefs().getBoolean(PREFS_LOGIN, false)) {
			rBooking.setEnabled(true);
			rBooking.setButtonDrawable(R.drawable.menu_booking);
			rFav.setEnabled(true);
			rFav.setButtonDrawable(R.drawable.menu_favorite);
			rOrder.setEnabled(true);
			rOrder.setButtonDrawable(R.drawable.menu_order);
		} else {
			rBooking.setEnabled(false);
			rBooking.setButtonDrawable(R.drawable.calender);
			rFav.setEnabled(false);
			rFav.setButtonDrawable(R.drawable.fav);
			rOrder.setEnabled(false);
			rOrder.setButtonDrawable(R.drawable.cart);
		}
	}

	public void setCustomTitle(String title, int type) {

		txtActionTitle.setText(title);
		TypefaceUtils mTypefaceUtils = TypefaceUtils.getInstance(this);
		mTypefaceUtils.applyTypeface(txtActionTitle, type);
	}

	@Override
	public void onSuccess(int id, String response) {

		if (!TextUtils.isEmpty(response)) {

			if (id == login_request) {

				try {

					JSONObject jObject = new JSONObject(response);

					if (jObject.has("access_token")) {

						Editor editor = prefsManager.getPrefs().edit();
						editor.putString(PARAM_TOKEN, jObject.getString("access_token"));
						editor.putInt(PARAM_TOKEN_EXPIRY_TIME, jObject.getInt("Expire_Time"));
						editor.putLong(PARAM_TOKEN_RECEIVED_TIME, Calendar.getInstance().getTimeInMillis());
						editor.apply();

						Log.e(TAG, "AccessToken =>> " + jObject.getString("access_token"));
						Log.e(TAG, "Token Expiry Time =>> " + jObject.getString("Expire_Time"));

						access_token = prefsManager.getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
						city_request = networManager.addRequest(Join8RequestBuilder.getCityList(jObject.getString("access_token")), RequestMethod.POST, this, Join8RequestBuilder.METHOD_GETCITY);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			} else if (id == city_request) {

				OrderDetailFragment.clearOrderList();

				try {
					JSONArray jCity = new JSONArray(response);
					arrCities = new ArrayList<City>();
					if (jCity != null && jCity.length() > 0) {
						for (int i = 0; i < jCity.length(); i++) {

							City city = new Gson().fromJson(jCity.getJSONObject(i).toString(), City.class);
							arrCities.add(city);
						}
					}

					if (TextUtils.isEmpty(prefsManager.getPrefs().getString(SelectCityFragment.PARAM_CITYDATA_NEW, ""))) {
						MenuChanger(HomeScreen.MENU_CITY);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				}

			} else if (id == request_city_new) {
				try {
					ArrayList<ResponseCity> arrCities = new Gson().fromJson(response, new TypeToken<ArrayList<ResponseCity>>() {
					}.getType());
					this.arrCitiesNew = new ArrayList<ResponseCity>();
					for (ResponseCity city : arrCities) {
						this.arrCitiesNew.add(city);
					}
					if (mBannerLoadedListener != null) {
						mBannerLoadedListener.onBAnnerGet();
					}
					if (TextUtils.isEmpty(prefsManager.getPrefs().getString(SelectCityFragment.PARAM_CITYDATA_NEW, ""))) {
						MenuChanger(HomeScreen.MENU_CITY);
					}
				} catch (JsonSyntaxException e) {
					Toast.displayText(this, e.toString());
				}

			} else if (id == tokenRequest) {
				TokenModel token = new Gson().fromJson(response, TokenModel.class);
				tokenCloud = token.getAccess_token();
				prefsManager.getPrefs().edit().putString(ConstantData.TOKEN_CLOUD, tokenCloud).commit();
				request_city_new = networManager.addRequest(null, this, true, UrlUtil.getCityListUrl(), "", ConstantData.method_get);
			} else if (id == signn_request) {

				isTouched = true;
				rSearch.setChecked(true);

				Log.e(TAG, response);
				try {
					final JSONObject jObject = new JSONObject(response);
					if (jObject.has("ActivationCode")) {
						prefsManager.getPrefs().edit().putBoolean(HomeScreen.PREFS_LOGIN, true).apply();
						prefsManager.getPrefs().edit().putString(HomeScreen.PARAMS_USERDATA, jObject.toString()).apply();
						Toast.displayText(HomeScreen.this, getString(R.string.signin_success));
						clearBackStackEntry();
						// HomeFragment homeFragment = new HomeFragment();
						HomeFragmentNew homeFragment = new HomeFragmentNew();
						getSupportFragmentManager().beginTransaction().add(HomeScreen.FRAGMENT_CONTAINER, homeFragment).addToBackStack(null).commit();

						if (!Utils.isEmpty(jObject.getString("Tag"))) {
							new Thread(new Runnable() {
								@Override
								public void run() {

									try {
										hub.register(ConstantData.RegistrationID, jObject.getString("Tag"));
									} catch (Exception e) {
										e.printStackTrace();
									}
								}
							}).start();
						}

					} else {
						Toast.displayText(this, getString(R.string.loginfail));
						prefsManager.getPrefs().edit().putBoolean(HomeScreen.PREFS_LOGIN, false);
					}
				} catch (JSONException e) {
					e.printStackTrace();
					Toast.displayText(this, getString(R.string.loginfail));
				}

			} else if (id == signout_request) {

				try {

					OrderDetailFragment.clearOrderList();

					if (response != null && (response.toLowerCase(Locale.getDefault()).contains("success") || response.toLowerCase().contains("no record deleted"))) {

						Log.e(TAG, "Logout Response" + response);

						prefsManager.getPrefs().edit().putBoolean(PREFS_LOGIN, false).apply();
						prefsManager.getPrefs().edit().putString(HomeScreen.PARAMS_USERDATA, null).apply();

						new Thread(new Runnable() {
							@Override
							public void run() {

								try {
									hub.unregisterAll(ConstantData.RegistrationID);
								} catch (Exception e) {
									e.printStackTrace();
									Log.e("Error in unregister Hub notification", e.toString());
								}
							}
						}).start();

						isLoggedIn(false);
						changeActionItems(false);
						clearBackStackEntry();
						// HomeFragment homeFragment = new HomeFragment();
						HomeFragmentNew homeFragment = new HomeFragmentNew();
						getSupportFragmentManager().beginTransaction().add(HomeScreen.FRAGMENT_CONTAINER, homeFragment).addToBackStack(null).commit();
						// showHideMenus();

					} else {
						Toast.displayText(this, getString(R.string.failed));
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	@Override
	public void onError(int id, String message) {
		Log.e(TAG, "Error message => " + message);
		Toast.displayText(HomeScreen.this, message);
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	private void showLogoutDialog() {

		DialogHelper.popupMessage(this, getString(R.string.app_name), getString(R.string.logoutalert), getString(R.string.yes), getString(R.string.no), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();

				// signout_request =
				// networManager.addRequest(Join8RequestBuilder.getSignoutRequest(access_token,
				// getRegistrationData().getEmail()), RequestMethod.POST,
				// HomeScreen.this, Join8RequestBuilder.METHOD_SINGOUT);
				// tokenCloud =
				// prefsManager.getPrefs().getString(ConstantData.TOKEN_CLOUD,
				// null);
				// HashMap<String, String> auth = new HashMap<String, String>();
				// auth.put("Authorization", "bearer " + tokenCloud);
				signout_request = networManager.addRequest(null, HomeScreen.this, true, UrlUtil.getLogoutUrl(prefsManager.getPrefs().getString(ConstantData.USER_NAME, "")), "", ConstantData.method_post);
			}
		}, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		}, true);

	}

	public void isLoggedIn(boolean isLogin) {

		if (isLogin) {

			try {
				Registration registration = getRegistrationData();
				boolean staffFalg = registration.isIsRestaurantStaff();

				tvUser.setText(registration.getFirstName() + " " + registration.getLastName());
				if (staffFalg)
					lnrStaff.setVisibility(View.GONE);
				else
					lnrStaff.setVisibility(View.GONE);

				imageLoader.displayImage(registration.getPhoto(), imgUser, options);
			} catch (Exception e) {
				e.printStackTrace();
			}
			lnrLogin.setVisibility(View.GONE);
			lnrRegister.setVisibility(View.GONE);
			lnrUser.setVisibility(View.VISIBLE);
			lnrSubMenu.setVisibility(View.VISIBLE);
			lnrHome.setVisibility(View.VISIBLE);
			lnrUserInfo.setVisibility(View.VISIBLE);
			lnrLogout.setVisibility(View.VISIBLE);
			lnrForgotPassword.setVisibility(View.GONE);

		} else {
			lnrUserInfo.setVisibility(View.GONE);
			lnrStaff.setVisibility(View.GONE);
			lnrLogin.setVisibility(View.VISIBLE);
			lnrRegister.setVisibility(View.VISIBLE);
			lnrUser.setVisibility(View.GONE);
			lnrSubMenu.setVisibility(View.GONE);
			lnrHome.setVisibility(View.VISIBLE);
			lnrLogout.setVisibility(View.GONE);
			// lnrForgotPassword.setVisibility(View.VISIBLE);
		}
		leftMenu.setVisibility(View.VISIBLE);
	}

	private Registration getRegistrationData() {

		Gson gson = new Gson();
		return gson.fromJson(prefsManager.getPrefs().getString(HomeScreen.PARAMS_USERDATA, null), Registration.class);

	}

	public ResponseCity getCityData() {
		try {
			String str = prefsManager.getPrefs().getString(SelectCityFragment.PARAM_CITYDATA_NEW, "");
			if (!Utils.isEmpty(str)) {
				return new Gson().fromJson(str, ResponseCity.class);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d(TAG, "onActivityResult");

		if (resultCode == RESULT_OK) {

			if (requestCode == REQUEST_CITY) {

				initActionBar();
				clearBackStackEntry();
				// HomeFragment homeFragment = new HomeFragment();
				HomeFragmentNew homeFragment = new HomeFragmentNew();
				getSupportFragmentManager().beginTransaction().add(HomeScreen.FRAGMENT_CONTAINER, homeFragment).addToBackStack(null).commit();

			} else if (requestCode == REQUEST_LANGUAGE) {
				Intent intent = getIntent();
				finish();
				startActivity(intent);

			} else if (requestCode == RestaurantDetailsFragment.INTENT_BOOKING) {

			} else if (requestCode == ProfileFragment.INTENT_CAMERA || requestCode == ProfileFragment.INTENT_GALLERY) {

			} else if (requestCode == 33) {
				isTouched = true;
				if (data != null) {
					rBooking.setChecked(true);
				} else
					rOrder.setChecked(true);
			}

			Fragment fragment = getSupportFragmentManager().findFragmentById(FRAGMENT_CONTAINER);
			if (fragment != null) {
				fragment.onActivityResult(requestCode, resultCode, data);
			}
		}
	}

	@Override
	public void onBackPressed() {

		try {
			FragmentManager fm = getSupportFragmentManager();

			if (fm.getBackStackEntryCount() > 0) {

				Fragment fragment = fm.getFragments().get(fm.getBackStackEntryCount() - 1);

				if (fragment.getClass().getSimpleName().equals(HomeFragment.class.getSimpleName())) {

					HomeFragment homeFragment = (HomeFragment) fragment;

					if (!homeFragment.isCusineList && homeFragment.isSearchRestaurant) {
						homeFragment.loadAllCusines(homeFragment.selectedRegionId);
					} else {

						finish();
					}

				} else if (fragment.getChildFragmentManager() != null && fragment.getChildFragmentManager().getBackStackEntryCount() > 0) {
					if (fragment.getClass() == StaffFragmentContainer.class) {
						((StaffFragmentContainer) fragment).popBackStack();
					} else {
						fragment.getChildFragmentManager().popBackStack();
					}

				} else if (fm.getBackStackEntryCount() == 1) {
					finish();
				} else {
					fm.popBackStack();
				}

			} else {
				super.onBackPressed();
			}
		} catch (Exception e) {
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		isTouched = true;
		return false;
	}

	public void setBannerListener(BannerLoadedListener lmiBannerLoadedListener) {
		mBannerLoadedListener = lmiBannerLoadedListener;

	}

}
