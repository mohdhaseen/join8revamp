package com.join8;

import java.util.Set;

import org.json.JSONObject;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.join8.listeners.Requestlistener;
import com.join8.model.BookingData;
import com.join8.model.FoodOptionItemData;
import com.join8.model.OrderData;
import com.join8.network.NetworkManager;
import com.join8.network.RequestMethod;
import com.join8.print.BluetoothService;
import com.join8.print.PrintUtils;
import com.join8.print.Print_XPC2008;
import com.join8.print.SocketManager;
import com.join8.request.Join8RequestBuilder;
import com.join8.utils.CryptoManager;
import com.join8.utils.Utils;

public class PrinterService extends Service implements Requestlistener {

	private static final String Tag = PrinterService.class.getSimpleName();
	private int reqIdOrder = -1, reqIdBooking = -1;
	private NetworkManager networManager;
	private OrderData orderData = null;
	private BookingData bookingData = null;

	private BluetoothAdapter mBluetoothAdapter;

	private BluetoothService mService;
	private SocketManager mSocketManager, mSocketManager1;

	public static final String DEVICE_NAME = "device_name";
	public static final String DEVICE_ADDRESS = "device_address";
	public static final String TOAST = "toast";

	// Message types sent from the BluetoothService Handler
	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d(Tag, "OnCreate");

		networManager = NetworkManager.getInstance();
		networManager.addListener(this);

		mSocketManager = new SocketManager(PrinterService.this);
		mSocketManager1 = new SocketManager(PrinterService.this);

		SharedPreferences mPreferences = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);

		if (mPreferences.getInt("printerTypePosition", 0) == 1) {

			String printerIp = mPreferences.getString("printerIP", null);
			String printerIp1 = mPreferences.getString("printerIP1", null);

			if (Utils.isEmpty(printerIp) && Utils.isEmpty(printerIp1)) {
				networManager.removeListeners(this);
				stopSelf();
				return;
			} else {

				if (!Utils.isEmpty(printerIp)) {
					connectWifiPrinter(printerIp);
				}

				if (!Utils.isEmpty(printerIp1)) {
					connectWifiPrinter1(printerIp1);
				}
			}

		} else {

			if (mBluetoothAdapter == null) {
				mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			}
			if (mBluetoothAdapter == null) {
				Log.e(Tag, "Bluetooth not available");
				Toast.displayText(PrinterService.this, getString(R.string.bluetooth_not_available));
				networManager.removeListeners(this);
				stopSelf();
				return;
			}
		}
	}

	private void connectWifiPrinter(String printerIp) {

		mSocketManager.mPrinterIP = printerIp;
		mSocketManager.threadconnect();
	}

	private void connectWifiPrinter1(String printerIp) {

		mSocketManager1.mPrinterIP = printerIp;
		mSocketManager1.threadconnect();
	}

	@Override
	public void onDestroy() {

		if (mSocketManager != null) {
			mSocketManager.close();
			mSocketManager = null;
		}

		if (mSocketManager1 != null) {
			mSocketManager1.close();
			mSocketManager1 = null;
		}

		super.onDestroy();
	}

	private final Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case MESSAGE_STATE_CHANGE :
					switch (msg.arg1) {
						case BluetoothService.STATE_CONNECTED :
							break;
						case BluetoothService.STATE_CONNECTING :
							break;
						case BluetoothService.STATE_LISTEN :
						case BluetoothService.STATE_NONE :
							break;
					}
					break;
				case MESSAGE_WRITE :
					break;
				case MESSAGE_READ :
					break;
				case MESSAGE_DEVICE_NAME :
					SharedPreferences mPreferences = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
					mPreferences.edit().putString("btAddress", msg.getData().getString(DEVICE_ADDRESS)).commit();
					printOrderOnBluetoothPrinter();
					break;
				case MESSAGE_TOAST :
					Toast.displayText(PrinterService.this, msg.getData().getString(BluetoothService.TOAST));
					stopSelf();
					break;
			}
		}
	};

	private void printOrderOnBluetoothPrinter() {
		Log.d(Tag, "printOrder");
		try {

			if (orderData != null) {

				if (orderData.getAutoPrintOrder() == 1) {
					for (int i = 0; i < orderData.getAutoPrintOrderCopies(); i++) {
						new PrintUtils(this, mService, null).printOrder(orderData, i + 1);
					}
				}
				stopSelf();

			} else if (bookingData != null) {

				new PrintUtils(this, mService, null).printBooking(bookingData);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e(Tag, "Json Exception");
			stopSelf();
		}
	}

	private void initBluetooth() {
		Log.d(Tag, "initBluetooth");

		if (mService == null) {
			mService = BluetoothService.getInstance(this);
			mService.setHandler(mHandler);
		}

		if (mService.getState() == BluetoothService.STATE_NONE) {
			mService.start();
			Log.d(Tag, "[initBluetooth] starting service");
		} else if (mService.getState() == BluetoothService.STATE_CONNECTED) {
			Log.d(Tag, "Already connected so trying to print");
			printOrderOnBluetoothPrinter();
			return;
		}

		SharedPreferences mPreferences = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);
		if (mPreferences.contains("btAddress")) {
			String deviceId = mPreferences.getString("btAddress", "");
			if (!TextUtils.isEmpty(deviceId)) {
				Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
				if (pairedDevices != null && pairedDevices.size() > 0) {
					for (BluetoothDevice device : pairedDevices) {
						if (device.getAddress().equals(deviceId)) {
							mService.connect(mBluetoothAdapter.getRemoteDevice(deviceId));
							Log.d(Tag, "[initBluetooth] get device from preference and trying to connect");
							return;
						}
					}
				}
			}
		}
		Log.e(Tag, "Can't connect to bluetooth stopping service...");
		networManager.removeListeners(this);
		stopSelf();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		int id = super.onStartCommand(intent, flags, startId);

		if (intent == null || !intent.hasExtra("reference") || intent.getStringExtra("reference").equals("0")) {			
			networManager.removeListeners(this);
			stopSelf(startId);
			return id;
		}

		if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled()) {
			Log.e(Tag, "Bluetooth is disable");
			Toast.displayText(PrinterService.this, "Bluetooth is disable");
			networManager.removeListeners(this);
			stopSelf(startId);
			return id;
		}

		try {
			if (intent != null && intent.getStringExtra("redirectTo").equalsIgnoreCase("StaffOrderDetail")) {
				String reference = intent.getStringExtra("reference");
				String[] arg = reference.split("/");
				if (arg == null) {
					networManager.removeListeners(this);
					stopSelf(startId);
				}
				String orderId = arg[0];
				String access_token = CryptoManager.getInstance(this).getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
				reqIdOrder = networManager.addRequest(Join8RequestBuilder.getParticularOrder(access_token, orderId), RequestMethod.POST, this, Join8RequestBuilder.METHOD_GET_PARTICULAR_ORDER, false);

			} else if (intent != null && intent.getStringExtra("redirectTo").equalsIgnoreCase("StaffBookingDetail")) {

				String bookingId = intent.getStringExtra("reference");

				String access_token = CryptoManager.getInstance(this).getPrefs().getString(HomeScreen.PARAM_TOKEN, "");
				reqIdBooking = networManager.addRequest(Join8RequestBuilder.getBookingDetails(access_token, bookingId), RequestMethod.POST, this, Join8RequestBuilder.METHOD_GET_PARTICULAR_BOOKING_OF_USER, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(Tag, "Problem in parameters");
			Toast.displayText(PrinterService.this, "Problem in responce data");
			networManager.removeListeners(this);
			stopSelf(id);
		}
		return id;
	}

	@Override
	public void onSuccess(int id, String response) {

		try {

			Log.e(Tag, "Response : " + response);

			if (Utils.isEmpty(response)) {
				return;
			}

			if (id == reqIdOrder) {

				JSONObject jObj = new JSONObject(response);

				orderData = new Gson().fromJson(jObj.toString(), OrderData.class);

				if (orderData.getMenuItem().size() > 0) {

					for (int i = 0; i < orderData.getMenuItem().size(); i++) {

						for (int j = 0; j < orderData.getMenuItem().get(i).getFoodOption().size(); j++) {

							double basePrice = orderData.getMenuItem().get(i).getFoodOption().get(j).getBasePrice();

							for (int k = 0; k < orderData.getMenuItem().get(i).getFoodOption().get(j).getFoodOptionItem().size(); k++) {

								FoodOptionItemData foodOptionItemData = orderData.getMenuItem().get(i).getFoodOption().get(j).getFoodOptionItem().get(k);
								foodOptionItemData.setExtraPrice(foodOptionItemData.getExtraPrice() + basePrice);
								basePrice = 0;
							}
						}
					}
				}

				SharedPreferences mPreferences = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);

				if (mPreferences.getInt("printerTypePosition", 0) == 1) {

					if (orderData.getAutoPrintOrder() == 1) {
						for (int i = 0; i < orderData.getAutoPrintOrderCopies(); i++) {
							if (mSocketManager.isConnected()) {
								new Print_XPC2008(PrinterService.this, mSocketManager).printOrder(orderData, i + 1);
								try {
									Thread.sleep(5000);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}

							if (mSocketManager1.isConnected()) {
								new Print_XPC2008(PrinterService.this, mSocketManager1).printOrder(orderData, i + 1);
								try {
									Thread.sleep(5000);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}

					stopSelf();

				} else {
					initBluetooth();
				}

			} else if (id == reqIdBooking) {

				JSONObject jObj = new JSONObject(response);

				bookingData = new Gson().fromJson(jObj.toString(), BookingData.class);

				SharedPreferences mPreferences = getSharedPreferences(getPackageName(), Context.MODE_PRIVATE);

				if (mPreferences.getInt("printerTypePosition", 0) == 1) {

					if (orderData.getAutoPrintOrder() == 1) {
						for (int i = 0; i < orderData.getAutoPrintOrderCopies(); i++) {
							if (mSocketManager.isConnected()) {
								new Print_XPC2008(PrinterService.this, mSocketManager).printOrder(orderData, i + 1);
								try {
									Thread.sleep(5000);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}

							if (mSocketManager1.isConnected()) {
								new Print_XPC2008(PrinterService.this, mSocketManager1).printOrder(orderData, i + 1);
								try {
									Thread.sleep(5000);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					}

					stopSelf();

				} else {
					initBluetooth();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e(Tag, e.toString());
			stopSelf();
		}
	}

	@Override
	public void onError(int id, String message) {
		Log.d(Tag, "onError Message=" + (TextUtils.isEmpty(message) ? "" : message));
		networManager.removeListeners(this);
		stopSelf();
	}
}
