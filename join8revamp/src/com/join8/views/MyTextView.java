package com.join8.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.join8.utils.TypefaceUtils;

public class MyTextView extends TextView {

	public MyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		init(attrs);
	}

	public MyTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public MyTextView(Context context) {
		super(context);
		init(null);
	}

	private void init(AttributeSet attrs) {

		if (!isInEditMode()) {
			TypefaceUtils.getInstance(getContext()).applyTypeface(this);
			/*
			 * Typeface mTypeface =
			 * TypefaceUtils1.getInstance(getContext()).getTypeface
			 * (TypefaceUtils1.REGULAR);
			 * 
			 * if (attrs != null) {
			 * 
			 * // TypedArray attr = getContext().obtainStyledAttributes(attrs,
			 * // R.styleable.Custom); Typeface fontStyle = this.getTypeface();
			 * 
			 * if (fontStyle != null) {
			 * 
			 * if (fontStyle.getStyle() == Typeface.BOLD) { mTypeface =
			 * TypefaceUtils1
			 * .getInstance(getContext()).getTypeface(TypefaceUtils1.BOLD); }
			 * else { mTypeface =
			 * TypefaceUtils1.getInstance(getContext()).getTypeface
			 * (TypefaceUtils1.REGULAR); } } // attr.recycle(); }
			 * setTypeface(mTypeface);
			 */
		}
	}
}
