package com.join8;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.join8.adpaters.ImagePreviewAdapter;
import com.join8.layout.ZoomViewPager;
import com.join8.utils.Log;

public class ImagePreviewScreen extends ActionBarActivity implements OnClickListener {

	private ImageView imgBack;
	private TextView txtActionTitle;
	private ArrayList<String> listImages;
	private ZoomViewPager viewPager;
	private LinearLayout lnrPagerIndicator = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.image_preview_gallery);

		listImages = getIntent().getExtras().getStringArrayList("images");

		initActionBar();

		viewPager = (ZoomViewPager) findViewById(R.id.viewPager);
		lnrPagerIndicator = (LinearLayout) findViewById(R.id.lnrPagerIndicator);

		viewPager.setAdapter(new ImagePreviewAdapter(this, listImages));

		displayPageDot(0);

		viewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int index) {

				displayPageDot(index);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});

	}

	private void initActionBar() {

		getSupportActionBar().setHomeButtonEnabled(false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		getSupportActionBar().setDisplayUseLogoEnabled(false);
		getSupportActionBar().setDisplayShowCustomEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);

		View mActionbarView = getLayoutInflater().inflate(R.layout.actionview, (ViewGroup) findViewById(R.id.rltMain), false);
		imgBack = (ImageButton) mActionbarView.findViewById(R.id.btn_back);
		imgBack.setOnClickListener(this);
		txtActionTitle = (TextView) mActionbarView.findViewById(R.id.txtTitle);
		txtActionTitle.setText(getIntent().getStringExtra("title"));
		getSupportActionBar().setCustomView(mActionbarView);
	}

	private void displayPageDot(int selectedIndex) {

		try {

			lnrPagerIndicator.removeAllViews();
			int margin = getResources().getDimensionPixelOffset(R.dimen.margin_2);

			for (int i = 0; i < listImages.size(); i++) {

				LinearLayout lnr = new LinearLayout(ImagePreviewScreen.this);
				ImageView imageView = new ImageView(ImagePreviewScreen.this);
				lnr.setPadding(margin, margin, margin, margin);
				lnr.addView(imageView);
				if (i == selectedIndex) {
					imageView.setBackgroundResource(R.drawable.dot_sel);
				} else {
					imageView.setBackgroundResource(R.drawable.dot);
				}
				lnrPagerIndicator.addView(lnr);
			}

		} catch (Exception e) {
			e.printStackTrace();
			Log.e("Error in displayPageDot", e.toString());
		}
	}

	@Override
	public void onClick(View v) {
		if (v == imgBack) {
			finish();
		}
	}
}
